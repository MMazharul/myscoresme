@extends('admin.master-layout')
@section('content')
    <div class="be-content">
    <div class="main-content container-fluid">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible" role="alert"  id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center ">{{ session('message') }}</div>
            </div>
        @endif
    <div class="row">
        <div class="col-md-12 ">
            <div class="card card-table">
                <div class="card-header">Field Type List
                </div>
                <div class="card-body">
                    <h4 class="text-center text-danger">{{session('delete')}}</h4>

                    <table class="table table-striped table-hover table-fw-widget" id="table3">
                        <thead>
                        <tr>
                            <th>Field Type Name</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($types as $type)

                                    <td> {{ $type->title }}</td></tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection