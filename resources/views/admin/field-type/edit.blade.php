@extends('admin.master-layout')
@section('content')
    <div class="be-content">

        <div class="main-content container-fluid">
            <div class="row wizard-row">
                <div class="col-md-10 offset-1 fuelux">
                    <div class="block-wizard col-md-10 offset-1">
                        <div class="wizard wizard-ux" id="wizard1">

                            <div class="step-content">
                                <div class="step-pane active" data-step="1">
                                    <div class="container  p-0">

                                        <form class="form-horizontal group-border-dashed" method="post" action="{{url('/type/'.$typeEdit->id)}}">
                                            {{csrf_field()}}
                                            {{method_field('PUT')}}
                                            <div class="form-group row pt-0">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Field Edit Form</h3>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row">
                                                <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Field Name</label>
                                                <div class="col-12 col-sm-8 col-lg-6">
                                                    <input class="form-control" type="text" value="{{ $typeEdit->title }}" name="title" placeholder="content name">
                                                </div>
                                            </div>


                                            <div class="form-group row pt-0 pb-0">
                                                <label class="col-12 col-sm-3 col-form-label text-sm-right">Status</label>
                                                <div class="col-12 col-sm-8 col-lg-6 mt-1">
                                                    <div class="form-group">
                                                        <label class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input is-valid" type="radio" value="1" @if($typeEdit->status==1) {{'checked'}} @endif  name="status" required><span class="custom-control-label">Publish</span>
                                                        </label>
                                                        <label class="custom-control custom-radio custom-control-inline">
                                                            <input class="custom-control-input is-invalid" type="radio" value="0" @if($typeEdit->status==0) {{'checked'}} @endif name="status" required><span class="custom-control-label">Unpublish</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-6 offset-3">
                                                    <button class="btn btn-success btn-space wizard-next pt-1 pb-1">Update Field</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection