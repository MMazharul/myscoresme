@extends('admin.master-layout')
@section('content')


    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Field Type Create</h3>
           <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/type')}}">
                @csrf

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Field Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection