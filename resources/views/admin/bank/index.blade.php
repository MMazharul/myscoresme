@extends('admin.master-layout')
@section('content')



    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center ">{{ session('message') }}</div>
            </div>
        @endif
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> List</button>
                </div>

                <h3 class="box-title m-b-0 col-1"><a href="{{url('bank/create')}}" class="btn btn-dropbox"><i
                                class="mdi mdi-plus-circle"></i> Add New</a></h3>

            </div>
            <div class="m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Bank Name</th>
                     
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banks as $bank)
                        <tr>
                            <td>{{ $bank->bank_name }}</td>
                          

                            <td>
                                <div class="dropdown row">
                                  

                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ url('/bank/'.$bank->id.'/edit' ) }}"><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href="{{url('/bank-delete/'.$bank->id)}}"><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>

                                </div>
                            </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection