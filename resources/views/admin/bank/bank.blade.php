
<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Bank</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
        <select name="bank_name" class="form-control dynamic" id="bank">
            <option value="0" disabled="true" selected="true">-Select Banak-</option>
            @foreach($bankList as $key=>$bank)
                <option value="{{$key}}">{{ $bank}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Branch</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">

        <select name="branch_name"  class="form-control  dynamic" id="branches">
            <option value="0" disabled="true" selected="true"></option>
        </select>
    </div>
</div>

