@extends('admin.master-layout')
@section('content')


    <div class="col-md-10 offset-md-1 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Bank Edit Form</h3>
            <hr class="mb-4">
            <form class="form-horizontal" method="post" action="{{url('/bank/'.$bankEdit->id)}}">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank Name*</label>
                    <div class="col-sm-7">
                        <input type="text" value="{{ $bankEdit->bank_name }}" name="bank_name" class="form-control"
                               id="inputEmail3" placeholder="Username">
                    </div>
                </div>

                <div class="form-group row disabled">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish</label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Bank Add</button>
                    </div>
                </div>




            </form>
        </div>
    </div>

@endsection