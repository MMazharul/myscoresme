@extends('admin.master-layout')

@section('content')
    <div class="card">

        @if(session('message'))
            <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong>{{ session('message') }}</strong>
            </div>
        @endif

          <div class="card-body">

                <div class="col-12 row">
                    <div class="col-11">

                  <h2><i class="fa fa-list"></i> Sector List</h2>

              </div>
                    <h3 class="box-title m-b-0 col-1"><a href="{{url('sector/create')}}" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>

                </div>

                <div class="table-responsive m-t-40">

                <table id="myTable" class="table table-bordered table-striped">

                  <thead>

                  <tr>

                      <th>Sector Name</th>
                      <th>Sector Code</th>
               
                      <th>Action</th>
                  </tr>

                  </thead>

                  <tbody>

                  @foreach($sectors as $sector)

                      <tr>
                          <td>{{ $sector->sector_name }}</td>
                          <td>{{ $sector->sector_code }}</td>
                         
                          <td>

                              <div class="dropdown row">

                           

                                  <div class="btn-group ml-1">

                                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                          <i class="icon mdi mdi-settings"></i></button>

                                      <div class="dropdown-menu">

                                          <a class="dropdown-item" href="{{ url('sector/'.$sector->id.'/edit' ) }}"><i

                                                      class="mdi mdi-table-edit"></i> Edit</a>

                                          <a class="dropdown-item" href="{{url('/sector-delete/'.$sector->id)}}"><i

                                                      class="mdi mdi-delete"></i> Delete</a>

                                      </div>

                                  </div>

                              </div>

                          </td>





                      </tr>

                  @endforeach

                  </tbody>

              </table>

          </div>

          </div>

  </div>
@endsection
