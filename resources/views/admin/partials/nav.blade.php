<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{asset('/ui/back-end/assets/admin3.png')}}" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{\Illuminate\Support\Facades\Auth::user()->first_name}}<span class="caret"></span></a>
                <div class="dropdown-menu animated flipInY">
                    <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Logout</a>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">ADMIN PANEL</li>
                <li>
                    <a  href="{{ url('/admin') }}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                </li>

                <li>
                    <a  class="has-arrow label" href="#" aria-expanded="false"><i class="icon- mdi mdi-account-settings-variant"></i><span class="hide-menu"> User </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/authorize/users')}}"><i class="mdi mdi-settings"></i>User Management</a></li>
                    </ul>
                </li>

                <li>
                    <a  class="has-arrow label" href="#" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Master Data</span></a>
                    <ul aria-expanded="false" class="collapse">




                        <li>
                            <a  class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-sliders"></i><span class="hide-menu"> Slider</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/slider/create') }}"><i class="fa fa-plus"></i>  Slider Add</a></li>
                                <li><a href="{{url('/slider')}}"><i class="mdi mdi-settings"></i> Slider Manage</a></li>
                            </ul>
                        </li>


                        <li>
                            <a  class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-facebook"></i><span class="hide-menu">  Field Type Elements</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('/type')}}">Field Type Show</a></li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-question-circle"></i><span class="hide-menu"> Question Category</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/question-category/create') }}"><i class="fa fa-plus"></i> Question Category Add</a></li>
                                <li><a href="{{ url('/question-category') }}"><i class="mdi mdi-settings"></i> Question Category Manage</a></li>
                            </ul>

                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-question-circle"></i><span class="hide-menu"> Question</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/question/create') }}"><i class="fa fa-plus"></i>  Question Add</a></li>
                                <li><a href="{{ url('/question') }}"><i class="mdi mdi-settings"></i> Question Manage</a></li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-question-circle"></i><span class="hide-menu"> Answer Option</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/option/create') }}"><i class="fa fa-plus"></i>  Answer Add</a></li>
                                <li><a href="{{ url('/option') }}"><i class="mdi mdi-settings"></i> Answer Manage</a></li>
                            </ul>

                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="ti-widget"></i><span class="hide-menu"> Industry Type</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/industry-type/create') }}"><i class="fa fa-plus"></i>  Industry Type Add</a></li>
                                <li><a href="{{ url('/industry-type') }}"><i class="mdi mdi-settings"></i> Industry Type Manage</a></li>

                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="mdi mdi-vector-intersection"></i><span class="hide-menu"> Sector</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/sector/create') }}"><i class="fa fa-plus"></i>  Sector Add</a></li>
                                <li><a href="{{ url('/sector') }}"><i class="mdi mdi-settings"></i> Sector Manage</a></li>

                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="mdi mdi-vector-intersection"></i><span class="hide-menu"> Sub-Sector</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('sub-sector/create') }}"><i class="fa fa-plus"></i> Sub-Sector Add</a></li>
                                <li><a href="{{ url('/sub-sector') }}"><i class="mdi mdi-settings"></i> Sub-Sector Manage</a></li>

                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="mdi mdi-google-maps"></i><span class="hide-menu"> Location</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="ti-location-pin"> Division</i><span class="hide-menu">  </span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="{{ url('/division/create') }}"><i class="fa fa-plus"></i>  Division Create</a></li>
                                        <li><a href="{{ url('/division') }}"><i class="mdi mdi-settings"></i> Division Manage</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="ti-location-pin"> District</i><span class="hide-menu"></span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="{{ url('/district/create') }}"><i class="fa fa-plus"></i> District Create</a></li>
                                        <li><a href="{{ url('/district') }}"><i class="mdi mdi-settings"></i> District Manage</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="ti-location-pin"> Upazila</i><span class="hide-menu"></span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="{{ url('upazila/create') }}"><i class="fa fa-plus"></i>  Upazila Create</a></li>
                                        <li><a href="{{ url('/upazila') }}"><i class="mdi mdi-settings"></i> Upazila Manage</a></li>

                                    </ul>
                                </li>

                                <li>

                                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="ti-location-pin"> Post Office</i><span class="hide-menu"></span></a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="{{ url('/post-office/create') }}"><i class="fa fa-plus"></i>  Post Office Create</a></li>
                                        <li><a href="{{ url('/post-office') }}"><i class="mdi mdi-settings"></i> P.O Manage</a></li>

                                    </ul>
                                </li>

                                {{--<li>--}}
                                    {{--<a class="has-arrow " href="#" aria-expanded="false"><i class="ti-location-pin"> Post Code</i><span class="hide-menu"> </span></a>--}}
                                    {{--<ul aria-expanded="false" class="collapse">--}}
                                        {{--<li><a href="{{ url('/post-code/create') }}"><i class="fa fa-plus"></i>  Post Code Create</a></li>--}}
                                        {{--<li><a href="{{ url('/post-code') }}"><i class="mdi mdi-settings"></i> P.O Manage</a></li>--}}

                                    {{--</ul>--}}
                                {{--</li>--}}

                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-bank (alias)"></i><span class="hide-menu"> Bank</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/bank/create') }}"><i class="fa fa-plus"></i>  Bank Add</a></li>
                                <li><a href="{{ url('/bank') }}"><i class="mdi mdi-settings"></i> Bank Manage</a></li>

                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-bank (alias)"></i><span class="hide-menu"> Branch</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ url('/branch/create') }}"><i class="fa fa-plus"></i>  Branch Add</a></li>
                                <li><a href="{{ url('/branch') }}"><i class="mdi mdi-settings"></i> Branch Manage</a></li>

                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="nav-devider"></li>

                <li>
                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-bank (alias)"></i><span class="hide-menu">Bank</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/bank-register') }}"><i class="fa fa-plus"></i>  Bank Add</a></li>
                        <li><a href="{{ url('/bank-users') }}"><i class="mdi mdi-settings"></i> Bank Information</a></li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-bank (alias)"></i><span class="hide-menu">Branch</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/branch-users') }}"><i class="mdi mdi-settings"></i> Branch Information</a></li>

                    </ul>
                </li>

                <li>
                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">SME</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/sme-users') }}"><i class="mdi mdi-settings"></i> SME Information</a></li>

                    </ul>
                </li>
                <li>
                    <a class="has-arrow label" href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">Affiliate</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ url('/affiliate-users') }}"><i class="mdi mdi-settings"></i>Affiliate Information</a></li>

                    </ul>
                </li>

                <li class="nav-devider"></li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form><i class="mdi mdi-power"></i>

    <div class="sidebar-footer">
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <!-- item-->
        <a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
