@extends('admin.master-layout')
@section('content')

 <div class="col-md-8 offset-2 pt-5">
     <div class="card card-body">
         <div class="col-12 row">
             <h3 class="box-title m-b-0 col-11">Post Code Create</h3>
             <div class="col-1">
                 <a href="{{url('/post-code')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
             </div>
         </div>
         <hr class="mb-5">
         <form class="form-horizontal" method="post" action="{{url('/post-code')}}">
             @csrf
             <div class="form-group row">
                 <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select Post Office</label>
                 <div class="col-sm-8">
                     <select class="{{$errors->has('postoffice_id') ? 'form-control is-invalid' : 'form-control'}}" name="postoffice_id" id="inlineFormCustomSelect" required autofocus>
                         <option selected="">Choose Post Office...</option>
                         @foreach($postoffices as $key=>$postoffice)
                             <option value="{{$key}}" @if($key==old('postoffice_id')) {{'selected'}} @endif>{{$postoffice}}</option>
                         @endforeach
                     </select>
                     @if ($errors->has('postoffice_id'))
                         <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('postoffice_id') }}</strong>
                            </span>
                     @endif

                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Code Number*</label>
                 <div class="col-sm-8">
                     <input type="text"  name="post_code" value="{{old('post_code')}}" class="{{$errors->has('post_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Post Code Number">
                     @if ($errors->has('post_code'))
                         <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_code') }}</strong>
                     </span>
                     @endif
                 </div>
             </div>


             <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                 <div class="col-sm-8">
                     <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Post Code Add</button>
                 </div>
             </div>






             {{--<div class="form-group m-b-0">--}}
             {{--<div class="offset-sm-3 col-sm-8">--}}
             {{--<button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Field Add</button>--}}
             {{--</div>--}}
             {{--</div>--}}

         </form>
     </div>
 </div>
@endsection