@extends('admin.master-layout')
@section('content')


    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">District Create</h3>
                <div class="col-1">
                    <a href="{{url('/district')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>

            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/district')}}">

                @csrf

                <div class="form-group row">
                    <label for="division_id" class="col-sm-3 text-right control-label col-form-label">Select Division</label>
                    <div class="col-sm-8">
                        <select class="{{$errors->has('division_id') ? 'form-control is-invalid' : 'form-control'}}" name="division_id" id="division_id">
                            <option>Choose district...</option>
                            @foreach($divisions as $key=>$division)
                                <option value="{{$key}}" @if($key==old('division_id')) {{'selected'}} @endif >{{$division}}</option>

                            @endforeach
                        </select>
                        @if ($errors->has('division_id'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('division_id') }}</strong>
                     </span>
                        @endif

                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="title" value="{{old('title')}}" class="{{$errors->has('title') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="District Name">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                     </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Geo code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="geo_code" class="{{$errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="District Geo Code">

                        @if ($errors->has('geo_code'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('geo_code') }}</strong>
                     </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">District Add</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
@endsection