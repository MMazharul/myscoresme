@extends('admin.master-layout')

@section('content')





   <div class="col-md-8 offset-2 pt-5">

       <div class="card card-body">

           <h3 class="box-title m-b-0">District Edit</h3>

           <hr class="mb-5">

           <form class="form-horizontal" method="post" action="{{url('/district/'.$districtEdit->id)}}">
               @csrf
               {{method_field('PUT')}}
               <div class="form-group row">

                   <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>

                   <div class="col-sm-8">

                       <select class="custom-select col-sm-12" name="division_id" id="inlineFormCustomSelect">

                           <option selected="">Choose district...</option>

                           @foreach($divisions as $key=>$division)

                               <option value="{{$key}}" @if($key==$districtEdit->division_id) {{'selected'}} @endif>{{$division}}</option>
                           @endforeach
                       </select>
                   </div>
               </div>
               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Name*</label>
                   <div class="col-sm-8">
                       <input type="text" name="title" value="{{$districtEdit->title}}" class="form-control" id="inputEmail3" placeholder="District Name">
                   </div>
               </div>


               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Geo code *</label>
                   <div class="col-sm-8">
                       <input type="text" name="geo_code" value="{{$districtEdit->geo_code}}" class="{{$errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="District Geo Code">
                       @if ($errors->has('geo_code'))
                           <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('geo_code') }}</strong>
                     </span>
                       @endif
                   </div>
               </div>

               <div class="form-group row">

                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>

                   <div class="col-sm-8">

                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">District Update</button>

                   </div>

               </div>





           </form>

       </div>

   </div>

@endsection
