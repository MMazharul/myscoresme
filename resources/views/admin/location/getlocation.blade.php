@extends('admin.master-layout')
@section('content')
<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Division</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
        <select name="division" class="form-control division" >
            <option value="0" disabled="true" selected="true">-Select division-</option>
            @foreach($divisions as $division)
                <option value="{{ $division->id}}">{{ $division->title }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Distirct</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">

        <select name="district"  class="form-control  district">
            <option value="0" disabled="true" selected="true"></option>
        </select>
    </div>
</div>


<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Thana</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">

        <select name="thana" class="form-control  thana">
            <option value="0" disabled="true" selected="true"></option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">Sub Office</label>
    <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">

        <select name="sub_office"  class="form-control suboffice">

            <option value="0" disabled="true" selected="true"></option>
        </select>

    </div>
</div>

@endsection
