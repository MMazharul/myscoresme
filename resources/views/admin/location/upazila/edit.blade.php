@extends('admin.master-layout')
@section('content')

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Upazila Edit</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/upazila/'.$upazilaEdit->id)}}">

                @csrf
                {{method_field('PUT')}}

                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12" name="district_id" id="inlineFormCustomSelect">
                            <option selected="">Choose District...</option>
                            @foreach($districts as $key=>$district)
                                <option value="{{$key}}" @if($key == $upazilaEdit->district_id) {{ 'selected' }} @endif >{{$district}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="title" value="{{$upazilaEdit->title}}" class="form-control" id="inputEmail3" placeholder="Thana Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Geo code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="geo_code" value="{{$upazilaEdit->geo_code}}" class="{{$errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Upazila Geo Code">

                        @if ($errors->has('geo_code'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('geo_code') }}</strong>
                     </span>
                        @endif
                    </div>
                </div>



                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" @if($upazilaEdit->status==1) {{'checked'}} @endif name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" @if($upazilaEdit->status==0) {{'checked'}} @endif name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Upazila Update</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
@endsection