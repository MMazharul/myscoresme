@extends('admin.master-layout')
@section('content')

 <div class="col-md-8 offset-2 pt-5">
     <div class="card card-body">
         <div class="col-12 row">
             <h3 class="box-title m-b-0 col-11">Upazila Create</h3>
             <div class="col-1">
                 <a href="{{url('/upazila')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
             </div>
         </div>
         <hr class="mb-5">
         <form class="form-horizontal" method="post" action="{{url('/upazila')}}">
             @csrf
             <div class="form-group row">
                 <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select District</label>
                 <div class="col-sm-8">
                     <select class="{{$errors->has('district_id') ? 'form-control is-invalid' : 'form-control'}}" name="district_id" id="inlineFormCustomSelect" required autofocus>
                         <option selected="">Choose District...</option>
                         @foreach($districts as $key=>$district)
                             <option value="{{$key}}" @if($key==old('district_id')) {{'selected'}} @endif>{{$district}}</option>
                         @endforeach

                     </select>
                     @if ($errors->has('district_id'))
                         <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('district_id') }}</strong>
                            </span>
                     @endif
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Name*</label>
                 <div class="col-sm-8">
                     <input type="text" name="title" value="{{old('title')}}" class="{{$errors->has('title') ? 'form-control is-invalid' : 'form-control'}}"  id="inputEmail3" placeholder="Upazila Name">
                     @if ($errors->has('title'))
                         <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                     </span>
                     @endif
                 </div>
             </div>

             <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Geo code *</label>
                 <div class="col-sm-8">
                     <input type="text" name="geo_code" class="{{$errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Upazila Geo Code">

                     @if ($errors->has('geo_code'))
                         <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('geo_code') }}</strong>
                     </span>
                     @endif
                 </div>
             </div>


             <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                 <div class="col-sm-8">
                     <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Upazila Add</button>
                 </div>
             </div>






             {{--<div class="form-group m-b-0">--}}
             {{--<div class="offset-sm-3 col-sm-8">--}}
             {{--<button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Field Add</button>--}}
             {{--</div>--}}
             {{--</div>--}}

         </form>
     </div>
 </div>
@endsection