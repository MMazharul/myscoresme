@extends('admin.master-layout')
@section('content')

   <div class="col-md-8 offset-2 pt-5">
       <div class="card card-body">
           <div class="col-12 row">
               <h3 class="box-title m-b-0 col-11">Post Office Create</h3>
               <div class="col-1">
                   <a href="{{url('/post-office')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
               </div>
           </div>
           <hr class="mb-5">
           <form class="form-horizontal" method="post" action="{{url('/post-office')}}">

               @csrf

               <div class="form-group row">
                   <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select Upazila</label>
                   <div class="col-sm-8">
                       <select class="{{$errors->has('upazila_id') ? 'form-control is-invalid' : 'form-control'}}" name="upazila_id" id="inlineFormCustomSelect" required autofocus>
                           <option selected="">Choose Upazila...</option>
                           @foreach($upazilas as $key=>$upazila)
                               <option value="{{$key}}" @if($key==old('upazila_id')) {{'selected'}} @endif>{{$upazila}}</option>
                           @endforeach
                       </select>
                       @if ($errors->has('upazila_id'))
                           <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('upazila_id') }}</strong>
                            </span>
                       @endif
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Office Name*</label>
                   <div class="col-sm-8">
                       <input type="text" name="title" class="{{$errors->has('title') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Post Office Name">
                       @if ($errors->has('title'))
                           <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                     </span>
                       @endif
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Code Number*</label>
                   <div class="col-sm-8">
                       <input type="text"  name="post_code" value="{{old('post_code')}}" class="{{$errors->has('post_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Post Code Number">
                       @if ($errors->has('post_code'))
                           <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_code') }}</strong>
                     </span>
                       @endif
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                   <div class="col-sm-8">
                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Post Office Add</button>
                   </div>
               </div>




           </form>
       </div>
   </div>
@endsection