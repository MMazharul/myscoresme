
@extends('admin.master-layout')
@section('content')

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Post Office Edit</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/post-office/'.$id)}}">

                @csrf
                {{method_field('PUT')}}
                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12" name="upazila_id" id="inlineFormCustomSelect">
                            <option selected="">Choose Upazila...</option>
                            @foreach($upazilas as $upazila)
                                <option value="{{$upazila->id}}" @if($upazila->id == $postOfficeEdit->upazila_id) {{'selected'}} @endif>{{$upazila->title}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">PostOffice Name*</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{$postOfficeEdit->title}}" name="title" class="form-control" id="inputEmail3" placeholder="Sub-Office Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="post_code" value="{{$postOfficeEdit->postcode->post_code}}" class="{{$errors->has('post_code') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="PostOffice Geo Code">

                        @if ($errors->has('post_code'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('post_code') }}</strong>
                     </span>
                        @endif
                    </div>
                </div>



                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">PostOffice Update</button>
                    </div>
                </div>




            </form>
        </div>
    </div>
@endsection
