@extends('admin.master-layout')
@section('content')
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-table">

                        <div class="card-header">
                            Question Information
                            <div class="tools dropdown"><a href="{{ url('/question/'.$questionCategoryShow->id.'/edit' ) }}"><span class="icon mdi mdi-edit"></span></a>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Label English </th>
                                    <td>{{ $questionCategoryShow->label_en }}</td>
                                </tr>
                                <tr>
                                    <th>Label Bangla</th>
                                    <td>{{ $questionCategoryShow->label_bn }}</td>
                                </tr>
                                <tr>
                                    <th>Help English</th>
                                    <td>{{ $questionCategoryShow->help_en }}</td>
                                </tr>
                                <tr>
                                    <th>Help Bangla</th>
                                    <td>{{ $questionCategoryShow->help_bn }}</td>
                                </tr>
                                <tr>
                                    <th>Example English</th>
                                    <td>{{ $questionCategoryShow->example_en }}</td>
                                </tr>
                                <tr>

                                    <th>Example Bangla</th>
                                    <td>{{ $questionCategoryShow->example_en }}</td>
                                </tr>
                                <tr>
                                    <th>Field Type</th>
                                    <td>{{ $questionCategoryShow->field_type }}</td>
                                </tr>
                                <tr>
                                    <th>Placeholder</th>
                                    <td>{{ $questionCategoryShow->placeholder }}</td>
                                </tr>
                                <tr>
                                    <th>Column Name</th>
                                    <td>{{ $questionCategoryShow->column_name }}</td>
                                </tr>
                                <tr>
                                    <th>Created at</th>
                                    <td>{{ $questionCategoryShow->created_at }}</td>
                                </tr>
                                <tr>
                                    <th>Updated at</th>
                                    <td>{{ $questionCategoryShow->updated_at }}</td>
                                </tr>

                                <tr>
                                    <th>Status</th>
                                    <td>
                                        @if($questionCategoryShow->status==1)
                                            {{'Publish'}}
                                        @elseif($questionCategoryShow->status==0)
                                            {{'Unpublish'}}
                                        @endif
                                    </td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection