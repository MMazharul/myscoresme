@extends('admin.master-layout')
@section('content')

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Question Category Create</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/question-category')}}">
                @csrf
                <div class="form-group row">
                    <label for="question_category" class="col-sm-4 text-right control-label col-form-label">Question Category Name*</label>
                    <div class="col-sm-7">
                        <input type="text" name="question_category"
                               class="{{$errors->has('question_category') ? 'form-control is-invalid' : 'form-control'}}"
                           id="question_category" placeholder="Enter question category">
                        @if ($errors->has('question_category'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('question_category') }}</strong>
                        </span>
                        @endif
                    </div>

                </div>


                {{--<div class="form-group row">--}}
                    {{--<label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Division Name*</label>--}}
                    {{--<div class="col-sm-8">--}}
                        {{--<input type="text" name="title" class="{{$errors->has('title') ? 'form-control is-invalid' : 'form-control'}}" id="inputEmail3" placeholder="Division Name">--}}

                        {{--@if ($errors->has('title'))--}}
                            {{--<span class="invalid-feedback" role="alert">--}}
                                {{--<strong>{{ $errors->first('title') }}</strong>--}}
                            {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14" value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"><i class="fa fa-plus"></i> Create</button>
                    </div>
                </div>

            </form>
        </div>
    </div>


@endsection