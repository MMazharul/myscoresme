@extends('admin.master-layout')

@section('content')



    <div class="card">

        @if(session('message'))
            <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong>{{ session('message') }}</strong>
            </div>
        @endif

        <div class="card-body">

            <div class="col-12 row">
                <div class="col-11">
                    <h2><i class="fa fa-list"></i> Slider List</h2>
                </div>
                <h3 class="box-title m-b-0 col-1"><a href="{{url('slider/create')}}" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>
            </div>

            <div class="table-responsive m-t-40">

                <table id="myTable" class="table table-bordered table-striped">

                    <thead>

                    <tr>

                        <th>Title</th>

                        {{--<th>Position</th>--}}

                        <th>Short Description</th>

                        <th>Image</th>

                        <th>Button Title</th>

                        <th>Status</th>

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    @foreach($sliders as $slider)

                    <tr>

                        <td class="pt-lg-5">{{ $slider->title }}</td>

                        {{--<td> {{ $bank->address }}</td>--}}

                        <td class="pt-lg-5"> {{ $slider->short_description }}</td>

                        <td><img src="{{ asset('ui/back-end/slider-image/'.$slider->picture)  }}" height="100px" width="150px"></td>

                        <td class="pt-lg-5"> {{ $slider->button_title }}</td>

                        <td class="pt-lg-5">

                            @if($slider->status==1)

                                {{'Publish'}}

                            @elseif($slider->status==0)

                                {{'Unpublish'}}

                            @endif

                        </td>

                        <td class="pt-lg-5">

                            <div class="dropdown row">

                                @if($slider->status == 1)

                                    <a href="{{url('slider/status/'.$slider->id)}}" title="Click to Unpublished"

                                       class="btn btn-success waves-effect waves-light ml-1" type="button"

                                       aria-haspopup="true" aria-expanded="false">

                                        <i class="fa fa-check"></i>

                                    </a>

                                @else

                                    <a href="{{url('/slider/status/'.$slider->id)}}" title="Click to Published"

                                       class="btn btn-warning waves-effect waves-light ml-1" type="button"

                                       aria-haspopup="true" aria-expanded="false">

                                        <i class="fa fa-warning"></i>

                                    </a>

                                @endif

                                <a href="{{url('/slider/'.$slider->id)}}" title="Click to More details"

                                   class="btn btn-outline-default ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                    <i class="mdi mdi-eye"></i>

                                </a>

                                <div class="btn-group ml-1">

                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle"

                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        <i class="icon mdi mdi-settings"></i></button>

                                    <div class="dropdown-menu">

                                        <a class="dropdown-item" href="{{ url('/slider/'.$slider->id.'/edit' ) }}"><i

                                                    class="mdi mdi-table-edit"></i> Edit</a>

                                        <a class="dropdown-item" href="{{url('/slider-delete/'.$slider->id)}}"><i

                                                    class="mdi mdi-delete"></i> Delete</a>

                                    </div>

                                </div>

                            </div>

                        </td>

                    </tr>

                    @endforeach

                    </tbody>

                </table>

            </div>

        </div>

    </div>



@endsection
