@extends('admin.master-layout')

@section('content')

    @if(session('message'))
        <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong>{{ session('message') }}</strong>
        </div>
    @endif


    <div class="col-md-10 offset-md-1 pt-5">

        <div class="card card-body">

            <div class="col-12 row">

                <h3 class="box-title m-b-0 col-11">Slider Create</h3>

                <div class="col-1">

                    <a href="{{url('/slider')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>

                </div>

            </div>

            <hr class="mb-4">

            <form class="form-horizontal" method="post" action="{{url('/slider')}}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group row">

                    <label for="title" class="col-sm-3 text-right control-label col-form-label"> Title*</label>

                    <div class="col-sm-7">

                        <input type="text" name="title" class="form-control" id="title" placeholder="Title">

                    </div>

                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 text-right control-label col-form-label">Slider Image</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="picture" type="file" name="picture" placeholder="Enter Your Mobile Number">
                    </div>
                </div>

                <div class="form-group row">

                    <label for="ShortDescription" class="col-sm-3 text-right control-label col-form-label">Short Description*</label>

                    <div class="col-sm-7">

                        <textarea class="form-control" id="ShortDescription" cols="10" rows="4" name="short_description" placeholder="Short Description"></textarea>

                    </div>

                </div>



                <div class="form-group row">

                    <label for="buttonTitle" class="col-sm-3 text-right control-label col-form-label">Button Title*</label>

                    <div class="col-sm-7">

                        <input class="form-control" id="buttonTitle" type="text" name="button_title" placeholder="link">

                    </div>

                </div>





                <div class="form-group row">

                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>

                    <div class="radio radio-success mt-2">

                        <input type="radio" name="status" id="radio14" value="1" checked>

                        <label for="radio14"> Publish </label>

                    </div>

                    <div class="radio radio-warning mt-2">

                        <input type="radio" name="status" id="radio17" value="0">

                        <label for="radio17"> Unpublish </label>

                    </div>

                </div>





                <div class="form-group row">

                    <label for="branch_type" class="col-sm-3 text-right control-label col-form-label"></label>

                    <div class="col-sm-7">

                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Slider Add</button>

                    </div>

                </div>
            </form>

        </div>

    </div>




@endsection
