@extends('admin.master-layout')
@section('content')
    <div class="be-content">
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-table">

                        <div class="card-header">
                            Bank Information
                            <div class="tools dropdown"><a href="{{ url('/bank/'.$bankShow->id.'/edit' ) }}"><span class="icon mdi mdi-edit"></span></a>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Bank Name</th>
                                    <td>{{ $bankShow->bank_name }}</td>
                                </tr>
                                <tr>
                                    <th>Bank Address</th>
                                    <td>{{ $bankShow->address }}</td>
                                </tr>
                                <tr>
                                    <th>Mobile</th>
                                    <td>{{ $bankShow->mobile }}</td>
                                </tr>
                                <tr>
                                    <th>Fax</th>
                                    <td>{{ $bankShow->fax }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $bankShow->email }}</td>
                                </tr>
                                <tr>

                                    <th>Website</th>
                                    <td><a href="{{ $bankShow->website }}" target="_blank">{{ $bankShow->website }}</a></td>
                                </tr>
                                <tr>
                                    <th>Swift Code</th>
                                    <td>{{ $bankShow->swift_code }}</td>
                                </tr>
                                <tr>
                                    <th>Branch Type</th>
                                    <td>{{ $bankShow->branch_type }}</td>
                                </tr>
                                <tr>
                                    <th>Founded</th>
                                    <td>{{ $bankShow->establish }}</td>
                                </tr>
                                <tr>
                                    <th>Loan Type</th>
                                    <td>{{ $bankShow->loan_type }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        @if($bankShow->status==1)
                                            {{'Publish'}}
                                        @elseif($bankShow->status==0)
                                            {{'Unpublish'}}
                                        @endif
                                    </td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection