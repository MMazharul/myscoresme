@extends('admin.master-layout')

@section('content')
  <div class="card">
      @if(session('message'))
          <div class="alert alert-success alert-dismissible" role="alert"  id="message">
              <div class="icon"><span class="mdi mdi-check"></span></div>
              <div class="message text-center ">{{ session('message') }}</div>
          </div>
      @endif
      <div class="card-body">
          <div class="col-12 row">

              <div class="col-11">
                  <h2><i class="fa fa-list"></i> Sub-sector List</h2>
              </div>

              <h3 class="box-title m-b-0 col-1"><a href="{{url('sub-sector/create')}}" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>

          </div>
          <div class="table-responsive m-t-40">
              <table id="myTable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>Sub Sector Name</th>
                      <th>Sub Sector Code</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($subSectors as $subSector)
                      <tr>
                          <td>{{ $subSector->sub_sector }}</td>
                          <td>{{ $subSector->sub_sector_code }}</td>
                          <td>
                              <div class="dropdown row">

                                  <div class="btn-group ml-1">
                                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="icon mdi mdi-settings"></i></button>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="{{ url('sub-sector/'.$subSector->id.'/edit' ) }}"><i
                                                      class="mdi mdi-table-edit"></i> Edit</a>
                                          <a class="dropdown-item" href="{{url('/sub-sector-delete/'.$subSector->id)}}"><i
                                                      class="mdi mdi-delete"></i> Delete</a>
                                      </div>
                                  </div>
                              </div>
                          </td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
          </div>
      </div>
  </div>


@endsection
