@extends('admin.master-layout')
@section('content')
    @if(session('message'))
        <div class="alert alert-success alert-dismissible" role="alert"  id="message">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message text-center ">{{ session('message') }}</div>
        </div>
    @endif

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Sub-sector Edit</h3>
                <div class="col-1">
                    <a href="{{url('/sub-sector/')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="{{url('/sub-sector/'.$subSectorEdit->id)}}">
                @csrf
                {{method_field('PUT')}}

                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Sector Type</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12 {{$errors->has('	sector_id') ? 'form-control is-invalid' : 'form-control'}}" name="sector_id" id="inlineFormCustomSelect">
                            <option selected=""> --Choose a Sector Type--</option>
                            @foreach($sectors as $id=>$sector)
                                <option value="{{$id}}" @if($id==$subSectorEdit->sector_id) {{'selected'}} @endif>{{$sector}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('	sector_id'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('	sector_id') }}</strong>
                     </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="subsector" class="col-sm-3 text-right control-label col-form-label">Sub sector Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="sub_sector" value="{{$subSectorEdit->sub_sector}}" class="form-control" id="subsector" placeholder="Enter Sector Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sector" class="col-sm-3 text-right control-label col-form-label">Sub sector Code*</label>
                    <div class="col-sm-8">
                        <input type="text" name="code" value="{{$subSectorEdit->code}}" class="form-control" id="sector" placeholder="Enter Industry Sector Code">
                    </div>
                </div>

                <div class="form-group row disabled">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish</label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Subsector Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection