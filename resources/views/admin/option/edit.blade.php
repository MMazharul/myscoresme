@extends('admin.master-layout')
@section('content')

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Option Edit</h3>
                <div class="col-1">
                    <a href="{{url('/option')}}" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-5">
            <form class="form-horizontal group-border-dashed" method="post" action="{{url('/option/'.$optionEdit->id)}}">
                {{csrf_field()}}
                {{method_field('PUT')}}

                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Question</label>
                    <div class="col-12 col-sm-8 col-lg-6 ">
                        <select class="select2 form-control" name="question_id">
                            <option disabled="true" selected="true">-- Choose a Catagory --</option>
                            @foreach($questions as $key=>$question)
                                <option value="{{$key}}" @if($key == $optionEdit->question_id) {{ 'selected' }} @endif >{{$question}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="optionRow">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Option</label>
                        <div class="col-sm-6">
                            <input type="text" name="title" value="{{$optionEdit->title}}" class="form-control" id="inputEmail3" placeholder="Enter Sector Name">
                        </div>

                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14" @if($optionEdit->status==1) {{'checked'}} @endif  value="1" >
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" @if($optionEdit->status==0) {{'checked'}} @endif id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6 offset-3">
                        <button class="btn btn-success btn-space wizard-next pt-1 pb-1">Option Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection