@extends('admin.master-layout')

@section('content')


    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center ">{{ session('message') }}</div>
            </div>
        @endif
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> Branch Information</button>
                </div>



            </div>
            <div class="m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Branch Name</th>
                        <th>Date Estabilish</th>
                        <th>profesional Staff</th>
                        <th>Email</th>
                        <th>Contact Number</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($branchUser as $branch)
                        <tr>
                            <td><img width="100px" height="50px" src="{{asset('ui/back-end/bank/bank-logo/'. $bank->bank_logo )}}"></td>
                            <td>{{ $branch->branch_name }}</td>
                            <td>{{ $branch->date_estabilish }}</td>
                            <td>{{ $branch->retype_email }}</td>
                            <td>{{ $branch->profesional_staff }}</td>
                            <td>{{ $branch->owner_number }}</td>
                            <td class="pt-lg-5">
                                <div class="dropdown row">

                                    <a href="" title="Click to More details"
                                       class="btn btn-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-eye"></i>
                                    </a>
                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection