@extends('admin.master-layout')

@section('content')


    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center ">{{ session('message') }}</div>
            </div>
        @endif
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> SME Information</button>
                </div>



            </div>
            <div class="m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($smeUser as $sme)
                        <tr>
                            <td>{{ $sme->title }}</td>
                            <td>{{ $sme->first_name }}</td>
                            <td>{{ $sme->middle_name }}</td>
                            <td>{{ $sme->last_name }}</td>
                            <td>{{ $sme->owner_contact }}</td>
                            <td>{{ $sme->email }}</td>

                            <td class="pt-lg-5">
                                <div class="dropdown row">

                                    <a href="" title="Click to More details"
                                       class="btn btn-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-eye"></i>
                                    </a>
                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection