@extends('front-end.partials.master-layout')
@section('title','Agent Register')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11 mt-5">
                <div class="card bg-white">

                    <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="mb10 mt20  section-title text-center  ">
                            <!-- section title start-->
                            <h2>Affiliate Register</h2>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="{{url('/affiliate-store')}}">
                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail3">Category </label>

                                    <select class="form-control" name="category" id="company" onclick="showHide()">
                                        <option disabled="true" selected="true">-- Choose a Category --</option>
                                        <option value="company">Company</option>
                                        <option value="individual">Individual</option>
                                    </select>

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail3">Title </label>
                                    <input class="form-control" type="text" name="affilate_name"
                                           placeholder="Enter Your Title">
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="division1">Division</label>
                                    <select id="division1" name="division"
                                            class="form-control{{ $errors->has('division') ? ' is-invalid' : '' }} division4">
                                        <option value="0" disabled="true" selected="true">-Select division-</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id}}">{{ $division->title }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group col-md-3">
                                    <label for="district1">Distirct</label>

                                    <select id="district1" name="district"
                                            class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}  district4">
                                        <option value="0" disabled="true" selected="true"></option>
                                    </select>

                                </div>


                                <div class="form-group col-md-3">
                                    <label for="upazila1">Upazila</label>

                                    <select id="upazila1" name="upazila"
                                            class="form-control{{ $errors->has('upazila') ? ' is-invalid' : '' }}  upazila4">
                                        <option value="0" disabled="true" selected="true"></option>
                                    </select>

                                </div>


                                <div class="form-group col-md-3">
                                    <label for="post_office1">Post
                                        Office</label>
                                    <select id="post_office1" name="post_office"
                                            class="form-control{{ $errors->has('post_office') ? ' is-invalid' : '' }} suboffice4">
                                        <option value="0" disabled="true" selected="true"></option>
                                    </select>
                                </div>


                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3 postcode4">
                                    <label for="post_code1">Post Code:</label>

                                    <input id="post_code1" required="required" type="text" name="post_code1"
                                           class=" form-control{{ $errors->has('post_code') ? ' is-invalid' : '' }}"
                                           readonly>

                                </div>
                                <div class="form-group col-md-9">
                                    <label for="post_code1">Address Line-1</label>
                                    <input type="text" class="form-control" name="address_1">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="contact_number">Contact Number</label>

                                    <input class="form-control" id="contact_number" type="text" name="contact_number">
                                </div>

                                <div class="form-group col-md-4" id="email1">
                                    <label for="email">Email</label>
                                    <input id="email" type="text"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{old('email')}}" placeholder="Enter Your email address" >
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                              </span>
                                    @endif
                                </div>


                                <div class="form-group col-md-4">
                                    <label for="joining_date">Joining Date</label>

                                    <input class="form-control" id="joining_date" type="date" name="joining_date">

                                </div>
                            </div>

                            <div class="hidden" style="display:none" id="hidden">
                                <div class="card">
                                    <div class="card-header">

                                        In case of Company Contact Person:

                                    </div>
                                    <div class="card-body bg-white">
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="title">Title (Mrs/Mr/Ms)</label>

                                                <select id="title"
                                                        class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                        name="title">
                                                    <option disabled selected>-Select Title-</option>
                                                    <option value="Mrs." {{ old('title') == "Mrs." ? 'selected' : '' }}>
                                                        Mrs.
                                                    </option>
                                                    <option value="Mr." {{ old('title') == "Mr." ? 'selected' : '' }}>
                                                        Mr.
                                                    </option>
                                                    <option value="Ms" {{ old('title') == "Ms" ? 'selected' : '' }}>Ms
                                                    </option>
                                                </select>
                                                @if ($errors->has('title'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>


                                            <div class="form-group col-md-3">
                                                <label for="first_name">First Name</label>

                                                <input id="first_name" type="text"
                                                       class="form-control{{ $errors->has('contact_first_name') ? ' is-invalid' : '' }}"
                                                       name="contact_first_name" value="{{old('contact_first_name')}}"
                                                       placeholder="Enter your first name">
                                                @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('contact_first_name') }}</strong>
                                                     </span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="middle_name">Middle
                                                    Name</label>

                                                <input id="middle_name" type="text"
                                                       class="form-control{{ $errors->has('contact_middle_name') ? ' is-invalid' : '' }}"
                                                       name="middle_name" value="{{old('contact_middle_name')}}"
                                                       placeholder="Enter your middle name">
                                                @if ($errors->has('contact_middle_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('contact_middle_name') }}</strong>
                                                    </span>
                                                @endif

                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="last_name">Last Name</label>

                                                <input id="last_name" type="text"
                                                       class="form-control{{ $errors->has('contact_last_name') ? ' is-invalid' : '' }}"
                                                       name="last_name" value="{{old('last_name')}}"
                                                       placeholder="Enter your last name">
                                                @if ($errors->has('contact_last_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $errors->first('contact_last_name') }}</strong>
                                                    </span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="designation">Designation</label>
                                                <input type="text" class="form-control" name="designation"
                                                       id="designation" placeholder="Enter Your Designation">
                                            </div>


                                            <div class="form-group col-md-4">
                                                <label for="contact_number">Contact
                                                    Number</label>

                                                <input type="text" class="form-control" name="owner_contact"
                                                       id="contact_number" placeholder="Enter Your Phone Number">

                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="contact_email">Email</label>

                                                <input type="email" class="form-control" name="person_email"
                                                       id="contact_email" placeholder="Enter Your Email">

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="trade_license">Trade License Number</label>

                                    <input type="text" class="form-control" name="trade_license" id="trade_license">

                                </div>


                                <div class="form-group col-md-4">
                                    <label for="expire_date"> Expire Date</label>

                                    <input type="date" class="form-control" name="expire_date" id="expire_date">

                                </div>

                                <div class="form-group col-md-4">
                                    <label for="expire_date">Commission Rate</label>

                                    {{--<input type="number" class="form-control" name="commision_rate" id="commision_rate">--}}



                                    <div class="input-group">
                                        <input type="number" class="form-control" name="commision_rate" id="validationDefaultUsername" aria-describedby="inputGroupPrepend2" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend2">%</span>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="password">Create Password
                                    </label>

                                    <input type="password"
                                           class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           id="password" name="password" placeholder="Enter your password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif

                                </div>


                                <div class="form-group col-md-6">
                                    <label for="password">Confirm Password
                                    </label>

                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation"
                                           placeholder="Enter your confirm password">

                                </div>
                            </div>


                            <button type="submit" class="btn btn-primary">Affiliate Sign Up</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function showHide() {

            var value = $('#company').val();

            if (value == 'company') {
                $('#hidden').show();
            }
            else if (value == 'individual') {
                $('#hidden').hide();
            }

        }
    </script>


@endsection
