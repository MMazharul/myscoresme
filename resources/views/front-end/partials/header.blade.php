<div class="collapse searchbar" id="searchbar">

    <div class="search-area">

        <div class="container">

            <div class="row">

                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                    <div class="input-group">

                        <input type="text" class="form-control" placeholder="Search for...">

                        <span class="input-group-btn">

            <button class="btn btn-default" type="button">Go!</button>

            </span> </div>

                    <!-- /input-group -->

                </div>

                <!-- /.col-lg-6 -->

            </div>

        </div>

    </div>

</div>

<div class="top-bar">

    <!-- top-bar -->

    <div class="container">

        <div class="row">

            <div class="col-xl-4 col-lg-5 col-md-4 col-sm-6 col-6 d-none d-xl-block d-lg-block">

                <p class="mail-text">Welcome to our Borrow Loan Website Templates</p>

            </div>

            <div class="col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 text-right">

                <div class="top-nav"> <span class="top-text"><a href="#">View Locations</a> </span> <span class="top-text"><a href="#">+1800-123-4567</a></span> <span class="top-text"><a href="#">EMI calculator</a></span> </div>

            </div>

        </div>

    </div>

</div>

<!-- /.top-bar -->

<div class="header">

    <div class="container">

        <div class="row">

            <div class="col-xl-2 col-lg-2 col-md-12 col-sm-6 col-6">

                <!-- logo -->

                <div class="logo">

                    <a href="#"><img src="{{asset('/ui/front-end/images/sme-logo/sme-logo.png')}}" style="margin: -15px; height: 90px; width: 120px" alt="Borrow - Loan Company Website Template"></a>

                </div>

            </div>

            <!-- logo -->

            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

                <div id="navigation">

                    <!-- navigation start-->

                    <ul>

                        <li class="active"><a href="{{url('/')}}" class="animsition-link">Home</a></li>



                        <li><a href="#" class="animsition-link">Loan Product</a>

                            <ul>

                                <li><a href="#" title="Education Loan" class="animsition-link">Credit Card <span class="badge">New</span></a>

                                    <ul>

                                        <li><a href="credit-card-listing.html" title="Credit Card Listing" class="animsition-link">Credit Card Listing</a></li>

                                        <li><a href="credit-card-single.html" title="Credit Card Single" class="animsition-link">Credit Card Single</a></li>

                                    </ul>

                                </li>

                                <li><a href="#" title="Lenders" class="animsition-link">Lenders <span class="badge">New</span></a>

                                    <ul>

                                        <li><a href="lender-listing.html" title="Credit Card Listing" class="animsition-link">Lenders Listing</a></li>

                                        <li><a href="lender-single.html" title="Credit Card Single" class="animsition-link">Lenders Single</a></li>

                                    </ul>

                                </li>

                                <li><a href="product-single-sidebar.html">Product page side tab <span class="badge">New</span></a>

                                </li>

                                <li><a href="loan-listing-image.html" title="Loan Image Listing" class="animsition-link">Loan Image Listing</a></li>

                                <li><a href="loan-listing-icon.html" title="Loan Icon Listing" class="animsition-link">Loan Icon Listing</a></li>

                                <li><a href="car-loan.html" title="Car Loan" class="animsition-link">Car Loan Single</a></li>

                                <li><a href="personal-loan.html" title="Personal Loan" class="animsition-link">Personal Loan Single</a></li>

                                <li><a href="home-loan.html" title="Home Loan" class="animsition-link">Home Loan Single</a></li>

                                <li><a href="education-loan.html" title="Education Loan" class="animsition-link">Education Loan Single</a></li>

                            </ul>

                        </li>

                        <li><a href="about.html" class="animsition-link">About us</a>

                            <ul>

                                <li><a href="about.html" title="About us" class="animsition-link">About us</a></li>

                                <li><a href="team.html" title="Team" class="animsition-link">Team</a></li>

                            </ul>

                        </li>

                        <li><a href="blog-listing.html" class="animsition-link">Blog</a>

                            <ul>

                                <li><a href="blog-listing.html" title="Blog Listing" class="animsition-link">Blog Listing</a></li>

                                <li><a href="blog-single.html" title="Blog Single" class="animsition-link">Blog Single</a></li>

                                <li><a href="blog-two-column.html" title="Blog Two Column" class="animsition-link">Blog Two Column</a></li>

                                <li><a href="blog-three-column.html" title="Blog Three Column" class="animsition-link">Blog Three Column</a></li>

                            </ul>

                        </li>

                        <li><a href="compare-loan.html" class="animsition-link">Features</a>

                            <ul>

                                <li><a href="#" title="Compare Loan" class="animsition-link">Compare Page</a>

                                    <ul>



                                        <li><a href="compare-credit-card.html" title="Compare Loan" class="animsition-link">Compare Credit Card</a>

                                        <li><a href="compare-personal-loan.html" title="Compare Loan" class="animsition-link">Compare Personal</a></li>

                                        <li><a href="compare-student-loan.html" title="Compare Loan" class="animsition-link">Compare Student Loan</a></li>

                                        <li><a href="compare-loan.html" title="Compare Loan" class="animsition-link">Compare Loan</a></li>

                                    </ul>

                                </li>

                                <li><a href="#" class="animsition-link">Landing Pages</a>

                                    <ul>

                                        <li><a href="landing-page-car-loan.html" title="Loan Calculator" class="animsition-link">Car Loan

                                                <small class="highlight">Landing Page</small>

                                            </a></li>

                                        <li><a href="landing-page-home-loan.html" title="Loan Calculator" class="animsition-link">Home Loan

                                                <small class="highlight">Landing Page</small></a></li>

                                        <li><a href="refinancing-landing-page.html" title="Refinancing Page" class="animsition-link">Refinancing Page

                                                <small class="highlight">Landing Page</small></a></li>

                                        <li><a href="lead-genrator-landing-page.html" title="Lead Genrator" class="animsition-link">Lead Genrator

                                                <small class="highlight">Landing Page</small></a></li>

                                        <li><a href="card-landing-page.html" title="Card Landing" class="animsition-link">Card Page

                                                <small class="highlight">Landing Page</small></a></li>

                                    </ul>

                                </li>

                                <li><a href="loan-calculator.html" title="Loan Calculator" class="animsition-link">Loan Calculator</a></li>

                                <li><a href="loan-eligibility.html" title="Loan Calculator" class="animsition-link">Loan Eligibility Cal <span class="badge">NEW</span></a></li>

                                <li><a href="faq.html" title="Faq" class="animsition-link">Faq page</a></li>

                                <li><a href="testimonial.html" title="Testimonial" class="animsition-link">Testimonial</a></li>

                                <li><a href="error.html" title="404 Error" class="animsition-link">404 Error</a></li>

                                <li><a href="gallery-filter-2.html" class="animsition-link">Gallery</a>

                                    <ul>

                                        <li><a href="gallery-filter-2.html" title="Filterable Gallery 2 column" class="animsition-link">Filterable Gallery 2 column</a></li>

                                        <li><a href="gallery-filter-3.html" title="Filterable Gallery 3 column" class="animsition-link">Filterable Gallery 3 column</a></li>

                                        <li><a href="gallery-masonry.html" title="Masonry Gallery" class="animsition-link">Masonry Gallery</a></li>

                                        <li><a href="gallery-zoom.html" title="Zoom Gallery" class="animsition-link">Zoom Gallery</a></li>

                                    </ul>

                                </li>

                                <li><a href="shortcodes-tabs.html" class="animsition-link">Shortcodes</a>

                                    <ul>

                                        <li><a href="shortcodes-tables.html" title="Table" class="animsition-link">Table</a></li>

                                        <li><a href="shortcodes-tabs.html" title="Tab" class="animsition-link">Tab</a></li>

                                        <li><a href="shortcodes-accordion.html" title="Accordion" class="animsition-link">Accordion</a></li>

                                        <li><a href="shortcodes-alert.html" title="Alert" class="animsition-link">Alert</a></li>

                                        <li><a href="shortcodes-button.html" title="Button" class="animsition-link">Button</a></li>

                                        <li><a href="shortcodes-column.html" title="Column" class="animsition-link">Column</a></li>

                                    </ul>

                                </li>

                                <li><a href="contact-us.html" title="Contact us" class="animsition-link">Contact us</a></li>

                            </ul>

                        </li>

                        <li><a href="compare-loan.html" class="animsition-link">Bank Account</a>

                            <ul>

                                <li><a href="personal-bank-account.html" title="Bank Account" class="animsition-link">Bank Account</a></li>

                                <li><a href="personal-bank-saving.html" title="Bank Saving" class="animsition-link">Bank Account list</a></li>

                                <li><a href="borrow-account-saving.html" title="Bank Account Single" class="animsition-link">Bank Account Single</a></li>

                            </ul>

                        </li>



                        <li><a href="{{url('/login')}}" class="animsition-link">Login</a>



                        </li>



                    </ul>

                </div>

                <!-- /.navigation start-->

            </div>

            <div class="col-xl-l col-lg-1 col-md-1 col-sm-1 col-12 d-none d-xl-block d-lg-block">

                <!-- search start-->

                <div class="search-nav"> <a class="search-btn" role="button" data-toggle="collapse" href="#searchbar" aria-expanded="false"><i class="fa fa-search"></i></a> </div>

            </div>

            <!-- /.search start-->

        </div>

    </div>

</div>
