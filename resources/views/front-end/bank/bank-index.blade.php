@extends('admin.master-layout')
@section('content')



    <div class="card">
        @if(session('message'))
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center ">{{ session('message') }}</div>
            </div>
        @endif
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> List</button>
                </div>

                <h3 class="box-title m-b-0 col-1"><a href="{{url('bank/create')}}" class="btn btn-dropbox"><i
                                class="mdi mdi-plus-circle"></i> Add New</a></h3>

            </div>
            <div class="table-responsive m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Bank logo</th>
                        <th>Organization Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($banks as $bank)
                        <tr>
                            <td>{{ $bank->organization_name }}</td>
                            <td><img src="{{asset('ui/back-end/bank/bank-logo/'.$bank->bank_logo)}}" width="100px" height="50px"> </td>

                            <td>
                                @if($bank->status==1)
                                    {{'Publish'}}
                                @elseif($bank->status==0)
                                    {{'Unpublish'}}
                                @endif
                            </td>

                            <td>
                                <div class="dropdown row">
                                    @if($bank->status == 1)
                                        <a href="{{url('bank/status/'.$bank->id)}}" title="Click to Unpublished"
                                           class="btn btn-success waves-effect waves-light ml-1" type="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-check"></i>
                                        </a>
                                    @else
                                        <a href="{{url('/bank/status/'.$bank->id)}}" title="Click to Published"
                                           class="btn btn-warning waves-effect waves-light ml-1" type="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-warning"></i>
                                        </a>
                                    @endif

                                    <a href="{{url('/bank/'.$bank->id)}}" title="Click to More details"
                                       class="btn btn-light ml-1" type="button" aria-haspopup="true"
                                       aria-expanded="false">
                                        <i class="mdi mdi-eye"></i>
                                    </a>


                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ url('/bank/'.$bank->id.'/edit' ) }}"><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href="{{url('/bank-delete/'.$bank->id)}}"><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>


                                    --}}{{--<a class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton"--}}{{--
                                    --}}{{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}{{--
                                    --}}{{--<i class="icon mdi mdi-settings"></i>--}}{{--
                                    --}}{{--</a>--}}{{--
                                    --}}{{----}}{{--
                                    --}}{{----}}{{--


                                    --}}{{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}{{--
                                    --}}{{----}}{{--
                                    --}}{{--</div>--}}{{--

                                </div>
                            </td>


                        </tr>
                    @endforeach--}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection