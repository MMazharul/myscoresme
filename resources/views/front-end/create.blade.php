<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from foxythemes.net/preview/products/beagle/form-wizard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Sep 2018 12:39:41 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('/') }}back-end/img/logo-fav.png">
    <title>Home</title>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/') }}back-end/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/') }}back-end/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/') }}back-end/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/') }}back-end/lib/bootstrap-slider/css/bootstrap-slider.min.css"/>
    <link rel="stylesheet" href="{{ asset('/') }}back-end/css/app.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <style type="text/css">
        .box{
            width:600px;
            margin:0 auto;
            border:1px solid #ccc;
        }
    </style>

</head>
<body>
<div class="be-wrapper">
    <div class="col-lg-12">
        <div class="page-head">


        </div>
        <div class="main-content container-fluid">
            <div class="row wizard-row">
                <div class="col-md-10 offset-1 fuelux">
                    <div class="block-wizard">
                        <div class="wizard wizard-ux" id="wizard1">
                            <div class="steps-container">
                                <ul class="steps">
                                    <li class="active" data-step="1">Step 1<span class="chevron"></span></li>
                                    <li data-step="2">Step 2<span class="chevron"></span></li>
                                    <li data-step="3">Step 3<span class="chevron"></span></li>
                                </ul>
                            </div>
                            <div class="actions">
                                <button class="btn btn-xs btn-prev btn-secondary" type="button"><i
                                            class="icon mdi mdi-chevron-left"></i> Prev
                                </button>
                                <button class="btn btn-xs btn-next btn-secondary" type="button" data-last="Finish">
                                    Next<i class="icon mdi mdi-chevron-right"></i></button>
                            </div>
                            <form class="form-horizontal group-border-dashed" action="{{url('/business-background')}}" method="POST"
                                  data-parsley-namespace="data-parsley-" data-parsley-validate=""
                                  novalidate="">
                            <div class="step-content">
                                <div class="step-pane active" data-step="1">
                                    <div class="container p-0">
                                            {{csrf_field()}}
                                            <div class="form-group row mb-4">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Business Background Information</h3>
                                                </div>
                                            </div>
                                        <input class="form-control" type="hidden" name="user_id" value="{{Auth::user()->id}}">

                                            @foreach($questions1 as $question)

                                                @if($question->field_type=='text')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">
                                                            <input class="form-control" type="text" name="{{$question->column_name}}"
                                                                   placeholder="{{$question->placeholder}}" required>
                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='number')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">

                                                            <input class="form-control" type="number" name="{{$question->column_name}}" placeholder="{{$question->placeholder}}" required>

                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='textarea')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">
                                                            <textarea class="form-control" type="password" name="{{$question->column_name}}"
                                                                      placeholder="{{$question->placeholder}}" required></textarea>
                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='calender')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"> {{ $question->label_en }} </label>
                                                        <div class="col-12 col-sm-8 col-md-5 col-lg-8 col-xl-3 ml-4">
                                                            <div class="input-group date datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd">
                                                                <input class="form-control" type="date" name="{{$question->column_name}}" required>
                                                            </div>
                                                        </div>
                                                    </div>



                                                @elseif($question->field_type =='select')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            <select class="form-control"  name="{{$question->column_name}}" required>
                                                                <option disabled="true" selected="true"> ---Choice a Option --- </option>
                                                                @foreach($options as $option)
                                                                    @if($question->id == $option->question_id)
                                                                        <option  value="{{$option->title}}" >{{$option->title}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>

                                                @elseif($question->field_type =='checkbox')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            @foreach($options as $option)
                                                                @if($question->id == $option->question_id)
                                                                    @if($option->question->field_type =='checkbox')
                                                                        <label class="custom-control custom-checkbox custom-control-inline">
                                                                            <input class="custom-control-input" value="{{$option->title}}" name="{{$question->column_name.'[]'}}"
                                                                                   type="checkbox"
                                                                                   required><span
                                                                                    class="custom-control-label">{{ $option->title }}</span>
                                                                        </label>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>



                                                @elseif($question->field_type =='radio')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            @foreach($options as $option)
                                                                @if($question->id == $option->question_id)
                                                                    @if($option->question->field_type =='radio')
                                                                        <label class="custom-control custom-radio custom-control-inline">
                                                                            <input class="custom-control-input" type="radio" value="{{$option->title}}" name="{{$question->column_name}}"
                                                                                   required>{{ $option->title }}<span class="custom-control-label"></span>
                                                                        </label>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                         {{--   @include('admin.location.getlocation')
                                            @include('admin.bank.bank')--}}


                                            <div class="form-group row">
                                                <div class="col-12 offset-3">

                                                    <a href="{{url('/home')}}" class="btn btn-secondary btn-space ml-4" >Cancel</a>
                                                    <button class="btn btn-primary btn-space wizard-next"
                                                            data-wizard="#wizard1">Next Step
                                                    </button>
                                                </div>
                                            </div>



                                    </div>
                                </div>

                                <div class="step-pane active" data-step="2">
                                    <div class="container p-0">
                                       {{-- <form class="form-horizontal group-border-dashed" action="#"
                                              data-parsley-namespace="data-parsley-" data-parsley-validate=""
                                              novalidate="">--}}
                                            <div class="form-group row mb-4">
                                                <div class="col-sm-7">
                                                    <h3 class="wizard-title">Business Background Information Stape-2</h3>
                                                </div>
                                            </div>

                                            @foreach($questions2 as $question)

                                                @if($question->field_type=='text')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">
                                                            <input class="form-control" type="text" name="{{$question->column_name}}"
                                                                   placeholder="{{$question->placeholder}}" required>
                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='number')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">

                                                            <input class="form-control" type="number" name="{{$question->column_name}}" placeholder="{{$question->placeholder}}" required>

                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='textarea')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 ml-4">
                                                            <textarea class="form-control" type="password" name="{{$question->column_name}}"
                                                                      placeholder="{{$question->placeholder}}" required></textarea>
                                                        </div>
                                                    </div>

                                                @elseif($question->field_type=='calender')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-sm-right"> {{ $question->label_en }} </label>
                                                        <div class="col-12 col-sm-8 col-md-5 col-lg-8 col-xl-3 ml-4">
                                                            <div class="input-group date datetimepicker" data-min-view="2" data-date-format="yyyy-mm-dd">
                                                                <input class="form-control" type="date" name="{{$question->column_name}}" required>
                                                            </div>
                                                        </div>
                                                    </div>



                                                @elseif($question->field_type =='select')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            <select class="form-control" name="{{$question->column_name}}" required>
                                                                <option disabled="true" selected="true"> ---Choice a Option --- </option>
                                                                @foreach($options as $option)
                                                                    @if($question->id == $option->question_id)
                                                                        <option  value="{{$option->title}}">{{$option->title}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>

                                                @elseif($question->field_type =='checkbox')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            @foreach($options as $option)
                                                                @if($question->id == $option->question_id)
                                                                    @if($option->question->field_type =='checkbox')
                                                                        <label class="custom-control custom-checkbox custom-control-inline">
                                                                            <input class="custom-control-input"
                                                                                   type="checkbox" name="{{$question->column_name.'[]'}}"
                                                                            value="{{$option->title}}" required><span
                                                                                    class="custom-control-label">{{ $option->title }}</span>
                                                                        </label>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>



                                                @elseif($question->field_type =='radio')
                                                    <div class="form-group row">
                                                        <label class="col-12 col-sm-3 col-form-label text-left text-sm-right">{{ $question->label_en }}</label>
                                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1 ml-4">
                                                            @foreach($options as $option)
                                                                @if($question->id == $option->question_id)
                                                                    @if($option->question->field_type =='radio')
                                                                        <label class="custom-control custom-radio custom-control-inline">
                                                                            <input class="custom-control-input"
                                                                                   type="radio" value="{{$option->title}}" name="{{$question->column_name}}"
                                                                                   required>{{ $option->title }}<span
                                                                                    class="custom-control-label"></span>
                                                                        </label>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                       {{--     @include('admin.location.getlocation')
                                            @include('admin.bank.bank')

                                            @include('admin.location.getlocation')
                                            @include('admin.bank.bank')--}}

                                            <div class="form-group row">
                                                <div class="col-12 offset-3">


                                                  {{--  <button class="btn btn-secondary btn-space ml-4">Back</button>--}}

                                              {{--      <button class="btn btn-secondary btn-space ml-4">Back</button>--}}
                                                  {{--  <button class="btn btn-prev btn-secondary btn-space ml-4" type="button"><i

                                                                class="icon mdi mdi-chevron-left"></i> Prev</button>--}}




                                                   <button class="btn btn-prev btn-secondary btn-space ml-4" type="button">Back
                                                    </button>

                                                    {{--<button class="btn btn-secondary ">Back</button>--}}

                                                    {{--<button class="btn btn-primary btn-space wizard-next"--}}
                                                            {{--data-wizard="#wizard1">Next Step </button>--}}


                                                    {{--<button type="submit" class="btn btn-primary btn-space wizard-next"--}}
                                                            {{--data-wizard="#wizard1">Finish--}}
                                                    {{--</button>--}}


                                                    <button type="submit" class="btn btn-primary btn-space"
                                                            data-wizard="#wizard1">Finish
                                                    </button>

                                                    {{--<input type="submit" class="btn btn-primary" value="finish">--}}






                                                    {{--<button class="btn btn-primary btn-space wizard-next"
                                                            data-wizard="#wizard1">Next Step
                                                    </button>--}}
                                                   {{-- <input type="submit" class="btn btn-primary" value="finish">--}}

                                                </div>
                                            </div>


                                    </div>
                                </div>

                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>




<script src="{{ asset('/ajax/location.js') }}" type="text/javascript"></script>
<script src="{{ asset('/ajax/branch.js') }}" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/js/app.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/fuelux/js/wizard.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}back-end/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //-initialize the javascript
        App.init();
        App.wizard();
    });
</script>
</body>
<!-- Mirrored from foxythemes.net/preview/products/beagle/form-wizard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Sep 2018 12:39:41 GMT -->
</html>