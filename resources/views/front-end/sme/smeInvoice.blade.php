<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sme Information Pdf</title>
    <style>
        .table{width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}.table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6}
        .table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}.table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6}
        .col-md-10{-webkit-box-flex:0;-ms-flex:0 0 83.33333333%;flex:0 0 83.33333333%;max-width:83.33333333%}
        .col-md-2{-webkit-box-flex:0;-ms-flex:0 0 16.66666667%;flex:0 0 16.66666667%;max-width:16.66666667%}
    </style>
</head>
<body>
<h2 class="text-center">Sme Information Pdf</h2>

<div class="card">

    <div class="card-body">
        <h3>Business Background Information</h3>
        <table class="table">

            @foreach($businessBackgrounds as $businessBackground)
                <tr class="row">
                    <td class="col-md-10">{{$businessBackground->label_en}}</td>
                    <td class="col-md-2">{{$backgroundinformation[$businessBackground->column_name]}}</td>
                </tr>
            @endforeach
        </table>
        <h3>Owner Background Information</h3>
        <table class="table">

            @foreach($ownerBackgrounds as $ownerBackground)
                <tr class="row">
                    <td class="col-md-10">{{$ownerBackground->label_en}}</td>
                    <td class="col-md-2">{{$ownerinformation[$ownerBackground->column_name]}}</td>
                </tr>
            @endforeach
        </table>
        <h3>Business Liquiditie Management Information</h3>
        <table class="table">

            @foreach($businessLiquidities as $businessLiquiditie)
                <tr class="row">
                    <td class="col-md-10">{{$businessLiquiditie->label_en}}</td>
                    <td class="col-md-2">{{$liquidityMmanagement[$businessLiquiditie->column_name]}}</td>
                </tr>
            @endforeach
        </table>
        <h3>Business Acount&Inventory Information</h3>
        <table class="table">


            @foreach($accountInventories as $accountInventorie)
                <tr class="row">
                    <td class="col-md-10">{{$accountInventorie->label_en}}</td>
                    <td class="col-md-2">{{$inventoryaccount[$accountInventorie->column_name]}}</td>
                </tr>
            @endforeach
        </table>


    </div>
</div>


</body>
</html>