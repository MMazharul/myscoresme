@extends('front-end.partials.master-layout')
@section('title', 'Register')@section('content')
    <style>
        .capture_button {
            color: gainsboro;
            bottom: 10px;
            border-radius: 50px;
            border: 2px solid grey;
            background: transparent;
            font-weight: bold;
            margin: 10px;
            outline: none;
            transition: .5s background, border ease;
            cursor: pointer;
        }

        .capture_button:hover {
            background: lightgrey;
            color: black;
            border: 2px solid lightgrey;
        }
        .button_grp {
            width: 100%;
            position: absolute;
            bottom: 10px;
            background: transparent;
        }

        .flex {
            display: flex;
            justify-content: center;
        }
    </style>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Director/Partners Details</h3></div>
                    <div class="card-body">
                        <form id="partner_validation" class="form-horizontal" action="{{ url('/partner-director-info/store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            @if (Auth::user()->businessInfo->organization == 'Partnership' || Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')
                                @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
                                    <div class="card">
                                        @if(Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')
                                            <div class="card-header"><h4 class="mb-0">Director - {{ $i }}</h4></div>
                                        @else
                                            <div class="card-header"><h4 class="mb-0">Partner - {{ $i }}</h4></div>
                                        @endif
                                        <div class="card-body bg-white">
                                            <div id="present">
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_title">Title (Mrs./Mr./Ms.)</label>
                                                        <select id="owner_title{{$i}}" class="form-control{{ $errors->has('owner_title'.$i) ? ' is-invalid' : '' }}" name="owner_title{{$i}}">
                                                            <option disabled selected>Select Your Title</option>
                                                            <option value="Mrs." {{ old('owner_title'.$i) == "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                                            <option value="Mr." {{ old('owner_title'.$i) == "Mr." ? 'selected' : '' }}>Mr.</option>
                                                            <option value="Ms." {{ old('owner_title'.$i) == "Ms" ? 'selected' : '' }}>Ms.</option>
                                                        </select>
                                                        @if ($errors->has('owner_title'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_title'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_first_name">First Name</label>
                                                        <input id="owner_first_name{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('owner_first_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_first_name{{$i}}" value="{{old('owner_first_name'.$i)}}" placeholder="Enter your first name">
                                                        @if ($errors->has('owner_first_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_first_name'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_middle_name">Middle Name</label>
                                                        <input id="owner_middle_name{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('owner_middle_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_middle_name{{$i}}" value="{{old('owner_middle_name'.$i)}}" placeholder="Enter your middle name">
                                                        @if ($errors->has('owner_middle_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_middle_name'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_last_name">Last Name</label>
                                                        <input id="owner_last_name{{$i}}" type="text" class="form-control{{ $errors->has('owner_last_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_last_name{{$i}}" value="{{old('owner_last_name'.$i)}}" placeholder="Enter your last name">
                                                        @if ($errors->has('owner_last_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_last_name'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">

                                                    <div class="form-group col-md-4">
                                                        <label for="owner_nid">NID Number</label>
                                                        <input id="owner_nid{{$i}}" type="number"
                                                               class="form-control{{ $errors->has('owner_nid'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_nid{{$i}}" value="{{old('owner_nid'.$i)}}" placeholder="Enter Your nid number">
                                                        @if ($errors->has('owner_nid'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_nid'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_front_pic">Upload NID Front Side</label>

                                                        <input type="file" accept="image/*"  data-max-file-size="512K" id="nid_front_pic{{$i}}" name="nid_front_pic{{$i}}" class="dropify" data-height="150"/>
                                                        <span class="mt-2">Maximum file size is 512KB </span>


                                                        @if ($errors->has('nid_front_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('nid_front_pic'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_back_pic">Upload NID Back Side</label>

                                                        <input type="file" accept="image/*"  data-max-file-size="512K" id="nid_back_pic{{$i}}" name="nid_back_pic{{$i}}" class="dropify" data-height="150"/>
                                                        <span class="mt-2">Maximum file size is 512KB</span>

                                                        @if ($errors->has('nid_back_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nid_back_pic'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="organization">Position in the Organizatrion</label>
                                                        <select id="position_{{$i}}" class="form-control{{ $errors->has('position'.$i) ? ' is-invalid' : '' }} position"
                                                                name="position{{$i}}" onclick="positionChange()">
                                                            <option disabled selected>Choose Option</option>

                                                            @if(Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')

                                                                <option value="Chairman" @if(old('position'.$i)=='Chairman') {{'selected'}} @endif>
                                                                    Chairman
                                                                </option>
                                                                <option value="Managing Director"  @if(old('position'.$i)=='Managing Director') {{'selected'}} @endif>
                                                                    Managing Director
                                                                </option>
                                                                <option value="Shareholder Director" @if(old('position'.$i)=='Shareholder Director') {{'selected'}} @endif>
                                                                    Shareholder Director
                                                                </option>
                                                            @else
                                                                <option value="Managing Partner" @if(old('position'.$i)=='Managing Partner') {{'selected'}} @endif>
                                                                    Managing Partner
                                                                </option>
                                                                <option value="Partner" @if(old('position'.$i)=='Partner') {{'selected'}} @endif>
                                                                    Partner
                                                                </option>
                                                            @endif

                                                        </select>
                                                        @if ($errors->has('postion'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('position'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6" >
                                                        <label for="shareholder">Shareholder (%)</label>
                                                        <input id="shareholder_{{$i}}" type="number"
                                                               class="txt form-control{{ $errors->has('shareholder'.$i) ? ' is-invalid' : '' }}"
                                                               name="shareholder{{$i}}" value="{{old('shareholder'.$i)}}"
                                                               placeholder="Enter your ShareHolder %">
                                                        <div id="meg{{ $i }}">

                                                        </div>
                                                        @if ($errors->has('shareholder'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('shareholder'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="datepicker{{$i}}">Date Of Birth</label>
                                                        <input id="datepicker{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('date_of_birth'.$i) ? ' is-invalid' : '' }} dob{{$i}}"
                                                               name="date_of_birth{{$i}}" value="{{old('date_of_birth'.$i)}}" placeholder="dd-mm-yyyy" readonly>
                                                        @if ($errors->has('date_of_birth'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_of_birth'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="form-row">


                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Capture Your Photo</label>
                                                        <a class=" btn btn-block btn-outline mb-1"
                                                           id="cap_btn{{$i}}" onclick="capture{{$i}}()" data-toggle="modal" data-target="#exampleModal{{$i}}">
                                                            <i class="fa fa-camera"></i> Camera</a>
                                                        <input type="hidden" name="capture_pic{{$i}}" class="image-tag{{$i}}">
                                                        {{--@if ($errors->has('capture_pic'.$i))--}}
                                                        {{--<span class="invalid-feedback" role="alert">--}}
                                                        {{--<strong>{{ $errors->first('capture_pic'.$i) }}</strong>--}}
                                                        {{--</span>--}}
                                                        {{--@endif--}}
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Upload Your Photo</label>
                                                        <input type="file" accept="image/*"  data-max-file-size="512K" id="owner_pic{{$i}}" class="dropify" data-height="150" name="owner_pic{{$i}}" >
                                                        <span>Maximum file size is 512KB</span>

                                                    </div>

                                                </div>


                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="division">Present Address</label>
                                                        <hr class="bg-success mt-0 mb-0">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="row">
                                                            <label class="col-sm-6 mr-0" for="present_address1">Permanent Address</label>
                                                        </div>
                                                        <hr class="bg-dark-blue mt-0 mb-0">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_division">Division</label>
                                                        <select onchange="" id="present_division{{$i}}" name="present_division{{$i}}"
                                                                class="form-control{{ $errors->has('present_division'.$i) ? ' is-invalid' : '' }}">
                                                            <option  disabled="true" selected="true">-Select division-</option>

                                                            @foreach($divisions as $division)

                                                                <option value="@if(old('present_division'.$i)) {{old('present_division'.$i)}} @else {{ $division->id }}@endif"
                                                                @if(old('present_division'.$i) == $division->id)
                                                                    {{'selected'}}

                                                                    @endif>
                                                                    {{ $division->title }}
                                                                </option>
                                                            @endforeach

                                                        </select>
                                                        @if ($errors->has('present_division'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_division'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="division{{$i}}">
                                                        <div class="row col-sm-12">
                                                            <label for="division3" class="col-sm-3 pl-0">Division</label>
                                                            <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0">
                                                                <i>*Same as Permanent Address</i></label>
                                                            <input id="check_{{$i}}" onclick="permanentFunction_{{$i}}()" type="checkbox" value="" class="col-sm-1 mt-1">
                                                        </div>
                                                        <select id="permanent_division{{$i}}" name="permanent_division{{$i}}"
                                                                class="form-control{{ $errors->has('permanent_division'.$i) ? ' is-invalid' : '' }} ">
                                                            <option  disabled="true" selected="true">-Select division-</option>


                                                            @foreach($divisions as $division)

                                                                <option value="@if(old('permanent_division'.$i)) {{old('permanent_division'.$i)}} @else {{ $division->id }}@endif"
                                                                @if(old('permanent_division'.$i) == $division->id)
                                                                    {{'selected'}}

                                                                    @endif>
                                                                    {{ $division->title }}
                                                                </option>
                                                            @endforeach

                                                        </select>
                                                        @if ($errors->has('permanent_division'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_division'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_district">Distirct</label>
                                                        <select id="present_district{{$i}}" name="present_district{{$i}}"
                                                                class="form-control{{ $errors->has('present_district'.$i) ? ' is-invalid' : '' }} ">

                                                            <option  disabled="true" selected="true"></option>

                                                        </select>
                                                        @if ($errors->has('present_district'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_district'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="district{{$i}}">
                                                        <label for="permanent_district">Distirct</label>
                                                        <select id="permanent_district{{$i}}" name="permanent_district{{$i}}"
                                                                class="form-control{{ $errors->has('permanent_district'.$i) ? ' is-invalid' : '' }}">
                                                            <option  disabled="true" selected="true"></option>

                                                        </select>
                                                        @if ($errors->has('permanent_district'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_district'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_upazila">Upazila</label>
                                                        <select id="present_upazila{{$i}}" name="present_upazila{{$i}}"
                                                                class="form-control{{ $errors->has('present_upazila'.$i) ? ' is-invalid' : '' }} ">
                                                            <option  disabled="true" selected="true"></option>

                                                        </select>
                                                        @if ($errors->has('present_upazila'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_upazila'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="upazila{{$i}}">
                                                        <label for="permanent_upazila">Upazila</label>
                                                        <select id="permanent_upazila{{$i}}" name="permanent_upazila{{$i}}"
                                                                class="form-control{{ $errors->has('permanent_upazila'.$i) ? ' is-invalid' : '' }}">
                                                            <option  disabled="true" selected="true"></option>
                                                        </select>
                                                        @if ($errors->has('permanent_upazila'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_upazila'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="presentpost_office">Post Office</label>
                                                        <select id="present_suboffice{{$i}}" name="presentpost_office{{$i}}"
                                                                class="form-control{{ $errors->has('presentpost_office'.$i) ? ' is-invalid' : '' }} ">
                                                            <option  disabled="true" selected="true"></option>
                                                        </select>
                                                        @if ($errors->has('presentpost_office'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('presentpost_office'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="postoffice{{$i}}">
                                                        <label for="permanentpost_office">Post Office</label>
                                                        <select id="permanent_suboffice{{$i}}" name="permanentpost_office{{$i}}"
                                                                class="form-control{{ $errors->has('permanentpost_office'.$i) ? ' is-invalid' : '' }} ">
                                                            <option  disabled="true" selected="true"></option>
                                                        </select>
                                                        @if ($errors->has('permanentpost_office'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanentpost_office'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6" id="present_postcode{{$i}}">
                                                        <label for="post_code2">Post Code</label>

                                                        <input  type="text"  id="present_post_code{{$i}}"   name="present_post_code{{$i}}"
                                                                class="form-control{{ $errors->has('present_post_code'.$i) ? ' is-invalid' : '' }} " readonly>

                                                        @if ($errors->has('present_post_code'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_post_code'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 " id="permanent_postcode{{$i}}">
                                                        <label for="permanentpost_code">Post Code</label>
                                                        @if(old('post_code'.$i))
                                                            <input  type="text" name="post_code{{$i}}" value="{{old('post_code'.$i)}}"
                                                                    class="form-control{{ $errors->has('post_code'.$i) ? ' is-invalid' : '' }} " readonly>
                                                        @else
                                                            <input  type="text" name="post_code{{$i}}"
                                                                    class="form-control{{ $errors->has('post_code'.$i) ? ' is-invalid' : '' }} " readonly>
                                                        @endif
                                                        @if ($errors->has('post_code'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('post_code'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_address">Address line</label>
                                                        <textarea id="present_address_{{$i}}" rows="4" type="text"
                                                                  class="form-control{{ $errors->has('present_address'.$i) ? ' is-invalid' : '' }}"
                                                                  name="present_address{{$i}}"
                                                                  placeholder="Enter Your present address">{{old('present_address'.$i)}}</textarea>
                                                        @if ($errors->has('present_address'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_address'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6" id="permanent_{{$i}}">
                                                        <label for="permanent_address">Address line</label>
                                                        <textarea id="permanent_address_{{$i}}" rows="4" type="text"
                                                                  class="form-control{{ $errors->has('permanent_address'.$i) ? ' is-invalid' : '' }}"
                                                                  name="permanent_address{{$i}}" placeholder="Enter Your permanent address">{{old('permanent_address'.$i)}}</textarea>
                                                        @if ($errors->has('permanent_address'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_address'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                @endfor
                            @endif

                            <div class="form-group m-b-0">
                                <a href="{{ url('register/edit') }}" class="btn btn-outline pt-3 pb-3 pull-left">Back</a>
                                <button type="submit" name="action" value="next" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>
                                <button type="submit" name="action" value="save" class="btn btn-success waves-effect waves-light m-t-10 pt-3 pb-3 pull-right mr-3">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
        <div class="modal fade" id="exampleModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div style="box-shadow: 0px 7px 21px 1px rgba(0,0,0,0.47);width:720px;padding: 0px;
                     border-radius: 20px;border:none; overflow: hidden;" class="modal-content">
                    <div style="padding:0" class="modal-body">
                        <button style="margin-top:5px;position: absolute; right: 10px;z-index: 5;color: white;width: 25px;height: 25px;"
                                type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="display: block;line-height: 0.3">&times;</span>
                        </button>

                        <div class="row">
                            <div class="col-12">
                                <div id="my_camera{{$i}}"></div>
                                <div class="button_grp">
                                    <div id="btns" class="flex">
                                        <button id="capture{{$i}}" style="display: none" onclick="capture{{$i}}()" class="capture_button">New</button>
                                        <button onclick="take_snapshot{{$i}}()" class="capture_button"> <i class="fa fa-camera" style="font-size: 30px"></i></button>
                                        <button id="save{{$i}}" class="capture_button" style="display: none" onclick="cap_save{{$i}}()" data-dismiss="modal" aria-label="Close">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endfor

    <!-- Configure a few settings and attach camera -->

    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
        <script language="JavaScript">
            function capture{{$i}}() {
                Webcam.set({
                    width: 720,
                    height: 405,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });

                Webcam.attach('#my_camera{{$i}}');
            };


            function take_snapshot{{$i}}() {
                Webcam.snap(function (data_uri) {
                    $(".image-tag{{$i}}").val(data_uri);
                    document.getElementById('my_camera{{$i}}').innerHTML = '<img src="' + data_uri + '"/>';
                    $('#capture{{$i}}').css('display', "block");
                    $('#save{{$i}}').css('display', "block");
                });
            }

            function cap_save{{$i}}() {
                take_snapshot{{$i}}();
                $("#cap_btn{{$i}}").append(" ✔");
            }

            function calculateSum() {

                var sum = 0;
                //iterate through each textboxes and add the values
                $(".txt").each(function() {
                    //add only if the value is number
                    if (!isNaN(this.value) && this.value.length != 0) {
                        sum += parseFloat(this.value);
                        $(this).css("background-color", "#FEFFB0");
                    }
                    else if (this.value.length != 0){
                        $(this).css("background-color", "red");
                    }
                });
                if(sum>100)
                {
                    {{--$('#meg{{$i}}').html('Total shareholder present above 100%').css('color', 'red');--}}
                    {{--$('#meg{{$i}}').removeClass('hidden');--}}
                    alert("Total Share holder Should 100%");
                }
                else if(sum<100)
                {
                    $('#meg{{$i}}').addClass('hidden');
                }

                // $("input#sum1").val(sum.toFixed(2));
            }


        </script>
    @endfor



    <script type="text/javascript">

        $(document).ready(function() {
            //this calculates values automatically

            calculateSum();

            $(".txt").on("keydown keyup", function() {
                calculateSum();
            });
        });

        function positionChange()
        {
            @for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++)

            $("#position_{{$i}}").change(function () {


                    var value=$("#position_{{$i}}  :selected").val();

                    var id = $(this).children(":selected").closest("select").attr("id");
                    var check_{{$i}}=$("#position_{{$i}} :selected").val();


                    if(value=='Chairman')
                    {
                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)

                        $("#position_{{$x}} option[value='Chairman']").remove();
                        $("#"+id).html('<option value="Chairman" selected>Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');

                        @endfor

                    }
                    else if(value=="Managing Director")
                    {
                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)

                        $("#position_{{$x}} option[value='Managing Director']").remove();

                        if(check_{{$i}} == 'Managing Director')
                        {
                            $("#"+id).html('<option value="Managing Director" selected>Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');

                        }
                        else if(check_{{$i}} != 'Managing Director') {

                            var abc1 = $("#"+id).html('<option value="Managing Director" >Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');


                        }
                        @endfor
                    }
                    else if(value=='Shareholder Director')
                    {

                        if(check_{{$i}}=='Managing Director' && check_{{$i}}=='Chairman')
                        {
                            console.log(true);
                            $("#position_{{$i}}").html('<option value="Shareholder Director" selected>Shareholder Director<option>');
                        }
                        $("#position_{{$i}}").html('<option value="Chairman">Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director" selected>Shareholder Director<option>');
                    }
                    else if(value=='Managing Partner')
                    {

                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)
                        $("#position_{{$x}} option[value='Managing Partner']").remove();
                        $("#"+id).html('<option value="Managing Partner" selected>Managing Partner</option><option value="Partner">Partner</option>');
                        @endfor
                    }
                    else if(value=='Partner')
                    {
                        $("#position_{{$i}}").html('<option value="Managing Partner">Managing Partner</option><option value="Partner" selected>Partner</option>');

                    }


                }

            );
            @endfor
        }

        @for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++)
        function permanentFunction_{{$i}}() {


            if ($("#check_{{$i}}").is(':checked')) {
                var division2 = $('#present_division{{$i}} :selected').text();
                var divisionValue = $('#present_division{{$i}} :selected').val();
                $('#permanent_division{{$i}}').html("<option value="+divisionValue+">" + division2 + "<option>");
                var district2 = $('#present_district{{$i}} :selected').text();
                var districtValue = $('#present_district{{$i}} :selected').val();
                $('#permanent_district{{$i}}').html("<option value="+districtValue+">" + district2 + "<option>");
                var upazila3 = $('#present_upazila{{$i}} :selected').text();
                var upazilaValue = $('#present_upazila{{$i}} :selected').val();
                $('#permanent_upazila{{$i}}').html("<option value="+upazilaValue+">" + upazila3 + "<option>");
                var suboffice3 = $('#present_suboffice{{$i}} :selected').text();
                var subofficeVal = $('#present_suboffice{{$i}} :selected').val();
                $('#permanent_suboffice{{$i}}').html("<option value="+subofficeVal+">" + suboffice3 + "<option>");

                var postcode3 = $("input:text#present_post_code{{$i}}").val();


                $('#permanent_postcode{{$i}}').html('<label>Post Code</label><input type="text"  name="post_code{{$i}}"  class="form-control" value="' + postcode3 + '" readonly>');


                var present_address=$("textarea#present_address_{{$i}}").val();
                $('#permanent_{{$i}}').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "                                                          name=\"permanent_address{{$i}}\" placeholder=\"Enter Your permanent address\" >"+present_address+"</textarea>");
                var divisionSpanHidden=$('#division{{$i}}').find("span");
                divisionSpanHidden.addClass("hidden");
                var districtSpanHidden=$('#district{{$i}}').find("span");
                districtSpanHidden.addClass("hidden");
                var upazilaSpanHiddden=$('#upazila{{$i}}').find("span");
                upazilaSpanHiddden.addClass("hidden");
                var postSpanHiddden=$('#postoffice{{$i}}').find("span");
                postSpanHiddden.addClass("hidden");
            } else {
                $('#permanent_division{{$i}}').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " @foreach($divisions as $division)\n" + "  <option value=\"{{ $division->id}}\">{{ $division->title }}</option>\n" + " @endforeach");
                $('#permanent_district{{$i}}').html("<option><option>");
                $('#permanent_upazila{{$i}}').html("<option><option>");
                $('#permanent_suboffice{{$i}}').html("<option><option>");
                $('#permanent_postcode{{$i}}').html('<label>Post Code</label><input type="text"  name="post_code{{$i}}"  class="form-control" value="" readonly>');
                $('#permanent_{{$i}}').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "                                                          name=\"permanent_address{{$i}}\" placeholder=\"Enter Your permanent address\" ></textarea>");
            }
        }
        @endfor
    </script>

    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
        <script>
            $( function() {

                $( "#datepicker{{$i}}" ).datepicker( "option", "showAnim", $( this ).val() );
                $( "#datepicker{{$i}}" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    yearRange: "-120:+0",

                    onSelect: function(date) {
                        var dateSplit = date.split("-");
                        var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                        var today = new Date();
                        if (dob.getFullYear() + 18 > today.getFullYear()) {

                            alert('You must be atleast 18 years old to submit register');
                            // $.alert({
                            //     title: 'Alert!',
                            //     content: 'You must be atleast 18 years old to submit register',
                            //     type: 'red',
                            //     typeAnimated: true,
                            // });
                            $(".dob{{$i}}").val("");
                        }
                    }

                });
            });

        </script>
    @endfor


@endsection


