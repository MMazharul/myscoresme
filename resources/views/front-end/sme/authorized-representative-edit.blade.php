@extends('front-end.partials.master-layout')
@section('title', 'Register')
@section('content')
    <style>
        .inputGroup {
            background-color: #eeeeee;
            display: block;
            margin: 10px 0;
            position: relative;
        }
        .inputGroup label {
            padding: 12px 30px;
            width: 100%;
            display: block;
            text-align: left;
            color: #3c454c;
            cursor: pointer;
            position: relative;
            z-index: 2;
            transition: color 200ms ease-in;
            overflow: hidden;
        }
        .inputGroup label:before {
            width: 15px;
            height: 10px;
            border-radius: 50%;
            content: '';
            background-color: #5562eb;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%) scale3d(1, 1, 1);
            transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);
            opacity: 0;
            z-index: -1;
        }
        .inputGroup label:after {
            width: 32px;
            height: 32px;
            content: '';
            border: 2px solid #d1d7dc;
            background-color: #fff;
            background-image: url("data:image/svg+xml,%3Csvg width='32' height='32' viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.414 11L4 12.414l5.414 5.414L20.828 6.414 19.414 5l-10 10z' fill='%23fff' fill-rule='nonzero'/%3E%3C/svg%3E ");
            background-repeat: no-repeat;
            background-position: 2px 3px;
            border-radius: 50%;
            z-index: 2;
            position: absolute;
            right: 30px;
            top: 50%;
            transform: translateY(-50%);
            cursor: pointer;
            transition: all 200ms ease-in;
        }
        .inputGroup input:checked ~ label {
            color: #fff;
        }
        .inputGroup input:checked ~ label:before {
            transform: translate(-50%, -50%) scale3d(56, 56, 1);
            opacity: 1;
        }
        .inputGroup input:checked ~ label:after {
            background-color: #54e0c7;
            border-color: #54e0c7;
        }
        .inputGroup input {
            width: 32px;
            height: 32px;
            order: 1;
            z-index: 2;
            position: absolute;
            right: 30px;
            top: 50%;
            transform: translateY(-50%);
            cursor: pointer;
            visibility: hidden;
        }
        .form {
            padding: 0 16px;
            max-width: 550px;
            margin: 50px auto;
            font-size: 18px;
            font-weight: 600;
            line-height: 36px;
        }
        body {
            background-color: #d1d7dc;
            font-family: 'Fira Sans', sans-serif;
        }
        *, *::before, *::after {
            box-sizing: inherit;
        }
        html {
            box-sizing: border-box;
        }
        code {
            background-color: #9aa3ac;
            padding: 0 8px;
        }

    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Authorized Representative</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" action="{{url('/authorized-representative/update/')}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}


                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label text-right"><strong class="text-danger">Attention:</strong></label>
                                <div class="col-sm-10 pl-0 mt-2">
                                   <p class="text-danger">Please select one authorized representative from the list should be treated as applicant.</p>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-12 col-form-label">Authorized Representative of <strong>{{\Illuminate\Support\Facades\Auth::user()->businessInfo->enterprise_name}}</strong></label>
                            </div>
                            <hr>


                            <div class="form-row">
                                {{--<label for="staticEmail" class="col-sm-4 col-form-label">Position/Status in Company :</label>--}}

                                {{--<div class="col-md-6">--}}

                                {{--</div>--}}
                                <div class="col-md-12">
                                    <div class="col-sm-12">
                                        @if(Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')

                                            <div class="inputGroup">
                                                <input class="form-check-input check-chairman"
                                                       onclick="getPositionInfo()"
                                                       @if(isset(Auth::user()->contactInfo->position) && Auth::user()->contactInfo->position=='Chairman') {{'checked'}} @endif  type="radio"
                                                       name="profile_id" value="Chairman" id="position1">
                                                <label class="" for="position1">
                                                    @foreach($chairmans as $chairman)
                                                        Chairman ({{ $chairman->owner_title.' '.$chairman->owner_first_name }})
                                                    @endforeach
                                                </label>
                                            </div>
                                            <div class="inputGroup">
                                                <input id="position2"
                                                       onclick="getManagingDirector()"
                                                       @if(isset(Auth::user()->contactInfo->position) && Auth::user()->contactInfo->position=='Managing Director') {{'checked'}} @endif  type="radio"
                                                       name="profile_id" value="Managing Director">
                                                <label class="" for="position2">
                                                    @foreach($managingDirectors as $managingDirector)
                                                        Managing Director ({{ $managingDirector->owner_title.' '.$managingDirector->owner_first_name }})
                                                    @endforeach
                                                </label>
                                            </div>


                                            @foreach($shareHolders  as $key=>$shareHolder)
                                                <div class="inputGroup">
                                                    <input class='form-check-input' id="position{{ $shareHolder->id }}" onclick="getShareHolderId({{$shareHolder->id}})"  type='radio' name="profile_id" @if(Auth::user()->contactInfo->profile_id==$shareHolder->id) {{'checked'}} @endif  value="{{$shareHolder->id}}" id="">
                                                    <label class="form-check-label" for="position{{ $shareHolder->id }}">
                                                        Shareholder ({{ $shareHolder->owner_title.' '.$shareHolder->owner_first_name }})
                                                    </label>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="inputGroup">
                                                <input class="form-check-input check-managing-partner"
                                                       onclick="getManagingDirector()"
                                                       @if(isset(Auth::user()->contactInfo->position) && Auth::user()->contactInfo->position=='Managing Partner') {{'checked'}} @endif  type="radio"
                                                       name="profile_id" value="Managing Partner" id="position2">
                                                <label class="form-check-label" for="position2">
                                                    @foreach($managingPartners as $managingPartner)
                                                        Managing Partner ({{ $managingPartner->owner_title.' '.$managingPartner->owner_first_name }})
                                                    @endforeach
                                                </label>
                                            </div>
                                            @foreach($partners  as $partner)
                                                <div class="inputGroup">
                                                    <input class='form-check-input' onclick="getShareHolderId({{$partner->id}})" id="position{{ $partner->id }}"  type='radio' name="profile_id" @if(Auth::user()->contactInfo->profile_id==$partner->id) {{'checked'}} @endif  value="{{$partner->id}}" >
                                                    <label class="form-check-label" for="position{{ $partner->id }}">
                                                        Partner ({{ $partner->owner_title.' '.$partner->owner_first_name }})
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>

                            </div>
                            <hr>



                            {{--<div id="checkShare">--}}
                                {{--@if(isset(Auth::user()->contactInfo->position))--}}
                                    {{--<div id="shareholder" class="form-group row">--}}
                                        {{--<label for='staticEmail' class='col-sm-5 col-form-label'>Please Select one Share Holder Name :</label>--}}
                                        {{--<div class="col-sm-7" id="share">--}}

                                            {{--@foreach($shareHolders  as $shareHolder)--}}
                                                {{--<div class='col-sm-7'><div class='form-check mt-1'>--}}
                                                        {{--<input class='form-check-input' onclick="getShareHolderId({{$shareHolder->id}})"  type='radio' name="profile_id" @if(Auth::user()->contactInfo->profile_id==$shareHolder->id) {{'checked'}} @endif  value="{{$shareHolder->id}}" id="">--}}
                                                        {{--<label class='form-check-label' for='share_holder1'>{{$shareHolder->owner_first_name.' '.$shareHolder->owner_middle_name.' '.$shareHolder->owner_last_name}}</label>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--@endforeach--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--</div>--}}

                            {{--<div id="shareholder" class="form-group row">--}}
                                {{--<label for='staticEmail' class='col-sm-5 col-form-label'>Please Select one Share Holder Name :</label>--}}
                                {{--<div class="col-sm-7" id="share">--}}

                                {{--</div>--}}
                            {{--</div>--}}


                            <div id="conduct_info" class="hidden">
                                <div class="form-row">
                                    <div class="form-group col-md-2" >
                                        <label for="owner_title">Title (Mrs./Mr./Ms.)</label>
                                        <select id="owner_title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title">
                                            <option disabled selected>Select Your Title</option>
                                            <option value="Mrs." {{ old('title') == "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                            <option value="Mr." {{ old('title') == "Mr." ? 'selected' : '' }}>Mr.</option>
                                            <option value="Ms" {{ old('title') == "Ms" ? 'selected' : '' }}>Ms</option>
                                        </select>
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3" id="owner_first_name">
                                        <label for="owner_first_name">First Name</label>
                                        <input  type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                name="first_name" value="" placeholder="Enter your first name">
                                    </div>
                                    <div class="form-group col-md-3" id="owner_middle_name">
                                        <label for="middle_name">Middle Name</label>
                                        <input type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}"
                                               name="middle_name" value="{{old('middle_name')}}" placeholder="Enter your middle name">
                                        @if ($errors->has('middle_name'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3" id="owner_last_name">
                                        <label for="last_name">Last Name</label>
                                        <input  type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                name="last_name" value="{{old('last_name')}}" placeholder="Enter your last name">
                                        @if ($errors->has('last_name'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4" >
                                        <label for="organization">Position in the Organizatrion</label>
                                        <select id="organization" class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}"
                                                name="position">
                                            <option disabled selected>Choose a option</option>

                                        </select>
                                        @if ($errors->has('position'))
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('position') }}</strong>
                                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-md-4" id="shareholder_persent">
                                        <label for="shareholder">Shareholder (%)</label>
                                        <input type="number"
                                               class="form-control{{ $errors->has('shareholder') ? ' is-invalid' : '' }}"
                                               name="shareholder" value="{{old('shareholder')}}">
                                        @if ($errors->has('shareholder'))
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('shareholder') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4" id="date_of_birth">
                                        <label for="date">Date Of Birth</label>
                                        <input id="date" type="datetime-local"
                                               class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                               name="date_of_birth" value="{{old('date_of_birth')}}">
                                        @if ($errors->has('date_of_birth'))
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">Present Address</div>
                                    <div id="ownerapp"  class="card-body bg-white">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="division1" >Division</label>
                                                <select  id="present_division" name="present_division" class="form-control{{ $errors->has('present_division') ? ' is-invalid' : '' }} division1" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>

                                                    <option value=""></option>
                                                </select>
                                                @if ($errors->has('present_division'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_division') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="district1">Distirct</label>
                                                <select id="present_district" name="present_district"  class="form-control{{ $errors->has('present_district') ? ' is-invalid' : '' }}  district1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('present_district'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_district') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="upazila1">Upazila</label>
                                                <select id="present_upazila" name="present_upazila" class="form-control{{ $errors->has('present_upazila') ? ' is-invalid' : '' }}  upazila1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('present_upazila'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_upazila') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="post_office1">Post Office</label>
                                                <select id="present_postoffice"  name="present_postoffice"  class="form-control{{ $errors->has('present_postoffice') ? ' is-invalid' : '' }} suboffice1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('present_postoffice'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_postoffice') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3" id="present_postcode">
                                                <label for="post_code1">Post Code</label>
                                                <input id="post_code1" required="required" type="text" name="present_postcode" class="form-control{{ $errors->has('present_postcode') ? ' is-invalid' : '' }}" readonly>
                                                @if ($errors->has('present_postcode'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_postcode') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6" id="present_address">
                                                <textarea class="form-control" cols="" rows=""></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="card">
                                    <div class="card-header">Permanent Address</div>
                                    <div id="ownerapp"  class="card-body bg-white">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="division1" >Division</label>
                                                <select  id="permanent_division" name="permanent_division" class="form-control{{ $errors->has('permanent_division') ? ' is-invalid' : '' }} division1" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    <option value=""></option>
                                                </select>
                                                @if ($errors->has('permanent_division'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_division') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="district1">Distirct</label>
                                                <select id="permanent_district" name="permanent_district"  class="form-control{{ $errors->has('permanent_district') ? ' is-invalid' : '' }}  district1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('permanent_district'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_district') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="upazila1">Upazila</label>
                                                <select id="permanent_upazila" name="permanent_upazila" class="form-control{{ $errors->has('permanent_upazila') ? ' is-invalid' : '' }}  upazila1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('permanent_upazila'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_upazila') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="post_office1">Post Office</label>
                                                <select id="permanent_postoffice"  name="permanent_postoffice"  class="form-control{{ $errors->has('permanent_postoffice') ? ' is-invalid' : '' }} suboffice1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('permanent_postoffice'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_postoffice') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3" id="permanent_postcode">
                                                <label for="post_code1">Post Code</label>
                                                <input id="post_code1" required="required" type="text" name="permanent_postcode" class="form-control{{ $errors->has('permanent_postcode') ? ' is-invalid' : '' }}" readonly>
                                                @if ($errors->has('permanent_postcode'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_postcode') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6" id="permanent_address">
                                                <textarea class="form-control" cols="" rows=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <a href="{{ url('/partner-director-info/edit') }}" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb-3 pull-left">Back</a>
                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        if($(".check-managing-partner").attr('checked'))
        {
            $('#checkShare').addClass('hidden');

        }
        if($(".check-managing-director").attr('checked'))
        {
            $('#checkShare').addClass('hidden');
        }
        if($(".check-chairman").attr('checked'))
        {
            $('#checkShare').addClass('hidden');
        }




        function getPositionInfo(){
            var chairman=document.getElementById("position1").value;
            getPosition(chairman);


        }
        function getManagingDirector() {

            var managingDirector=document.getElementById("position2").value;
            getPosition(managingDirector);
        }
        function getShareHolder() {
            var shareHolder=document.getElementById("position3").value;
            getShareInfo(shareHolder);
        }


        function getPosition(compnayposition) {

            $.get('/position-info?position=' + compnayposition, function (data) {


                $('#conduct_info').removeClass('hidden');
                $('#shareholder').addClass('hidden');
                $('#owner_title').html('<option value="'+ data.owner_title +'">'+ data.owner_title +'</option>');
                $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ data.owner_first_name +'" readonly>');
                $('#owner_middle_name').html('<label>Middle Name</label> <input type="text" id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ data.owner_middle_name +'" readonly>');
                $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ data.owner_last_name +'" readonly>');
                $("#organization").html('<option value="'+data.position+'">'+data.position+'</option>');
                $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ data.shareholder +'" readonly>');
                $("#date_of_birth").html('<label>Date Of Birth</label> <input type="text" id="" name="date_of_birth"  class="form-control" value="'+ data.date_of_birth +'" readonly>');
                $("#present_division").html('<option value="'+data.permanent_division+'">'+data.permanent_division+'</option>');
                $("#present_district").html('<option value="'+data.present_district+'">'+data.present_district+'</option>');
                $("#present_upazila").html('<option value="'+data.present_upazila+'">'+data.present_upazila+'</option>');
                $("#present_postoffice").html('<option value="'+data.presentpost_office+'">'+data.presentpost_office+'</option>');
                $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ data.post_code2 +'" readonly>');
                $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+data.present_address+'</textarea>');
                $("#permanent_division").html('<option value="'+data.permanent_division+'">'+data.permanent_division+'</option>');
                $("#permanent_district").html('<option value="'+data.permanent_district+'">'+data.permanent_district+'</option>');
                $("#permanent_upazila").html('<option value="'+data.permanent_upazila+'">'+data.permanent_upazila+'</option>');
                $("#permanent_postoffice").html('<option value="'+data.permanentpost_office+'">'+data.permanentpost_office+'</option>');
                $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ data.post_code3 +'" readonly>');
                $("#permanent_address").html('<label>Address</label><textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+data.permanent_address+'</textarea>');
            });

        }

        function getShareHolderId(id) {
            var abc= $('input[type=radio][name=profile_id]:checked').attr('id');
            getSingleShareInfo(id);

        }
        function getSingleShareInfo(id) {
            $.get('/single-share-info?id=' + id, function (singleData) {
                $('#conduct_info').removeClass('hidden');
                /*$('#shareholder').addClass('hidden');*/
                $('#owner_title').html('<option value="'+ singleData.owner_title +'">'+ singleData.owner_title +'</option>');
                $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ singleData.owner_first_name +'" readonly>');
                $('#owner_middle_name').html('<label>Middle Name</label> <input type="text"  id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ singleData.owner_middle_name +'" readonly>');
                $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ singleData.owner_last_name +'" readonly>');
                $("#organization").html('<option value='+singleData.position+'>'+singleData.position+'</option>');
                $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ singleData.shareholder +'" readonly>');
                $("#date_of_birth").html('<label>Date Of Birth</label> <input type="text" id="" name="date_of_birth"  class="form-control" value="'+ singleData.date_of_birth +'" readonly>');
                $("#present_division").html('<option value="'+singleData.permanent_division+'">'+singleData.permanent_division+'</option>');
                $("#present_district").html('<option value="'+singleData.present_district+'">'+singleData.present_district+'</option>');
                $("#present_upazila").html('<option value="'+singleData.present_upazila+'">'+singleData.present_upazila+'</option>');
                $("#present_postoffice").html('<option value="'+singleData.presentpost_office+'">'+singleData.presentpost_office+'</option>');
                $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ singleData.post_code2 +'" readonly>');
                $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+singleData.present_address+'</textarea>');
                $("#permanent_division").html('<option value="'+singleData.permanent_division+'">'+singleData.permanent_division+'</option>');
                $("#permanent_district").html('<option value="'+singleData.permanent_district+'">'+singleData.permanent_district+'</option>');
                $("#permanent_upazila").html('<option value="'+singleData.permanent_upazila+'">'+singleData.permanent_upazila+'</option>');
                $("#permanent_postoffice").html('<option value="'+singleData.permanentpost_office+'">'+singleData.permanentpost_office+'</option>');
                $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ singleData.post_code3 +'" readonly>');
                $("#permanent_address").html('<label>Address</label><textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+singleData.permanent_address+'</textarea>');
            });
        }
        function getShareInfo(shareHolder) {

            $.get('/shareHolder?chairman=' + shareHolder, function (shareHolders) {
                if(shareHolders.length>1)
                {

                    $('#checkShare').removeClass('hidden');
                    $('#shareholder').removeClass('hidden');
                    $('#conduct_info').addClass('hidden');
                    $('#share').empty();
                    $.each(shareHolders, function (shareHolders, shareHolderObj) {


                        $('#share').append("<div class='col-sm-7'><div class='form-check mt-1'>\n" +
                            "                    <input class='form-check-input' onclick=\"getShareHolderId('"+shareHolderObj.id+"')\"  type='radio' name='profile_id' value='"+shareHolderObj.id+"' id='"+shareHolderObj.owner_last_name+"'>\n" +
                            "                    <label class='form-check-label' for='share_holder1'>"+shareHolderObj.owner_first_name +' '+shareHolderObj.owner_middle_name +' '+ shareHolderObj.owner_last_name+"</label>\n" +
                            "                    </div></div>");
                    })
                }
                else
                {
                    $('#conduct_info').removeClass('hidden');
                    $('#shareholder').addClass('hidden');
                    $('#owner_title').html('<option value="'+ shareHolders.owner_title +'">'+ shareHolders.owner_title +'</option>');
                    $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ shareHolders.owner_first_name +'" readonly>');
                    $('#owner_middle_name').html('<label>Middle Name</label> <input type="text"  id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ shareHolders.owner_middle_name +'" readonly>');
                    $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ shareHolders.owner_last_name +'" readonly>');
                    $("#organization").html('<option value='+shareHolders.position+'>'+shareHolders.position+'</option>');
                    $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ shareHolders.shareholder +'" readonly>');
                    $("#date_of_birth").html('<label>Date Of Birth</label> <input type="text" id="" name="date_of_birth"  class="form-control" value="'+ shareHolders.date_of_birth +'" readonly>')
                    $("#present_division").html('<option value="'+shareHolders.permanent_division+'">'+shareHolders.permanent_division+'</option>');
                    $("#present_district").html('<option value="'+shareHolders.present_upazila+'">'+shareHolders.present_district+'</option>');
                    $("#present_upazila").html('<option value="'+shareHolders.present_upazila+'">'+shareHolders.present_upazila+'</option>');
                    $("#present_postoffice").html('<option value="'+shareHolders.presentpost_office+'">'+shareHolders.presentpost_office+'</option>');
                    $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ shareHolders.post_code2 +'" readonly>')
                    $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+shareHolders.present_address+'</textarea>');
                    $("#permanent_division").html('<option value="'+shareHolders.permanent_division+'">'+shareHolders.permanent_division+'</option>');
                    $("#permanent_district").html('<option value="'+shareHolders.permanent_district+'">'+shareHolders.permanent_district+'</option>');
                    $("#permanent_upazila").html('<option value="'+shareHolders.permanent_upazila+'">'+shareHolders.permanent_upazila+'</option>');
                    $("#permanent_postoffice").html('<option value="'+shareHolders.permanentpost_office+'">'+shareHolders.permanentpost_office+'</option>');
                    $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ shareHolders.post_code3 +'" readonly>');
                    $("#permanent_address").html('<label>Address</label> <textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+shareHolders.permanent_address+'</textarea>');
                }
            });
        }
    </script>
@endsection
