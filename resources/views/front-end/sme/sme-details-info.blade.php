@extends('front-end.frontend-layout')
@section('content')
    <div class="be-content container">
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-table mt-3">

                        <div class="card-header">
                            SME All Information
                            <div class="tools dropdown"><a href=""><span class="icon mdi mdi-edit"></span></a>
                            </div>
                        </div>

                        <div class="card-body">
                            <table class="table table-striped table-hover table-bordered">
                                @if($loanview)
                                <thead>
                                <tr>
                                    <th>Email Address</th>
                                    <td>{{$loanview->email_address}}</td>
                                </tr>
                                <tr>
                                    <th>Enterprize Name</th>
                                    <td>{{$loanview->enterprize}}</td>
                                </tr>
                                <tr>
                                    <th>Firm Name</th>
                                    <td>{{$loanview->firm}}</td>
                                </tr>
                                <tr>
                                    <th>Business Started</th>
                                    <td>{{$loanview->business_startd}}</td>
                                </tr>
                                <tr>
                                    <th>Business Address</th>
                                    <td>{{$loanview->business_address}}</td>
                                </tr>
                                <tr>
                                    <th>Business Location</th>
                                    <td>{{$loanview->business_located}}</td>
                                </tr>
                                <tr>
                                    <th>Business Nature</th>
                                    <td>{{$loanview->nature}}</td>
                                </tr>
                                <tr>
                                    <th>Business Employee</th>
                                    <td>{{$loanview->business_employee}}</td>
                                </tr>
                                <tr>
                                    <th>Business Asset</th>
                                    <td>{{$loanview->business_asset}}</td>
                                </tr>
                                <tr>
                                    <th>Business Owner Name</th>
                                    <td>{{$loanview->owner_name}}</td>
                                </tr>
                                <tr>
                                    <th>Marital Status</th>
                                    <td>{{$loanview->marital_status}}</td>
                                </tr>
                                </thead>
                                    @endif
                            </table>


                  {{--          <a href="{{url('/smeInvoice/'.Auth::user()->id)}}" class="btn btn-primary">Pdf Download</a>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
