@extends('front-end.sme.layout.master-dashboard')
@section('main-content')



    <div class="container-fluid">
        <div>
            <div class="col-12">
                <div class="card wizard-content">
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="col-md-6 mb-4"><h4 class="card-title mb-4">SME Information</h4><h6
                                    class="card-subtitle text-danger"><strong>Required *</strong></h6></div>
                        </div>
                        <form id="submit-form" name="form_name" action="{{url('/question-store')}}"
                              method="post">
                            {{csrf_field()}}
                            {{--<input type="hidden" name="user_id" value="{{Auth::user()->id }}">--}}
                            <div class="wizards">
                                <div class="progressbar">
                                    <div class="progress-line" data-now-value="6.45" data-number-of-steps="10"
                                         style="width: 6.45%;"></div> <!-- 19.66% --></div>
                                <div class="col-md-12 row">
                                    <div class="form-wizard active col-md-1 ml-5 mr-3">
                                        <div class="wizard-icon"><i class="">1</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">2</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">3</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-3">
                                        <div class="wizard-icon"><i class="">4</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-3">
                                        <div class="wizard-icon"><i class="">5</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">6</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">7</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">8</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">9</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">10</i></div>
                                    </div>
                                </div>
                            </div> @foreach($questionCategories as $key=>$questionCategory)
                                <fieldset><h5 class="mt-5 ml-4">{{$questionCategory->question_category}}</h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">

                                                    @foreach($questions as $question)
                                                        @if($question->question_category_id==$questionCategory->id)
                                                            @if($question->field_type=='text')
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4">
                                                                        {{$question->label_en }}
                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <input id="{{$question->column_name}}" type="text" name="{{$question->column_name}}"
                                                                               placeholder="{{$question->placeholder}}"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='number')
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4">{{ $question->label_en }}</label>
                                                                    <div class="col-md-8">
                                                                        <input type="number" id="number"
                                                                               name="{{$question->column_name}}"
                                                                               value=""
                                                                               placeholder="{{$question->placeholder}}"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='select')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4">{{ $question->label_en }}</label>
                                                                    <div class="col-md-8"><select
                                                                            name="{{$question->column_name}}"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> @foreach($options as $option)                                                                                @if($question->id == $option->question_id)
                                                                                <option value="{{$option->title}}">{{$option->title}}</option>                                                                                @endif                                                                            @endforeach
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='calender')
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4">{{ $question->label_en }}</label>
                                                                    <div class="col-md-8 row">
                                                                        <div class="col-md-4">
                                                                            <select id="dobmonth" name="{{$question->column_name}}" class="form-control">
                                                                                <option value="" disabled>Choose option</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <select id="dobyear" name="{{$question->column_name}}" class="form-control">
                                                                                <option value="" disabled>Choose option</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr>
                                                            @elseif($question->field_type=='radio')
                                                                <div class="form-group row prentid">
                                                                    <label class="control-label text-right col-md-4"> {{ $question->label_en }}</label>
                                                                    <div class="col-md-8 row ml-0">
                                                                        @foreach($options as $option)
                                                                            @if($question->id == $option->question_id)
                                                                                @if($option->question->field_type =='radio')
                                                                                    <div class="m-b-10 ml-1 row">
                                                                                        <label class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="{{$question->column_name}}"
                                                                                                   {{--value="{{$option->title}}"--}}
                                                                                                   type="radio"
                                                                                                   class="custom-control-input">
                                                                                            <span class="custom-control-label"> {{ $option->title }}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    </div>

                                                                    <div class="offset-4">
                                                                        <span class="radioError text-danger ml-3 mt-0"></span>
                                                                    </div>


                                                                    @foreach($dependents as $dependent)
                                                                        @if($question->id == $dependent->question_id)
                                                                            <hr>
                                                                            <div id="open{{$dependent->question_id}}" class="{{$dependent->question_id}} form-group row" style="display: none">
                                                                                <label for="{{$dependent->placeholder}}" class="control-label text-right col-sm-4">
                                                                                    {{$dependent->label_en }}
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <input  id="{{$dependent->question_id}}"  type="hidden" name="{{$dependent->column_name}}"
                                                                                            placeholder="{{$dependent->placeholder}}" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach

                                                                </div>

                                                                <hr>
                                                            @elseif($question->field_type=='checkbox')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> {{ $question->label_en }}</label>
                                                                    <div class="col-md-8">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            @foreach($options as $option)
                                                                                @if($question->id == $option->question_id)
                                                                                    @if($option->question->field_type =='checkbox')
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="{{$question->column_name.'[]'}}"
                                                                                                   value="{{$option->title}}" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label">{{ $option->title }}</span>
                                                                                        </label>
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='textarea')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4">{{ $question->label_en }}</label>
                                                                    <div class="col-md-8"><textarea rows="3"
                                                                                                    name="{{$question->column_name}}"
                                                                                                    placeholder="{{$question->placeholder}}"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='multi-Select')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4">{{ $question->label_en }}</label>
                                                                    <div class="col-md-8"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            @foreach($options as $option)
                                                                                @if($question->id == $option->question_id)
                                                                                    <option value="{{$question->column_name.'[]'}}">{{$option->title}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            @elseif($question->field_type=='amount')

                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> {{ $question->label_en }}</label><div class="col-md-8">
                                                                        <input type="text" name="{{$question->column_name}}" value=""
                                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace();"
                                                                           placeholder="{{$question->placeholder}}" class="amount form-control"></div>
                                                                </div>
                                                                <hr>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <button type="button" class="btn btn-next">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            @endforeach
                            @foreach($catagoryLastStep as $catagorylast)
                                <fieldset><h5 class="mt-5 ml-4">{{$catagorylast->question_category}}</h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">
                                                    @foreach($questions as $question)
                                                        @if($question->question_category_id==$catagorylast->id)
                                                            @if($question->field_type=='text')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> {{ $question->label_en }}</label>
                                                                    <div class="col-md-9"><input type="text"
                                                                                                 name="{{$question->column_name}}"
                                                                                                 value=""
                                                                                                 placeholder="{{$question->placeholder}}"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            @elseif($question->field_type=='number')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3">{{ $question->label_en }}</label>
                                                                    <div class="col-md-9"><input type="number"
                                                                                                 name="{{$question->column_name}}"
                                                                                                 value=""
                                                                                                 placeholder="{{$question->placeholder}}"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            @elseif($question->field_type=='select')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3">{{ $question->label_en }}</label>
                                                                    <div class="col-md-9"><select
                                                                            name="{{$question->column_name}}"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> @foreach($options as $option)                                                                                @if($question->id == $option->question_id)
                                                                                <option value="{{$option->title}}">{{$option->title}}</option>                                                                                @endif                                                                            @endforeach
                                                                        </select></div>
                                                                </div>
                                                            @elseif($question->field_type=='calender')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3">{{ $question->label_en }}</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" name="{{$question->column_name}}"
                                                                               value="" class="form-control singledate" placeholder="dd/mm/yyyy"></div>
                                                                </div>
                                                            @elseif($question->field_type=='radio')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> {{ $question->label_en }}</label>
                                                                    <div class="col-md-9 row ml-0">
                                                                        @foreach($options as $option)
                                                                            @if($question->id == $option->question_id)
                                                                                @if($option->question->field_type =='radio')
                                                                                    <div class="m-b-10 ml-1 row"><label
                                                                                            class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="{{$question->column_name}}"
                                                                                                   value="{{$option->title}}" type="radio"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"> {{ $option->title }}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            @elseif($question->field_type=='checkbox')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> {{ $question->label_en }}</label>
                                                                    <div class="col-md-9">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            @foreach($options as $option)
                                                                                @if($question->id == $option->question_id)
                                                                                    @if($option->question->field_type =='checkbox')
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="{{$question->column_name.'[]'}}"
                                                                                                   value="{{$option->title}}" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label">{{ $option->title }}</span>
                                                                                        </label>
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @elseif($question->field_type=='textarea')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3">{{ $question->label_en }}</label>
                                                                    <div class="col-md-9"><textarea rows="3"
                                                                                                    name="{{$question->column_name}}"
                                                                                                    placeholder="{{$question->placeholder}}"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                            @elseif($question->field_type=='multi-Select')
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3">{{ $question->label_en }}</label>
                                                                    <div class="col-md-9"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            @foreach($options as $option)
                                                                                @if($question->id == $option->question_id)
                                                                                    <option value="{{$question->column_name.'[]'}}">{{$option->title}}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select></div>
                                                                </div>
                                                            @elseif($question->field_type=='amount')
                                                                <div class="form-group row"><label
                                                                            class="control-label text-right col-md-4"> {{ $question->label_en }}</label><div class="col-md-8">
                                                                        <input type="text" name="{{$question->column_name}}" value=""
                                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace();"
                                                                               placeholder="{{$question->placeholder}}" class="amount form-control"></div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <input onclick="submit_by_name()" type="submit"
                                                           class="btn btn-success" value="Complete"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('input.amount').keyup(function(event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function(index, value) {
                return value
                    // .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
            });
        });
    </script>

    <!-- Material unchecked -->

    <script>

        function optionQustionId(id){

            @foreach($dependents as $dependent)
            if(id[0].id == {{$dependent->question_id}})
            {
                if(id[0].value == "No")
                {

                    {{--$("#open{{$dependent->question_id}}").show();--}}
                    $('#open{{$dependent->question_id}} input[type=hidden]').attr('type','text');

                    $(".{{$dependent->question_id}}").show();

                }

            }
            if(id[0].id == {{$dependent->question_id}})
            {

                if(id[0].value=="Yes")
                {
                    console.log(id[0]);
                    {{--$("#open{{$dependent->question_id}}").hide();--}}
                    $('#open{{$dependent->question_id}} input[type=text]').attr('type','hidden');
                    $(".{{$dependent->question_id}}").hide();
                }
            }

            if(id[0].id == {{$dependent->question_id}})
            {

                if(id[0].value=="Maybe")
                {
                    {{--$("#open{{$dependent->question_id}}").hide();--}}
                    $('#open{{$dependent->question_id}} input[type=hidden]').attr('type','text');
                    $(".{{$dependent->question_id}}").show();
                }

            }

            @endforeach
        }
    </script>

    <script>
        $(document).ready(function () {
            $("#submit-form").submit(function (e) {
                e.preventDefault();
                var that = $(this);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: that.attr('action'),
                    data: that.serialize(),
                    cache: false,
                    success: function (data) {
                        console.log(data)
                    }/*,                     error: function (xhr, status, error) {                        alert("An AJAX error occured: " + status + "\nError: " + error);                     }*/
                });
            });
        });
    </script>

    <script>
        function submit_by_name() {
            var x = document.getElementsByName('form_name');
            x[0].submit();       }
    </script>

    <script>
        $(document).ready(function() {
            $.dobPicker({
                // daySelector: '#dobday', /* Required */
                monthSelector: '#dobmonth', /* Required */
                yearSelector: '#dobyear', /* Required */
                // dayDefault: 'Day', /* Optional */
                monthDefault: 'Month', /* Optional */
                yearRange: "-120:+0",
                yearDefault: 'Year', /* Optional */
                // minimumAge: 12, /* Optional */
                // maximumAge: 80 /* Optional */
            });
        });
    </script>



@endsection
