@extends('front-end.sme.layout.master-dashboard')

@section('main-content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">

                    <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="mb10 mt20  section-title text-center  ">
                            <!-- section title start-->
                            <h2>SME Basic Information</h2>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <form method="post" action="{{url('/profile-update/'.$SmeProfileEdit->id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{method_field('PUT')}}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="enterprise_name">Enterprize Name (As Per Trade License)</label>
                                    <input id="enterprise_name" type="text"
                                           class="form-control{{ $errors->has('enterprise_name') ? ' is-invalid' : '' }}"
                                           name="enterprise_name" value="{{$SmeProfileEdit->enterprise_name}}" placeholder="Enter Your Enterprise Name" required autofocus>
                                    @if ($errors->has('enterprise_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('enterprise_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="organization">Organizatrion Structure</label>
                                    <select class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}" name="organization">
                                        <option disabled selected>Choose Your Organizatrion</option>
                                        <option value="Sole Proprietorship" @if($SmeProfileEdit->organization=='Sole Proprietorship') {{'selected'}} @endif>Sole Proprietorship).</option>
                                        <option value="Partnership" @if($SmeProfileEdit->organization=='Partnership') {{'selected'}} @endif>Partnership</option>
                                        <option value="PLC" @if($SmeProfileEdit->organization=='PLC') {{'selected'}} @endif>PLC</option>
                                    </select>
                                    @if ($errors->has('organization'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('organization') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="title">Title</label>
                                    <select class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title">
                                        <option disabled selected>Choose Your Title</option>
                                        <option value="Mrs." @if($SmeProfileEdit->title=='Mrs.') {{'selected'}} @endif>Mrs.</option>
                                        <option value="Mr." @if($SmeProfileEdit->title=='Mr.') {{'selected'}} @endif>Mr.</option>
                                        <option value="Ms" @if($SmeProfileEdit->title=='Ms.') {{'selected'}} @endif>Ms</option>
                                    </select>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="first_name">First Name</label>
                                    <input id="first_name" type="text"
                                           class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                           name="first_name" value="{{$SmeProfileEdit->first_name}}" required autofocus>
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="middle_name">Middle Name</label>
                                    <input id="middle_name" type="text"
                                           class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}"
                                           name="middle_name" value="{{$SmeProfileEdit->middle_name}}" required autofocus>
                                    @if ($errors->has('middle_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('middle_name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="last_name">Last Name</label>
                                    <input id="last_name" type="text"
                                           class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                           name="last_name" value="{{$SmeProfileEdit->last_name}}" required autofocus>
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="sector">Sector</label>
                                    <div class="col-md-10">
                                        @foreach($sectors as $sector)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" name="sector[]" @if(in_array($sector,$sectorExplode)) {{'checked'}} @endif value="{{$sector}}" type="checkbox" id="gridCheck1">
                                                <label class="form-check-label" for="gridCheck1" id="sector">
                                                    {{$sector}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="industry">Industry</label>
                                    <div class="col-md-10">
                                        @foreach($industries as $industry)
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" @if(in_array($industry,$industriExplode)) {{'checked'}} @endif  name="industry[]" value="{{$industry}}" type="checkbox" id="gridCheck1">
                                                <label class="form-check-label" for="gridCheck1" id="industry">
                                                    {{$industry}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>



                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="mobile_number">Mobile Number</label>
                                    <input type="number" value="{{$SmeProfileEdit->mobile_number}}" name="mobile_number" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" id="mobile" required>
                                    @if ($errors->has('mobile_number'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="date_of_birth">Date Of Birth</label>
                                    <input id="date_of_birth" type="date"
                                           class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                           name="date_of_birth" value="{{$SmeProfileEdit->date_of_birth}}" required autofocus>
                                    @if ($errors->has('date_of_birth'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('date_of_birth') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>


                            <div class="form-row">



                                <div class="form-group col-md-6">
                                    <label for="picture">Picture</label>
                                    <input id="picture" type="file"
                                           class="form-control{{ $errors->has('picture') ? ' is-invalid' : '' }}"
                                           name="picture" value="">
                                    @if ($errors->has('picture'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('picture') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <img src="{{asset('ui/back-end/sme/'.$SmeProfileEdit->picture)}}" width="100px" height="80px">
                                </div>
                            </div>

                            <label>Business Address</label>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Address Line 1</label>
                                    <textarea cols="" rows="3" placeholder="Example : " class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '' }}" name="address_1" required>{{$SmeProfileEdit->address_1}}</textarea>
                                    @if ($errors->has('address_1'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address_1') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label> Address Line 2</label>
                                    <textarea cols="" rows="3" placeholder="Example : "  class="form-control{{ $errors->has('address_2') ? ' is-invalid' : '' }}" name="address_2" required>{{$SmeProfileEdit->address_2}}</textarea>
                                    @if ($errors->has('address_2'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address_2') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label>Division</label>
                                    <select  required="required" name="division" class="form-control division" >
                                        <option value="0" disabled="true" selected="true">-Select division-</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id}}" @if($division->title==$SmeProfileEdit->division) {{'selected'}} @endif>{{ $division->title }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group col-md-3">
                                    <label>Distirct</label>
                                    <select required="required" name="district"  class="form-control district" >
                                        <option value="0" disabled="true" selected="true">{{$SmeProfileEdit->district}}</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-2">
                                    <label>Upazila</label>
                                    <select required="required" name="upazila" class="form-control  upazila">
                                        <option value="0" disabled="true" selected="true">{{$SmeProfileEdit->upazila}}</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-2">
                                    <label>Post Office</label>
                                    <select required="required" name="post_office"  class="form-control suboffice">
                                        <option value="0" disabled="true" selected="true">{{$SmeProfileEdit->post_office}}</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-2 postcode">
                                    <label>Post Code</label>
                                    <input required="required" value="{{$SmeProfileEdit->post_code}}" type="text" name="post_code" disabled class="form-control">
                                </div>
                            </div>



                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection