@extends('front-end.partials.master-layout')@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Director/Partners Details</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ url('/sme-personal-info/store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card">
                                <div class="card-header">Company Information Details</div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-6"><label for="enterprise_name"
                                                                                class=" text-right control-label col-form-label">
                                                Trade License:</label> <input id="enterprise_name" type="text"
                                                                              class="form-control{{ $errors->has('enterprise_name') ? ' is-invalid' : '' }}"
                                                                              name="trade_license"
                                                                              value="{{old('enterprise_name')}}"
                                                                              placeholder="Enter Your Enterprise Name"> @if ($errors->has('enterprise_name'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('enterprise_name') }}</strong>                                            </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="datepicker-autoclose" class=" text-right control-label col-form-label">Expire Date: </label>
                                            <input type="date" class="form-control{{ $errors->has('enterprise_name') ? ' is-invalid' : '' }}"
                                                                             value="{{old('expire_date')}}" name="expire_date" id="datepicker-autoclose"
                                                                             placeholder="mm/dd/yyyy"> @if ($errors->has('expire_date'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('expire_date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-12 mb-4"><label>Add Attach file</label>
                                            <input type="file" id="input-file-now" name="trade_license_pic" class="dropify" data-height="150"/>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-3"><label for="e_tin" class=" text-right control-label col-form-label">E-TIN</label>
                                            <input id="e_tin" type="text" class="form-control{{ $errors->has('e_tin') ? ' is-invalid' : '' }}"
                                                   name="e_tin" value="{{old('e_tin')}}"
                                                   placeholder="Enter Your Enterprise Name">
                                            <label>Add Attach file</label>
                                            <input type="file" id="input-file-now" name="e_tin_pic" class="dropify"
                                                   data-height="150"/>
                                            @if ($errors->has('e_tin'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('e_tin') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="bin" class=" text-right control-label col-form-label">BIN</label>
                                            <input id="bin" type="text" class="form-control{{ $errors->has('bin') ? ' is-invalid' : '' }}" name="bin" value="{{old('bin')}}"
                                                   placeholder="Enter Your Enterprise Name"> <label>Add Attach
                                                file</label> <input type="file" id="input-file-now" name="bin_pic"
                                                                    class="dropify"
                                                                    data-height="150"/> @if ($errors->has('bin'))
                                                <span
                                                    class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('bin') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3"><label for="certificate_incorporation"
                                                                                class=" text-right control-label col-form-label">
                                                Certificate of Incorporation No:</label> <input
                                                    id="certificate_incorporation" type="text"
                                                    class="form-control{{ $errors->has('certificate_incorporation') ? ' is-invalid' : '' }}"
                                                    name="certificate_incorporation"
                                                    value="{{old('certificate_incorporation')}}"
                                                    placeholder="Enter Your Enterprise Name"> <label>Add Attach
                                                file</label> <input type="file" id="input-file-now"
                                                                    name="certificate_incorporation_pic" class="dropify"
                                                                    data-height="150"/> @if ($errors->has('certificate_incorporation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('certificate_incorporation') }}</strong>
                                                </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-3"><label for="partnership_agreement"
                                                                                class=" text-right control-label col-form-label">Partnership
                                                Agreement Reg. No.</label>
                                            <input id="partnership_agreement" type="text"
                                                   class="form-control{{ $errors->has('partnership_agreement') ? ' is-invalid' : '' }}"
                                                   name="partnership_agreement"
                                                   value="{{$profile->partnership_agreement}}"
                                                   placeholder="Enter Your Enterprise Name">
                                            <!--upload file-->
                                            <label>Add Attach file</label>
                                            <input type="file"
                                                   id="input-file-now"
                                                   name="partnership_agreement_pic"
                                                   class="dropify"
                                                   data-height="150"/> @if ($errors->has('partnership_agreement'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('partnership_agreement') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4"><label for="export_import_license"
                                                                                class=" text-right control-label col-form-label">
                                                Export/Import License:</label> <input id="export_import_license" type="text"
                                                                                      class="form-control{{ $errors->has('export_import_license') ? ' is-invalid' : '' }}"
                                                                                      name="export_import_license"
                                                                                      value="{{old('export_import_license')}}"
                                                                                      placeholder="Enter Your Enterprise Name">
                                            <label>Add Attach file</label> <input type="file" id="input-file-now"
                                                                                  name="export_import_license_pic"
                                                                                  class="dropify"
                                                                                  data-height="150"/> @if ($errors->has('export_import_license'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('export_import_license') }}</strong>
                                                </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-4"><label for="fire_license"
                                                                                class=" text-right control-label col-form-label">
                                                Fire License:</label> <input id="fire_license" type="text"
                                                                             class="form-control{{ $errors->has('fire_license') ? ' is-invalid' : '' }}"
                                                                             name="fire_license"
                                                                             value="{{old('fire_license')}}"
                                                                             placeholder="Enter Your Enterprise Name">
                                            <label>Add Attach file</label> <input type="file" id="input-file-now"
                                                                                  name="fire_license_pic"
                                                                                  class="dropify"
                                                                                  data-height="150"/> @if ($errors->has('fire_license'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('fire_license') }}</strong>                                            </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-4"><label for="reg_certificate"
                                                                                class=" text-right control-label col-form-label">Registration
                                                Certificate</label> <input id="reg_certificate" type="text"
                                                                           class="form-control{{ $errors->has('reg_certificate') ? ' is-invalid' : '' }}"
                                                                           name="reg_certificate"
                                                                           value="{{old('reg_certificate')}}"
                                                                           placeholder="Enter Your Enterprise Name">
                                            <label>Add Attach file</label> <input type="file" id="input-file-now"
                                                                                  name="reg_certificate_pic"
                                                                                  class="dropify"
                                                                                  data-height="150"/> @if ($errors->has('reg_certificate'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('reg_certificate') }}</strong>                                            </span>                                            @endif
                                        </div>
                                    </div>
                                    <label class="text-right control-label col-form-label">Bank Details</label>
                                    <hr class="bg-dark-blue mt-1">
                                    <div class="form-group row"><label for="inputEmail3"
                                                                       class="col-sm-3 text-right control-label col-form-label">Account
                                            Title*</label>
                                        <div class="col-sm-9"><input type="text" class="form-control"
                                                                     name="company_account_title" id="inputEmail3"
                                                                     placeholder="Enter your account title"></div>
                                    </div>
                                    <div class="form-group row"><label for="inputEmail3"
                                                                       class="col-sm-3 text-right control-label col-form-label">Account
                                            No*</label>
                                        <div class="col-sm-9"><input type="number" class="form-control"
                                                                     name="company_account_no" id="inputEmail3"
                                                                     placeholder="Enter your account no"></div>
                                    </div>
                                    <div class="form-group row"><label for="inputEmail3"
                                                                       class="col-sm-3 text-right control-label col-form-label">Bank*</label>
                                        <div class="col-sm-9"><select id="organization"
                                                                      class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }} bank"
                                                                      name="company_bank" onchange="showHide()">
                                                <option disabled selected>Choose Bank</option>
                                                <option>DDL</option> @foreach($banks as $bank)
                                                    <option value="{{ $bank->bank_name }}">{{ $bank->bank_name }}</option>                                                @endforeach
                                            </select></div>
                                    </div>
                                    <div class="form-group row"><label for="inputPassword3"
                                                                       class="col-sm-3 text-right control-label col-form-label">Branch*</label>
                                        <div class="col-sm-9"><select id="organization"
                                                                      class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }} bank"
                                                                      name="company_branch" onchange="showHide()">
                                                <option disabled selected>Choose Branch</option>
                                                <option>Dhaka branch</option>
                                            </select></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">Owners Background</div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-3"><label
                                                    class=" text-right control-label col-form-label"
                                                    for="title">Title</label> <select id="title"
                                                                                      class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                                                      disabled>
                                                <option disabled selected>Choose Your Title</option>
                                                <option value="Mrs." {{ Auth::user()->title == "Mrs." ? 'selected' : '' }}>
                                                    Mrs.
                                                </option>
                                                <option value="Mr." {{ Auth::user()->title == "Mr." ? 'selected' : '' }}>
                                                    Mr.
                                                </option>
                                                <option value="Ms" {{ Auth::user()->title == "Ms" ? 'selected' : '' }}>
                                                    Ms
                                                </option>
                                            </select></div>
                                        <div class="form-group col-md-3"><label for="first_name"
                                                                                class=" text-right control-label col-form-label">
                                                First Name</label> <input id="first_name" type="text"
                                                                          class="form-control"
                                                                          value="{{Auth::user()->first_name }}"
                                                                          disabled></div>
                                        <div class="form-group col-md-3"><label for="middle_name"
                                                                                class=" text-right control-label col-form-label">Middle
                                                Name</label> <input id="middle_name" type="text" class="form-control"
                                                                    value="{{Auth::user()->middle_name}}" disabled>
                                        </div>
                                        <div class="form-group col-md-3"><label for="last_name"
                                                                                class=" text-right control-label col-form-label">Last
                                                Name</label> <input id="last_name" type="text" class="form-control"
                                                                    value="{{Auth::user()->last_name}}" disabled></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12"><label for="enterprise_name"
                                                                                 class=" text-right control-label col-form-label">
                                                NID No:</label> <input id="enterprise_name" type="text"
                                                                       class="form-control"
                                                                       value="{{ $profile->owner_nid }}"
                                                                       placeholder="Enter Your Enterprise Name"
                                                                       disabled>
                                            <div class="row">
                                                <div class="col-sm-6"><label>Attach NID Front Side</label> <input
                                                            type="file" id="input-file-now" name="nid_front_pic"
                                                            class="dropify" data-height="150"/></div>
                                                <div class="col-sm-6"><label>Attach NID Back Side</label> <input
                                                            type="file" id="input-file-now" name="nid_back_pic"
                                                            class="dropify" data-height="150"/></div>
                                            </div> @if ($errors->has('enterprise_name'))<span class="invalid-feedback"
                                                                                              role="alert">                                                <strong>{{ $errors->first('enterprise_name') }}</strong>                                            </span>                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4"><label for="owner_e_tin"
                                                                                class=" text-right control-label col-form-label">
                                                E-TIN</label> <input id="owner_e_tin" type="text"
                                                                     class="form-control{{ $errors->has('owner_e_tin') ? ' is-invalid' : '' }}"
                                                                     name="owner_e_tin"
                                                                     value="{{ $profile->owner_e_tin }}"
                                                                     placeholder="Enter Your Enterprise Name"> <label>Add
                                                Attach file</label> <input type="file" id="input-file-now"
                                                                           name="owner_etin_pic" class="dropify"
                                                                           data-height="150"/> @if ($errors->has('owner_e_tin'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('owner_e_tin') }}</strong>                                            </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-4"><label for="owner_passport"
                                                                                class=" text-right control-label col-form-label">
                                                Passport No</label> <input id="owner_passport" type="text"
                                                                           class="form-control{{ $errors->has('owner_passport') ? ' is-invalid' : '' }}"
                                                                           name="owner_passport"
                                                                           value="{{ $profile->owner_passport }}"
                                                                           placeholder="Enter Your Enterprise Name">
                                            <label>Add Attach file</label> <input type="file" id="input-file-now"
                                                                                  name="owner_passport_pic"
                                                                                  class="dropify"
                                                                                  data-height="150"/> @if ($errors->has('owner_passport'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('owner_passport') }}</strong>                                            </span>                                            @endif
                                        </div>
                                        <div class="form-group col-md-4"><label for="owner_driving_license"
                                                                                class=" text-right control-label col-form-label">Driving
                                                License</label> <input id="owner_driving_license" type="text"
                                                                       class="form-control{{ $errors->has('owner_driving_license') ? ' is-invalid' : '' }}"
                                                                       name="owner_driving_license"
                                                                       value="{{ $profile->owner_driving_license }}"
                                                                       placeholder="Enter Your Enterprise Name"> <label>Add
                                                Attach file</label> <input type="file" name="owner_driving_license_pic"
                                                                           id="input-file-now" class="dropify"
                                                                           data-height="150"/> @if ($errors->has('owner_driving_license'))
                                                <span class="invalid-feedback" role="alert">                                                <strong>{{ $errors->first('owner_driving_license') }}</strong>                                            </span>                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="text-right control-label col-form-label">Bank
                                            Details</label>
                                        <hr class="bg-dark-blue mt-1">
                                        <div class="form-group row"><label for="inputEmail3"
                                                                           class="col-sm-3 text-right control-label col-form-label">Account
                                                Title*</label>
                                            <div class="col-sm-9"><input type="text" class="form-control"
                                                                         name="owner_account_title"
                                                                         value="{{ $profile->owner_account_title }}"
                                                                         id="inputEmail3"
                                                                         placeholder="Enter your account title"></div>
                                        </div>
                                        <div class="form-group row"><label for="inputEmail3"
                                                                           class="col-sm-3 text-right control-label col-form-label">Account
                                                No*</label>
                                            <div class="col-sm-9"><input type="number" name="owner_account_no"
                                                                         class="form-control"
                                                                         value="{{ $profile->owner_account_no }}"
                                                                         id="inputEmail3"
                                                                         placeholder="Enter your account no"></div>
                                        </div>
                                        <div class="form-group row"><label for="inputEmail3"
                                                                           class="col-sm-3 text-right control-label col-form-label">Bank*</label>
                                            <div class="col-sm-9"><select id="organization" name="owner_bank"
                                                                          class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}">
                                                    <option disabled selected>Choose Your Bank</option>
                                                    <option>UCBL</option> @foreach($banks as $bank)
                                                        <option value="{{ $bank->bank_name }}">{{ $bank->bank_name }}</option>                                                    @endforeach
                                                </select></div>
                                        </div>
                                        <div class="form-group row"><label for="inputPassword3"
                                                                           class="col-sm-3 text-right control-label col-form-label">Branch*</label>
                                            <div class="col-sm-9"><select id="organization" name="owner_branch"
                                                                          class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}"
                                                                          onchange="showHide()">
                                                    <option disabled selected>Choose Your Branch</option>
                                                    <option value="Sole Proprietorship" {{ old('organization') == "Sole Proprietorship" ? 'selected' : '' }}>
                                                        Dhaka branch
                                                    </option>
                                                </select></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(Auth::User()->profile->organization == "Partnership" || Auth::User()->profile->organization == "Public Limited" || Auth::User()->profile->organization == "Private Limited")
                                @for($i=1; $i <=Auth::User()->profile->total_director - 1; $i++ )
                                    <div class="card">
                                    <div class="card-header">Owners Background Details</div>
                                    <div class="card-body bg-white">
                                        @if( Auth::User()->profile->organization == "Public Limited" || Auth::User()->profile->organization == "Private Limited")
                                            <div class="form-row"><label class="bg-boxshadow pl-3 pr-3 pt-2 pb-2">Director - {{$i}}</label></div>
                                            <hr>
                                        @elseif(Auth::User()->profile->organization == "Partnership")
                                            <div class="form-row"><label class="">Partners - {{$i}} </label></div>
                                            <hr>
                                        @endif
                                        <div class="form-row">
                                            <div class="form-group col-md-3"><label
                                                        class="text-right control-label col-form-label" for="title">Title</label>
                                                <select id="title"
                                                        class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                        name="title[]">
                                                    <option disabled selected>Choose Your Title</option>
                                                    <option value="Mrs." {{ old('title') == "Mrs." ? 'selected' : '' }}>
                                                        Mrs.
                                                    </option>
                                                    <option value="Mr." {{ old('title') == "Mr." ? 'selected' : '' }}>
                                                        Mr.
                                                    </option>
                                                    <option value="Ms" {{ old('title') == "Ms" ? 'selected' : '' }}>Ms
                                                    </option>
                                                </select>
                                                @if ($errors->has('title'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="first_name" class=" text-right control-label col-form-label">First Name</label>
                                                <input id="first_name" type="text"
                                                                              class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                                              name="first_name[]"
                                                                              value="{{old('first_name')}}"
                                                                              placeholder="Enter Your Enterprise Name">
                                                @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="middle_name" class=" text-right control-label col-form-label">Middle Name</label>
                                                <input id="middle_name" type="text"
                                                                        class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}"
                                                                        name="middle_name[]"
                                                                        value="{{old('middle_name')}}"
                                                                        placeholder="Enter Your Enterprise Name">
                                                @if ($errors->has('middle_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="last_name" class=" text-right control-label col-form-label">Last Name</label>
                                                <input id="last_name" type="text"
                                                                        class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                                        name="last_name[]" value="{{old('last_name')}}"
                                                                        placeholder="Enter Your Enterprise Name">
                                                @if ($errors->has('last_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4"><label for="contact_number" class="text-right control-label col-form-label">Contact Number:</label>
                                                <input id="contact_number" type="number"
                                                       class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}"
                                                       name="contact_number[]"
                                                       value="{{old('contact_number')}}"
                                                       placeholder="Enter Your Enterprise Name">
                                                @if ($errors->has('contact_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="email" class=" text-right control-label col-form-label">Email</label>
                                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                       name="email[]" value="{{old('email')}}" placeholder="Enter Your Enterprise Name">
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="date_of_birth" class=" text-right control-label col-form-label">Date of Birth</label>
                                                <input id="date_of_birth" type="date"
                                                                            class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                                                            name="date_of_birth[]"
                                                                            value="{{old('date_of_birth')}}"
                                                                            placeholder="Enter Your Enterprise Name"> @if ($errors->has('date_of_birth'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label for="division">Present
                                                    Address</label>
                                                <hr class="bg-success mt-0 mb-0">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="row"><label class="col-sm-6 mr-0" for="present_address1">Permanent
                                                        Address</label></div>
                                                <hr class="bg-dark-blue mt-0 mb-0">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label
                                                        for="present_division">Division</label> <select
                                                        id="present_division" name="present_division[]"
                                                        class="form-control{{ $errors->has('present_division') ? ' is-invalid' : '' }} division{{$i}}">
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    @foreach($divisions as $division)
                                                        <option value="{{ $division->title}}">{{ $division->title }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('present_division'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_division') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="row col-sm-12"><label for="division3" class="col-sm-3 pl-0">Division</label>
                                                    <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0"> <i>*Same as Permanent Address</i></label>
                                                    <input id="check" onclick="myFunction()" type="checkbox" value="" class="col-sm-1 mt-1">
                                                </div>
                                                <select id="permanent_division" name="permanent_division"
                                                        class="form-control{{ $errors->has('permanent_division') ? ' is-invalid' : '' }} division{{$i}}2">
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    @foreach($divisions as $division)
                                                        <option value="{{ $division->title}}">{{ $division->title }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('permanent_division'))
                                                    <span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_division') }}</strong>
                                                     </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label
                                                        for="present_district">Distirct</label>
                                                <select id="present_district" name="present_district[]" class="form-control{{ $errors->has('present_district') ? ' is-invalid' : '' }}  district{{$i}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('present_district'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('present_district') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="permanent_district">Distirct</label>
                                                <select id="permanent_district" name="permanent_district[]"
                                                        class="form-control{{ $errors->has('permanent_district') ? ' is-invalid' : '' }}  district{{$i+1}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('permanent_district'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_district') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label
                                                        for="present_upazila">Upazila</label> <select
                                                        id="present_upazila" name="present_upazila[]"
                                                        class="form-control{{ $errors->has('present_upazila') ? ' is-invalid' : '' }}  upazila{{$i}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('present_upazila'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_upazila') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6"><label
                                                        for="permanent_upazila">Upazila</label> <select
                                                        id="permanent_upazila" name="permanent_upazila[]"
                                                        class="form-control{{ $errors->has('permanent_upazila') ? ' is-invalid' : '' }}  upazila{{$i+1}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('permanent_upazila'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanent_upazila') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label for="presentpost_office">Post Office</label>
                                                <select id="presentpost_office" name="presentpost_office[]" class="form-control{{ $errors->has('presentpost_office') ? ' is-invalid' : '' }} suboffice{{$i}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                @if ($errors->has('presentpost_office'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('presentpost_office') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6"><label for="permanentpost_office">Post Office</label>
                                                <select id="permanentpost_office" name="permanentpost_office[]"
                                                                           class="form-control{{ $errors->has('permanentpost_office') ? ' is-invalid' : '' }} suboffice{{$i+1}}">
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select> @if ($errors->has('permanentpost_office'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('permanentpost_office') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6 postcode2"><label for="presentpost_code">Post Code</label>
                                                <input id="presentpost_code" required="required" type="text" name="post_code2[]"
                                                       class="form-control{{ $errors->has('post_code2') ? ' is-invalid' : '' }}" readonly>
                                                @if ($errors->has('post_code2'))
                                                    <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $errors->first('post_code2') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6 postcode3"><label for="permanentpost_code">Post Code</label>
                                                <input id="permanentpost_code" required="required" type="text" name="post_code3[]"
                                                       class="form-control{{ $errors->has('post_code3') ? ' is-invalid' : '' }}" readonly>
                                                @if ($errors->has('post_code3'))<span
                                                        class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('post_code3') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6"><label for="present_address">Address line-1</label>
                                                <textarea id="present_address" rows="4" type="text" class="form-control{{ $errors->has('present_address') ? ' is-invalid' : '' }}"
                                                          name="present_address[]" placeholder="Enter Your present address">{{old('present_address')}}
                                                </textarea>
                                                @if ($errors->has('present_address'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6"><label for="permanent_address">Address line-1</label>
                                                <textarea id="permanent_address" rows="4" type="text"
                                                                             class="form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}" name="permanent_address[]"
                                                                             placeholder="Enter Your permanent address"></textarea>
                                                @if ($errors->has('permanent_address'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label for="nit_no" class=" text-right control-label col-form-label">NID No:</label>
                                                <input id="nit_no" type="number" class="form-control{{ $errors->has('nit_no') ? ' is-invalid' : '' }}"
                                                   name="nid_no[]" value="{{old('nit_no')}}"
                                                   placeholder="Enter Your Enterprise Name">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Attach NID Front Side</label>
                                                        <input type="file" id="input-file-now" name="nid_front_pic"
                                                               class="dropify" data-height="150" accept="image/*" />
                                                    </div>
                                                    <div class="col-sm-6"><label>Attach NID Back Side</label>
                                                        <input type="file" id="input-file-now" name="nid_back_pic"
                                                                class="dropify" data-height="150"  accept="image/*"/>
                                                    </div>
                                                </div>
                                                @if ($errors->has('enterprise_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('enterprise_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="picture" class=" text-right control-label col-form-label">Upload Your Photo</label>
                                                <input id="picture" type="number" class="form-control{{ $errors->has('picture') ? ' is-invalid' : '' }}"
                                                                              value="{{old('picture')}}" disabled>
                                                <label>Attach Your Photo</label>
                                                <input type="file" id="input-file-now" name="picture_{{ $i }}" class="dropify" data-height="150"/>
                                                @if ($errors->has('picture'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('picture') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4"><label for="owner_e_tin" class=" text-right control-label col-form-label">E-TIN</label>
                                                <input id="owner_e_tin" type="text" class="form-control{{ $errors->has('owner_e_tin') ? ' is-invalid' : '' }}"
                                                                         name="owner_e_tin[]"
                                                                         value="{{old('owner_e_tin')}}"
                                                                         placeholder="Enter Your Enterprise Name">
                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="owner_etin_pic[]" multiple class="dropify" data-height="150"/>
                                                @if ($errors->has('owner_e_tin'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_e_tin') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="passport_no" class=" text-right control-label col-form-label">Passport No</label>
                                                <input id="passport_no" type="text" class="form-control{{ $errors->has('passport_no') ? ' is-invalid' : '' }}"
                                                                               name="passport_no[]"
                                                                               value="{{old('passport_no')}}"
                                                                               placeholder="Enter Your Enterprise Name">
                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="passport_pic[]" class="dropify" data-height="150"/>
                                                @if ($errors->has('passport_no'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('passport_no') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="driving_license" class=" text-right control-label col-form-label">Driving License</label>
                                                <input id="driving_license" type="text" class="form-control{{ $errors->has('driving_license') ? ' is-invalid' : '' }}" name="driving_license[]"
                                                                           value="{{old('driving_license')}}"
                                                                           placeholder="Enter Your Enterprise Name">
                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="driving_license_pic" class="dropify" data-height="150"/> @if ($errors->has('driving_license'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('driving_license') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="text-right control-label col-form-label">Bank
                                                Details</label>
                                            <hr class="bg-dark-blue mt-1">
                                            <div class="form-group row"><label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Account Title*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="account_title[]" class="form-control" id="inputEmail3" placeholder="Enter your account title">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Account No*</label>
                                                <div class="col-sm-9">
                                                    <input type="number" name="account_no[]" class="form-control" id="inputEmail3" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank*</label>
                                                <div class="col-sm-9">
                                                    <select id="organization" name="bank[]" class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}" onchange="showHide()">
                                                        <option disabled selected>Choose Your Bank</option>
                                                        <option>bd Bank</option> @foreach($banks as $bank)
                                                            <option value="{{ $bank->bank_name }}">{{ $bank->bank_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row"><label for="inputPassword3"
                                                                               class="col-sm-3 text-right control-label col-form-label">Branch*</label>
                                                <div class="col-sm-9">
                                                    <select id="organization" name="branch[]" class="form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}" onchange="showHide()">
                                                        <option disabled selected>Choose Your Branch</option>
                                                        <option value="Sole Proprietorship" {{ old('organization') == "Sole Proprietorship" ? 'selected' : '' }}>
                                                            Dhaka branch
                                                        </option>
                                                    </select></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endfor
                            @endif
                            <div class="form-group m-b-0">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function myFunction() {
            if ($("#check").is(':checked')) {
                var division2 = $('.division2 :selected').text();
                $('.division3').html("<option>" + division2 + "<option>");
                var district2 = $('.district2 :selected').text();
                $('.district3').html("<option>" + district2 + "<option>");
                var upazila3 = $('.upazila2 :selected').text();
                $('.upazila3').html("<option>" + upazila3 + "<option>");
                var suboffice3 = $('.suboffice2 :selected').text();
                $('.suboffice3').html("<option>" + suboffice3 + "<option>");
                var postcode3 = $("input:text#post_code2").val();
                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code"  class="form-control" value="' + postcode3 + '" readonly>');
            } else {
                $('.division3').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " @foreach($divisions as $division)\n" + "  <option value=\"{{ $division->title}}\">{{ $division->title }}</option>\n" + " @endforeach");
                $('.district3').html("<option><option>");
                $('.upazila3').html("<option><option>");
                $('.suboffice3').html("<option><option>");
                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code"  class="form-control" value="" readonly>');
            }
        }
    </script>




@endsection