@extends('front-end.partials.master-layout')
@section('title', 'Register')@section('content')
    <style>
        .capture_button {
            color: gainsboro;
            bottom: 10px;
            border-radius: 50px;
            /* border: 2px solid grey;*/
            background: transparent;
            font-weight: bold;
            margin: 10px;
            outline: none;
            transition: .5s background, border ease;
            cursor: pointer;
        }

        .capture_button:hover {
            background: lightgrey;
            color: black;
            border: 2px solid lightgrey;
        }
        .button_grp {
            width: 100%;
            position: absolute;
            bottom: 10px;
            background: transparent;
        }

        .flex {
            display: flex;
            justify-content: center;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Director/Partners Details</h3></div>
                    <div class="card-body">
                        <form id="partnerEditValidation" class="form-horizontal" action="{{ url('/partner-director-info/update') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            @if (Auth::user()->businessInfo->organization == 'Partnership' || Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')
                                @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
                                    <div class="card">
                                        @if(Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')
                                            <div class="card-header"><h4 class="mb-0">Director - {{ $i }}</h4></div>
                                        @else
                                            <div class="card-header"><h4 class="mb-0">Partner - {{ $i }}</h4></div>
                                        @endif
                                        <div class="card-body bg-white">
                                            <div id="present">
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_title">Title (Mrs./Mr./Ms.)</label>
                                                        <select id="owner_title{{$i}}" class="form-control{{ $errors->has('owner_title'.$i) ? ' is-invalid' : '' }}" name="owner_title{{$i}}">
                                                            <option disabled selected>select your title</option>
                                                            <option value="Mrs."@if(old('owner_title'.$i)=='Mrs.') {{'selected'}} @elseif(isset($directorsParners['owner_title'.$i])){{ $directorsParners['owner_title'.$i] == "Mrs." ? 'selected' : '' }} @endif>Mrs.</option>
                                                            <option value="Mr." @if(old('owner_title'.$i)=='Mr.') {{'selected'}}  @elseif(isset($directorsParners['owner_title'.$i])) {{ $directorsParners['owner_title'.$i] == "Mr." ? 'selected' : '' }} @endif>Mr.</option>
                                                            <option value="Ms."  @if(old('owner_title'.$i)=='Ms') {{'selected'}}   @elseif(isset($directorsParners['owner_title'.$i])) {{ $directorsParners['owner_title'.$i] == "Ms." ? 'selected' : '' }} @endif>Ms.</option>
                                                        </select>
                                                        @if ($errors->has('owner_title'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_title'.$i) }}</strong>
                                                             </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_first_name">First Name</label>
                                                        <input id="owner_first_name{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('owner_first_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_first_name{{$i}}" value="@if(isset($directorsParners['owner_first_name'.$i])) {{$directorsParners['owner_first_name'.$i]}} @endif"  placeholder="enter your first name">
                                                        @if ($errors->has('owner_first_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_first_name'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_middle_name">Middle Name</label>
                                                        <input id="owner_middle_name{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('owner_middle_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_middle_name{{$i}}" value="@if(isset($directorsParners['owner_middle_name'.$i])) {{$directorsParners['owner_middle_name'.$i]}} @endif" placeholder="enter your middle name">
                                                        @if ($errors->has('owner_middle_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_middle_name'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_last_name">Last Name</label>
                                                        <input id="owner_last_name{{$i}}" type="text" class="form-control{{ $errors->has('owner_last_name'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_last_name{{$i}}" value="@if(isset($directorsParners['owner_last_name'.$i])) {{$directorsParners['owner_last_name'.$i]}} @endif" placeholder="enter your last name">
                                                        @if ($errors->has('owner_last_name'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_last_name'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="owner_nid">NID Number</label>
                                                        <input id="owner_nid{{$i}}" type="text"
                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                               class="form-control{{ $errors->has('owner_nid'.$i) ? ' is-invalid' : '' }}"
                                                               name="owner_nid{{$i}}" value="@if(isset($directorsParners['owner_nid'.$i])){{$directorsParners['owner_nid'.$i]}}@endif" placeholder="enter your nid number">
                                                        @if ($errors->has('owner_nid'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('owner_nid'.$i) }}</strong>
                                                            </span>
                                                        @endif

                                                        <div id="alert_message{{$i}}" style="color:#D9474F;display:none">
                                                            Either 10 Digit OR 13 Digit should be allowed.
                                                        </div>

                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_front_pic" >Upload NID Front Side</label>

                                                        @if(isset($directorsParners['nid_front_pic'.$i]))
                                                            <input type="file" id="nid_front_pic{{$i}}" name="nid_front_pic{{$i}}"
                                                                   data-default-file="{{ asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['nid_front_pic'.$i]) }}"
                                                                   class="dropify imageClass" data-height="150" accept="image/*"/>
                                                        @else
                                                            <input type="file" id="nid_front_pic{{$i}}"
                                                                   name="nid_front_pic{{$i}}" class="dropify imageClass"
                                                                   data-height="150" accept="image/*"/>
                                                        @endif

                                                        <button class="btn btn-outline btn-block pt-2 pb-2 mt-1 imgUpload text-info" type="button" id="upload"><i class="glyphicon glyphicon-upload"></i>Click fot save</button>


                                                        @if ($errors->has('nid_front_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                 <strong>{{ $errors->first('nid_front_pic'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_back_pic">Upload NID Back Side</label>

                                                        @if(isset($directorsParners['nid_back_pic'.$i]))
                                                            <input type="file" id="nid_back_pic{{$i}}" name="nid_back_pic{{$i}}"
                                                                   data-default-file="{{ asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['nid_back_pic'.$i]) }}" class="dropify imageClass" data-height="150" accept="image/*"/>
                                                        @else
                                                            <input type="file" id="nid_back_pic{{$i}}" name="nid_back_pic{{$i}}" data-default-file=""
                                                                   class="form-control {{ $errors->has('nid_back_pic'.$i) ? ' is-invalid' : '' }} dropify imageClass"  data-height="150" accept="image/*"/>
                                                        @endif

                                                        <button class="btn btn-outline btn-block pt-2 pb-2 mt-1 imgUpload text-info" type="button" id="upload">Click fot save</button>

                                                        {{--<span style="border:0px;margin: -15px;"   class="form-control {{ $errors->has('nid_back_pic'.$i) ? ' is-invalid' : '' }}"></span>--}}

                                                        @if ($errors->has('nid_back_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('nid_back_pic'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="organization">Position in the Organization</label>
                                                        <select id="position_{{$i}}" class="form-control{{ $errors->has('position'.$i) ? ' is-invalid' : '' }} position"
                                                                name="position{{$i}}" onclick="positionChange()">
                                                            <option disabled selected>choose option</option>

                                                            @if( Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited')
                                                                <option value="Chairman" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Chairman') {{'selected'}} @endif>Chairman</option>
                                                                <option value="Managing Director" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Managing Director') {{'selected'}} @endif>Managing Director</option>
                                                                <option value="Chairman & Managing Director" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Chairman & Managing Director') {{'selected'}} @endif>Chairman & Managing Director</option>
                                                                <option value="Shareholder Director" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Shareholder Director') {{'selected'}} @endif>Shareholder Director</option>


                                                            @else
                                                                <option value="Managing Partner" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Managing Partner') {{'selected'}} @endif>Managing Partner</option>
                                                                <option value="Partner" @if(isset($directorsParners['position'.$i]) && $directorsParners['position'.$i] =='Partner') {{'selected'}} @endif>Partner</option>
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('position'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('position'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="shareholder">Shareholding Percentages (%)</label>
                                                        <input id="shareholder{{$i}}" type="text"
                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                               class="txt form-control{{ $errors->has('shareholder'.$i) ? ' is-invalid' : '' }}"
                                                               name="shareholder{{$i}}" onkeyup="shareHolderCal()" value="@if(old('shareholder'.$i)) {{old('shareholder'.$i)}} @elseif(isset($directorsParners['shareholder'.$i])){{$directorsParners['shareholder'.$i]}}@endif"
                                                               placeholder="enter shareholding percentages">
                                                        @if ($errors->has('shareholder'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('shareholder'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12" id="dob{{$i}}">
                                                        <label for="date_of_birth{{$i}}">Date of Birth</label>
                                                        <input id="date_of_birth{{$i}}" type="text"
                                                               class="form-control{{ $errors->has('date_of_birth'.$i) ? ' is-invalid' : '' }} dob{{$i}}"
                                                               name="date_of_birth{{$i}}" value="@if(isset($directorsParners['date_of_birth'.$i])){{$directorsParners['date_of_birth'.$i]}}@endif" placeholder="dd-mm-yyyy" readonly>

                                                        @if ($errors->has('date_of_birth'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('date_of_birth'.$i) }}</strong>
                                                             </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">

                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Capture Your Photo</label>
                                                        <a class="form-control{{ $errors->has('capture_pic'.$i) ? ' is-invalid' : '' }} btn btn-block btn-outline mb-1"
                                                           id="cap_btn{{$i}}" onclick="capture{{$i}}()" data-toggle="modal" data-target="#exampleModal{{$i}}">
                                                            <i class=" fa fa-camera"></i> Camera</a>
                                                        <input type="hidden" name="capture_pic{{$i}}" class="image-tag{{$i}}">
                                                        @if(isset($directorsParners['capture_pic'.$i]))
                                                            <img style="width: 150px; width: 200px;" src="{{ asset('/ui/back-end/uploads/sme-photo/'.$directorsParners['capture_pic'.$i]) }}">
                                                        @endif


                                                        @if ($errors->has('capture_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('capture_pic'.$i) }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Upload Your Photo</label>
                                                        @if(isset($directorsParners['owner_pic'.$i]))
                                                            <input type="file" id="owner_pic{{$i}}" name="owner_pic{{$i}}"
                                                                   data-default-file="{{ asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['owner_pic'.$i]) }}"
                                                                   class="dropify" data-height="150" accept="image/*"/>
                                                        @else
                                                            <input type="file" id="owner_pic{{$i}}" name="owner_pic{{$i}}" class="dropify"
                                                                   data-height="150" accept="image/*"/>
                                                        @endif

                                                        <button class="btn btn-outline btn-block pt-2 pb-2 mt-1 imgUpload text-info" type="button" id="upload">Click for Save</button>

                                                        @if ($errors->has('owner_pic'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                          <strong>{{ $errors->first('owner_pic'.$i) }}</strong>
                                                         </span>
                                                        @endif
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="division">Present Address</label>
                                                        <hr class="bg-success mt-0 mb-0">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="row">
                                                            <label class="col-sm-6 mr-0" for="present_address1">Permanent Address</label>
                                                        </div>
                                                        <hr class="bg-dark-blue mt-0 mb-0">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_division">Division</label>
                                                        <select onchange="" id="present_division{{$i}}" name="present_division{{$i}}"
                                                                class="form-control{{ $errors->has('present_division'.$i) ? ' is-invalid' : '' }}">
                                                            <option  disabled="true" selected="true">-select division-</option>
                                                            @foreach($divisions as $division)

                                                                <option value="{{ $division->id }}"
                                                                @if(isset($directorsParners['present_division'.$i]) == $division->title)
                                                                    {{ 'selected' }}
                                                                        @endif>
                                                                    {{ $division->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('present_division'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_division'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6" id="division{{$i}}">
                                                        <div class="row col-sm-12">
                                                            <label for="division3" class="col-sm-3 pl-0">Division</label>
                                                            <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0">
                                                                <i>*Same as Present Address</i></label>
                                                            <input id="check_{{$i}}" onclick="permanentFunction_{{$i}}()" type="checkbox" value="" class="col-sm-1 mt-1">
                                                            {{--<input type="checkbox" value="" class="col-sm-1 mt-1">--}}
                                                        </div>

                                                        <select id="permanent_division{{$i}}" name="permanent_division{{$i}}"
                                                                class="form-control{{ $errors->has('permanent_division'.$i) ? ' is-invalid' : '' }} ">
                                                            <option value="0" disabled="true" selected="true">-select division-</option>

                                                            @foreach($divisions as $division)
                                                                <option value="{{ $division->id }} "
                                                                @if($directorsParners['permanent_division'.$i] == $division->title)
                                                                    {{ 'selected' }}
                                                                        @endif
                                                                >
                                                                    {{ $division->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('permanent_division'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_division'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_district">Distirct</label>
                                                        <select id="present_district{{$i}}" name="present_district{{$i}}"
                                                                class="form-control{{ $errors->has('present_district'.$i) ? ' is-invalid' : '' }} ">

                                                            @if(isset($directorsParners['present_district'.$i]))
                                                                <option value="{{ $directorsParners['present_district'.$i] }}">{{ $directorsParners['present_district'.$i] }}</option>
                                                            @else
                                                                <option value=""></option>
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('present_district'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_district'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="district{{$i}}">
                                                        <label for="permanent_district">Distirct</label>
                                                        <select name="permanent_district{{$i}}" id="permanent_district{{$i}}"
                                                                class="form-control{{ $errors->has('permanent_district'.$i) ? ' is-invalid' : '' }}">

                                                            @if(isset($directorsParners['permanent_district'.$i]))
                                                                <option value="{{$directorsParners['permanent_district'.$i] }}">{{ $directorsParners['permanent_district'.$i] }}</option>
                                                            @else
                                                                <option value=""></option>
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('permanent_district'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_district'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_upazila">Upazila</label>
                                                        <select id="present_upazila{{$i}}" name="present_upazila{{$i}}"
                                                                class="form-control{{ $errors->has('present_upazila'.$i) ? ' is-invalid' : '' }} ">

                                                            @if(isset( $directorsParners['present_upazila'.$i]))
                                                                <option value="{{  $directorsParners['present_upazila'.$i] }}">{{  $directorsParners['present_upazila'.$i] }}</option>
                                                            @else
                                                                <option value="" ></option>
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('present_upazila'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_upazila'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="upazila{{$i}}">
                                                        <label for="permanent_upazila">Upazila</label>
                                                        <select id="permanent_upazila{{$i}}" name="permanent_upazila{{$i}}" class="form-control{{ $errors->has('permanent_upazila'.$i) ? ' is-invalid' : '' }}">


                                                            @if(isset( $directorsParners['permanent_upazila'.$i]))
                                                                <option value="{{ $directorsParners['permanent_upazila'.$i] }}">{{ $directorsParners['permanent_upazila'.$i] }}</option>
                                                            @else
                                                                <option value=""></option>

                                                            @endif
                                                        </select>
                                                        @if ($errors->has('permanent_upazila'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_upazila'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="presentpost_office">Post Office</label>
                                                        <select id="present_suboffice{{$i}}" name="presentpost_office{{$i}}"
                                                                class="form-control{{ $errors->has('presentpost_office'.$i) ? ' is-invalid' : '' }} ">

                                                            @if(isset($directorsParners['presentpost_office'.$i]))
                                                                <option value="{{ $directorsParners['presentpost_office'.$i]  }}">{{ $directorsParners['presentpost_office'.$i] }}</option>
                                                            @else
                                                                <option value=""></option>
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('presentpost_office'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('presentpost_office'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6" id="postoffice{{$i}}">
                                                        <label for="permanentpost_office">Post Office</label>

                                                        <select id="permanent_suboffice{{$i}}" name="permanentpost_office{{$i}}" class="form-control{{ $errors->has('permanentpost_office'.$i) ? ' is-invalid' : '' }} ">

                                                            @if(isset($directorsParners['permanentpost_office'.$i]))
                                                                <option value="{{$directorsParners['permanentpost_office'.$i]}}">{{ $directorsParners['permanentpost_office'.$i] }}</option>
                                                            @else
                                                                <option value="" disabled="true" selected="true"></option>
                                                            @endif
                                                        </select>

                                                        @if ($errors->has('permanentpost_office'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanentpost_office'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6" id="present_postcode{{$i}}">
                                                        <label for="post_code2">Post Code</label>
                                                        {{--<input  required="required" type="text" name="present_post_code{{$i}}" value="@if(old('present_post_code'.$i)) {{old('present_post_code'.$i)}} @elseif(isset($directorsParners['present_post_code'.$i])) {{$directorsParners['present_post_code'.$i]}} @endif"--}}
                                                        {{--class="form-control{{ $errors->has('present_post_code'.$i) ? ' is-invalid' : '' }} " readonly>--}}


                                                        <input  type="text"  id="present_post_code{{$i}}"  value="@if(isset($directorsParners['present_post_code'.$i])) {{$directorsParners['present_post_code'.$i]}} @endif"   name="present_post_code{{$i}}"
                                                                class="form-control{{ $errors->has('present_post_code'.$i) ? ' is-invalid' : '' }} " readonly>

                                                        @if ($errors->has('present_post_code'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_post_code'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6 " id="permanent_postcode{{$i}}">
                                                        <label for="permanentpost_code">Post Code</label>

                                                        <input  required="required" type="text" id="permanent_post_code{{$i}}"   name="post_code{{$i}}" value="@if(isset($directorsParners['post_code'.$i])){{$directorsParners['post_code'.$i]}} @endif"
                                                                class="form-control{{ $errors->has('post_code'.$i) ? ' is-invalid' : '' }} " readonly>

                                                        @if ($errors->has('post_code'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('post_code'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_address">Address line</label>
                                                        <textarea id="present_address_{{$i}}" rows="4" type="text"
                                                                  class="form-control{{ $errors->has('present_address'.$i) ? ' is-invalid' : '' }}"
                                                                  name="present_address{{$i}}"
                                                                  placeholder="enter your present address">@if(isset($directorsParners['present_address'.$i])){{ $directorsParners['present_address'.$i] }}@endif</textarea>
                                                        @if ($errors->has('present_address'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_address'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group col-md-6" id="permanent_{{$i}}">
                                                        <label for="permanent_address">Address line</label>
                                                        <textarea id="permanent_address_{{$i}}" rows="4" type="text"
                                                                  class="form-control{{ $errors->has('permanent_address'.$i) ? ' is-invalid' : '' }}"
                                                                  name="permanent_address{{$i}}" placeholder="enter your permanent address">@if(isset($directorsParners['permanent_address'.$i])){{ $directorsParners['permanent_address'.$i] }}@endif</textarea>
                                                        @if ($errors->has('permanent_address'.$i))
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_address'.$i) }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                @endfor

                            @endif

                            <div class="form-group m-b-0">
                                <a href="{{ url('/register/edit') }}" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb-3 pull-left">Back</a>
                                <button id="btn-submit" type="submit" name="action" value="next" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>
                                <button type="submit" name="action" value="save" class="btn btn-success waves-effect waves-light m-t-10 pt-3 pb-3 pull-right mr-3">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
        <div class="modal fade" id="exampleModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div style="margin-left:200px;/*box-shadow: 0px 7px 21px 1px rgba(0,0,0,0.47);*/width:420px;padding: 0px;
                 border-radius: 20px;border:none; overflow: hidden;" class="modal-content">
                    <div style="padding:0" class="modal-body">
                        <button style="margin-top:5px;position: absolute; right: 10px;z-index: 5;color: white;width: 25px;height: 25px;"
                                type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="display: block;line-height: 0.3">&times;</span>
                        </button>

                        <div class="row">
                            <div class="col-12 ">
                                <div id="my_camera{{$i}}"></div>
                                <div class="button_grp">
                                    <div id="btns" class="flex">
                                        <button id="capture{{$i}}" style="display: none" onclick="capture{{$i}}()" class="capture_button">New</button>
                                        <button onclick="take_snapshot{{$i}}()" class="capture_button"> <i class="fa fa-camera" style="font-size: 30px"></i></button>
                                        <button id="save{{$i}}" class="capture_button" style="display: none" onclick="cap_save{{$i}}()" data-dismiss="modal" aria-label="Close">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endfor

    <script>
            $(".imgUpload").click(function(){
               // var  data=$(".dropify");
               // console.log(data);
               //  alert(this.files[0].size);

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    data: new FormData($("#partnerEditValidation")[0]),
                    url:"update",
                    // dataType:'json',
                    // async:false,
                    processData: false,
                    contentType: false,
                    success:function(response){

                        let fileId =  $('.imageClass').val();
                        if (fileId == ''){
                            $.toast({
                                heading: 'Success',
                                text: 'File uploaded! ',
                                position: 'top-right',
                                icon: 'success',
                            })
                        }else {
                            $.toast({
                                heading: 'Success',
                                text: 'File uploaded! ',
                                position: 'top-right',
                                icon: 'success',
                            })
                        }
                    },
                });
            });
        </script>

    <script>
        $('form').keyup(function(e)
        {
            var data = $("form").serialize();
            console.log(data);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url:"update",
                data:data,
                success: function (data) {
                    console.log(data)
                },
                error: function (xhr, status, error) {
//                 alert("An AJAX error occured: " + status + "\nError: " + error);
                }
            });
            e.preventDefault();
        });


        $('select').change(function(e)
        {

            var data = $("form").serialize();
            console.log(data);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url:"update",
                data:data,

                success: function (data) {

                    console.log(data)
                },
                error: function (xhr, status, error) {
//                    alert("An AJAX error occured: " + status + "\nError: " + error);
                }
            });
            e.preventDefault();

        });

        $('.autofill').click(function(e)
        {

            var data = $("form").serialize();


            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url:"update",
                data:data,

                success: function (data) {

                    console.log(data)
                },
                error: function (xhr, status, error) {
//                    alert("An AJAX error occured: " + status + "\nError: " + error);
                }
            });
            e.preventDefault();

        });


    </script>


    <script>

        $('.txt').keyup(function () {
            $('.txt').css('border','2px solid #e6ecef');
        });


        $('#btn-submit').bind('click', function(e) {
            let shareHolderParsent = 0;
            $("input.txt").each(function () {
                return shareHolderParsent += (parseInt(this.value) || 0);
            });

            if (shareHolderParsent <100) {

                $.alert({
                    title: 'Alert!',
                    content: '<span class="text-danger">Please ensure sum of your shareholdings equal to 100%</span><br><br>' +
                        '<strong>Your current total shareholdings '+shareHolderParsent+'% </strong> ',
                    type: 'red',
                    typeAnimated: true
                });
                $('.txt').css('border','1px solid red');

                return false;

            }
            if(shareHolderParsent>100)
            {

                $.alert({
                    title: 'Alert!',
                    content: '<span class="text-danger">Please ensure sum of your shareholdings equal to 100%</span><br><br>' +
                        '<strong>Your current total shareholdings '+shareHolderParsent+'% </strong> ',
                    type: 'red',
                    typeAnimated: true
                });

                $('.txt').css('border','1px solid red');

                return false;
            }
            if(shareHolderParsent==100)
            {
                var form=document.getElementsByTagName('form');
                form.submit();
            }


        });

    </script>

    <!-- Configure a few settings and attach camera -->
    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )

        <script>
            $("#owner_nid{{ $i }}").keyup(function (val) {

                value= $("#owner_nid{{ $i }}").val();


                if(value.length==13)
                {
                    $("#alert_message{{ $i }}").css("display","none");
                }
                else if(value.length==10)
                {
                    $("#alert_message{{ $i }}").css("display","none");

                }
                else if(value=='')
                {

                    $("#alert_message{{ $i }}").css("display","none");
                }
                else
                {
                    $("#alert_message{{ $i }}").css("display","block");
                }
            });
        </script>

        {{--------------------------------}}
            Camera Script
        {{--------------------------------}}
        <script language="JavaScript">
            function capture{{$i}}() {
                Webcam.set({
                    width: 420,
                    height: 320,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });

                Webcam.attach('#my_camera{{$i}}');
            };


            function take_snapshot{{$i}}() {
                Webcam.snap(function (data_uri) {
                    $(".image-tag{{$i}}").val(data_uri);
                    document.getElementById('my_camera{{$i}}').innerHTML = '<img src="' + data_uri + '"/>';
                    $('#capture{{$i}}').css('display', "block");
                    $('#save{{$i}}').css('display', "block");
                });
            }

            function cap_save{{$i}}() {
                take_snapshot{{$i}}();
                $("#cap_btn{{$i}}").append(" ✔");
            }
        </script>
    @endfor


    <script type="text/javascript">

        function shareHolderCal() {
            //           let to nth-last-child(1)
            let shareHolderParsent = 0;
            $(".txt").each(function () {
                return shareHolderParsent += (parseInt(this.value) || 0);
            });

            if (shareHolderParsent > 100) {

                $.alert({
                    title: 'Alert!',
                    content: '<span class="text-danger">Please ensure sum of your shareholdings equal to 100%</span><br><br>' +
                        '<strong>Your current total shareholdings '+shareHolderParsent+'% </strong> ',
                    type: 'red',
                    typeAnimated: true
                });

            }

        }

        function positionChange()
        {
            @for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++)

            $("#position_{{$i}}").change(function () {
                    var value=$("#position_{{$i}}  :selected").val();
                    var id = $(this).children(":selected").closest("select").attr("id");
                    var check_{{$i}}=$("#position_{{$i}} :selected").val();

                    if(value=='Chairman')
                    {
                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)
                        $("#position_{{$x}} option[value='Chairman']").remove();
                        $("#"+id).html('<option value="Chairman" selected>Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');
                        @endfor
                    }

                    else if(value=="Managing Director")
                    {
                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)
                        $("#position_{{$x}} option[value='Managing Director']").remove();
                        if(check_{{$i}} == 'Managing Director')
                        {
                            $("#"+id).html('<option value="Managing Director" selected>Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');
                        }
                        else if(check_{{$i}} != 'Managing Director') {
                            var abc1 = $("#"+id).html('<option value="Managing Director" >Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');
                        }
                        @endfor
                    }
                    else if(value=="Chairman & Managing Director")
                    {

                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)
                        $("#position_{{$x}} option[value='Chairman & Managing Director']").remove();
                        if(check_{{$i}} == 'Chairman & Managing Director')
                        {
                            $("#"+id).html('<option value="Chairman & Managing Director" selected>Chairman & Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');
                        }
                        else if(check_{{$i}} != 'Chairman & Managing Director') {
                            var abc1 = $("#"+id).html('<option value="Chairman & Managing Director" >Chairman & Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');
                        }
                        @endfor
                    }

                    else if(value=='Shareholder Director')
                    {
                        if(check_{{$i}}=='Managing Director' && check_{{$i}}=='Chairman')
                        {
                            console.log(true);
                            $("#position_{{$i}}").html('<option value="Shareholder Director" selected>Shareholder Director<option>');
                        }
                        $("#position_{{$i}}").html('<option value="Chairman">Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director" selected>Shareholder Director<option>');
                    }
                    else if(value=='Managing Partner')
                    {

                        @for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++)
                        $("#position_{{$x}} option[value='Managing Partner']").remove();
                        $("#"+id).html('<option value="Managing Partner" selected>Managing Partner</option><option value="Partner">Partner</option>');
                        @endfor
                    }
                    else if(value=='Partner')
                    {
                        $("#position_{{$i}}").html('<option value="Managing Partner">Managing Partner</option><option value="Partner" selected>Partner</option>');
                    }
                }
            );
            @endfor
        }

        @for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++)
        function permanentFunction_{{$i}}() {
            if ($("#check_{{$i}}").is(':checked')) {
                var division2 = $('#present_division{{$i}} :selected').text();
                var divisionValue = $('#present_division{{$i}} :selected').val();
                $('#permanent_division{{$i}}').html("<option value="+divisionValue+">" + division2 + "<option>");
                var district2 = $('#present_district{{$i}} :selected').text();
                var districtValue = $('#present_district{{$i}} :selected').val();
                $('#permanent_district{{$i}}').html("<option value="+districtValue+">" + district2 + "<option>");
                var upazila3 = $('#present_upazila{{$i}} :selected').text();
                var upazilaValue = $('#present_upazila{{$i}} :selected').val();
                $('#permanent_upazila{{$i}}').html("<option value="+upazilaValue+">" + upazila3 + "<option>");
                var suboffice3 = $('#present_suboffice{{$i}} :selected').text();
                var subofficeVal = $('#present_suboffice{{$i}} :selected').val();
                $('#permanent_suboffice{{$i}}').html("<option value="+subofficeVal+">" + suboffice3 + "<option>");
                var postcode3 = $("input:text#present_post_code{{$i}}").val();
                $('#permanent_postcode{{$i}}').html('<label>Post Code</label><input type="text"  name="post_code{{$i}}"  class="form-control" value="' + postcode3 + '" readonly>');
                var present_address=$("textarea#present_address_{{$i}}").val();
                $('#permanent_{{$i}}').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "                                                          name=\"permanent_address{{$i}}\" placeholder=\"enter your permanent address\" >"+present_address+"</textarea>");
                var divisionSpanHidden=$('#division{{$i}}').find("span");
                divisionSpanHidden.addClass("hidden");
                var districtSpanHidden=$('#district{{$i}}').find("span");
                districtSpanHidden.addClass("hidden");
                var upazilaSpanHiddden=$('#upazila{{$i}}').find("span");
                upazilaSpanHiddden.addClass("hidden");
                var postSpanHiddden=$('#postoffice{{$i}}').find("span");
                postSpanHiddden.addClass("hidden");
            }
            else {
                $('#permanent_division{{$i}}').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " @foreach($divisions as $division)\n" + "  <option value=\"{{ $division->id}}\">{{ $division->title }}</option>\n" + " @endforeach");
                $('#permanent_district{{$i}}').html("<option><option>");
                $('#permanent_upazila{{$i}}').html("<option><option>");
                $('#permanent_suboffice{{$i}}').html("<option><option>");
                $('#permanent_postcode{{$i}}').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="" readonly>');
                $('#permanent_{{$i}}').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "name=\"permanent_address{{$i}}\" placeholder=\"enter your permanent address\" ></textarea>");
            }
        }
        @endfor
    </script>

    @for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ )
        <script>
            $( function() {

                $( "#date_of_birth{{$i}}" ).datepicker( "option", "showAnim", $( this ).val() );
                $( "#date_of_birth{{$i}}" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    yearRange: "-120:+0",

                    onSelect: function(date) {
                        var dateSplit = date.split("-");
                        var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                        var today = new Date();
                        if (dob.getFullYear() + 18 > today.getFullYear()) {

                            alert('You must be at least 18 years old to submit register');
                            // $.alert({
                            //     title: 'Alert!',
                            //     content: 'You must be atleast 18 years old to submit register',
                            //     type: 'red',
                            //     typeAnimated: true,
                            // });
                            $(".dob{{$i}}").val("");
                        }

                        {{--let dateOfBirth{{$i}} = $('#date_of_birth{{$i}}').val();--}}
                        {{--if (dateOfBirth{{$i}}){--}}
                            {{--console.log(dod);--}}
                            {{--$('#dod{{$i}}').children('span').remove();--}}
                        {{--}--}}
                    }
                });
            });
        </script>
    @endfor

@endsection


