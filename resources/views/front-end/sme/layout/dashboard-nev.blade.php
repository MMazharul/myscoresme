<aside class="left-sidebar ">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar ">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img">
                <img src="{{ asset('/') }}ui/back-end/sme/{{ Auth::user()->picture }}" height="50" alt="user"/>
            </div>            <!-- User profile text-->
            <div class="profile-text"><a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown"
                                         role="button" aria-haspopup="true"
                                         aria-expanded="true">{{Auth::user()->first_name}}<span
                            class="caret"></span></a>
                <div class="dropdown-menu animated flipInY"><a href="#" class="dropdown-item"><i class="ti-user"></i> My
                        Profile</a> <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a> <a
                            href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <div class="dropdown-divider"></div>
                    <a href="" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a></div>
            </div>
        </div>        <!-- End User profile text-->        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li><a href="{{url('/sme-dashboard')}}" aria-expanded="false"><i class="mdi mdi mdi-gauge"></i><span
                                class="hide-menu">Dashboard </span></a></li> {{--  @if(!$businessBackground1)--}}
                <li><a class="has-arrow" href="{{url('/sme-question')}}" aria-expanded="false"><i
                                class="fa fa-vcard-o"></i><span class="hide-menu"> Profile Create </span></a>
                    <ul aria-expanded="false"
                        class="collapse">                       {{-- <li><a href="{{url('sme-profile-edit/'. Auth::user()->id)}}">Basic Profile Informatiom</a></li>--}}
                        <li><a href="{{url('/sme-question')}}">Profile Management</a></li>
                        <li><a href="">Business Background</a></li>
                        <li><a href="">Owner Background</a></li>
                        <li><a href="">Liquility Managements</a></li>
                        <li><a href="">Inventory & Acounts</a></li>
                    </ul>
                </li> {{--    @elseif($businessBackground1)                <li class="nav-devider"></li><li>                    <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-profile"></i><span class="hide-menu">SME Profile Information</span></a>                    <ul aria-expanded="false" class="collapse">                        --}}{{--<li><a href="{{url('sme-profile-edit/'. Auth::user()->id)}}">Basic Profile Information</a></li>--}}{{--                        <li><a href="{{url('/business-background-edit')}}">Business Background</a></li>                        <li><a href="{{url('/owner-background-edit/'. Auth::user()->id)}}">Owner Background</a></li>                        <li><a href="{{url('/business-liquidity-edit/'. Auth::user()->id)}}">Liquility Managements</a></li>                        <li><a href="{{url('/inventory-account-edit/'. Auth::user()->id)}}">Inventory & Acounts</a></li>                    </ul>                </li>                @endif--}}
                <li class="nav-devider"></li>
            </ul>
        </nav>        <!-- End Sidebar navigation -->
    </div>    <!-- End Sidebar scroll-->    <!-- Bottom points-->


    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

        @csrf

    </form><i class="mdi mdi-power"></i>

    <div class="sidebar-footer">

        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Settings"><i
                    class="ti-settings"></i>
        </a>        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Email">
            <i class="mdi mdi-gmail"></i>
        </a>
        <!-- item-->
        <a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();

                  document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
    </div>    <!-- End Bottom points-->
</aside>