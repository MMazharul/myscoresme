<!DOCTYPE html>

<html lang="en">





<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon icon -->

    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}ui/back-end/assets/images/favicon.png">

    <title>Sme Home</title>

    <!-- Bootstrap Core CSS -->

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Page plugins css -->

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">

    <!-- Color picker plugins css -->

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">

    <!-- Date picker plugins css -->

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

    <!-- Daterange picker plugins css -->

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">

    <link href="{{ asset('/') }}ui/back-end/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="{{ asset('/') }}ui/back-end/css/style.css" rel="stylesheet">



    <!-- multi-select -->

    <link href="{{asset('/')}}ui/back-end/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

    <link href="{{asset('/')}}ui/back-end/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Popup CSS -->
    <link href="{{ asset('/') }}ui/back-end/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

    {{--<!-- Wizard CSS -->--}}

    <link href="{{ asset('/') }}ui/back-end/wizard-ui/css/bootstrap.min.css" rel="stylesheet"/>

    <link href="{{ asset('/') }}ui/back-end/wizard-ui/css/font-awesome.min.css" rel="stylesheet"/>

    <link href="{{ asset('/') }}ui/back-end/wizard-ui/css/style.css" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ asset('/') }}ui/back-end/wizard-ui/assets/prism/prism-okaida.css">

    <link rel="stylesheet" href="{{ asset('/') }}ui/back-end/wizard-ui/validatrix/validatrix.css">




    <!-- You can change the theme colors from here -->

    <link href="{{ asset('/') }}ui/back-end/css/colors/blue.css" id="theme" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Datepicker css -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">






    {{--ajax work js file--}}

    <script src="{{asset('/')}}ui/front-end/js/jquery.min.js"></script>





    {{--Range Slider--}}

    <link href="{{ asset('/') }}range-slider/range-slider.css" rel="stylesheet">

    {{--<script src="{{ asset('/') }}range-slider/jquery-2.1.3.min.js"></script>--}}





</head>

{{--</head>--}}



<body class="fix-header fix-sidebar card-no-border">

<!-- ============================================================== -->

<!-- Preloader - style you can find in spinners.css -->

<!-- ============================================================== -->

<div class="preloader">

    <svg class="circular" viewBox="25 25 50 50">

        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>

</div>

<!-- ============================================================== -->

<!-- Main wrapper - style you can find in pages.scss -->

<!-- ============================================================== -->

<div id="main-wrapper">

    <!-- ============================================================== -->

    @include('front-end.sme.layout.dashboard-header')


    @include('front-end.sme.layout.dashboard-nev')


    <div class="page-wrapper">

        <!-- ============================================================== -->

    @yield('main-content')

    <!-- ============================================================== -->

        <footer class="footer">

            © 2018 Brique N Connect by myscoresme.com
        </footer>

        <!-- ============================================================== -->

    </div>

</div>

<!-- ============================================================== -->



<script src="{{ asset('/') }}ui/back-end/assets/plugins/jquery/jquery.min.js"></script>



<!-- Bootstrap tether Core JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap/js/popper.min.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="{{ asset('/') }}ui/back-end/js/jquery.slimscroll.js"></script>

<!--Wave Effects -->

<script src="{{ asset('/') }}ui/back-end/js/waves.js"></script>

<!--Menu sidebar -->

<script src="{{ asset('/') }}ui/back-end/js/sidebarmenu.js"></script>

<!--stickey kit -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>

<!--Custom JavaScript -->

<script src="{{ asset('/') }}ui/back-end/js/custom.min.js"></script>

<!-- ============================================================== -->

<!-- Plugins for this page -->

<!-- ============================================================== -->

<!-- Plugin JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/moment/moment.js"></script>

<!-- Clock Plugin JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>

<!-- Color Picker Plugin JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>

<!-- Date Picker Plugin JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Date range Plugin JavaScript -->

<script src="{{ asset('/') }}ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/daterangepicker/daterangepicker.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/moment/moment.js"></script>



<script src="{{asset('ajax/location.js')}}"></script>



{{--<!-- Wizard-Form -->--}}

<script src="{{ asset('/') }}ui/back-end/wizard-ui/js/script.js"></script>
<script src="{{ asset('/') }}ui/back-end/wizard-ui/assets/prism/prism.js"></script>
<script src="{{ asset('/') }}ui/back-end/wizard-ui/validatrix/validatrix.js"></script>




<!-- multi select -->

<script type="text/javascript" src="{{ asset('/') }}ui/back-end/assets/plugins/multiselect/js/jquery.multi-select.js"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

<script src="{{ asset('/') }}ui/back-end/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>



{{--mounth year picker--}}
<script src="{{ asset('/') }}jquery-datepicker/dobpicker.js"></script>



<script>

    /*******************************************/

    // Basic Date Range Picker

    /*******************************************/

    $('.daterange').daterangepicker();



    /*******************************************/

    // Date & Time

    /*******************************************/

    $('.datetime').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        locale: {

            format: 'MM/DD/YYYY h:mm A'

        }

    });



    /*******************************************/

    //Calendars are not linked

    /*******************************************/

    $('.timeseconds').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        timePicker24Hour: true,

        timePickerSeconds: true,

        locale: {

            format: 'MM-DD-YYYY h:mm:ss'

        }

    });



    /*******************************************/

    // Single Date Range Picker

    /*******************************************/

    $('.singledate').daterangepicker({

        singleDatePicker: true,
        showDropdowns: true,
        autoclose: true,
        todayHighlight: true

    });



    /*******************************************/

    // Auto Apply Date Range

    /*******************************************/

    $('.autoapply').daterangepicker({

        autoApply: true,

    });



    /*******************************************/

    // Calendars are not linked

    /*******************************************/

    $('.linkedCalendars').daterangepicker({

        linkedCalendars: false,

    });



    /*******************************************/

    // Date Limit

    /*******************************************/

    $('.dateLimit').daterangepicker({

        dateLimit: {

            days: 7

        },

    });



    /*******************************************/

    // Show Dropdowns

    /*******************************************/

    $('.showdropdowns').daterangepicker({

        showDropdowns: true,

    });



    /*******************************************/

    // Show Week Numbers

    /*******************************************/

    $('.showweeknumbers').daterangepicker({

        showWeekNumbers: true,

    });



    /*******************************************/

    // Date Ranges

    /*******************************************/

    $('.dateranges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        }

    });



    /*******************************************/

    // Always Show Calendar on Ranges

    /*******************************************/

    $('.shawCalRanges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        alwaysShowCalendars: true,

    });



    /*******************************************/

    // Top of the form-control open alignment

    /*******************************************/

    $('.drops').daterangepicker({

        drops: "up" // up/down

    });



    /*******************************************/

    // Custom button options

    /*******************************************/

    $('.buttonClass').daterangepicker({

        drops: "up",

        buttonClasses: "btn",

        applyClass: "btn-info",

        cancelClass: "btn-danger"

    });



    /*******************************************/

    // Language

    /*******************************************/

    $('.localeRange').daterangepicker({

        ranges: {

            "Aujourd'hui": [moment(), moment()],

            'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],

            'Les 7 derniers jours': [moment().subtract('days', 6), moment()],

            'Les 30 derniers jours': [moment().subtract('days', 29), moment()],

            'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],

            'le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]

        },

        locale: {

            applyLabel: "Vers l'avant",

            cancelLabel: 'Annulation',

            startLabel: 'Date initiale',

            endLabel: 'Date limite',

            customRangeLabel: 'SÃ©lectionner une date',

            // daysOfWeek: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi','Samedi'],

            daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

            monthNames: ['Janvier', 'fÃ©vrier', 'Mars', 'Avril', 'ÐœÐ°i', 'Juin', 'Juillet', 'AoÃ»t', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],

            firstDay: 1

        }

    });

</script>



<script>

    // Clock pickers

    $('#single-input').clockpicker({

        placement: 'bottom',

        align: 'left',

        autoclose: true,

        'default': 'now'

    });

    $('.clockpicker').clockpicker({

        donetext: 'Done',

    }).find('input').change(function() {

        console.log(this.value);

    });

    $('#check-minutes').click(function(e) {

        // Have to stop propagation here

        e.stopPropagation();

        input.clockpicker('show').clockpicker('toggleView', 'minutes');

    });

    if (/mobile/i.test(navigator.userAgent)) {

        $('input').prop('readOnly', true);

    }

    // Colorpicker

    $(".colorpicker").asColorPicker();

    $(".complex-colorpicker").asColorPicker({

        mode: 'complex'

    });

    $(".gradient-colorpicker").asColorPicker({

        mode: 'gradient'

    });

    // Date Picker

    jQuery('.mydatepicker, #datepicker').datepicker();

    jQuery('.datepicker-autoclose').datepicker({

        autoclose: true,

        todayHighlight: true

    });

    jQuery('#date-range').datepicker({

        toggleActive: true

    });

    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true

    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({

        buttonClasses: ['btn', 'btn-sm'],

        applyClass: 'btn-danger',

        cancelClass: 'btn-inverse'

    });

    $('.input-daterange-timepicker').daterangepicker({

        timePicker: true,

        format: 'MM/DD/YYYY h:mm A',

        timePickerIncrement: 30,

        timePicker12Hour: true,

        timePickerSeconds: false,

        buttonClasses: ['btn', 'btn-sm'],

        applyClass: 'btn-danger',

        cancelClass: 'btn-inverse'

    });

    $('.input-limit-datepicker').daterangepicker({

        format: 'MM/DD/YYYY',

        minDate: '06/01/2015',

        maxDate: '06/30/2015',

        buttonClasses: ['btn', 'btn-sm'],

        applyClass: 'btn-danger',

        cancelClass: 'btn-inverse',

        dateLimit: {

            days: 6

        }

    });

</script>

<!-- ============================================================== -->

<!-- Style switcher -->

<!-- ============================================================== -->

<script>

    jQuery(document).ready(function() {

        // Switchery

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        $('.js-switch').each(function() {

            new Switchery($(this)[0], $(this).data());

        });

        // For select 2

        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin

        $(".vertical-spin").TouchSpin({

            verticalbuttons: true,

            verticalupclass: 'ti-plus',

            verticaldownclass: 'ti-minus'

        });

        var vspinTrue = $(".vertical-spin").TouchSpin({

            verticalbuttons: true

        });

        if (vspinTrue) {

            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();

        }

        $("input[name='tch1']").TouchSpin({

            min: 0,

            max: 100,

            step: 0.1,

            decimals: 2,

            boostat: 5,

            maxboostedstep: 10,

            postfix: '%'

        });

        $("input[name='tch2']").TouchSpin({

            min: -1000000000,

            max: 1000000000,

            stepinterval: 50,

            maxboostedstep: 10000000,

            prefix: '$'

        });

        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({

            initval: 40

        });

        $("input[name='tch5']").TouchSpin({

            prefix: "pre",

            postfix: "post"

        });

        // For multiselect

        $('#pre-selected-options').multiSelect();

        $('#optgroup').multiSelect({

            selectableOptgroup: true

        });

        $('#public-methods').multiSelect();

        $('#select-all').click(function() {

            $('#public-methods').multiSelect('select_all');

            return false;

        });

        $('#deselect-all').click(function() {

            $('#public-methods').multiSelect('deselect_all');

            return false;

        });

        $('#refresh').on('click', function() {

            $('#public-methods').multiSelect('refresh');

            return false;

        });

        $('#add-option').on('click', function() {

            $('#public-methods').multiSelect('addOption', {

                value: 42,

                text: 'test 42',

                index: 0

            });

            return false;

        });

        $(".ajax").select2({

            ajax: {

                url: "https://api.github.com/search/repositories",

                dataType: 'json',

                delay: 250,

                data: function(params) {

                    return {

                        q: params.term, // search term

                        page: params.page

                    };

                },

                processResults: function(data, params) {

                    // parse the results into the format expected by Select2

                    // since we are using custom formatting functions we do not need to

                    // alter the remote JSON data, except to indicate that infinite

                    // scrolling can be used

                    params.page = params.page || 1;

                    return {

                        results: data.items,

                        pagination: {

                            more: (params.page * 30) < data.total_count

                        }

                    };

                },

                cache: true

            },

            escapeMarkup: function(markup) {

                return markup;

            }, // let our custom formatter work

            minimumInputLength: 1,

            templateResult: formatRepo, // omitted for brevity, see the source of this page

            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

        });

    });

</script>




<!-- Magnific popup JavaScript -->
<script src="{{ asset('') }}ui/back-end/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="{{ asset('') }}ui/back-end/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>


<script src="{{ asset('/') }}ui/back-end/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

{{--Datepicker for javascript plugin--}}
{{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

</body>
</html>

