@extends('front-end.partials.master-layout')
@section('content')
    @if (session('resend'))
        <div class="container">
            <div class="col-md-8 offset-2 alert alert-success mt-4">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> {{ session('resend') }}
            </div>
        </div>
     @endif

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-white">{{ __('Verify Your Email Address') }}</div>
                <div class="card-body bg-white">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ url('/email/resend') }}">{{ __('click here to request resend e-mail') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

