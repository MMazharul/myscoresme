@extends('front-end.partials.master-layout')
@section('title', 'Register')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if (session('massage'))
                <div class="col-md-8 alert alert-warning mt-4" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-warning"><i class="fa fa-warning"></i> Warning</h3><strong>{{ session('massage') }}</strong>
                </div>
            @elseif(session('invalid'))
                <div class="col-md-8 alert alert-danger mt-4" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-danger"><i class="fa fa-warning"></i> Error</h3><strong>{{ session('invalid') }}</strong>
                </div>
            @elseif(session('resend'))
                <div class="col-md-8 alert alert-success mt-4" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3><strong>{{ session('resend') }}</strong>
                </div>
            @elseif(session('mailVerified'))
                <div class="col-md-8 alert alert-success mt-4" id="message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3><strong>{{ session('mailVerified') }}</strong>
                </div>
            @endif


            <div class="col-md-8 mt-5">
                <div class="card bg-white">
                    <div class="card-body">
                        <form name="myForm"  onsubmit="return validateCode()" class="form-horizontal" action="{{ url('/mobile-verified') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-12 col-form-label" style="text-transform: none;">
                                    A text message with a 6-digit verification code was just sent to : <strong class="ml-1 control-label">{{Auth::user()->mobile_number}}</strong>
                                </label>
                            </div><hr>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="code">Verification Code</label>
                                    <label class="pull-right">Code expired time : <span class="control-label" style="text-transform: none" id="time"></span></label>
                                    <input id="" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }} mb-0"
                                           name="code" value="{{old('code')}}" placeholder="Enter the code">
                                </div>
                            </div>

                            <div class="form-group m-b-0 mt-3">
                                <input type="submit" value="Next" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3">
                                <label class="ml-3 control-label" style="text-transform: none">Didn't get the code
                                    <a class="text-primary" href="{{ url('/mobile-verified/resend') }}">{{ __('RESEND IT') }}</a>
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

            var oldTime = {{ Auth::user()->time_expired }}
            var newTime = Math.floor(new Date().valueOf() / 1000);

            var timeDistance = newTime - oldTime;


            // var hrs = ~~(timeDistance / 3600);
            var mins = ~~((timeDistance % 3600) / 60);
            var secs = ~~timeDistance % 60;
            // console.log(mins);
            // var date = new Date();
            var sec = secs;
            var min = mins;

            var handler = setInterval(function() {
                if (++sec === 60) {
                    sec = 0;
                    if (++min === 60) min = 0;
                }
                document.getElementById("time").innerHTML = (min < 10 ? "0" + min : min) + ":" + (sec < 10 ? "0" + sec : sec) + "/" + "05:00";
                if (min >= 5 && sec === 0 )
                {
                    clearInterval(handler);
                    $('#time').html('Time Expired').css("color", "red")
                    // document.getElementById("time").innerHTML = ('Time Expired');
                }
            }, 1000);

            if (timeDistance > 300){
                clearInterval(handler);
                $('#time').html('Time Expired').css("color", "red")
                // document.getElementById("time").innerHTML =  ('Time Expired');
            }
    </script>
@endsection


