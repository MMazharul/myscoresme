@extends('front-end.partials.master-layout')
@section('title', 'Register')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if (session('massage'))

                <div class="col-md-8 alert alert-success mt-4">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> {{ session('massage') }}
                </div>

                {{--<div class="col-md-8 alert alert-success mt-4"><i class="fa fa-check-circle"></i> {{ session('massage') }}</div>--}}
            @endif

            <div class="col-md-8 mt-4">
                <div class="card bg-white">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ url('/password/store') }}" role="form" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="text-right control-label col-form-label" style="text-transform: none" for="password">Create Password</label>
                                    <input id="password" type="password"
                                           data-toggle="tooltip" data-placement="left" title="Password must be"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" value="" placeholder="Create your password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                {{--<div id="pswd_info">--}}
                                    {{--<h4>Password must meet the following requirements:</h4>--}}
                                    {{--<ul>--}}
                                        {{--<li id="letter" class="invalid">At least <strong>one letter</strong></li>--}}
                                        {{--<li id="capital" class="invalid">At least <strong>one capital letter</strong></li>--}}
                                        {{--<li id="number" class="invalid">At least <strong>one number</strong></li>--}}
                                        {{--<li id="length" class="invalid">Be at least <strong>8 characters</strong></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}

                                <div id="pswd_info" class="col-md-6">
                                    <ul  id="d1" class="list-group">
                                        <li class="list-group-item list-group-item-success">Password Conditions</li>
                                        <li class="list-group-item" id=d12><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Upper Case Letter</li>
                                        <li class="list-group-item" id=d13 ><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Lower Case Letter </li>
                                        <li class="list-group-item" id=d14><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Special Char </li>
                                        <li class="list-group-item" id=d15><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Number</li>
                                        <li class="list-group-item" id=d16><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Length 8 Char</li>
                                    </ul>
                                </div>


                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="owner_first">Re-type Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Retype Your Password" required>
                                    <span id="span" style="display:none;color:#DD354B">Password does not match</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" value="1" name="policy_agree" type="checkbox"
                                           id="gridCheck" required>
                                    <label class="form-check-label" for="gridCheck">
                                        Declaration will be provided by MyScoreSME authority. After Getting the
                                        statement we will incorporate shortly.
                                    </label>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3">Sign up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <style>
        .list-group {
            z-index: 10;
            display: none;
            position: absolute;
            color: red;
            /*left: 717px;*/
            /*bottom:-23px;*/
            content: "\f0d9";
        }
        #pswd_info {
             position:absolute;
             bottom: -115px\9; /* IE Specific */
             /*content: "\f0d9";*/
             right:55px;
             width:250px;
             padding:15px;
             /*background:#fefefe;*/
             font-size:.875em;
             /*border-radius:5px;*/
             /*box-shadow:0 1px 3px #ccc;*/
             /*border:1px solid #ddd;*/
             left: 717px;
             /*display:none;*/


             /*font-family: FontAwesome;*/
}
        .msg
        {
            position:absolute;
            color:red;
        }
    </style>



    {{--Password valid ui--}}
    <script>
        $(document).ready(function() {
            $('#password').keyup(function(){
                var str=$('#password').val();
                var upper_text= new RegExp('[A-Z]');
                var lower_text= new RegExp('[a-z]');
                var number_check=new RegExp('[0-9]');
                var special_char= new RegExp('[!/\'^�$%&*()}{@#~?><>,|=_+�-\]');

                var flag='T';

                let pass = $('#password').val();

                if (pass){
                    $('#pswd_info').removeClass('display', 'block')
                }


                if(str.match(upper_text)){
                    $('#d12').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> One Upper Case Letter ");
                    $('#d12').css("color", "green");
                }else
                    {$('#d12').css("color", "red");
                     $('#d12').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Upper Case Letter ");
                    flag='F';}

                if(str.match(lower_text)){
                    $('#d13').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> One Lower Case Letter ");
                    $('#d13').css("color", "green");
                }else
                    {$('#d13').css("color", "red");
                     $('#d13').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Lower Case Letter ");
                    flag='F';}

                if(str.match(special_char)){
                    $('#d14').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> One Special Char ");
                    $('#d14').css("color", "green");
                }else{
                    $('#d14').css("color", "red");
                    $('#d14').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Special Char ");
                    flag='F';}

                if(str.match(number_check)){
                    $('#d15').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> One Number ");
                    $('#d15').css("color", "green");
                }else{
                    $('#d15').css("color", "red");
                    $('#d15').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> One Number ");
                    flag='F';}


                if(str.length>7){
                    $('#d16').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> Length 8 Char ");
                    $('#d16').css("color", "green");
                }else{
                    $('#d16').css("color", "red");
                    $('#d16').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Length 8 Char ");
                    flag='F';}


                if(flag=='T'){
                    $("#d1").fadeOut();
                    $('#display_box').css("color","green");
                    $('#display_box').html("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span> "+str);
                }else{
                    $("#d1").show();
                    $('#display_box').css("color","red");
                    $('#display_box').html("<span class='glyphicon glyphicon-remove' aria-hidden='true'></span> "+str);
                }
            });
///////////////////
            $('#t1').blur(function(){
                $("#d1").fadeOut();
            });
///////////
            $('#t1').focus(function(){
                $("#d1").show();
            });
////////////

        })

        /*confirm password validation*/

        $("#password-confirm").keyup(function(){

            let password=$("#password").val();

            let confirmPassword=$("#password-confirm").val();


            if(password!=confirmPassword)
            {
                $("#span").css('display','block');
            }
            else
            {
                $("#span").css('display','none');
            }
        });
    </script>






@endsection


