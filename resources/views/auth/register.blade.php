@extends('front-end.partials.master-layout')
@section('title', 'Register')@section('content')
    <style>
        .capture_button {
            color: gainsboro;
            bottom: 10px;
            border-radius: 50px;
            /* border: 2px solid grey;*/
            background: transparent;
            font-weight: bold;
            margin: 10px;
            outline: none;
            transition: .5s background, border ease;
            cursor: pointer;
        }

        .capture_button:hover {
            background: lightgrey;
            color: black;
            border: 2px solid lightgrey;
        }
        .button_grp {
            width: 100%;
            position: absolute;
            bottom: 10px;
            background: transparent;
        }

        .flex {
            display: flex;
            justify-content: center;
        }
    </style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="mb10 mt20  section-title text-center  ">
                            <!-- section title start-->
                            <h2>MSME Registration</h2>
                            <span style="color: #0a4587"><strong>Smart & Intelligent Credit Scoring & Underwriting Platform</strong></span>
                        </div>
                    </div>
                    <hr>

                    <div class="card-body">
                        <form  id="signupForm" method="post" action="{{ route('register') }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="card">
                                <div class="card-header"><strong class="control-label" style="text-transform: none">Enterprise/Company/Organization  Information</strong> </div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-sm-6" id="error">
                                            <label for="enterprise_name">Enterprise Name (As Per Trade License)</label>
                                            <input id="enterprise_name" type="text"
                                                   class="form-control{{ $errors->has('enterprise_name') ? ' is-invalid' : '' }}"
                                                   name="enterprise_name" value="{{old('enterprise_name')}}" placeholder="enter your enterprise name"  >
                                            @if ($errors->has('enterprise_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('enterprise_name') }}</strong>
                                                </span>
                                            @endif
                                            <span id="msg"></span>
                                        </div>
                                        <div class="form-group col-md-6" >
                                            <label for="organization">Organization Structure</label>
                                            <select  id="organization"  class="organization form-control{{ $errors->has('organization') ? ' is-invalid' : '' }}" name="organization" onchange="showHide()"  >
                                                <option value="0" disabled selected="true">Choose Your Organization Structure</option>
                                                <option value="Sole Proprietorship" {{ old('organization') == "Sole Proprietorship" ? 'selected' : '' }}>Sole Proprietorship</option>
                                                <option value="Partnership" @if(old('organization') == 'Partnership'){{'selected'}}@endif >Partnership</option>
                                                <option value="Public Limited" {{ old('organization') == "Public Limited" ? 'selected' : '' }}>Public Limited</option>
                                                <option value="Private Limited" {{ old('organization') == "Private Limited" ? 'selected' : '' }}>Private Limited</option>
                                                <option value="Co-operative" disabled {{ old('organization') == "Co-operative" ? 'selected' : '' }}>Co-operative</option>
                                            </select>


                                            @if ($errors->has('organization'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('organization') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="industry">Industry</label>
                                            <select class="form-control {{ $errors->has('industry') ? ' is-invalid' : '' }} " id="industry"
                                                    name="industry">
                                                <option disabled selected>---select industry---</option>
                                                @foreach($industries as $id=>$industry)
                                                    <option value="{{$id}}"  {{ old('industry') == $id ? 'selected' : '' }}>{{$industry}}</option>
                                                @endforeach

                                            </select>
                                            @if ($errors->has('industry'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('industry') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div id="sector1" class="form-group col-md-12 hidden">
                                            <label for="sector">Sector</label>
                                            <select class="form-control {{ $errors->has('sector') ? ' is-invalid' : '' }}" id="sector"
                                                    name="sector">
                                                <option disable="true" selected="true">---select sector---</option>

                                            </select>

                                            @if ($errors->has('sector'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('sector') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div id="sub_sector1" class="form-group col-md-12 hidden">
                                            <label for="sub_sector">Sub sector</label>
                                            <select class="form-control {{ $errors->has('sector') ? ' is-invalid' : '' }}" id="sub_sector"
                                                    name="sub_sector">
                                                <option value="0" disable="true" selected="true">---select sub sector---</option>

                                            </select>
                                            @if ($errors->has('sector'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('sector') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header"><strong class="control-label" style="text-transform: none"> Business Address</strong></div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="division1" >Division</label>
                                            <select  id="division1" name="org_division" class="form-control{{ $errors->has('org_division') ? ' is-invalid' : '' }} division1"  aria-required="true" >
                                                <option  disabled="true" selected="true">select division</option>
                                                @foreach($divisions as $division)
                                                    <option value="{{ $division->id}}" {{ old('org_division') == $division->id ? 'selected' : '' }}>{{ $division->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('org_division'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('org_division') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="district1">District</label>
                                            <select id="district1" name="org_district"  class="form-control{{ $errors->has('org_district') ? ' is-invalid' : '' }}  district1"  >
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('org_district'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('org_district') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="upazila1">Upazila</label>
                                            <select id="upazila1" name="org_upazila" class="form-control{{ $errors->has('org_upazila') ? ' is-invalid' : '' }}  upazila1">
                                                <option  disabled="true" selected="true"></option>

                                            </select>
                                            @if ($errors->has('org_upazila'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('org_upazila') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="post_office1">Post Office</label>
                                            <select id="post_office1"  name="orgpost_office"  class="form-control{{ $errors->has('orgpost_office') ? ' is-invalid' : '' }} suboffice1"  >
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('orgpost_office'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('orgpost_office') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-2 postcode1">
                                            <label for="post_code1">Post Code</label>
                                            <input id="post_code1" required="required" value="{{old('post_code1')}}" type="text" name="post_code1" class="form-control{{ $errors->has('post_code1') ? ' is-invalid' : '' }}" readonly >
                                            @if ($errors->has('post_code1'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('post_code1') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="address_1">Address Line</label>
                                            <textarea cols="" rows="3"  id="org_address" placeholder="example : house no #20/a, road #2 "
                                                      class="form-control{{ $errors->has('org_address') ? ' is-invalid' : '' }}" name="org_address" >{{old('org_address')}}</textarea>
                                            @if ($errors->has('org_address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('org_address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card" id="proprietorship" style="display: none">
                                <div class="card-header"><strong class="control-label" style="text-transform: none">Proprietor Information</strong> </div>
                                <div id="ownerapp"  class="card-body bg-white ">

                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="title">Title (Mrs./Mr./Ms.)</label>
                                            <select id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" >
                                                <option disabled selected>Select Your Title</option>
                                                <option value="Mrs."  {{ old('title') == "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                                <option value="Mr."  {{ old('title') == "Mr." ? 'selected' : '' }}>Mr.</option>
                                                <option value="Ms."  {{ old('title') == "Ms." ? 'selected' : '' }}>Ms.</option>
                                            </select>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('title') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="owner_first">First Name</label>
                                            <input id="owner_first" type="text"
                                                   class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                   name="first_name" value="{{old('first_name')}}" placeholder="enter your first name">
                                            @if ($errors->has('first_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="owner_middle">Middle Name</label>
                                            <input id="owner_middle" type="text"
                                                   class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}"
                                                   name="middle_name" value="{{old('middle_name')}}" placeholder="enter your middle name">
                                            @if ($errors->has('middle_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="owner_last">Last Name</label>
                                            <input id="owner_last" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                   name="last_name" value="{{old('last_name')}}" placeholder="enter your last name">
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">

                                        <div class="form-group col-md-4">
                                            <label for="owner_nid">NID Number</label>
                                            <input id="owner_nid" type="text"
                                                   class="form-control{{ $errors->has('owner_nid') ? ' is-invalid' : '' }}"
                                                   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                   name="owner_nid" value="{{old('owner_nid')}}" placeholder="enter your nid number" >
                                            @if ($errors->has('owner_nid'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_nid') }}</strong>
                                                    </span>
                                            @endif

                                            <div id="alert_message" style="color:#D9474F;display:none">
                                                Either 10 Digit OR 13 Digit should be allowed.
                                            </div>

                                        </div>

                                        <div class=" col-md-4" >
                                            <label for="owner_contact">Upload NID Front Side</label>

                                            <input type="file" accept="image/*"  data-max-file-size="4M" id="nid_front_pic" name="nid_front_pic" class="dropify" data-height="150"/>

                                            {{--<span style="border:0px;"  class="form-control">Maximum file size is 512KB picture </span>--}}


                                        </div>

                                        <div class="form-group col-md-4" >
                                            <label for="nid_back_pic">Upload NID Back Side</label>

                                            <input type="file" accept="image/*"  data-max-file-size="4M" id="nid_back_pic" name="nid_back_pic" class="dropify" data-height="150"/>

                                            {{--<span style="border:0px;"  class="form-control">Maximum file size is 512KB picture </span>--}}

                                            @if ($errors->has('nid_back_pic'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('nid_back_pic') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-12" id="dod">
                                            <label for="date_of_birth">Date of Birth</label>
                                            <input id="date_of_birth" type="text"
                                                   class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }} datepicker dob"
                                                   name="date_of_birth" value="{{old('date_of_birth')}}" placeholder="dd-mm-yyyy" readonly>
                                            @if ($errors->has('date_of_birth'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-row">


                                        <div class="form-group col-md-6">
                                            <label for="picture">Capture Your Photo</label>
                                            <a class="form-control{{ $errors->has('capture_pic') ? ' is-invalid' : '' }} btn btn-block btn-outline mb-1"
                                               id="cap_btn" onclick="capture()" data-toggle="modal" data-target="#exampleModal">
                                                <i class="fa fa-camera"></i> Camera</a>

                                            {{--<img class="image-tag" height="100" width="200" alt="smePic">--}}
                                            <input type="hidden" name="capture_pic" class="image-tag">

                                            {{--<input type="file" id="input-file-now" name="nid_back_pic" class="dropify" data-height="150"/>--}}

                                            {{--<input type="file"  name="capture_pic" class="form-control{{ $errors->has('owner_pic') ? ' is-invalid' : '' }}">--}}
                                            {{--@if ($errors->has('capture_pic'))--}}
                                            {{--<span class="invalid-feedback" role="alert">--}}
                                            {{--<strong>{{ $errors->first('capture_pic') }}</strong>--}}
                                            {{--</span>--}}
                                            {{--@endif--}}
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="picture">Upload Your Photo</label>
                                            <input type="file" accept="image/*"  data-max-file-size="4M" id="owner_pic" name="owner_pic" class="dropify" data-height="150"/>

                                            {{--<span style="border:0px;"  class="form-control">Maximam file size is 512KB picture </span>--}}
                                            @if ($errors->has('owner_pic'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('owner_pic') }}</strong>
                                                    </span>
                                            @endif
                                        </div>

                                    </div>




                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="division" >Present Address</label>
                                            <hr class="bg-success mt-0 mb-0">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <div class="row">
                                                <label class="col-sm-6 mr-0" for="present_address1">Permanent Address</label>
                                            </div>
                                            <hr class="bg-dark-blue mt-0 mb-0">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="present_division" >Division</label>
                                            <select  id="present_division" name="present_division" class="form-control{{ $errors->has('present_division') ? ' is-invalid' : '' }} division2" >
                                                <option value="0" disabled="true" selected="true">--select division--</option>
                                                @foreach($divisions as $division)
                                                    <option value="{{ $division->id}}" {{ old('present_division') == $division->id ? 'selected' : '' }}>{{ $division->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('present_division'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_division') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6" id="division">
                                            <div class="row col-sm-12">
                                                <label for="division3" class="col-sm-3 pl-0">Division</label>
                                                <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0">
                                                    <i>*Same as Present Address</i></label>
                                                <input  id="check" onclick="permanentFunction()" type="checkbox" value="" class="col-sm-1 mt-1">
                                            </div>
                                            <select  id="permanent_division" name="permanent_division" class="form-control{{ $errors->has('permanent_division') ? ' is-invalid' : '' }} division3" >
                                                <option value="0" disabled="true" selected="true">--select division--</option>
                                                @foreach($divisions as $division)
                                                    <option value="{{ $division->id}}"{{ old('permanent_division') == $division->id ? 'selected' : '' }}>{{ $division->title }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('permanent_division'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_division') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="present_district">District</label>
                                            <select id="present_district" name="present_district"  class="form-control{{ $errors->has('present_district') ? ' is-invalid' : '' }}  district2" >
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('present_district'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_district') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6" id="district">
                                            <label for="permanent_district">District</label>
                                            <select id="permanent_district" name="permanent_district"  class="form-control district3" >
                                                <option  disabled="true" selected="true"></option>

                                            </select>
                                            {{--@if ($errors->has('permanent_district'))--}}
                                            {{--<span class="invalid-feedback" role="alert">--}}
                                            {{--<strong>{{ $errors->first('permanent_district') }}</strong>--}}
                                            {{--</span>--}}
                                            {{--@endif--}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="present_upazila">Upazila/Area</label>
                                            <select id="present_upazila" name="present_upazila" class="form-control{{ $errors->has('present_upazila') ? ' is-invalid' : '' }}  upazila2" >
                                                {{--<option value="0" disabled="true" selected="true"></option>--}}
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('present_upazila'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_upazila') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6" id="upazila">
                                            <label for="permanent_upazila">Upazila/Area</label>
                                            <select id="permanent_upazila" name="permanent_upazila" class="form-control{{ $errors->has('permanent_upazila') ? ' is-invalid' : '' }}  upazila3" >
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('permanent_upazila'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_upazila') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="presentpost_office">Post Office</label>
                                            <select id="presentpost_office"  name="presentpost_office"  class="form-control{{ $errors->has('presentpost_office') ? ' is-invalid' : '' }} suboffice2" >

                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('presentpost_office'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('presentpost_office') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6" id="postoffice">
                                            <label for="permanentpost_office">Post Office</label>
                                            <select id="permanentpost_office"  name="permanentpost_office"  class="form-control{{ $errors->has('permanentpost_office') ? ' is-invalid' : '' }} suboffice3" >
                                                <option  disabled="true" selected="true"></option>
                                            </select>
                                            @if ($errors->has('permanentpost_office'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanentpost_office') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6 postcode2">
                                            <label for="post_code2">Post Code</label>
                                            <input id="post_code2" required="required" value="{{old('post_code2')}}" type="text" name="post_code2" class="form-control{{ $errors->has('post_code2') ? ' is-invalid' : '' }}" readonly>
                                            @if ($errors->has('post_code2'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('post_code2') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 postcode3">
                                            <label for="permanentpost_code">Post Code</label>
                                            <input id="permanentpost_code" required="required" value="{{old('post_code3')}}" type="text" name="post_code3" class="form-control{{ $errors->has('post_code3') ? ' is-invalid' : '' }}" readonly>
                                            @if ($errors->has('post_code3'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('post_code3') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="present_address">Address line</label>
                                            <textarea id="present_address"  rows="4" type="text"
                                                      class="form-control{{ $errors->has('present_address') ? ' is-invalid' : '' }} present_address"
                                                      name="present_address" placeholder="enter your present address" >{{old('present_address')}}</textarea>
                                            @if ($errors->has('present_address'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('present_address') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 permanent_address" id="permanent" >
                                            <label for="permanent_address">Address line</label>
                                            <textarea id="permanent_address" rows="4" type="text"
                                                      class="form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }} "
                                                      name="permanent_address" placeholder="enter your permanent address" >{{old('permanent_address')}}</textarea>
                                            @if ($errors->has('permanent_address'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('permanent_address') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card" id="partnership"  style="display: none">
                                <div class="card-header" >
                                    <strong class="control-label" style="text-transform: none" id="headlabel"></strong>
                                </div>
                                <div id="ownerapp"  class="card-body bg-white ">

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label id="label" for="total_director_partner"></label>
                                            <input id="total_director_partner"  type="text" oninput="this.value = this.value.replace(/[^1-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                                   class="form-control{{ $errors->has('total_director_partner') ? ' is-invalid' : '' }}"
                                                   name="total_director_partner" value="{{old('total_director_partner')}}" placeholder="" >
                                            @if ($errors->has('total_director_partner'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('total_director_partner') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <button id="btn-next-click" type="submit"  class="btn btn-success pt-3 pb-3 pull-right">NEXT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Camera Modal -->
    <div style="width:auto" class="modal  fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div style="box-shadow: 0px 7px 21px 1px rgba(0,0,0,0.47);width:720px;padding: 0px;
                 border-radius: 20px;border:none; overflow: hidden;" class="modal-content">
                <div style="padding:0" class="modal-body">
                    <button style="margin-top:5px;position: absolute; right: 10px;z-index: 5;color: white;width: 25px;height: 25px;"
                            type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="display: block;line-height: 0.3">&times;</span>
                    </button>

                    <div class="row">
                        <div class="col-12">
                            <div id="my_camera"></div>
                            <div class="button_grp">
                                <div id="btns" class="flex">
                                    <button id="capture" style="display: none" onclick="capture()" class="capture_button">New</button>
                                    <button onclick="take_snapshot()" class="capture_button"> <i class="fa fa-camera" style="font-size: 30px"></i></button>
                                    <button id="save" class="capture_button" style="display: none" onclick="cap_save()" data-dismiss="modal" aria-label="Close">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>

        $('select[name=organization]').click(function()
        {
            value = $("#enterprise_name").val();
            let check= value.split(' ');

            if(value=='')
            {
                $('#msg').html('Field is requerd').css('color', 'red');
                // alert("please first selectl enterprize");
                let meg = 'Please first enter enterprise name ';
                $.alert({
                    title: 'Error!',
                    content: '<span class="text-danger">'+meg+'</span>',
                    type: 'red',
                    typeAnimated: true,
                });
                $("#enterprise_name").keyup(function (){
                    $('#error').children('span').remove();
                });
            }


            if($('#organization').val()=="Public Limited" || $('#organization').val()=="Private Limited")
            {
                if(check.includes('LTD') || check.includes('LIMITED'))
                {

                }

                else
                {
                    let meg = "Error in Enterprise Name, If your enterprise is Private Limited or Public Limited your enterprize name should be "+$("#enterprise_name").val()+' LIMITED';
                    $.alert({
                        title: 'Error!',
                        content: '<span class="text-danger">'+meg+'</span>',
                        type: 'red',
                        typeAnimated: true,
                    });

                    $("#organization").val(0);
                }


            }
            if($('#organization').val()=="Sole Proprietorship" || $('#organization').val()=="Partnership")
            {
                if(check.includes('LTD')||check.includes('LIMITED')) {

                    let meg = 'If your enterprise is Proprietorship/Partnership, then you cannot use LIMITED or LTD after your enterprise name';
                    $.alert({
                        title: 'Error!',
                        content: '<span class="text-danger">'+meg+'</span>',
                        type: 'red',
                        typeAnimated: true,
                    });

                    $("#organization").val(0);

                }

            }

        });

        $("#owner_nid").keyup(function (val) {

            value= $("#owner_nid").val();


            if(value.length==13)
            {
                $("#alert_message").css("display","none");
            }
            else if(value.length==10)
            {
                $("#alert_message").css("display","none");

            }
            else if(value=='')
            {

                $("#alert_message").css("display","none");
            }
            else
            {
                $("#alert_message").css("display","block");
            }
        });

        $("#enterprise_name").keyup(function () {

            this.value = this.value.toUpperCase();
            $("#organization").val(0);
        });

    </script>

    <!-- Configure a few settings and attach camera -->


    <script language="JavaScript">
        function capture() {
            Webcam.set({
                width: 420,
                height: 320,
                image_format: 'jpeg',
                jpeg_quality: 90
            });

            Webcam.attach('#my_camera');
        };


        function take_snapshot() {
            Webcam.snap(function (data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('my_camera').innerHTML = '<img src="' + data_uri + '"/>';
                $('#capture').css('display', "block");
                $('#save').css('display', "block");
            });
        }

        function cap_save() {
            take_snapshot();
            $("#cap_btn").append(" ✔");
        }
    </script>



    <script type="text/javascript">
        function showHide() {
            var hidden = $('#organization').val();
            var   label_1 = $('#lavel');
            var   label_2 = $('#headlabel');
            if (hidden == 'Sole Proprietorship'){
                $('#proprietorship').show();
                $('#enterprise_name_label').html("Number of Partners");
                $('#total_director_partner').attr('min', '2');
            }

            else {
                $('#proprietorship').hide();
            }
            if (hidden == 'Partnership'){
                $('#partnership').show();
                $('#label').html("Number of Partners");
                $('#headlabel').html("Partners Information");
                $('#total_director_partner').attr("placeholder", "enter number of partners");

            }
            else if (hidden == 'Public Limited')
            {
                $('#partnership').show();
                $('#label').html("Number of Directors");
                $('#headlabel').html("Board of Directors Information");
                $('#total_director_partner').attr("placeholder", "enter number of directors");
            }
            else if (hidden == 'Private Limited')
            {
                $('#partnership').show();
                $('#label').html("Number of Directors");
                $('#headlabel').html("Board of Directors Information");
                $('#total_director_partner').attr("placeholder", "enter number of directors");
            }
            else {
                $('#partnership').hide();
            }
        }
    </script>

    <script>
        function permanentFunction() {
            if ($("#check").is(':checked')) {
                var division2 =$('.division2 :selected').text();

                var divisionValue =$('.division2 :selected').val();
                $('.division3').html("<option value="+ divisionValue +">" + division2 + "<option>");
                var district2 = $('.district2 :selected').text();

                var districtValue = $('.district2 :selected').val();
                $('.district3').html("<option value="+districtValue+">" + district2 + "<option>");

                var upazila3 = $('.upazila2 :selected').text();
                var upazilaValue = $('.upazila2 :selected').val();
                $('.upazila3').html("<option value="+upazilaValue+">" + upazila3 + "<option>");
                var suboffice3 = $('.suboffice2 :selected').text();
                var subofficeVallue = $('.suboffice2 :selected').val();
                $('.suboffice3').html("<option  value="+subofficeVallue+">" + suboffice3 + "<option>");
                var postcode3 = $("input:text#post_code2").val();
                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="' + postcode3 + '" readonly>');
                var present_address=$("textarea.present_address").val();
                $('.permanent_address').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "                                                          name=\"permanent_address\" placeholder=\"enter your permanent address\" >"+present_address+"</textarea>");

                var divisionSpanHidden=$('#division').find("span");
                divisionSpanHidden.addClass("hidden");
                var districtSpanHidden=$('#district').find("span");
                districtSpanHidden.addClass("hidden");
                var upazilaSpanHiddden=$('#upazila').find("span");
                upazilaSpanHiddden.addClass("hidden");
                var postSpanHiddden=$('#postoffice').find("span");
                postSpanHiddden.addClass("hidden");

            } else {
                $('.division3').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " @foreach($divisions as $division)\n" + "  <option value=\"{{ $division->id}}\">{{ $division->title }}</option>\n" + " @endforeach");
                $('.district3').html("<option><option>");

                $('.upazila3').html("<option><option>");
                $('.suboffice3').html("<option><option>");
                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="" readonly>');
                $('.permanent_address').html("<label for=\"permanent_address\">Address line</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control{{ $errors->has('permanent_address') ? ' is-invalid' : '' }}\"\n" +
                    "                                                          name=\"permanent_address\" placeholder=\"enter your permanent address\" ></textarea>");


            }
        }
    </script>

    <script>
        $( function() {

            $( ".datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                yearRange: "-120:+0",

                onSelect: function(date) {
                    var dateSplit = date.split("-");
                    var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                    var today = new Date();
                    if (dob.getFullYear() + 18 > today.getFullYear()) {

                        alert('You must be atleast 18 years old to submit register');
                        // $.alert({
                        //     title: 'Alert!',
                        //     content: 'You must be atleast 18 years old to submit register',
                        //     type: 'red',
                        //     typeAnimated: true,
                        // });
                        $(".dob").val("");
                    }

                    let dod = $('#date_of_birth').val();
                    if (dod){
                        $('#dod').children('span').remove();
                    }
                }

            });
        });

    </script>


@endsection
