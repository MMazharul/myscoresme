@extends('front-end.partials.master-layout')

@section('title', 'login')

@section('content')

{{--<div class="container">--}}

    {{--<div class="row justify-content-center mt-4">--}}

        {{--<div class="col-md-8">--}}

            {{--<div class="card">--}}

                {{--<div class="card-header">{{ __('Login') }}</div>--}}

                {{--<div class="card-body">--}}

                    {{--<form method="POST" action="{{ route('login') }}">--}}

                        {{--@csrf--}}

                        {{--<div class="form-group row">--}}

                            {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}



                            {{--<div class="col-md-6">--}}

                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" autofocus>--}}



                                {{--@if ($errors->has('email'))--}}

                                    {{--<span class="invalid-feedback" role="alert">--}}

                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}

                                    {{--</span>--}}

                                {{--@endif--}}

                            {{--</div>--}}

                        {{--</div>--}}



                        {{--<div class="form-group row">--}}

                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}



                            {{--<div class="col-md-6">--}}

                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >--}}



                                {{--@if ($errors->has('password'))--}}

                                    {{--<span class="invalid-feedback" role="alert">--}}

                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}

                                    {{--</span>--}}

                                {{--@endif--}}

                            {{--</div>--}}

                        {{--</div>--}}



                        {{--<div class="form-group row">--}}

                            {{--<div class="col-md-6 offset-md-4">--}}

                                {{--<div class="form-check">--}}

                                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}



                                    {{--<label class="form-check-label" for="remember">--}}

                                        {{--{{ __('Remember Me') }}--}}

                                    {{--</label>--}}

                                {{--</div>--}}

                            {{--</div>--}}

                        {{--</div>--}}



                        {{--<div class="form-group row mb-0">--}}

                            {{--<div class="col-md-8 offset-md-4">--}}

                                {{--<button type="submit" class="btn btn-primary">--}}

                                    {{--{{ __('Login') }}--}}

                                {{--</button>--}}



                                {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}

                                    {{--{{ __('Forgot Your Password?') }}--}}

                                {{--</a>--}}

                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</form>--}}

                {{--</div>--}}

            {{--</div>--}}

        {{--</div>--}}

    {{--</div>--}}

{{--</div>--}}



<div class="limiter">
    <div class="container-login100" style="background-image: url('login-ui/images/bg-01.jpg');">


        <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">

                <form method="POST"  action="{{ route('login') }}">
                    @csrf
                    <span class="login100-form-title p-b-30">Login</span>

                    <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                        <label for="cemail" class="control-label" style="text-transform: none">Username</label>
                        <input id="email" class="form-control input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" name="email" placeholder="Type your username">

                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>

                        @endif
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <label class="control-label" style="text-transform: none">Password</label>
                        <input id="password" class="form-control input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Type your password">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif

                        <span class="focus-input100" data-symbol="&#xf190;"></span>
                    </div>


                    <div class="text-right p-t-8 p-b-31">
                        <a href="{{ route('password.request') }}">
                            Forgot password?
                        </a>
                    </div>



                    {{--<div class="container-login100-form-btn">--}}
                    {{--<div class="wrap-login100-form-btn">--}}
                    {{--<div class="login100-form-bgbtn"></div>--}}
                    {{--<button class="-form-btn">--}}
                    {{--Login--}}
                    {{--</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <button type="submit" onclick="checkLogin()" class="btn btn-primary btn-block p-b-18 p-t-18" ><i class="fa fa-sign-in"></i> Login</button>

                    <div class="txt1 text-center p-b-10 p-t-20">
                        <span>Or</span>
                    </div>

                    <div class="text-center">
                        <span>Don't have an account?</span>
                        <a href="#" class="text-success">
                            <strong> Create Account</strong>
                        </a>
                    </div>
                </form>


        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>

<script>

</script>

{{--<script>--}}
    {{--$("#email").keyup(function(){--}}

        {{--var email = $("#email").val();--}}

        {{--var filter = /@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;--}}
        {{--if (!filter.test(email)) {--}}

{{--//            alert('Please provide a valid email address');--}}

           {{--$("#error").text("");--}}

            {{--email.focus;--}}
            {{--//return false;--}}
        {{--}--}}


        {{--else {--}}
            {{--$("#error_email").text(email+" is not a valid email");--}}
        {{--}--}}
    {{--});--}}
{{--</script>--}}

@endsection

