@extends('front-end.partials.master-layout')
@section('title', 'Register')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <div class="card bg-white">
                    <div class="card-body">
                        <form class="form-horizontal" action="{{ url('/email-phone/store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}


                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="email">Email Address</label>

                                    <input id="email" type="text"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mb-0"
                                           name="email" value="{{old('email')}}" placeholder="Enter your email address" >
                                    <small id="passwordHelpBlock" class="form-text text-muted ml-1">
                                         This email will be your user ID
                                    </small>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                    @endif
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="owner_first">Mobile Number</label>

                                    <input id="owner_first" type="text" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }} mb-0"
                                           name="mobile_number" value="{{old('mobile_number')}}" placeholder="Enter your mobile number">

                                    <small id="passwordHelpBlock" class="form-text text-muted ml-1">
                                       Will be used for verification
                                    </small>
                                    @if ($errors->has('mobile_number'))
                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group m-b-0">

                                @if( Auth::user()->businessInfo->organization == "Sole Proprietorship")
                                    <a href="{{url('register/edit')}}" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb- pull-left">Back</a>
                                @else
                                    <a href="{{url('authorized-representative/edit')}}" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb- pull-left">Back</a>
                                @endif
                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


