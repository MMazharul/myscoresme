<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_category_id');
            $table->unsignedInteger('question_id');
            $table->string('dependent')->default('Yes');
            $table->string('label_en')->nullable();
            $table->string('label_bn')->nullable();
            $table->string('field_type')->nullable();
            $table->string('help_en')->nullable();
            $table->string('help_bn')->nullable();
            $table->string('example_en')->nullable();
            $table->string('example_bn')->nullable();
            $table->boolean('status')->default(0);
            $table->string('column_name')->nullable();
            $table->string('placeholder');
            $table->timestamps();
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('question_category_id')->references('id')->on('question_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependents');
    }
}
