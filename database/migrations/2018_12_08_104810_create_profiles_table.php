<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateProfilesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('profiles', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('business_info_id');
            $table->string('owner_title')->nullable();
            $table->string('owner_first_name')->nullable();
            $table->string('owner_middle_name')->nullable();
            $table->string('owner_last_name')->nullable();
            $table->string('position')->nullable();
            $table->string('shareholder')->nullable();
            $table->integer('owner_nid')->nullable();
            $table->string('nid_front_pic')->nullable();
            $table->string('nid_back_pic')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('owner_pic')->nullable();
            $table->string('capture_pic')->nullable();
            $table->string('present_division')->nullable();
            $table->string('permanent_division')->nullable();
            $table->string('present_district')->nullable();
            $table->string('permanent_district')->nullable();
            $table->string('present_upazila')->nullable();
            $table->string('permanent_upazila')->nullable();
            $table->string('presentpost_office')->nullable();
            $table->string('permanentpost_office')->nullable();
            $table->string('post_code2')->nullable();
            $table->string('post_code3')->nullable();
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->boolean('policy_agree')->default(1);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
//            $table->foreign('business_info_id')->references('id')->on('business_infos')->onDelete('cascade');
            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('profiles');

    }

}

