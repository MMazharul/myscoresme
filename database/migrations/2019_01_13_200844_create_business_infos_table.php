<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('enterprise_name')->nullable();
            $table->string('organization')->nullable();
            $table->string('industry')->nullable();
            $table->string('sector')->nullable();
            $table->string('sub_sector')->nullable();
            $table->string('org_division')->nullable();
            $table->string('org_district')->nullable();
            $table->string('org_upazila')->nullable();
            $table->string('orgpost_office')->nullable();
            $table->string('post_code1')->nullable();
            $table->string('org_address')->nullable();
            $table->string('total_director_partner')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_infos');
    }
}
