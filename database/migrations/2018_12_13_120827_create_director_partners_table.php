<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectorPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('director_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('title');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('contact_number');
            $table->string('email');
            $table->string('date_of_birth');
            $table->string('present_division');
            $table->string('permanent_division');
            $table->string('present_district');
            $table->string('permanent_district');
            $table->string('present_upazila');
            $table->string('permanent_upazila');
            $table->string('presentpost_office');
            $table->string('permanentpost_office');
            $table->string('post_code2');
            $table->string('post_code3');
            $table->string('present_address');
            $table->string('permanent_address');
            $table->string('nid_no');
            $table->string('nid_front_pic');
            $table->string('nid_back_pic');
            $table->string('picture');
            $table->string('owner_e_tin');
            $table->string('owner_etin_pic');
            $table->string('passport_no');
            $table->string('passport_pic');
            $table->string('driving_license');
            $table->string('driving_license_pic');
            $table->string('account_title');
            $table->string('account_no');
            $table->string('bank');
            $table->string('branch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('director_partners');
    }
}
