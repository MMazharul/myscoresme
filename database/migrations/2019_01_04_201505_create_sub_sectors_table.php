<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_sectors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sector_id')->unsigned()->nullable();
            $table->string('sub_sector')->nullable();
            $table->integer('sub_sector_code')->nullable();
            $table->integer('code')->nullable();
            $table->boolean('status')->default(1)->nullable();
            $table->timestamps();
            $table->foreign('sector_id')->references('id')->on('sectors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_sectors');
    }
}
