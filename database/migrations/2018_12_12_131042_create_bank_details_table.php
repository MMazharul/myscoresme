<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('category')->nullable();
            $table->string('organization_name')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('country_incorporation')->nullable();
            $table->string('year_incorporation')->nullable();
            $table->string('registration_number')->nullable();
            $table->string('type_organization')->nullable();
            $table->string('division')->nullable();
            $table->string('district')->nullable();
            $table->string('upazila')->nullable();
            $table->string('post_office')->nullable();
            $table->string('post_code4')->nullable();
            $table->string('retype_email')->nullable();
            $table->string('alternate_email')->nullable();
            $table->string('professional_staff')->nullable();
            $table->string('branch_number')->nullable();
            $table->string('bank_logo')->nullable();
            $table->string('bank_attachment')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_details');
    }
}
