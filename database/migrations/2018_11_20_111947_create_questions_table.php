<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_category_id')->unsigned()->nullable();
            $table->string('label_en')->nullable();
            $table->string('label_bn')->nullable();
            $table->string('field_type')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->integer('step')->nullable();
            $table->string('help_en')->nullable();
            $table->string('help_bn')->nullable();
            $table->string('example_en')->nullable();
            $table->string('example_bn')->nullable();
            $table->boolean('status')->default(0);;
            $table->string('column_name')->nullable();
            $table->string('placeholder');
            $table->timestamps();

            $table->foreign('question_category_id')->references('id')->on('question_categories');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
