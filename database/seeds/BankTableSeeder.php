<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bankSeede = [
            [
                'bank_name' => 'Dutch-Bangla Bank',
                'address' => 'Sena Kalyan Bhaban, 4th Floor 195 Motijheel Commercial AreaDhaka-1000, Bangladesh',
                'mobile' => '(8802) 47110465',
                'fax' => ' (8802) 9561889',
                'email' => 'hossain.mosharraf@dutchbanglabank.com',
                'website' => 'https://www.dutchbanglabank.com/',
                'swift_code' => 'DBBLBDDH',
                'branch_type' => 'General',
                'loan_type' => 'long term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Bangladesh Commerce Bank Limited',
                'address' => 'Dilkusha C/A Motijheel Dhaka-1000',
                'mobile' => '(8802) 47110465',
                'fax' => '+880-2-9568218',
                'email' => 'info@bcbl.com.bd',
                'website' => 'http://www.bcblbd.com',
                'swift_code' => 'BCBLBDDH',
                'branch_type' => 'sme',
                'loan_type' => 'long term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Bank Asia Limited',
                'address' => 'Rangs Tower (Level 2 to Level 8) 68, Purana Paltan Dhaka - 1000',
                'mobile' => '+88-02- 47110062',
                'fax' => '+88-02-7175524 ',
                'email' => 'contact.center@bankasia-bd.com',
                'website' => 'http://www.bankasia-bd.com',
                'swift_code' => 'CCBLBDAA',
                'branch_type' => 'General',
                'loan_type' => 'long term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'BRAC Bank Limited',
                'address' => 'Anik Tower 220/B, Tejgaon Gulshan Link RoadTejgaon, Dhaka 1208',
                'mobile' => '8801301-32, 8801311',
                'fax' => ' (8802) 9561889',
                'email' => 'enquiry@bracbank.com',
                'website' => 'https://www.bracbank.com',
                'swift_code' => 'BRAKBDDH',
                'branch_type' => 'sme',
                'loan_type' => 'long term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'City Bank Limited',
                'address' => '136, Gulshan Avenue, Gulshan-2 Dhaka-1212, Bangladesh ',
                'mobile' => '(8802) 47110465',
                'fax' => '02 9884446',
                'email' => 'hossain.mosharraf@dutchbanglabank.com',
                'website' => 'https://www.thecitybank.com/',
                'swift_code' => 'DBBLBDDH',
                'branch_type' => 'General',
                'loan_type' => 'medium term',
                'establish' => '1993',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Dhaka Bank Limited',
                'address' => 'Sena Kalyan Bhaban, 4th Floor 195 Motijheel Commercial AreaDhaka-1000, Bangladesh',
                'mobile' => '(8802) 47110465',
                'fax' => ' (8802) 9561889',
                'email' => 'https://dhakabankltd.com',
                'website' => 'https://dhakabankltd.com/',
                'swift_code' => 'HHNNBBCC',
                'branch_type' => 'General',
                'loan_type' => 'medium term',
                'establish' => '1995-06-05',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Dutch-Bangla Bank Limited',
                'address' => 'Sena Kalyan Bhaban, 4th Floor 195 Motijheel Commercial AreaDhaka-1000, Bangladesh',
                'mobile' => '(8802) 47110465',
                'fax' => ' (8802) 9561889',
                'email' => 'hossain.mosharraf@dutchbanglabank.com',
                'website' => 'AB Bank Limited',
                'swift_code' => 'DBBLBDDH',
                'branch_type' => 'Islamic',
                'loan_type' => 'medium term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Eastern Bank Limited',
                'address' => '100 Gulshan Avenue Gulshan, Dhaka-1212,Bangladesh',
                'mobile' => '+ 88 09666777325',
                'fax' => ' (8802) 9561889',
                'email' => 'info@ebl.com.bd',
                'website' => 'https://www.ebl.com.bd/',
                'swift_code' => 'EBLDBDDH',
                'branch_type' => 'General',
                'loan_type' => 'short term',
                'establish' => '1992-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'IFIC Bank Limited',
                'address' => 'IFIC Tower 61 Purana Paltan, Dhaka-1000 Bangladesh',
                'mobile' => '(8802) 47110465',
                'fax' => '880-2- 9587154',
                'email' => 'info@ificbankbd.com ',
                'website' => 'https://www.ificbank.com.bd',
                'swift_code' => 'IFIC BD DH',
                'branch_type' => 'Islamic',
                'loan_type' => 'long term',
                'establish' => '1976-02-18',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'bank_name' => 'Jamuna Bank Limited',
                'address' => 'Hadi Mansion,2, Dilkusha C/A,Dhaka - 1000, Bangladesh',
                'mobile' => '+88 02 9570912',
                'fax' => ' (8802) 9561889',
                'email' => 'info@jamunabank.com.bd',
                'website' => 'http://jamunabankbd.com',
                'swift_code' => 'JAMUBDDH',
                'branch_type' => 'General',
                'loan_type' => 'short term',
                'establish' => '1995-02-08',
                'status'    => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],


        ];

        \Illuminate\Support\Facades\DB::table('banks')->insert($bankSeede);
    }
}
