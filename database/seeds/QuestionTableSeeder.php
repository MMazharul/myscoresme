<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questionSeed = [
            [
                'label_en' => 'Name Of the Firm/Enterprise',
                'label_bn' => 'Name Of the Firm/Enterprise',
                'field_type' => 'text',
                'help_en' => 'Firm Help English',
                'help_bn' => 'Firm Help Bangla',
                'example_en' => 'Example Firm Enaglish',
                'example_bn' => 'Example Firm Bangla',
                'status' => 1,
                'column_name' => 'enterprize',
                'placeholder' => "Please write your Firm/Enterprise",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'What is the nature of the Firm/Enterprise(Bangla)',
                'label_bn' => 'What is the nature of the Firm/Enterprise(Bangla)',
                'field_type' => 'radio',
                'help_en' => 'Firm Help English',
                'help_bn' => 'Firm Help Bangla',
                'example_en' => 'Example Firm Enaglish',
                'example_bn' => 'Example Firm Bangla',
                'status' => 1,
                'column_name' => 'firm',
                'placeholder' => "Please Select Firm Nature",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'you started your business',
                'label_bn' => 'you started your business(bangla))',
                'field_type' => 'calender',
                'help_en' => 'Year of Birth Help English',
                'help_bn' => 'Year of Birth Help Bangla',
                'example_en' => 'Example Year of Birth Enaglish',
                'example_bn' => 'Example Year of Birth Bangla',
                'status' => 1,
                'column_name' => 'business_startd',
                'placeholder' => "Enter Your started business time",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Your Business Address ',
                'label_bn' => 'Your Business Address (bangla)',
                'field_type' => 'textarea',
                'help_en' => 'Year of Birth Help English',
                'help_bn' => 'Year of Birth Help Bangla',
                'example_en' => 'Example Your Business Address ',
                'example_bn' => 'Example Your Business Address (bangla)',
                'status' => 1,
                'column_name' => 'business_address',
                'placeholder' => "Enter Your started business address",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Where is the enterprise located ',
                'label_bn' => 'Where is the enterprise located )',
                'field_type' => 'radio',
                'help_en' => 'Where is the enterprise located ',
                'help_bn' => 'Where is the enterprise located (bangla)',
                'example_en' => 'Example Where is the enterprise located ',
                'example_bn' => 'Example Where is the enterprise located ',
                'status' => 1,
                'column_name' => 'business_located',
                'placeholder' => "Enter Total Asset of your Business",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Nature Of Business ',
                'label_bn' => 'Nature Of Business (bangla)',
                'field_type' => 'checkbox',
                'help_en' => 'Nature Of Business ',
                'help_bn' => 'Nature Of Business (bangla)',
                'example_en' => 'Nature Of Business ',
                'example_bn' => 'Nature Of Business a',
                'status' => 1,
                'column_name' => 'nature',
                'placeholder' => "please write your business nature",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Number of Employees',
                'label_bn' => 'Number of Employees',
                'field_type' => 'number',
                'help_en' => 'Number of Employees',
                'help_bn' => 'Number of Employees',
                'example_en' => 'Number of Employees',
                'example_bn' => 'Number of Employees',
                'status' => 1,
                'column_name' => 'business_employee',
                'placeholder' => "Enter Your business employee",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Total Asset of your business (Amount in BDT)',
                'label_bn' => 'Total Asset of your business (Amount in BDT)',
                'field_type' => 'number',
                'help_en' => 'Total Asset of your business (Amount in BDT)',
                'help_bn' => 'Total Asset of your business (Amount in BDT)',
                'example_en' => 'Total Asset of your business (Amount in BDT)',
                'example_bn' => 'Total Asset of your business (Amount in BDT)',
                'status' => 1,
                'column_name' => 'business_asset',
                'placeholder' => "Enter Your Business Asset",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'label_en' => 'Owner Name',
                'label_bn' => 'Owner Name',
                'field_type' => 'text',
                'help_en' => 'Owner Name',
                'help_bn' => 'Owner Name',
                'example_en' => 'Owner Name',
                'example_bn' => 'Owner Name',
                'status' => 1,
                'column_name' => 'owner_name',
                'placeholder' => "Enter Your Owner Name",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
             ],
            [
                'label_en' => 'Marital Status',
                'label_bn' => 'Marital Status',
                'field_type' => 'select',
                'help_en' => 'Marital Status',
                'help_bn' => 'Marital Status',
                'example_en' => 'Marital Status',
                'example_bn' => 'Marital Status',
                'status' => 1,
                'column_name' => 'marital_status',
                'placeholder' => "Enter Your Marital Status",
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]
        ];

        \Illuminate\Support\Facades\DB::table('questions')->insert($questionSeed);
    }
}
