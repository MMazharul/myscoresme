<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [

            [

                'role_id' =>  \App\User::ADMIN_USER,

                'title' => 'mr',

                'first_name' => 'admin',

                'middle_name' => 'admin',

                'last_name' => 'admin',

                'mobile_number' => '123456789',

                'email' => 'admin@admin.com',

                'password' => '$2y$10$4.MBjlWDjswGAeEkgraL4OXiZM014z6y9hH/bfSKVS8K3.lQykrXG',

                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ]


        ];

        \Illuminate\Support\Facades\DB::table('users')->insert($admin);
    }
}
