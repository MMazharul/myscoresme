<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $orgTitle = [
            [
                'org_title' => 'Sole Proprietorship',
                'option' => 'Name of The Owner',
                'other' => null,

            ],
            [
                'org_title' => 'Partnership',
                'option' => 'Name of The Managing Partner',
                'other' => 'Number of partners'
            ],
            [
                'org_title' => 'PLC',
                'option' => 'Name of The Managing Director',
                'other' => null

            ]

        ];

        \Illuminate\Support\Facades\DB::table('organizatrion_titles')->insert($orgTitle);

    }
}