## MyScoresme
Smart & Intelligent Credit Scoring & Underwriting Platform

## How to Install

Run the following commands in composer: 

```
- git clone git@gitlab.com:mzrusdid/apams.git

- composer install
```

Add a `.env` file and config your database connection. And finally:

```
- composer dump-autoload

- php artisan key:generate

- php artisan migrate

- php artisan db:seed
```
