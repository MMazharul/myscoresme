<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectorPartners extends Model
{

    protected $fillable = ['user_id',
        'title', 'first_name', 'middle_name', 'last_name', 'contact_number', 'email',
        'date_of_birth', 'present_division','permanent_division','present_district',
        'permanent_district','present_upazila','permanent_upazila','presentpost_office',
        'permanentpost_office', 'post_code2', 'post_code3', 'present_address', 'permanent_address',
        'nid_no','nid_front_pic','nid_back_pic','picture','owner_e_tin','owner_etin_pic','passport_no',
        'passport_pic','driving_license','driving_license_pic','account_title','account_no','bank','branch'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
