<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    protected $fillable=['user_id','category','affilate_name','division','district','upazila','post_office','post_code4','address_1','joining_date','designation','owner_number','contact_email','trade_license','expire_date'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
