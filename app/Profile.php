<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class Profile extends Model

{

    public function user(){

        return $this->belongsTo(User::class);

    }

    public function businessInfo()
    {
        return $this->belongsTo(BusinessInfo::class);
    }



    protected $fillable = ['user_id','business_info_id',
        'owner_title','owner_first_name','owner_middle_name',
        'owner_last_name','position','shareholder', 'owner_nid', 'nid_front_pic', 'nid_back_pic', 'date_of_birth', 'owner_pic', 'capture_pic',

        'present_division', 'permanent_division', 'present_district', 'permanent_district', 'present_upazila', 'permanent_upazila',
        'presentpost_office', 'permanentpost_office', 'post_code2', 'post_code3', 'present_address', 'permanent_address',
        'policy_agree'];

}

