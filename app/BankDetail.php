<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $fillable=['user_id','category','organization_name','description','country_incorporation','year_incorporation','registration_number','type_organization','division','district','upazila','post_office','post_code4','retype_email','alternate_email','professional_staff','branch_number','bank_logo','bank_attachment'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function branchDetail(){
        return $this->hasMany(BankDetail::class);
    }

}
