<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSector extends Model
{
    protected $fillable = ['sector_id','sub_sector','sub_sector_code','status'];

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }
}
