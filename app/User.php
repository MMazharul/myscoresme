<?php



namespace App;



use Illuminate\Notifications\Notifiable;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Pondit\Authorize\Models\Authorizable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable implements MustVerifyEmail
{

    use Notifiable;

    use Authorizable;


    const ADMIN_USER = 1;

    const SME_USER = 2;

    const BANK_USER = 3;

    const BRANCH_USER=4;

    const AFFILIATE_USER=5;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */



    protected $fillable = ['role_id','title','first_name', 'middle_name', 'last_name', 'mobile_number', 'email', 'password', 'time_expired','verified'];



    public function profile(){

        return $this->hasOne(Profile::class);

    }
    public function businessInfo()
    {
        return $this->hasOne(BusinessInfo::class);
    }

    public function contactInfo(){

        return $this->hasOne(ContactInfo::class);

    }
    public function branchdetail(){

        return $this->hasOne(BranchDetail::class);

    }
    public function bankdetail()
    {
        return $this->hasOne(BankDetail::class);
    }
    public function affiliate()
    {
        return $this->hasOne(Affiliate::class);
    }







    public function directorPartners()

    {

        return $this->hasMany(DirectorPartners::class);

    }

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }





    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */



    protected $hidden = [

        'password', 'remember_token',

    ];







}

