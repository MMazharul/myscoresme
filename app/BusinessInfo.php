<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessInfo extends Model
{

    protected $fillable = ['user_id','enterprise_name','organization', 'industry', 'sector', 'sub_sector', 'org_division', 'org_district', 'org_upazila','orgpost_office','post_code1','org_address','total_director_partner'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function profile()
    {
        return $this->hasMany(Profile::class);
    }
}
