<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmeOtherImage extends Model
{
    protected $fillable = ['sme_attach_id','title','picture'];

    public function SmeAttach()
    {
        $this->belongsTo(SmeAttachment::class);
    }
}
