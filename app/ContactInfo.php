<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    protected $fillable=[
        'user_id',
        'title',
        'first_name',
        'middle_name',
        'last_name',
        'position',
        'shareholder',
        'date_of_birth',
        'nid_no',
        'present_division',
        'present_district',
        'present_upazila',
        'present_postoffice',
        'present_postcode',
        'permanent_division',
        'permanent_district',
        'permanent_upazila',
        'permanent_postoffice',
        'permanent_postcode',
        'present_address',
        'permanent_address',
    ];

    public function user(){

        return $this->belongsTo(User::class);

    }

}
