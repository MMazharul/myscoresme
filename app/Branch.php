<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
        protected $fillable = ['bank_id', 'division_id','district_id','branch_name'];

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
