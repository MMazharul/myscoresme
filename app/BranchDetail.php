<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchDetail extends Model
{
    protected $fillable=['user_id','bank_id','branch_name','date_estabilish','division','district','upazila','post_office','post_code4','telephone','owner_number','retype_email','alternate_email','profesional_staff','designation','joining_date','owner_number','contact_email','routing_number','attachment'];

    public function bankDetail(){
        return $this->belongsTo(BankDetail::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
