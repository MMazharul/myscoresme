<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable=['title','geo_code','status'];
    public function districts()
    {
        return $this->hasMany(District::class);
    }
}
