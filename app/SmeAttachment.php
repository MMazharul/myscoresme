<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmeAttachment extends Model
{
    protected $fillable = ['business_info_id','trade_license','expire_date','trade_license_pic',
        'e_tin', 'e_tin_pic', 'bin', 'bin_pic','partnership_agreement', 'partnership_agreement_pic',
        'certificate_incorporation', 'certificate_incorporation_pic','company_account_title',
        'company_account_no', 'company_bank', 'company_branch','bank_division', 'bank_district'];

    public function businessInfo()
    {
        $this->belongsTo(BusinessInfo::class);
    }

    public function SmeOtherImage()
    {
        $this->hasMany(SmeOtherImage::class);
    }
}
