<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    protected $fillable=['postoffice_id','postoffice_name','post_code','status'];

   public function  postoffice()
   {
       return $this->belongsTo(PostOffice::class);
   }
}
