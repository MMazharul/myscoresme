<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCategory extends Model
{
    protected $fillable=['question_category','status'];


    public function questions()
    {
        return $this->hasMany(Question::class);
    }

}
