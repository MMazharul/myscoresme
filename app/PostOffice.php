<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostOffice extends Model
{

    protected $fillable=['upazila_id','upazila_name','title','status'];

    public function upazila()
    {
        return $this->belongsTo(Upazila::class);
    }

    public function postcode()
    {
        return $this->hasOne(Postcode::class,'postoffice_id');
    }
}
