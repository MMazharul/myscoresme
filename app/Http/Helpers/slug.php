<?php
/**
 * Created by PhpStorm.
 * User: BS23
 * Date: 29-Nov-18
 * Time: 8:29 PM
 */


function tableNameConvert($str)
{
      $tableNameLower = strtolower($str);
      $tableNamePlural = str_plural($tableNameLower);
    return str_replace(array(' ',';',':','\'','(',')','[',']'),'_', $tableNamePlural);
}

