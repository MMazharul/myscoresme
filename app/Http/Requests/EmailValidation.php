<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'mobile_number' => 'required|numeric|digits_between:11,11',

        ];
    }

    public function messages()
    {

        return [

            'mobile_number.digits_between'=>'This Mobile Number should be 11 intergers.',

        ];

    }
}
