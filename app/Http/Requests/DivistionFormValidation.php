<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DivistionFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:divisions',
            'geo_code'=>'required|numeric|unique:divisions',
        ];
    }
    public function messages()
    {
        return
       [
           'title.required'=>'The Division field is required.',
           'title.required.unique.divisions'=>'The Division has already been taken..',
       ];
    }
}
