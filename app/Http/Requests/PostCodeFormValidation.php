<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCodeFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'postoffice_id'=>'required|integer',
            'post_code'=>'required|numeric|unique:postcodes',
        ];
    }
    public function messages()
    {
        return
            [
                'post_code.required'=>'The Post Code field is required.',
            ];
    }
}
