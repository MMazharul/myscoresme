<?php

namespace App\Http\Requests;

use App\Division;
use Illuminate\Foundation\Http\FormRequest;

class DistrictFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'division_id'=>'required|integer',
            'title'=>'required|unique:districts',
            'geo_code'=>'required|numeric|unique:districts',
        ];
    }
    public function messages()
    {
        return [
            'division_id.required.interger'=>'The Division field is required.',
            'title.required'=>'The District field is required.',
        ];
    }
}
