<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpazilaFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id'=>'required|integer',
            'title'=>'required|unique:upazilas',
            'geo_code'=>'required|numeric|unique:upazilas',

        ];
    }
    public function messages()
    {
        return
            [
                'title.required'=>'The Upazila field is required.',

            ];
    }
}
