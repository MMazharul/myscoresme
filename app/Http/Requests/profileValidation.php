<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class profileValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
   /* public function authorize()
    {
        return false;
    }*/

    public function authorize() {


        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enterprise_name' => 'required',
            'organization' => 'required',
            'total_director' => 'required',
            'industry' => 'required',
            'sector' => 'required',
            'org_division' => 'required',
            'org_district' => 'required',
            'org_upazila' => 'required',
            'orgpost_office' => 'required',
            'post_code1' => 'required',
            'title' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'owner_contact' => 'required',
            'address_1' => 'required',
            'owner_nid' => 'required',
            'date_of_birth' => 'required',
            'owner_pic' => 'required',
            'present_division' => 'required',
            'permanent_division' => 'required',
            'present_district' => 'required',
            'permanent_district' => 'required',
            'present_upazila' => 'required',
            'permanent_upazila' => 'required',
            'presentpost_office' => 'required',
            'permanentpost_office' => 'required',
            'post_code2' => 'required',
            'post_code3' => 'required',
            'present_address' => 'required',
            'permanent_address' => 'required',
            'contact_name' => 'required',
            'contact_middle' => 'required',
            'contact_last' => 'required',
            'position' => 'required',
            'contact_number' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:3|confirmed',
        ];
    }
}


