<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionCategoryValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'question_category'=>'required|unique:question_categories',
            'status'=>'required',
        ];
    }
        public function messages()
    {
        return
            [
                'question_category.required'=>'The Division field is required.',
                'question_category.required.unique.question_categories'=>'The category has already been taken..',
            ];
    }

}
