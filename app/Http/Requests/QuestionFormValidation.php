<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionFormValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_category_id'=>'required|integer',
            'label_en'=>'required',
            'label_bn'=>'required',
            'field_type'=>'required|alpha',
            'help_en'=>'required',
            'help_bn'=>'required',
            'example_en'=>'required',
            'example_bn'=>'required',
            'status'=>'required',

        ];


    }

    public function messages()
    {
        return
            [
                'field_type.required.alpha'=>'The field_type field is required',
            ];
    }

}
