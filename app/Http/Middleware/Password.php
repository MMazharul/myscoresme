<?php

namespace App\Http\Middleware;

use Closure;

class Password
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->mobile_verified == 1)
        {
            return $next($request);
        }

        if(auth()->user()->verified==0)
        {
//          return view('errors.404');

            return response()->view('errors.404', [], 500);

        }

        if(auth()->user()->password == null){
//            return abort(404);
            return  abort(403, 'Unauthorized action.');
//            return view('errors.404');
        }

    }
}
