<?php



namespace App\Http\Middleware;



use App\User;

use Closure;

//use Illuminate\Support\Facades\Auth;

use Pondit\Authorize\Models\Role;

use Auth;



class RedirectIfAuthenticated

{

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \Closure  $next

     * @param  string|null  $guard

     * @return mixed

     */

    public function handle($request, Closure $next, $guard = null)

    {

   /*     if (Auth::guard($guard)->check()) {

            return redirect('/sme-dashboard');

        }*/

           if(Auth::guard($guard)->check()) {



               if (Auth::user()->role_id == User::ADMIN_USER) {

                   return redirect('/admin');

               }
               else if(Auth::user()->role_id == User::SME_USER) {

                  /* if (Auth::user()->profile->organization == 'Partnership' || Auth::user()->profile->organization == 'Public Limited' || Auth::user()->profile->organization == 'Private Limited')
                   {
                       return redirect('/partner-director-info');
                   }*/


               }

               else if(Auth::user()->role_id == User::BANK_USER)

               {

                   return redirect('/bank-dashboard');

               }

               else if(Auth::user()->role_id == User::BRANCH_USER)

               {

                   return redirect('/branch-dashboard');

               }

               else if(Auth::user()->role_id == User::AFFILIATE_USER)

               {

                   return redirect('/affiliate-dashboard');

               }

           }

        return $next($request);

    }

}

