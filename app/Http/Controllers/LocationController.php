<?php



namespace App\Http\Controllers;



use App\Branch;
use App\District;

use App\Division;

use App\Postcode;

use App\PostOffice;

use App\Upazila;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;



class LocationController extends Controller

{

    public function index()
    {
        $divisions=Division::all();

        return view('admin.location.getlocation',compact('divisions'));

    }

    public function findDistrict()
    {
        $divistion_id = Input::get('division_id');
        $data=District::where('division_id',$divistion_id)->get();

        return response()->json($data);
    }

    public function findThana(Request $request)

    {
        $district_id = Input::get('district_id');
        $data=Upazila::where('district_id',$district_id)->get();
        return response()->json($data);
    }



    public function subOffices(Request $request)
    {

        $thana_id = Input::get('thana_id');

        $data=PostOffice::where('upazila_id',$thana_id)->get();

        return response()->json($data);



    }

    public function postcode()

    {

        $postcode_id=Input::get('postoffice_id');

        $data=Postcode::where('postoffice_id',$postcode_id)->first();

        return response()->json($data);

    }


    public function branchInfo()
    {
        $bank_id = Input::get('company_bank');
        $district_id = Input::get('bank_district');
        $division_id = Input::get('bank_division');

       $branches = DB::table('branches')->where('bank_id',$bank_id)
            ->where('division_id', $division_id)->where('district_id',$district_id)->select('branch_name')->get();


        return response()->json($branches);
    }



}

