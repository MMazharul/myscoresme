<?php

namespace App\Http\Controllers\Auth;

use App\BusinessInfo;
use App\Http\Controllers\SmeQuestion\SmeQuestionController;
use App\Http\Requests\profileValidation;
use App\Http\Requests\UserValidation;
use App\Mail\OrderShipped;
use App\Profile;
use App\Question;
use App\QuestionCategory;
use App\SubSector;
use App\User;

use App\VerifyUser;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use App\District;
use App\IndustryType;
use App\PostOffice;
use App\Sector;
use App\Upazila;
use App\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Intervention\Image\Facades\Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */



//    protected $redirectTo = '/email-phone';


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }



    public function showRegistrationForm()
    {
        Session::forget('directorsParners');
        $divisions = Division::all();
        $industries = IndustryType::where('status', 1)->orderBy('industry_type', 'asc')->pluck('industry_type', 'id');

        $sectors = Sector::where('status', 1)->orderBy('sector_name', 'asc')->pluck('sector_name');

        return view('auth.register', compact('divisions', 'industries', 'sectors'));
    }

    public function emailPhoneForm()
    {
        return view('auth.email-phone');
    }

    public function passwordForm()
    {
        return view('auth.sme-password')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    public function rules()
    {
        $rules= array(
            "enterprise_name"=>"required",
            "organization"=>"required",
            "industry"=>"required",
            "org_division"=>"required",
            "org_district"=>"required",
            "org_upazila"=>"required",
            "orgpost_office"=>"required",
            "post_code1"=>"required",
            "org_address"=>"required",

        );
        return $rules;
    }

    public  function RegisterValidation($data)
    {
        $rules=$this->rules();
        if(isset($data['organization']))
        {
            if($data['organization']=="Sole Proprietorship")
            {
                $rules['title']="required";
                $rules['first_name']="required";
                $rules['last_name']="required";
                $rules['owner_nid']="required";
//                $rules['nid_front_pic']="required";
//                $rules['nid_back_pic']="required";
                $rules['date_of_birth']="required";
//                $rules['owner_pic']="required";
//               $rules['capture_pic']="required";
                $rules['present_division']="required";
                $rules['permanent_division']="required";
                $rules['present_district']="required";
                $rules['permanent_district']="required";
                $rules['present_upazila']="required";
                $rules['permanent_upazila']="required";
                $rules['presentpost_office']="required";
                $rules['permanentpost_office']="required";
                $rules['post_code2']="required";
                $rules['post_code3']="required";
                $rules['present_address']="required";
                $rules['permanent_address']="required";
            }
            else if($data['organization']=="Partnership" || $data['organization']=="Public Limited" ||$data['organization']="Private Limited")
            {
                $rules['total_director_partner']="required";
            }
        }



        return Validator::make($data, $rules);

    }


    public function businessData($businessInfo,$request)
    {
        session()->forget('business');

        $industry=IndustryType::where('id',$request->industry)->pluck('industry_type')->first();
        $sector=Sector::where('id',$request->sector)->pluck('sector_name')->first();
        $SubSector=SubSector::where('id',$request->sector)->pluck('sub_sector')->first();

        $orgDivision = Division::where('id', $request->org_division )->pluck('title')->first();
        $orgtDistrict = District::where('id', $request->org_district)->pluck('title')->first();
        $orgUpazila = Upazila::where('id', $request->org_upazila)->pluck('title')->first();
        $orgPostOffice = PostOffice::where('id', $request->orgpost_office)->pluck('title')->first();

        $businessInfo['org_division'] = $orgDivision;
        $businessInfo['org_district'] = $orgtDistrict;
        $businessInfo['org_upazila'] = $orgUpazila;
        $businessInfo['orgpost_office'] = $orgPostOffice;

        $businessInfo['industry']=$industry;
        $businessInfo['sector']=$sector;
        $businessInfo['sub_sector']=$SubSector;

        Session::put('business',$businessInfo);

        return $businessInfo;

    }



    public function profileData($profile,$request)
    {
        session()->forget('profileInfo');

        $presentDivision = Division::where('id', $request->present_division )->pluck('title')->first();
        $PermanentDivision = Division::where('id', $request->permanent_division )->pluck('title')->first();

        $presentDistrict = District::where('id', $request->present_district)->pluck('title')->first();
        $permanentDistrict = District::where('id', $request->permanent_district)->pluck('title')->first();

        $presentUpazila = Upazila::where('id', $request->present_upazila)->pluck('title')->first();
        $permanentUpazila = Upazila::where('id', $request->permanent_upazila)->pluck('title')->first();

        $presentPostOffice = PostOffice::where('id', $request->presentpost_office)->pluck('title')->first();
        $permanentPostOffice = PostOffice::where('id', $request->permanentpost_office)->pluck('title')->first();

        $profile['present_division'] = $presentDivision;
        $profile['permanent_division'] = $PermanentDivision;
        $profile['present_district'] = $presentDistrict;
        $profile['permanent_district'] = $permanentDistrict;
        $profile['present_upazila'] = $presentUpazila;
        $profile['permanent_upazila'] = $permanentUpazila;
        $profile['presentpost_office'] = $presentPostOffice;
        $profile['permanentpost_office'] = $permanentPostOffice;

        Session::put('profileInfo',$profile);

        return $profile;

    }


    public function permanentAddress($profile,$request)
    {
        session()->forget('permanentInfo');


        $PermanentDivision = Division::where('id', $request->permanent_division )->pluck('title')->first();
        $permanentDistrict = District::where('id', $request->permanent_district)->pluck('title')->first();
        $permanentUpazila = Upazila::where('id', $request->permanent_upazila)->pluck('title')->first();
        $permanentPostOffice = PostOffice::where('id', $request->permanentpost_office)->pluck('title')->first();


        $profile['permanent_division'] = $PermanentDivision;

        $profile['permanent_district'] = $permanentDistrict;

        $profile['permanent_upazila'] = $permanentUpazila;

        $profile['permanentpost_office'] = $permanentPostOffice;

        Session::put('permanentInfo',$profile);

        return $profile;

    }


    public function presentAddess($profile,$request)
    {
        session()->forget('presentInfo');

        $presentDivision = Division::where('id', $request->present_division )->pluck('title')->first();
        $presentDistrict = District::where('id', $request->present_district)->pluck('title')->first();
        $presentUpazila = Upazila::where('id', $request->present_upazila)->pluck('title')->first();
        $presentPostOffice = PostOffice::where('id', $request->presentpost_office)->pluck('title')->first();

        $profile['present_division'] = $presentDivision;
        $profile['present_district'] = $presentDistrict;
        $profile['present_upazila'] = $presentUpazila;
        $profile['presentpost_office'] = $presentPostOffice;

        Session::put('presentInfo',$profile);

        return $profile;

    }



    public function register(Request $request)
    {
        $data=$request->all();

        $userData = $request->only('role_id', 'title', 'first_name','middle_name','last_name','mobile_number', 'email','password');
        $businessInfo = $request->only('user_id','enterprise_name','organization', 'industry', 'sector', 'sub_sector',
            'org_division', 'org_district', 'org_upazila','orgpost_office','post_code1','org_address','total_director_partner');

        $profile = $request->only('user_id','business_info_id','owner_title','owner_first_name','owner_middle_name',
            'owner_last_name','position','shareholder', 'owner_nid', 'date_of_birth', 'capture_pic',
            'present_division', 'permanent_division', 'present_district', 'permanent_district', 'present_upazila', 'permanent_upazila',
            'presentpost_office', 'permanentpost_office', 'post_code2', 'post_code3', 'present_address', 'permanent_address',
            'policy_agree');


        $userData['role_id'] = 2;

        $businessInfo= $this->businessData($businessInfo,$request);
        $profile = $this->presentAddess($profile,$request);
        $profile = $this->permanentAddress($profile,$request);

//          $Validators=$this->RegisterValidation($data);
//
//        if($Validators->fails())
//        {
//            return Redirect::back()->withErrors($Validators)->withInput();
//
//        }

        $user = User::create($userData);
        $profile['nid_front_pic']=$request->nid_front_pic;
        $profile['nid_back_pic']=$request->nid_back_pic;
        $profile['owner_pic']=$request->owner_pic;

        if (isset($request->capture_pic)){
            $folderPath = public_path('/ui/back-end/uploads/sme-photo/');
            $image_parts = explode(";base64,", $request->capture_pic);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = "file_name_".uniqid() . '.png';
            $file = $folderPath . $fileName;
            file_put_contents($file, $image_base64);
            $profile['capture_pic']=$fileName;
        }
        $ownerAttachments = array('nid_front_pic', 'nid_back_pic', 'owner_pic',);

        foreach ($ownerAttachments as $ownerAttachment)
        {
            if(isset($request[$ownerAttachment]))
            {
                $imgeFile = $request[$ownerAttachment];
                $nidFront = $request[$ownerAttachment]->getClientOriginalName();
                $directory = public_path('ui/back-end/uploads/sme-photo/');
                $imgUrl = $directory.$nidFront;
                Image::make($imgeFile)->resize(750, 600)->save($imgUrl);
                $profile[$ownerAttachment] = $nidFront;
            }
        }

        $businessInfo['user_id'] = $user->id;

        $businessData = BusinessInfo::create($businessInfo);


        $profile['user_id'] = $user->id;
        $profile['business_info_id'] = $businessData->id;


        if ($businessInfo['organization'] == 'Sole Proprietorship')
        {
            Profile::create($profile);

        }
        else{

            for($i=1;$i<=$request->total_director_partner;$i++)
            {
                Profile::create($profile);
            }
        }

        $categories = QuestionCategory::where('status', 1)->get();
        foreach ($categories as $category) {
            $tableName = tableNameConvert($category->question_category);
            DB::table($tableName)->insert(['user_id' => $user->id]);
        };


        session()->forget('business');
        session()->forget('presentInfo');
        session()->forget('permanentInfo');

        $this->guard()->login($user);

        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    public function updateRules()
    {
        $rules= array(
            "enterprise_name"=>"required",
            "industry"=>"required",
            "org_division"=>"required",
            "org_district"=>"required",
            "org_upazila"=>"required",
            "orgpost_office"=>"required",
            "post_code1"=>"required",
            "org_address"=>"required",

        );
        return $rules;
    }


    public  function RegisterUpdateValidation($data)
    {

        $rules=$this->updateRules();
        if(isset($data['organization']))
        {
            if($data['organization']=="Sole Proprietorship")
            {
                $rules['title']="required";
                $rules['first_name']="required";
                $rules['last_name']="required";
                $rules['owner_nid']="required";
                $rules['date_of_birth']="required";
                $rules['present_division']="required";
                $rules['permanent_division']="required";
                $rules['present_district']="required";
                $rules['permanent_district']="required";
                $rules['present_upazila']="required";
                $rules['permanent_upazila']="required";
                $rules['presentpost_office']="required";
                $rules['permanentpost_office']="required";
                $rules['post_code2']="required";
                $rules['post_code3']="required";
                $rules['present_address']="required";
                $rules['permanent_address']="required";

            }


            else if($data['organization']=="Partnership" || $data['organization']=="Public Limited" ||$data['organization']="Private Limited")
            {
                $rules['total_director_partner']="required";
            }

        }



        return Validator::make($data, $rules);

    }



    public function registerUpdate(Request $request)
    {

        $data=$request->all();


        $userData = $request->only('role_id', 'title', 'first_name','middle_name','last_name','mobile_number', 'email','password');
        $businessInfo = $request->only('user_id','enterprise_name','organization', 'industry', 'sector', 'sub_sector',
            'org_division', 'org_district', 'org_upazila','orgpost_office','post_code1','org_address','total_director_partner');

        $industry=IndustryType::where('id',$request->industry)->pluck('industry_type')->first();
        $sector = Sector::where('id',$request->sector)->pluck('sector_name')->first();
        $SubSector=SubSector::where('id',$request->sector)->pluck('sub_sector')->first();
        $businessInfo['industry']=$industry;
        $businessInfo['sector']=$sector;
        $businessInfo['sub_sector']=$SubSector;

        $profile = $request->only('user_id','business_info_id','owner_title','owner_first_name','owner_middle_name',
            'owner_last_name','position','shareholder','owner_nid','nid_front_pic', 'nid_back_pic', 'date_of_birth', 'owner_pic', 'capture_pic',
            'present_division', 'permanent_division', 'present_district', 'permanent_district', 'present_upazila', 'permanent_upazila',
            'presentpost_office', 'permanentpost_office', 'post_code2', 'post_code3', 'present_address', 'permanent_address',
            'policy_agree');





        if(is_numeric($request['present_division']) && is_numeric($request['present_district']))
        {
            $profile = $this->presentAddess($profile,$request);
        }
        else{
            $presentDivision = Division::where('id', $request->present_division )->pluck('title')->first();
            $profile['present_division'] = $presentDivision;

        }
        if(is_numeric($request['permanent_division']) && is_numeric($request['permanent_district'])) {
            $profile = $this->permanentAddress($profile,$request);
        }
        else{
            $PermanentDivision = Division::where('id', $request->permanent_division )->pluck('title')->first();
            $profile['permanent_division'] = $PermanentDivision;
        }

        if(is_numeric($request['org_division']) && is_numeric($request['org_district'])) {
            $businessInfo = $this->businessData($businessInfo, $request);
        }
        else{
            $orgDivision = Division::where('id', $request->org_division )->pluck('title')->first();
            $businessInfo['org_division'] = $orgDivision;

        }

//          $Validators=$this->RegisterUpdateValidation($data);
//
//        if($Validators->fails())
//        {
//            return Redirect::back()->withErrors($Validators)->withInput();
//
//        }

//        $ownerAttachments = array('nid_front_pic', 'nid_back_pic', 'owner_pic',);
//        foreach ($ownerAttachments as $ownerAttachment)
//        {
//            if(isset($request[$ownerAttachment]))
//            {
//
//                $nidFront = time().'-'.$request[$ownerAttachment]->getClientOriginalName();
//                $request[$ownerAttachment]->move(public_path('ui/back-end/uploads/sme-photo'), $nidFront);
//                $profile[$ownerAttachment]=$nidFront;
//            }
//            else
//            {
//                $profile[$ownerAttachment]= Auth::user()->profile[$ownerAttachment];
//            }
//        }


        $ownerAttachments = array('nid_front_pic', 'nid_back_pic', 'owner_pic',);

        foreach ($ownerAttachments as $ownerAttachment)
        {
            if(isset($request[$ownerAttachment]))
            {
                $imgeFile = $request[$ownerAttachment];
                $nidFront = $request[$ownerAttachment]->getClientOriginalName();
                $directory = public_path('ui/back-end/uploads/sme-photo/');
                $imgUrl = $directory.$nidFront;
                Image::make($imgeFile)->resize(750, 600)->save($imgUrl);
                $profile[$ownerAttachment] = $nidFront;
            }
            else
            {
                $profile[$ownerAttachment]= Auth::user()->profile[$ownerAttachment];
            }
        }


        if (isset($request->capture_pic)){
            $folderPath = public_path('/ui/back-end/uploads/sme-photo/');
            $image_parts = explode(";base64,", $request->capture_pic);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = Auth::user()->profile->owner_first_name.uniqid() . '.png';
            $file = $folderPath . $fileName;
            file_put_contents($file, $image_base64);
            $profile['capture_pic']=$fileName;
        }else{
            if(isset(Auth::user()->profile->capture_pic)){
                $profile['capture_pic']= Auth::user()->profile->capture_pic;
            }
        }


        Auth::user()->update($userData);
        BusinessInfo::find(Auth::user()->businessInfo->id)->update($businessInfo);
        if (Auth::user()->businessInfo->organization == 'Sole Proprietorship')
        {
            Auth::user()->profile->update($profile);
            return redirect('/email-phone');
        }
        return redirect('/partner-director-info/edit');
    }


    protected function guard()
    {
        return Auth::guard();
    }

    public function registerEdit()
    {
        $userSessionData = Session::get('userData');

        $businessSessionData = Session::get('businessInfo');

        $profileSessionData = Session::get('profile');
        $divisions = Division::all();
        $industries = IndustryType::pluck('industry_type','id');
        $sectors = Sector::where('status', 1)->pluck('sector_name');

        return view('auth.register-edit', compact('divisions', 'industries', 'sectors', 'userSessionData', 'businessSessionData', 'profileSessionData'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//   protected function validator(array $userData)
//      {
//          return Validator::make($userData, [
//
//              'title' => 'required',
//              'first_name' => 'required',
//              'middle_name' => 'required',
//              'last_name' => 'required',
//              'owner_contact' => 'required',
//              'email' => 'required|string|email|max:255|unique:users',
//              'password' => 'required|string|min:3|confirmed',
//          ]);
//      }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */

    protected function create(array $userData)
    {
        return User::create([
            'role_id' => $userData['role_id'],
            'title' => $userData['title'],
            'first_name' => $userData['first_name'],
            'middle_name' => $userData['middle_name'],
            'last_name' => $userData['last_name'],
            'mobile_number' => $userData['mobile_number'],
            'email' => $userData['email'],
            'password' => Hash::make($userData['password']),
        ]);
    }

    protected function verificationCode()
    {
        $to = Auth::user()->mobile_number;
        $token = "3b137b9da80a1a2f4a75b0ba51b6a82b";
        $otpCode = mt_rand(123108,567890);

        Session::put('otpCode', $otpCode);
        $otp = "Your Verification Code: ".$otpCode;
        $url = "http://api.greenweb.com.bd/api.php";
        $data= array(
            'to'=>"$to",
            'message'=>"$otp",
            'token'=>"$token"
        );
        // Add parameters in key value
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        curl_exec($ch);
        $id = Auth::user()->id;
        User::find($id)->update(['time_expired' => date(time())]);
//        return redirect('mobile-verify')->with('resend', 'Your new verification code send.');
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();

        if(isset($verifyUser) ){
            $user = $verifyUser->user;

            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $this->verificationCode();
                $verifyUser->user->time_expired = date(time());
                $verifyUser->user->save();
                $status = "Your Email is verified successfully";
            }else{
                $status = "Your Email is already verified. You can now create password.";
            }
        }else{
            return redirect('/mobile-verify')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/mobile-verify')->with('mailVerified', $status);
    }
}














