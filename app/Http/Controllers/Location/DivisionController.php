<?php



namespace App\Http\Controllers\Location;



use App\Division;

use App\Http\Requests\DivistionFormValidation;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Exception;



class DivisionController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        $divisions=Division::all();
        return view('admin.location.division.index',compact('divisions'));
    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('admin.location.division.create');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(DivistionFormValidation $request)

    {

            $data= $request->all();
            Division::create($data);
            return redirect('/division')->withMessage('Create Successfully!!!');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {
        $divisionEdit=Division::find($id);
        return view('/admin.location.division.edit',compact('divisionEdit'));
    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        try{
            $data=$request->all();
            $divisionUpdate=Division::find($id);
            $divisionUpdate->update($data);
            return redirect('/division')->withMessage('Division Update Successfully!!!');
        }
        catch (Exception $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }




    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function delete($id)

    {

        $divisionDelete=Division::find($id);

        $divisionDelete->delete();

        return redirect('/division')->withMessage('Division Delete Successfully!!!');

    }

}

