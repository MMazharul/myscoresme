<?php



namespace App\Http\Controllers\Location;



use App\Http\Requests\UpazilaFormValidation;

use App\Upazila;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\District;
use Illuminate\Support\Facades\DB;


class UpazilaController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
        $upazilas = DB::table('upazilas')
            ->join('districts', 'districts.id', '=', 'upazilas.district_id')
            ->join('divisions', 'divisions.id', '=', 'districts.division_id')
            ->select('upazilas.geo_code','districts.title as district_name', 'upazilas.title as upazila_name','divisions.title as division_name','upazilas.id')
            ->get();
        return view('admin.location.upazila.index', compact('upazilas'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $districts=District::pluck('title','id');



        return view('admin.location.upazila.create',compact('districts'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        $upazila = $request->all();
        $district_name=District::where('id',$request->district_id)->pluck('title')->first();
        $upazila['district_name']=$district_name;
        Upazila::create($upazila);

        return redirect('/upazila')->withMessage('Upazila Store Successfully!!!');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $upazilaEdit = Upazila::find($id);

        $districts=District::pluck('title','id');

        return view('admin.location.upazila.edit', compact('upazilaEdit', 'districts'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $data = $request->all();

        $district_name=District::where('id',$request->district_id)->pluck('title')->first();

        $data['district_name']=$district_name;

        $upazilaUpdate = Upazila::find($id);

         $upazilaUpdate->update($data);

        return redirect('/upazila')->withMessage('Upazila Update Successfully!!!');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function delete($id)

    {

        $upazilaDelete = Upazila::find($id);



        $upazilaDelete->delete();

        return redirect('/upazila')->withMessage('Upazila Delete Successfully!!!');

    }

}

