<?php



namespace App\Http\Controllers\Location;



use App\Http\Requests\PostofficeFormValidation;

use App\Postcode;

use App\PostOffice;

use App\Upazila;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class PostOfficeController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $postOffices=DB::table('post_offices')
            ->join('postcodes','post_offices.id','=','postcodes.postoffice_id')
            ->join('upazilas','upazilas.id','=','post_offices.upazila_id')
            ->join('districts','districts.id','=','upazilas.district_id')
            ->join('divisions','divisions.id','=','districts.division_id')
            ->select('postcodes.id','post_offices.title as postoffice','postcodes.post_code','districts.title as district','divisions.title as division','upazilas.title as upazila_name')->get();

        return view('admin.location.post-office.index', compact('postOffices'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        $upazilas=Upazila::pluck('title','id');

        return view('admin.location.post-office.create', compact('upazilas'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(PostofficeFormValidation $request)
    {

        $postOfficeData = $request->only('upazila_id','title');

        $upazila_name=Upazila::where('id',$request->upazila_id)->pluck('title')->first();

        $postOfficeData['upazila_name']=$upazila_name;

        $postOffice=PostOffice::create($postOfficeData);

        $postCodeData=$request->only('post_code');

        $postCodeData['postoffice_id']=$postOffice->id;

        $postofficeName=PostOffice::where('id',$postOffice->id)->pluck('title')->first();

        $postCodeData['postoffice_name']=$postofficeName;

        Postcode::create($postCodeData);

        return redirect('/post-office/create')->withMessage('Post-Office Store Successfully!!!');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $postOfficeId=Postcode::where('id',$id)->pluck('postoffice_id')->first();



        $upazilas = Upazila::all();

        $postOfficeEdit = PostOffice::with('postcode')->where('id',$postOfficeId)->first();



        return view('admin.location.post-office.edit', compact('postOfficeEdit', 'upazilas','id'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $postOfficeUpdate = $request->only('upazila_id','title');
        $postOfficeId=Postcode::where('id',$id)->pluck('postoffice_id')->first();
        $upazila_name=Upazila::where('id',$request->upazila_id)->pluck('title')->first();
        $postOfficeUpdate['upazila_name']=$upazila_name;
        $postOfficeId=PostOffice::find($postOfficeId);
        $postOfficeId->update($postOfficeUpdate);
        $postCodeUpdate=$request->only('post_code');
        $postCodeId=Postcode::find($id);
        $postCodeUpdate['postoffice_id']= $postCodeId->postoffice_id;;
        $postofficeName=PostOffice::where('id',$postCodeId->postoffice_id)->pluck('title')->first();
        $postCodeUpdate['postoffice_name']=$postofficeName;
        $postCodeId->update($postCodeUpdate);
        return redirect('/post-office')->withMessage('Post-Office Update Successfully!!!');

    }



    public function statusUpdate($id)

    {

        $status = PostOffice::find($id);

        if ($status->status == 1) {

            $status['status'] = 0;

            $status->save();

        } else {

            $status['status'] = 1;

            $status->save();

        }

        return redirect('/post-office');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function delete($id)

    {

        $postcode=Postcode::find($id);

        $postOffice =  PostOffice::find($postcode->postoffice_id);

        $postOffice->delete();

        $postcode->delete();

        return redirect('/post-office')->withMessage('Post-Office Delete Successfully!!!');

    }

}

