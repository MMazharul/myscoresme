<?php

namespace App\Http\Controllers\Location;

use App\Http\Requests\PostCodeFormValidation;
use App\Postcode;
use App\PostOffice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $postcodes = Postcode::with('postoffice')->get();

        return view('admin.location.post-code.index', compact('postcodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $postoffices=PostOffice::pluck('title','id');

        return view('admin.location.post-code.create',compact('postoffices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCodeFormValidation $request)
    {
        $data = $request->all();
        $postofficeName=PostOffice::where('id',$request->postoffice_id)->pluck('title')->first();
        $data['postoffice_name']=$postofficeName;
        Postcode::create($data);
        return redirect('/post-code')->withMessage('Post Code Store Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postCodeEdit = Postcode::find($id);
        $postoffices=PostOffice::where('status',1)->pluck('title','id');
        return view('admin.location.post-code.edit', compact('postCodeEdit', 'postoffices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $postofficeName=PostOffice::where('id',$request->postoffice_id)->pluck('title')->first();
        $data['postoffice_name']=$postofficeName;
        $postCodeUpdate = Postcode::find($id);
        $postCodeUpdate->update($data);
        return redirect('/post-code')->withMessage('Post Code Update Successfully!!!');
    }

    public function statusUpdate($id)
    {
        $status = Postcode::find($id);
        if ($status->status == 1) {
            $status['status'] = 0;
            $status->save();
        } else {
            $status['status'] = 1;
            $status->save();
        }
        return redirect('/post-code');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $postCodeDelete = Postcode::find($id);

        $postCodeDelete->delete();
        return redirect('/post-code')->withMessage('Post Code Delete Successfully!!!');
    }
}
