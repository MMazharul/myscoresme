<?php



namespace App\Http\Controllers\Location;



use App\District;

use App\Division;

use App\Http\Requests\DistrictFormValidation;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use PhpParser\Node\Expr\AssignOp\Div;



class DistrictController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $districts=District::with('division')->get();



        return view('admin.location.district.index',compact('districts'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
        $divisions=Division::pluck('title','id');
        return view('admin.location.district.create',compact('divisions'));
    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(DistrictFormValidation $request)

    {
        $data=$request->all();
        $division_name=Division::where('id',$request->division_id)->pluck('title')->first();
        $data['division_name']=$division_name;
        District::create($data);
        return redirect('/district/create')->withMessage('District Store Successfully!!!');
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $districtEdit=District::find($id);

        $divisions=Division::pluck('title','id');

        return view('admin.location.district.edit',compact('districtEdit','divisions'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $data=$request->all();

        $division_name=Division::where('id',$request->division_id)->pluck('title')->first();

        $data['division_name']=$division_name;

        $districtUpdate=District::find($id);

        $districtUpdate->update($data);

        return redirect('/district')->withMessage('District Update Successfully!!!');

    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function delete($id)

    {
        $districtDelete=District::find($id);
        $districtDelete->delete();
        return redirect('/district')->withMessage('District Delete Successfully!!!');

    }

}

