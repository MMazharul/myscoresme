<?php

namespace App\Http\Controllers\BranchDetails;

use App\BankDetail;
use App\BranchDetail;
use App\Division;
use App\Http\Requests\BranchDetailValidation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BranchDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::with('bank')->get();


        return view('admin.branch.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::where('status', 1)->pluck('bank_name','id');
        return view('admin.branch.create', compact('banks'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $branchAdd = $request->all();
        $branchAdd['loan_type']=implode(",",$branchAdd['loan_type']);
        Branch::create($branchAdd);
        return redirect('/branch/create')->withMessage('Branch Store Successfully!!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branchView=Branch::find($id);

        return view('admin.branch.show',compact('branchView'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchEdit = Branch::find($id);
        $loanExplode=explode(",",$branchEdit->loan_type);
        $bankEdits = Bank::where('status', 1)->pluck('bank_name','id');
        return view('admin.branch.edit', compact('branchEdit', 'bankEdits','loanExplode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $branchUpdate = Branch::find($id);

        $data=$request->all();
        $data['loan_type']=implode(",",$data['loan_type']);

        $branchUpdate->update($data);
        return redirect('/branch')->withMessage('Branch Update Successfully!!!');
    }

    public function statusUpdate($id)
    {
        $status = Branch::find($id);
        if ($status->status == 1) {
            $status['status'] = 0;
            $status->save();
        } else {
            $status['status'] = 1;
            $status->save();
        }
        return redirect('/branch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $branchDelete = Branch::find($id);
        $branchDelete->delete();
        return redirect('branch')->withMessage('Branch Delete Successfully!!!');
    }


    public function branchCreate()
    {

        $divisions=Division::all();
        $bankdetails=BankDetail::pluck('organization_name','id');


        return view('front-end.branch.branch-register',compact('divisions','bankdetails'));
    }

    public function branchStore(BranchDetailValidation $request)
    {
        $branchUserData=$request->only('title','first_name', 'middle_name', 'last_name', 'owner_contact','email', 'password');

        $branchUserData['password']=Hash::make($branchUserData['password']);

        $branchUserData['role_id']=4;

        $branchUser=User::create($branchUserData);



        $branchData=$request->only('bank_id','branch_name','date_estabilish','division','district','upazila','post_office','post_code4','telephone','contact_number','retype_email','alternate_email','profesional_staff','designation','joining_date','owner_number','contact_email','routing_number','attachment');

        $branchData['user_id']=$branchUser->id;

        if(isset($branchData['attachment']))
        {
            $branchAttachment = time().'-'.$request->attachment->getClientOriginalName();
            $request->attachment->move(public_path('ui/back-end/bank/bank-attachment/'), $branchAttachment);
            $branchData['attachment'] = $branchAttachment;
        }



        BranchDetail::create($branchData);

       return redirect('/branch-register')->withMessage('Branch store Successfully!!!');
    }


    public function branchDashboard()
    {

     /*   $bankPicture=BankDetail::where('id',Auth::user()->branchdetail->bank_id)->pluck('bank_logo')->first();*/

        return view('front-end.branch.branch-dashboard');
    }

    public function branchUsers()
    {
        $branchUser=BranchDetail::all();
        return view('/admin/branch-detail/branch-index',compact('branchUser'));
    }



}
