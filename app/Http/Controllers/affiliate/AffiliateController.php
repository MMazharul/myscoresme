<?php

namespace App\Http\Controllers\affiliate;

use App\Affiliate;
use App\Division;
use App\Http\Requests\AffiliateValidation;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Illuminate\Foundation\Auth\RegistersUsers;

class AffiliateController extends Controller
{
    use RegistersUsers;


    public function affiliateCreate()
    {

        $divisions=Division::all();

        return view('front-end.affiliate.affiliate-register',compact('divisions'));
    }

    public function affiliateStore(AffiliateValidation $request)
    {
        $affiliateUserData=$request->only('title','first_name', 'middle_name', 'last_name', 'owner_contact','email', 'password');

        $affiliateUserData['password']=Hash::make($affiliateUserData['password']);
        $affiliateUserData['role_id']=5;
        $affiliateUserData['verified']=1;


       $affiliateUser=User::create($affiliateUserData);

        $affiliateData=$request->only('category','affilate_name','division','district','upazila','post_office','post_code4','address_1','joining_date','designation','owner_number','contact_email','trade_license','expire_date','commision_rate');


        $affiliateData['user_id']=$affiliateUser->id;

         Affiliate::create($affiliateData);

        $this->guard()->login($affiliateUser);

        return $this->registered($request, $affiliateUser) ?: redirect('affiliate-dashboard');

    }


    public function affiliateDashboard()
    {
        return view('front-end.affiliate.affiliate-dashboard');
    }

    public function affiliateUsers()
    {
        $affiliateUser=Affiliate::all();
        return view('/admin/affiliate-detail/affiliate-index',compact('affiliateUser'));
    }
}
