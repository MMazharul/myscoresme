<?php

namespace App\Http\Controllers\BankDetails;

use App\BankDetail;
use App\Division;
use App\Http\Requests\BankDetailValidation;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;



class BankDetailsController extends Controller
{
    public function index()
    {
        $banks = Bank::all();


        return view('admin.bank.index', compact('banks'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions=Division::all();

        return view('admin/bank/create',compact('divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $bankLogo = time().'-'.$request->bank_logo->getClientOriginalName();

        $request->bank_logo->move(public_path('ui/back-end/bank/bank-logo/'), $bankLogo);
        $data['bank_logo'] = $bankLogo;

        $bankAttachment = time().'-'.$request->bank_attachment->getClientOriginalName();
        $request->bank_attachment->move(public_path('ui/back-end/bank/bank-attachment/'), $bankLogo);
        $data['bank_attachment'] = $bankAttachment;

        Bank::create($data);
        return redirect('/bank')->withMessage('Bank Store Successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $bankShow = Bank::find($id);
        return view('admin/bank/show', compact('bankShow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bankEdit = Bank::find($id);



        $loanExplode=explode(",",$bankEdit->loan_type);

        return view('admin/bank/edit', compact('bankEdit','loanExplode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bankUpdate = Bank::find($id);
        $updateData=$request->all();
        if(isset($updateData['loan_type'])) {
            $updateData['loan_type']=implode(",",$updateData['loan_type']);
        }
        else
        {

            $updateData['loan_type']=null;
        }
        $bankUpdate->update($updateData);
        return redirect('/bank')->withMessage('Bank Update Successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function statusUpdate($id)
    {
        $status = Bank::find($id);
        if ($status->status == 1) {
            $status['status'] = 0;
            $status->save();
        } else {
            $status['status'] = 1;
            $status->save();
        }
        return redirect('/bank');
    }

    public function delete($id)
    {
        $bankDelete = Bank::find($id);
        $bankDelete->delete();
        return redirect('/bank')->withMessage('Bank Delete Successfully!!!');
    }

    public function bankList()
    {
        $bankList=Bank::where('status',1)->pluck('bank_name','id');
        return view('admin/bank/bank',compact('bankList'));
    }
    public function branches()
    {
        $bank_id=Input::get('bank_id');

        $braches=Branch::where('bank_id',$bank_id)->get();
        return response()->json($braches);
    }


    public function bankCreate()
    {

        $divisions=Division::all();

        return view('front-end.bank.bank-registration',compact('divisions'));
    }

    public function bankStore(BankDetailValidation $request)
    {
        $bankUserData=$request->only('title','first_name', 'middle_name', 'last_name', 'owner_contact','email', 'password');

        $bankUserData['password']=Hash::make($bankUserData['password']);
        $bankUserData['role_id']=3;

        $bankUser=User::create($bankUserData);


         $bankAllData=$request->only('organization_name','description','country_incorporation','year_incorporation','registration_number','type_organization','division','district','upazila','post_office','post_code1','retype_email','alternate_email','professional_staff','branch_number','bank_logo','bank_attachment');

         $bankAllData['user_id']=$bankUser->id;

        if(isset($bankAllData['bank_logo']))
        {
            $banklogo = time().'-'.$request->bank_logo->getClientOriginalName();
            $request->bank_logo->move(public_path('ui/back-end/bank/bank-logo/'), $banklogo);
            $bankAllData['bank_logo']=$banklogo;
        }

        if(isset($bankAllData['bank_attachment']))
        {
            $bankAttachment = time().'-'.$request->bank_attachment->getClientOriginalName();
            $request->bank_attachment->move(public_path('ui/back-end/bank/bank-attachment/'), $bankAttachment);
            $bankAllData['bank_attachment'] = $bankAttachment;
        }


        BankDetail::create($bankAllData);
         return redirect('/bank-register')->withMessage('Bank Create Successfully!!!');
    }


    public function bankDashboard()
    {
        return view('front-end.bank.bank-dashboard');
    }

    public function bankUsers()
    {
        $bankUser=BankDetail::all();

        return view('admin/bank-detail/bank-index',compact('bankUser'));
    }


}
