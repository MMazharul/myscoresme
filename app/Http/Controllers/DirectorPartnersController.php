<?php

namespace App\Http\Controllers;

use App\Bank;
use App\BusinessInfo;
use App\ContactInfo;
use App\DirectorPartners;
use App\District;
use App\Division;
use App\Http\Requests\AuthorizedValidation;
use App\Http\Requests\EmailValidation;
use App\Http\Requests\PasswordValidation;
use App\IndustryType;
use App\Mail\VerifyMail;
use App\PostOffice;
use App\Profile;
use App\Sector;
use App\SubSector;
use App\Upazila;
use App\User;
use App\VerifyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Validator;



class DirectorPartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function partnerDeraactorForm()
    {
        $divisions = Division::all();
        return view('front-end/sme/partner-director-info', compact('divisions'));
    }

    public function emailPhoneForm()
    {

        return view('auth.email-phone');
    }

    public function mobileVerifyForm()
    {
        return view('auth.mobile-verify');
    }

    public function passwordForm()
    {
        return view('auth.sme-password');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function validator(array $directorsParners)
    {
        $rules = [];
        for($i=1;$i<=Auth::user()->businessInfo->total_director_partner; $i++)
        {
            $rules['owner_title'.$i] = 'required';
            $rules['owner_first_name'.$i] = 'required';
            $rules['owner_last_name'.$i] = 'required';
            $rules['position'.$i] = 'required';
            $rules['shareholder'.$i] = 'required';
            $rules['owner_nid'.$i] = 'required';
//            $rules['nid_front_pic'.$i] = 'required';
//            $rules['nid_back_pic'.$i] = 'required';
            $rules['date_of_birth'.$i] = 'required';
//            $rules['owner_pic'.$i] = 'required';
//            $rules['capture_pic'.$i] = 'required';
            $rules['present_division'.$i] = 'required';
            $rules['permanent_division'.$i] = 'required';
            $rules['present_district'.$i] = 'required';
            $rules['permanent_district'.$i] = 'required';
            $rules['present_upazila'.$i] = 'required';
            $rules['permanent_upazila'.$i] = 'required';
            $rules['presentpost_office'.$i] = 'required';
            $rules['permanentpost_office'.$i] = 'required';
            $rules['present_post_code'.$i] = 'required';
            $rules['post_code'.$i] = 'required';
            $rules['present_address'.$i] = 'required';
            $rules['permanent_address'.$i] = 'required';
        }

        $messages = array(
            'required' => 'This field is required.'
        );

        return Validator::make($directorsParners, $rules,$messages);
    }

    public function locationSesstionStore($request)
    {
        Session::forget('location');

        $directorsParners=[];
        for($i=1;$i<=Auth::user()->businessInfo->total_director_partner; $i++) {


            $presentDistrict = District::where('id', $request['present_district' . $i])->pluck('title')->first();
            $presentUpazila = Upazila::where('id', $request['present_upazila' . $i])->pluck('title')->first();
            $presentPostOffice = PostOffice::where('id', $request['presentpost_office' . $i])->pluck('title')->first();

            $permanentDistrict = District::where('id', $request['permanent_district'.$i] )->pluck('title')->first();
            $permanentUpazila = Upazila::where('id', $request['permanent_upazila'.$i])->pluck('title')->first();
            $permanentPostOffice = PostOffice::where('id', $request['permanentpost_office'.$i])->pluck('title')->first();

            $directorsParners['present_district'.$i] = $presentDistrict;
            $directorsParners['present_upazila'.$i] = $presentUpazila;
            $directorsParners['presentpost_office'.$i] =$presentPostOffice;

            $directorsParners['permanent_district'.$i] = $permanentDistrict;
            $directorsParners['permanent_upazila'.$i] = $permanentUpazila;
            $directorsParners['permanentpost_office'.$i] =$permanentPostOffice;

        }
        Session::put('location',$directorsParners);

        return $directorsParners;

    }

    public function store(Request $request)
    {
        switch ($request->input('action')) {
            case 'save':
                $directorsParners = $request->all();
                $this->locationSesstionStore($request);
                Session::get('location');

                $validators=$this->validator($directorsParners);
                if($validators->fails()) {
                    return Redirect::back()->withErrors($validators)->withInput();
                }

                Session::forget('location');


                for($i=1;$i<=Auth::user()->businessInfo->total_director_partner; $i++)
                {
                    $directors=new Profile();

                    $directors->user_id = Auth::user()->id;
                    $directors->owner_title=$request['owner_title'.$i];
                    $directors->owner_first_name=$request['owner_first_name'.$i];
                    $directors->owner_middle_name=$request['owner_middle_name'.$i];
                    $directors->owner_last_name=$request['owner_last_name'.$i];
                    $directors->owner_nid=$request['owner_nid'.$i];
                    $directors->position=$request['position'.$i];
                    $directors->shareholder=$request['shareholder'.$i];
                    $directors->date_of_birth=$request['date_of_birth'.$i];
                    $directors->post_code2=$request['present_post_code'.$i];
                    $directors->post_code3=$request['post_code'.$i];
                    $directors->present_address=$request['present_address'.$i];
                    $directors->permanent_address=$request['permanent_address'.$i];

                    if(is_numeric($request['present_district'.$i]) && is_numeric($request['present_division'.$i]))
                    {

                        $presentDivision = Division::where('id', $request['present_division'.$i])->pluck('title')->first();
                        $presentDistrict = District::where('id', $request['present_district'.$i])->pluck('title')->first();
                        $presentUpazila = Upazila::where('id', $request['present_upazila'.$i])->pluck('title')->first();
                        $presentPostOffice = PostOffice::where('id', $request['presentpost_office'.$i])->pluck('title')->first();

                        $directorsParners['present_division'.$i] = $presentDivision;
                        $directorsParners['present_district'.$i] = $presentDistrict;
                        $directorsParners['present_upazila'.$i] = $presentUpazila;
                        $directorsParners['presentpost_office'.$i] =$presentPostOffice;

                        $directors->present_division=$directorsParners['present_division'.$i];
                        $directors->present_district=$directorsParners['present_district'.$i];
                        $directors->present_upazila=$directorsParners['present_upazila'.$i];
                        $directors->presentpost_office=$directorsParners['presentpost_office'.$i];
                    }
                    if (is_numeric($request['permanent_district'.$i] ) && is_numeric($request['permanent_division'.$i]))
                    {
                        $PermanentDivision = Division::where('id', $request['permanent_division'.$i] )->pluck('title')->first();
                        $permanentDistrict = District::where('id', $request['permanent_district'.$i] )->pluck('title')->first();
                        $permanentUpazila = Upazila::where('id', $request['permanent_upazila'.$i])->pluck('title')->first();
                        $permanentPostOffice = PostOffice::where('id', $request['permanentpost_office'.$i])->pluck('title')->first();

                        $directorsParners['permanent_division'.$i] = $PermanentDivision;
                        $directorsParners['permanent_district'.$i] = $permanentDistrict;
                        $directorsParners['permanent_upazila'.$i] = $permanentUpazila;
                        $directorsParners['permanentpost_office'.$i] =$permanentPostOffice;

                        $directors->permanent_division=$directorsParners['permanent_division'.$i];
                        $directors->permanent_district=$directorsParners['permanent_district'.$i];
                        $directors->permanent_upazila=$directorsParners['permanent_upazila'.$i];
                        $directors->permanentpost_office=$directorsParners['permanentpost_office'.$i];
                    }


                    if (isset($request->capture_pic)){
                        $string=$request['capture_pic'.$i];
                        $folderPath = public_path('ui/back-end/uploads/director-Partner-image/');
                        $image_parts = explode(";base64,", $string);
                        $image_type_aux = explode("image/", $image_parts[1]);
                        $image_base64 = base64_decode($image_parts[1]);
                        $fileName = "file_name_".uniqid() . '.png';
                        $file = $folderPath . $fileName;
                        file_put_contents($file, $image_base64);
//                $directorsParners['capture_pic']=$fileName;
                        $directors->capture_pic=$fileName;
                    }
                    if(isset($request['owner_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['owner_pic'.$i]->getClientOriginalName();
                        $request['owner_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);

                        $directors->owner_pic=$picturename;
                        $directorsParners['owner_pic'.$i]=$picturename;

                    }
                    if(isset($request['nid_front_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['nid_front_pic'.$i]->getClientOriginalName();
                        $request['nid_front_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                        $directors->nid_front_pic=$picturename;
                        $directorsParners['nid_front_pic'.$i]=$picturename;
                    }
                    if(isset($request['nid_back_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['nid_back_pic'.$i]->getClientOriginalName();
                        $request['nid_back_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                        $directors->nid_back_pic=$picturename;
                        $directorsParners['nid_back_pic'.$i]=$picturename;
                    }
                    $directors->save();
                }

                Session::put('directorsParners', $directorsParners);

                $this->authorizedRepresentativeStore($request);
                return redirect('/partner-director-info/edit')->with('massage', 'Save Success');
                break;

            case 'next':

                $directorsParners = $request->all();
                $this->locationSesstionStore($request);
                Session::get('location');

                $validators=$this->validator($directorsParners);
                if($validators->fails()) {
                    return Redirect::back()->withErrors($validators)->withInput();
                }

                Session::forget('location');


                for($i=1;$i<=Auth::user()->businessInfo->total_director_partner; $i++)
                {
                    $directors=new Profile();

                    $directors->user_id = Auth::user()->id;
                    $directors->owner_title=$request['owner_title'.$i];
                    $directors->owner_first_name=$request['owner_first_name'.$i];
                    $directors->owner_middle_name=$request['owner_middle_name'.$i];
                    $directors->owner_last_name=$request['owner_last_name'.$i];
                    $directors->owner_nid=$request['owner_nid'.$i];
                    $directors->position=$request['position'.$i];
                    $directors->shareholder=$request['shareholder'.$i];
                    $directors->date_of_birth=$request['date_of_birth'.$i];
                    $directors->post_code2=$request['present_post_code'.$i];
                    $directors->post_code3=$request['post_code'.$i];
                    $directors->present_address=$request['present_address'.$i];
                    $directors->permanent_address=$request['permanent_address'.$i];

                    if(is_numeric($request['present_district'.$i]) && is_numeric($request['present_division'.$i]))
                    {

                        $presentDivision = Division::where('id', $request['present_division'.$i])->pluck('title')->first();
                        $presentDistrict = District::where('id', $request['present_district'.$i])->pluck('title')->first();
                        $presentUpazila = Upazila::where('id', $request['present_upazila'.$i])->pluck('title')->first();
                        $presentPostOffice = PostOffice::where('id', $request['presentpost_office'.$i])->pluck('title')->first();

                        $directorsParners['present_division'.$i] = $presentDivision;
                        $directorsParners['present_district'.$i] = $presentDistrict;
                        $directorsParners['present_upazila'.$i] = $presentUpazila;
                        $directorsParners['presentpost_office'.$i] =$presentPostOffice;

                        $directors->present_division=$directorsParners['present_division'.$i];
                        $directors->present_district=$directorsParners['present_district'.$i];
                        $directors->present_upazila=$directorsParners['present_upazila'.$i];
                        $directors->presentpost_office=$directorsParners['presentpost_office'.$i];
                    }
                    if (is_numeric($request['permanent_district'.$i] ) && is_numeric($request['permanent_division'.$i]))
                    {
                        $PermanentDivision = Division::where('id', $request['permanent_division'.$i] )->pluck('title')->first();
                        $permanentDistrict = District::where('id', $request['permanent_district'.$i] )->pluck('title')->first();
                        $permanentUpazila = Upazila::where('id', $request['permanent_upazila'.$i])->pluck('title')->first();
                        $permanentPostOffice = PostOffice::where('id', $request['permanentpost_office'.$i])->pluck('title')->first();

                        $directorsParners['permanent_division'.$i] = $PermanentDivision;
                        $directorsParners['permanent_district'.$i] = $permanentDistrict;
                        $directorsParners['permanent_upazila'.$i] = $permanentUpazila;
                        $directorsParners['permanentpost_office'.$i] =$permanentPostOffice;

                        $directors->permanent_division=$directorsParners['permanent_division'.$i];
                        $directors->permanent_district=$directorsParners['permanent_district'.$i];
                        $directors->permanent_upazila=$directorsParners['permanent_upazila'.$i];
                        $directors->permanentpost_office=$directorsParners['permanentpost_office'.$i];
                    }


                    if (isset($request->capture_pic)){
                        $string=$request['capture_pic'.$i];
                        $folderPath = public_path('ui/back-end/uploads/director-Partner-image/');
                        $image_parts = explode(";base64,", $string);
                        $image_type_aux = explode("image/", $image_parts[1]);
                        $image_base64 = base64_decode($image_parts[1]);
                        $fileName = "file_name_".uniqid() . '.png';
                        $file = $folderPath . $fileName;
                        file_put_contents($file, $image_base64);
//                $directorsParners['capture_pic']=$fileName;
                        $directors->capture_pic=$fileName;
                    }

                    if(isset($request['owner_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['owner_pic'.$i]->getClientOriginalName();
                        $request['owner_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);

                        $directors->owner_pic=$picturename;
                        $directorsParners['owner_pic'.$i]=$picturename;

                    }

                    if(isset($request['nid_front_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['nid_front_pic'.$i]->getClientOriginalName();
                        $request['nid_front_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                        $directors->nid_front_pic=$picturename;
                        $directorsParners['nid_front_pic'.$i]=$picturename;
                    }
                    if(isset($request['nid_back_pic'.$i]))
                    {
                        $picturename = time().$i.'-'.$request['nid_back_pic'.$i]->getClientOriginalName();
                        $request['nid_back_pic'.$i]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                        $directors->nid_back_pic=$picturename;
                        $directorsParners['nid_back_pic'.$i]=$picturename;

                    }

                    $directors->save();
                }

                Session::put('directorsParners', $directorsParners);
                return redirect('/authorized-representative');
                break;
        }


    }

    public function partnerDeraactorEdit(Request $request)
    {

        $directorsParners= Session::get('directorsParners');
        $divisions = Division::all();


        $this->authorizedRepresentativeStore($request);

        return view('front-end/sme/partner-director-info-edit', compact('divisions', 'directorsParners'));
    }

    public function locationSesstionUpdate($request)
    {
        Session::forget('location');
        $profile_id=Profile::where('user_id',Auth::user()->id)->pluck('id');

        $directorsParners=[];
        foreach($profile_id as $key=>$id) {
            $key+=1;

            $presentDistrict = District::where('id', $request['present_district' . $key])->pluck('title')->first();
            $presentUpazila = Upazila::where('id', $request['present_upazila' . $key])->pluck('title')->first();
            $presentPostOffice = PostOffice::where('id', $request['presentpost_office' . $key])->pluck('title')->first();

            $permanentDistrict = District::where('id', $request['permanent_district'.$key] )->pluck('title')->first();
            $permanentUpazila = Upazila::where('id', $request['permanent_upazila'.$key])->pluck('title')->first();
            $permanentPostOffice = PostOffice::where('id', $request['permanentpost_office'.$key])->pluck('title')->first();

            $directorsParners['present_district'.$key] = $presentDistrict;
            $directorsParners['present_upazila'.$key] = $presentUpazila;
            $directorsParners['presentpost_office'.$key] =$presentPostOffice;

            $directorsParners['permanent_district'.$key] = $permanentDistrict;
            $directorsParners['permanent_upazila'.$key] = $permanentUpazila;
            $directorsParners['permanentpost_office'.$key] =$permanentPostOffice;

        }
        Session::put('location',$directorsParners);

        return $directorsParners;

    }


    public function UpdateValidation(array $directorsParners)
    {
        $rules = [];
        for($i=1;$i<=Auth::user()->businessInfo->total_director_partner; $i++)
        {
            $rules['owner_title'.$i] = 'required';
            $rules['owner_first_name'.$i] = 'required';
//            $rules['owner_middle_name'.$i] = 'required';
            $rules['owner_last_name'.$i] = 'required';
            $rules['position'.$i] = 'required';
            $rules['shareholder'.$i] = 'required';
            $rules['owner_nid'.$i] = 'required';
            $rules['date_of_birth'.$i] = 'required';
//            $rules['capture_pic'.$i] = 'required';
            $rules['present_division'.$i] = 'required';
            $rules['permanent_division'.$i] = 'required';
            $rules['present_district'.$i] = 'required';
            $rules['permanent_district'.$i] = 'required';
            $rules['present_upazila'.$i] = 'required';
            $rules['permanent_upazila'.$i] = 'required';
            $rules['presentpost_office'.$i] = 'required';
            $rules['permanentpost_office'.$i] = 'required';
            $rules['present_post_code'.$i] = 'required';
            $rules['post_code'.$i] = 'required';
            $rules['present_address'.$i] = 'required';
            $rules['permanent_address'.$i] = 'required';
        }

        $messages = array(
            'required' => 'This field is required.'
        );

        return Validator::make($directorsParners, $rules,$messages);
    }

    public function partnerDirectorUpdate(Request $request)
    {
        $directorData = Session::get('directorsParners');
        $profile_id = Profile::where('user_id',Auth::user()->id)->pluck('id');
        $directorsParners = $request->all();

        if(is_numeric($request['permanent_district1']) && is_numeric($request['permanent_division1']))
        {
            $this->locationSesstionUpdate($request);
            Session::get('location');
        }

//        $validator=$this->UpdateValidation($directorsParners);
//        if($validator->fails()) {
//            return Redirect::back()->withErrors($validator)->withInput();
//        }

        foreach($profile_id as $key=>$id)
        {
            $key+=1;
            $directors = Profile::find($id);
            $directors->owner_title = $request['owner_title' . $key];
            $directors->owner_first_name = $request['owner_first_name'.$key];
            $directors->owner_middle_name = $request['owner_middle_name' . $key];
            $directors->owner_last_name = $request['owner_last_name'.$key];
            $directors->owner_nid = $request['owner_nid' . $key];
            $directors->position = $request['position' . $key];
            $directors->shareholder = $request['shareholder' . $key];
            $directors->date_of_birth = $request['date_of_birth' . $key];
            $directors->post_code2 = $request['present_post_code' . $key];
            $directors->post_code3 = $request['post_code' . $key];
            $directors->present_address = $request['present_address' . $key];
            $directors->permanent_address = $request['permanent_address' . $key];

            if(is_numeric($request['present_district'.$key]) && is_numeric($request['present_division'.$key]))
            {

                $presentDivision = Division::where('id', $request['present_division'.$key])->pluck('title')->first();
                $presentDistrict = District::where('id', $request['present_district'.$key])->pluck('title')->first();
                $presentUpazila = Upazila::where('id', $request['present_upazila'.$key])->pluck('title')->first();
                $presentPostOffice = PostOffice::where('id', $request['presentpost_office'.$key])->pluck('title')->first();

                $directorsParners['present_division'.$key] = $presentDivision;
                $directorsParners['present_district'.$key] = $presentDistrict;
                $directorsParners['present_upazila'.$key] = $presentUpazila;
                $directorsParners['presentpost_office'.$key] =$presentPostOffice;

                $directors->present_division=$directorsParners['present_division'.$key];
                $directors->present_district=$directorsParners['present_district'.$key];
                $directors->present_upazila=$directorsParners['present_upazila'.$key];
                $directors->presentpost_office=$directorsParners['presentpost_office'.$key];
            }
            else
            {
                $presentDivision = Division::where('id', $request['present_division'.$key])->pluck('title')->first();

                $directorsParners['present_division'.$key] = $presentDivision;
                $directors->present_division=$directorsParners['present_division'.$key];
                $directors->present_district=$request['present_district'.$key];
                $directors->present_upazila=$request['present_upazila'.$key];
                $directors->presentpost_office=$request['presentpost_office'.$key];
            }

            if (is_numeric($request['permanent_district'.$key] ) && is_numeric($request['permanent_division'.$key]))
            {
                $PermanentDivision = Division::where('id', $request['permanent_division'.$key] )->pluck('title')->first();
                $permanentDistrict = District::where('id', $request['permanent_district'.$key] )->pluck('title')->first();
                $permanentUpazila = Upazila::where('id', $request['permanent_upazila'.$key])->pluck('title')->first();
                $permanentPostOffice = PostOffice::where('id', $request['permanentpost_office'.$key])->pluck('title')->first();

                $directorsParners['permanent_division'.$key] = $PermanentDivision;
                $directorsParners['permanent_district'.$key] = $permanentDistrict;
                $directorsParners['permanent_upazila'.$key] = $permanentUpazila;
                $directorsParners['permanentpost_office'.$key] =$permanentPostOffice;

                $directors->permanent_division=$directorsParners['permanent_division'.$key];
                $directors->permanent_district=$directorsParners['permanent_district'.$key];
                $directors->permanent_upazila=$directorsParners['permanent_upazila'.$key];
                $directors->permanentpost_office=$directorsParners['permanentpost_office'.$key];
            }
            else
            {
                $PermanentDivision = Division::where('id', $request['permanent_division'.$key] )->pluck('title')->first();
                $directorsParners['permanent_division'.$key] = $PermanentDivision;
                $directors->permanent_division=$directorsParners['permanent_division'.$key];

                $directors->permanent_district=$request['permanent_district'.$key];
                $directors->permanent_upazila=$request['permanent_upazila'.$key];
                $directors->permanentpost_office=$request['permanentpost_office'.$key];
            }

            if(isset($request['owner_pic'.$key]))
            {
                $picturename = time().$key.'-'.$request['owner_pic'.$key]->getClientOriginalName();
                $request['owner_pic'.$key]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                $directors->owner_pic=$picturename;
                $directorsParners['owner_pic'.$key] = $picturename;
            }
            else
            {
                if(isset($directorData['owner_pic'.$key]))
                {
                    $directors->owner_pic = $directorData['owner_pic'.$key];
                    $directorsParners['owner_pic'.$key] = $directorData['owner_pic'.$key];
                }
            }

            if(isset($request['nid_front_pic'.$key]))
            {
                $picturename = time().$key.'-'.$request['nid_front_pic'.$key]->getClientOriginalName();
                $request['nid_front_pic'.$key]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                $directors->nid_front_pic = $picturename;
                $directorsParners['nid_front_pic'.$key] = $picturename;
            }
            else
            {
                if(isset($directorData['nid_front_pic'.$key])) {
                    $directors->nid_front_pic = $directorData['nid_front_pic' . $key];
                    $directorsParners['nid_front_pic'.$key] = $directorData['nid_front_pic'.$key];
                }
            }


            if(isset($request['nid_back_pic'.$key]))
            {
                $picturename = time().$key.'-'.$request['nid_back_pic'.$key]->getClientOriginalName();
                $request['nid_back_pic'.$key]->move(public_path('ui/back-end/uploads/director-Partner-image/'), $picturename);
                $directors->nid_back_pic=$picturename;
                $directorsParners['nid_back_pic'.$key] = $picturename;
            }
            else
            {
                if(isset($directorData['nid_back_pic'.$key]))
                {
                    $directors->nid_back_pic = $directorData['nid_back_pic'.$key];
                    $directorsParners['nid_back_pic'.$key] = $directorData['nid_back_pic'.$key];
                }

            }
            $directors->save();
        }

        Session::put('directorsParners', $directorsParners);


        if (isset(Auth::user()->contactInfo->id)){
            return redirect('/authorized-representative/edit');
        }
        return redirect('/authorized-representative/edit')->with('massage', 'Save Success');
    }



    public function emailUpdate(EmailValidation $request){

        $data = $request->only('email', 'mobile_number');
        $id = Auth::user()->id;
        User::find($id)->update($data);
        VerifyUser::create(['user_id' => $id, 'token' => str_random(40)]);
        $user=Auth::user();
        Mail::to($request->email)->send(new VerifyMail($user));
        Session::forget('directorsParners');
        return redirect('email-verify');

//        return view('auth.sme-password')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }


    public function emailResend(Request $request)
    {
        $user=Auth::user();
        Mail::to(Auth::user()->email)->send(new VerifyMail($user));
        return redirect('email-verify')->with('resend', 'Send a fresh new e-mail.');
    }

    public function verifyMassage()
    {
        return view('auth.verify');
    }

    public function mobileVerify(Request $request)
    {
        $oldTime = Auth::user()->time_expired;
        $newTime = date(time());

        $timeDiferance = $newTime - $oldTime;

//        Session::put('timeDiferance', $timeDiferance);

        if($request->code == Session::get('otpCode' ))
        {
            if($timeDiferance<=300)
            {

                Auth::user()->mobile_verified=1;
                Auth::user()->save();

                return redirect('password')->with('massage', 'Mobile Number has been verified successfully!');
            }
            return redirect('mobile-verify')->with('massage', 'Your verification code time is expired. please resend another code. ');
        }
        return redirect('mobile-verify')->with('invalid', 'Your verification code is invalid!!');
    }

    public function smsResend(Request $request)
    {
        $to = Auth::user()->mobile_number;
        $token = "3b137b9da80a1a2f4a75b0ba51b6a82b";
        $otpCode = mt_rand(123108,567890);

        Session::put('otpCode', $otpCode);
        $otp = "Your Verification Code: ".$otpCode;
        $url = "http://api.greenweb.com.bd/api.php";
        $data= array(
            'to'=>"$to",
            'message'=>"$otp",
            'token'=>"$token"
        );
        // Add parameters in key value
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        curl_exec($ch);
        $id = Auth::user()->id;
        User::find($id)->update(['time_expired' => date(time())]);
        return redirect('mobile-verify')->with('resend', 'Your new verification code send.');
    }


    public function passwordUpdate(PasswordValidation $request)
    {


        $password = $request->only('password');

        $password['password'] = Hash::make($request->password);

        User::find(Auth::user()->update($password));
        return redirect('sme-question');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function authorizedRepresentativeForm()
    {
        return view('front-end/sme/authorized-representative');
    }

    public function authorizedRepresentativeStore(Request $request)
    {
        $data=$request->all();
        $data['user_id'] = Auth::user()->id;
        ContactInfo::create($data);
        return redirect('email-phone');
    }



    public function positionInfo()
    {
        $chairman = Input::get('position');
        $data = Profile::where([['user_id', '=', Auth::user()->id], ['position', $chairman]])->first();
        return response()->json($data);
    }

    public function shareHolder()
    {
        $chairman = Input::get('chairman');
        $dataCount = Profile::where([['user_id', '=', Auth::user()->id], ['position', $chairman]])->count();

        if($chairman=='Partner' && $dataCount>1)
        {
            $shareHolders=Profile::where([['user_id', '=', Auth::user()->id], ['position', $chairman]])->get();

            return response()->json($shareHolders);
        }
        elseif($chairman=='Shareholder Director' && $dataCount>1)
        {
            $shareHolders=Profile::where([['user_id', '=', Auth::user()->id], ['position', $chairman]])->get();
            return response()->json($shareHolders);
        }
        else{
            $shareHolders = Profile::where([['user_id', '=', Auth::user()->id], ['position', $chairman]])->first();

            return response()->json($shareHolders);
        }


    }
    public function singleShareHolder()
    {
        $id=Input::get('id');
        $singleData=Profile::where([['user_id', '=', Auth::user()->id], ['id', $id]])->first();
        return response()->json($singleData);
    }

    public function authorizedEdit()
    {
        if(Auth::user()->businessInfo->organization=='Public Limited' || Auth::user()->businessInfo->organization=='Public Limited')
        {
            $chairmans = Profile::where('user_id',Auth::user()->id)->where('position','Chairman ')->select('id','user_id', 'owner_title','owner_first_name','owner_middle_name','owner_last_name')->get();
            $managingDirectors = Profile::where('user_id',Auth::user()->id)->where('position','Managing Director')->select('id','user_id','owner_title','owner_first_name','owner_middle_name','owner_last_name')->get();
            $shareHolders = Profile::where('user_id',Auth::user()->id)->where('position','Shareholder Director')->select('id','user_id','owner_title','owner_first_name','owner_middle_name','owner_last_name')->get();

        }
        else
        {
            $partners=Profile::where('user_id',Auth::user()->id)->where('position','Partner')->select('id','user_id', 'owner_title','owner_first_name','owner_middle_name','owner_last_name')->get();
            $managingPartners = Profile::where('user_id',Auth::user()->id)->where('position','Managing Partner')->select('id','user_id','owner_title','owner_first_name','owner_middle_name','owner_last_name')->get();

        }
        return view('/front-end/sme/authorized-representative-edit',compact('shareHolders', 'partners', 'managingPartners', 'managingDirectors', 'chairmans'));
    }
    public function authorizedUpdate(Request $request)
    {
        $contactUpdate=ContactInfo::find(Auth::user()->contactInfo->id);

        $contactUpdate['title']=$request->title;
        $contactUpdate['profile_id']=$request->profile_id;
        $contactUpdate['first_name']=$request->first_name;
        $contactUpdate['middle_name']=$request->middle_name;
        $contactUpdate['last_name']=$request->last_name;
        $contactUpdate['position']=$request->position;
        $contactUpdate['shareholder']=$request->shareholder;
        $contactUpdate['date_of_birth']=$request->date_of_birte;
        $contactUpdate['nid_no']=$request->nid_no;
        $contactUpdate['present_division']=$request->present_division;
        $contactUpdate['present_district']=$request->present_district;
        $contactUpdate['present_upazila']=$request->present_updazila;
        $contactUpdate['present_postoffice']=$request->present_postcode;
        $contactUpdate['present_postcode']=$request->present_postcode;
        $contactUpdate['permanent_division']=$request->permanent_division;
        $contactUpdate['permanent_district']=$request->permanent_district;
        $contactUpdate['permanent_upazila']=$request->permanent_upazila;
        $contactUpdate['permanent_postoffice']=$request->permanent_postoffice;
        $contactUpdate['permanent_postcode']=$request->permanent_postcode;
        $contactUpdate['present_address']=$request->present_address;
        $contactUpdate['permanent_address']=$request->permanent_address;

        $contactUpdate->save();

        return redirect('/email-phone');
    }
}
