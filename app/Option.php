<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable=['question_id','title','status'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
