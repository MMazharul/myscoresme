<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fieldtype extends Model
{
    protected $fillable=['title','status'];
}
