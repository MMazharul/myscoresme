<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    protected $fillable = ['question_category_id', 'question_id','dependent','label_en','label_bn','field_type',
        'help_en','help_bn','example_en','example_bn','status','column_name','placeholder'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
