<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessBackground extends Model
{
    protected $fillable=['user_id','email_address','enterprize', 'firm', 'business_startd','business_address', 'business_located','nature','business_employee', 'business_asset','owner_name','marital_status'];
}
