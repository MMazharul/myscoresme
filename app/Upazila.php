<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    protected $fillable=['district_id','district_name','title','geo_code'];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function postOffices()
    {
        return $this->hasMany(PostOffice::class);
    }
}
