<!doctype html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="Borrow - is the loan company, Business Website Template.">

    <meta name="keywords" content="Financial Website Template, Bootstrap Template, Loan Product, Personal Loan">



    <title><?php echo $__env->yieldContent('title'); ?></title>

    <script src="<?php echo e(asset('ajax/jquery.min.js')); ?>"></script> 

    <!-- multi-select -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap -->

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/style.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/fontello.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/css/dropify.min.css">

    <!-- Login design -->
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/css/main.css">
    <!--===============================================================================================-->


    <!-- Camera Webcam -->
    <script src="<?php echo e(asset('/camera-ui/js/webcam.min.js')); ?>"></script>


    <!-- Daterange picker plugins css -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">



    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CMerriweather:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- owl Carousel Css -->

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/owl.carousel.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/owl.theme.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script src="<?php echo e(asset('/')); ?>ui/back-end/vue/vue.js"></script>

    <style>
        .hidden{
            display:none;
        }
    </style>

</head>

<body>

<?php echo $__env->make('front-end.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



<?php echo $__env->yieldContent('content'); ?>



<a href="#0" class="cd-top" title="Go to top">Top</a>



<?php echo $__env->make('front-end.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/jquery/jquery.min.js"></script>



<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="<?php echo e(asset('/')); ?>ui/front-end/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/menumaker.js"></script>



<!-- multi select -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/multiselect/js/jquery.multi-select.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>



<!-- sticky header -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/jquery.sticky.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/sticky-header.js"></script>

<!-- slider script -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/slider-carousel.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/service-carousel.js"></script>

<!-- Back to top script -->

<script src="<?php echo e(asset('/')); ?>ui/front-end/js/back-to-top.js" type="text/javascript"></script>

<script src="<?php echo e(asset('ajax/location.js')); ?>"></script>

<script src="<?php echo e(asset('ajax/branch.js')); ?>"></script>
<script src="<?php echo e(asset('ajax/sector.js')); ?>"></script>



<!-- Date Picker Plugin JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Date range Plugin JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/daterangepicker/daterangepicker.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/moment/moment.js"></script>







<!--Custom JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/custom.min.js"></script>



<!-- jQuery file upload -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/js/dropify.min.js"></script>

<?php if(isset(Auth::User()->businessInfo->total_director_partner)): ?>
    <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>
        <script type="text/javascript">

            $('#present_division<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var division_id = e.target.value;

                getPresentDistrict(division_id, <?php echo e($i); ?>);
            });

            $('#permanent_division<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var division_id = e.target.value;

                getPermanentDistrict(division_id, <?php echo e($i); ?>)
            });

            function getPresentDistrict(divisionId, step){
                $.get('/finddistrict?division_id=' + divisionId,function(data) {
                    console.log(data);
                    var district = $('#present_district'+step);


                    district.empty();
                    district.append('<option value="0" disabled="true" selected="true">--Select District--</option>');
                    $.each(data, function(index, districtObj){
                        district.append('<option value="'+ districtObj.id +'">'+ districtObj.title +'</option>');
                    })
                });
            }

            function getPermanentDistrict(divisionId, step){
                $.get('/finddistrict?division_id=' + divisionId,function(data) {
                    console.log(data);

                    var district = $('#permanent_district'+step);
                    district.empty();
                    district.append('<option value="0" disabled="true" selected="true">--Select District--</option>');
                    $.each(data, function(index, districtObj){
                        district.append('<option value="'+ districtObj.id +'">'+ districtObj.title +'</option>');
                    })
                });
            }

            $('#present_district<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var district_id = e.target.value;
                console.log(district_id);
                getPresentUpazila(district_id, <?php echo e($i); ?>)
            });

            $('#permanent_district<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var district_id = e.target.value;
                console.log(district_id);
                getPermanentUpazila(district_id,<?php echo e($i); ?>)
            });


            function  getPresentUpazila(districtId, step) {
                $.get('/findthana?district_id=' + districtId,function(data) {
                    var updazila = $('#present_upazila'+step);
                    console.log(data);
                    updazila.empty();
                    updazila.append('<option value="0" disabled="true" selected="true">-Select Upazila-</option>');
                    $.each(data, function(index, thanaObj){
                        updazila.append('<option value="'+ thanaObj.id +'">'+ thanaObj.title +'</option>');
                    })
                });
            }

            function  getPermanentUpazila(districtId, step) {
                $.get('/findthana?district_id=' + districtId,function(data) {
                    var updazila = $('#permanent_upazila'+step);
                    console.log(data);
                    updazila.empty();
                    updazila.append('<option value="0" disabled="true" selected="true">-Select Upazila-</option>');
                    $.each(data, function(index, thanaObj){
                        updazila.append('<option value="'+ thanaObj.id +'">'+ thanaObj.title +'</option>');
                    })
                });
            }

            $('#present_upazila<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var upazilaId = e.target.value;
                getPresentPostOffice(upazilaId, <?php echo e($i); ?>)
            });

            $('#permanent_upazila<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var upazilaId = e.target.value;
                getPermanentPostOffice(upazilaId,<?php echo e($i); ?>)
            });


            function getPresentPostOffice(upazilaId,step) {
                $.get('/find-suboffice?thana_id=' + upazilaId,function(data) {
                    var postoffice=$('#present_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');
                    $.each(data, function(index, subofficeObj){
                        postoffice.append('<option value="'+ subofficeObj.id +'">'+ subofficeObj.title +'</option>');
                    })
                });
            }


            function getPermanentPostOffice(upazilaId,step) {
                $.get('/find-suboffice?thana_id=' + upazilaId,function(data) {
                    var postoffice=$('#permanent_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');
                    $.each(data, function(index, subofficeObj){
                        postoffice.append('<option value="'+ subofficeObj.id +'">'+ subofficeObj.title +'</option>');
                    })
                });
            }

            $('#present_suboffice<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var postCodeId = e.target.value;
                getPresentPostCode(postCodeId, <?php echo e($i); ?>)
            });

            $('#permanent_suboffice<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var postCodeId = e.target.value;
                getPermanentPostCode(postCodeId, <?php echo e($i); ?>)
            });


            function getPresentPostCode(postCodeId,step) {
                $.get('/find-post-code?postoffice_id=' + postCodeId,function(data) {
                    var postCode=$('#present_postcode'+step);
                    console.log(data);
                    postCode.empty();
                    postCode.append('<label>Post Code</label>');

                   /* $.each(data, function(index, subofficeObj){*/
                        postCode.append('<input type="text" id="post_code'+step+'" name="post_code'+step+'"  class="form-control" value="'+ data.post_code +'" readonly>');
                   /* })*/
                });

            }

            function getPermanentPostCode(postCodeId,step) {
                $.get('/find-post-code?postoffice_id=' + postCodeId,function(data) {
                    var postCode=$('#permanent_postcode'+step);
                    postCode.empty();
                    postCode.append('<label>Post Code</label>');

                    /*$.each(data, function(index, subofficeObj){*/
                        postCode.append('<input type="text" id="post_code'+step+'" name="post_code'+step+'"  class="form-control" value="'+ data.post_code +'" readonly>');
             /*       })*/
                });

            }

        </script>
    <?php endfor; ?>
<?php endif; ?>





<script>

    /*******************************************/

    // Basic Date Range Picker

    /*******************************************/

    $('.daterange').daterangepicker();



    /*******************************************/

    // Date & Time

    /*******************************************/

    $('.datetime').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        locale: {

            format: 'MM/DD/YYYY h:mm A'

        }

    });



    /*******************************************/

    //Calendars are not linked

    /*******************************************/

    $('.timeseconds').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        timePicker24Hour: true,

        timePickerSeconds: true,

        locale: {

            format: 'MM-DD-YYYY h:mm:ss'

        }

    });



    /*******************************************/

    // Single Date Range Picker

    /*******************************************/

    $('.singledate').daterangepicker({

        singleDatePicker: true,

        showDropdowns: true

    });



    /*******************************************/

    // Auto Apply Date Range

    /*******************************************/

    $('.autoapply').daterangepicker({

        autoApply: true,

    });



    /*******************************************/

    // Calendars are not linked

    /*******************************************/

    $('.linkedCalendars').daterangepicker({

        linkedCalendars: false,

    });



    /*******************************************/

    // Date Limit

    /*******************************************/

    $('.dateLimit').daterangepicker({

        dateLimit: {

            days: 7

        },

    });



    /*******************************************/

    // Show Dropdowns

    /*******************************************/

    $('.showdropdowns').daterangepicker({

        showDropdowns: true,

    });



    /*******************************************/

    // Show Week Numbers

    /*******************************************/

    $('.showweeknumbers').daterangepicker({

        showWeekNumbers: true,

    });



    /*******************************************/

    // Date Ranges

    /*******************************************/

    $('.dateranges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        }

    });



    /*******************************************/

    // Always Show Calendar on Ranges

    /*******************************************/

    $('.shawCalRanges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        alwaysShowCalendars: true,

    });



    /*******************************************/

    // Top of the form-control open alignment

    /*******************************************/

    $('.drops').daterangepicker({

        drops: "up" // up/down

    });



    /*******************************************/

    // Custom button options

    /*******************************************/

    $('.buttonClass').daterangepicker({

        drops: "up",

        buttonClasses: "btn",

        applyClass: "btn-info",

        cancelClass: "btn-danger"

    });



    /*******************************************/

    // Language

    /*******************************************/

    $('.localeRange').daterangepicker({

        ranges: {

            "Aujourd'hui": [moment(), moment()],

            'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],

            'Les 7 derniers jours': [moment().subtract('days', 6), moment()],

            'Les 30 derniers jours': [moment().subtract('days', 29), moment()],

            'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],

            'le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]

        },

        locale: {

            applyLabel: "Vers l'avant",

            cancelLabel: 'Annulation',

            startLabel: 'Date initiale',

            endLabel: 'Date limite',

            customRangeLabel: 'SÃ©lectionner une date',

            // daysOfWeek: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi','Samedi'],

            daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

            monthNames: ['Janvier', 'fÃ©vrier', 'Mars', 'Avril', 'ÐœÐ°i', 'Juin', 'Juillet', 'AoÃ»t', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],

            firstDay: 1

        }

    });

</script>


<script>
    jQuery(
        function($) {
            $('#message').fadeIn (5550);
            $('#message').fadeOut (5550);
        }
    )
</script>


<script>

    $(document).ready(function() {

        // Basic

        $('.dropify').dropify();



        // Translated

        $('.dropify-fr').dropify({

            messages: {

                default: 'Glissez-déposez un fichier ici ou cliquez',

                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',

                remove: 'Supprimer',

                error: 'Désolé, le fichier trop volumineux'

            }

        });



        // Used events

        var drEvent = $('#input-file-events').dropify();



        drEvent.on('dropify.beforeClear', function(event, element) {

            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");

        });



        drEvent.on('dropify.afterClear', function(event, element) {

            alert('File deleted');

        });



        drEvent.on('dropify.errors', function(event, element) {

            console.log('Has Errors');

        });



        var drDestroy = $('#input-file-to-destroy').dropify();

        drDestroy = drDestroy.data('dropify')

        $('#toggleDropify').on('click', function(e) {

            e.preventDefault();

            if (drDestroy.isDropified()) {

                drDestroy.destroy();

            } else {

                drDestroy.init();

            }

        })

    });

</script>


<script>

    jQuery(document).ready(function() {

        // Switchery

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        $('.js-switch').each(function() {

            new Switchery($(this)[0], $(this).data());

        });

        // For select 2

        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin

        $(".vertical-spin").TouchSpin({

            verticalbuttons: true,

            verticalupclass: 'ti-plus',

            verticaldownclass: 'ti-minus'

        });

        var vspinTrue = $(".vertical-spin").TouchSpin({

            verticalbuttons: true

        });

        if (vspinTrue) {

            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();

        }

        $("input[name='tch1']").TouchSpin({

            min: 0,

            max: 100,

            step: 0.1,

            decimals: 2,

            boostat: 5,

            maxboostedstep: 10,

            postfix: '%'

        });

        $("input[name='tch2']").TouchSpin({

            min: -1000000000,

            max: 1000000000,

            stepinterval: 50,

            maxboostedstep: 10000000,

            prefix: '$'

        });

        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({

            initval: 40

        });

        $("input[name='tch5']").TouchSpin({

            prefix: "pre",

            postfix: "post"

        });

        // For multiselect

        $('#pre-selected-options').multiSelect();

        $('#optgroup').multiSelect({

            selectableOptgroup: true

        });

        $('#public-methods').multiSelect();

        $('#select-all').click(function() {

            $('#public-methods').multiSelect('select_all');

            return false;

        });

        $('#deselect-all').click(function() {

            $('#public-methods').multiSelect('deselect_all');

            return false;

        });

        $('#refresh').on('click', function() {

            $('#public-methods').multiSelect('refresh');

            return false;

        });

        $('#add-option').on('click', function() {

            $('#public-methods').multiSelect('addOption', {

                value: 42,

                text: 'test 42',

                index: 0

            });

            return false;

        });

        $(".ajax").select2({

            ajax: {

                url: "https://api.github.com/search/repositories",

                dataType: 'json',

                delay: 250,

                data: function(params) {

                    return {

                        q: params.term, // search term

                        page: params.page

                    };

                },

                processResults: function(data, params) {

                    // parse the results into the format expected by Select2

                    // since we are using custom formatting functions we do not need to

                    // alter the remote JSON data, except to indicate that infinite

                    // scrolling can be used

                    params.page = params.page || 1;

                    return {

                        results: data.items,

                        pagination: {

                            more: (params.page * 30) < data.total_count

                        }

                    };

                },

                cache: true

            },

            escapeMarkup: function(markup) {

                return markup;

            }, // let our custom formatter work

            minimumInputLength: 1,

            templateResult: formatRepo, // omitted for brevity, see the source of this page

            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

        });

    });

</script>



</body>

</html>
