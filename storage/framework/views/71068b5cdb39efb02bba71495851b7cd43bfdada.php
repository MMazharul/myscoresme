<?php $__env->startSection('content'); ?>



    <div class="card">

        <?php if(session('message')): ?>
            <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong><?php echo e(session('message')); ?></strong>
            </div>
        <?php endif; ?>

        <div class="card-body">

            <div class="col-12 row">



                <div class="col-11">

                    <h2><i class="fa fa-list"></i> Question List</h2>

                </div>

                <h3 class="box-title m-b-0 col-1"><a href="<?php echo e(url('question/create')); ?>" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>



            </div>

            <div class=" m-t-40">

                <table id="myTable" class="table table-bordered table-striped">

                    <thead>

                    <tr>



                        <th>Q.C Name</th>



                        <th>Label En</th>

                        <th>Lable bn</th>

                        <th>Field Type</th>

                        <th>Help En</th>

                        <th>Help Bn</th>

                        <th>Example En</th>

                        <th>Example Bn</th>

                        <th>Status</th>

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                        <tr>

                            <td><?php echo e($question->questionCategory['question_category']); ?></td>

                            <td><?php echo e($question->label_en); ?></td>

                            <td><?php echo e($question->label_bn); ?></td>

                            <td><?php echo e($question->field_type); ?></td>

                            <td><?php echo e($question->help_en); ?></td>

                            <td><?php echo e($question->help_bn); ?></td>

                            <td><?php echo e($question->example_en); ?></td>

                            <td><?php echo e($question->example_bn); ?></td>



                            <td>

                                <?php if($question->status==1): ?>

                                    <?php echo e('Publish'); ?>


                                <?php elseif($question->status==0): ?>

                                    <?php echo e('Unpublish'); ?>


                                <?php endif; ?>

                            </td>



                            <td>

                                <div class="dropdown row">

                                    <?php if($question->status == 1): ?>

                                        <a href="<?php echo e(url('question/status/'.$question->id)); ?>" title="Click to Unpublished"

                                           class="btn btn-success waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                            <i class="fa fa-check"></i>

                                        </a>

                                    <?php else: ?>

                                        <a href="<?php echo e(url('/question/status/'.$question->id)); ?>" title="Click to Published"

                                           class="btn btn-warning waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                            <i class="fa fa-warning"></i>

                                        </a>

                                    <?php endif; ?>



                                    <a href="<?php echo e(url('/question/'.$question->id)); ?>" title="Click to More details"

                                       class="btn btn-outline-info ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                        <i class="mdi mdi-eye"></i>

                                    </a>





                                    <div class="btn-group ml-1">

                                        <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <i class="icon mdi mdi-settings"></i></button>

                                        <div class="dropdown-menu">

                                            <a class="dropdown-item" href="<?php echo e(url('/question/'.$question->id.'/edit' )); ?>"><i

                                                        class="mdi mdi-table-edit"></i> Edit</a>

                                            <a class="dropdown-item" href="<?php echo e(url('/question-delete/'.$question->id)); ?>"><i

                                                        class="mdi mdi-delete"></i> Delete</a>

                                        </div>

                                    </div>





                                </div>

                            </td>





                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>