<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Edit Role <?php echo e($role->id); ?></div>
        <div class="panel-body">

            <?php if($errors->any()): ?>
                <ul class="alert alert-danger">
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>

            <?php echo Form::model($role, [
                'method' => 'PATCH',
                'url' => ['/' . Config("authorization.route-prefix") . '/roles', $role->id],
                'class' => 'form-horizontal',
                'files' => true
            ]); ?>


            <?php echo $__env->make('vendor.authorize.roles.form', ['submitButtonText' => 'Update'], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo Form::close(); ?>


        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.authorize.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>