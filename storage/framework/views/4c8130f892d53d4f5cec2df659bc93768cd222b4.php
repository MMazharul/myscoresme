<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">

    <!-- Favicon icon -->

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('/')); ?>ui/back-end/assets/images/favicon.png">

    <title>Admin</title>



    <!-- Bootstrap Core CSS -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- chartist CSS -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/css-chart/css-chart.css" rel="stylesheet">

    <!-- toast CSS -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/css/style.css" rel="stylesheet">





    <link rel="stylesheet" href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/css/dropify.min.css">



    <!-- You can change the theme colors from here -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/css/colors/blue.css" id="theme" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>



    <![endif]-->

    <script>

        (function(i, s, o, g, r, a, m) {

            i['GoogleAnalyticsObject'] = r;

            i[r] = i[r] || function() {

                (i[r].q = i[r].q || []).push(arguments)

            }, i[r].l = 1 * new Date();

            a = s.createElement(o), m = s.getElementsByTagName(o)[0];

            a.async = 1;

            a.src = g;

            m.parentNode.insertBefore(a, m)

        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-85622565-1', 'auto');

        ga('send', 'pageview');

    </script>



    <script src="<?php echo e(asset('/')); ?>ui/back-end/vue/vue.js"></script>



</head>



<body class="fix-header fix-sidebar card-no-border">

<!-- ============================================================== -->

<!-- Preloader - style you can find in spinners.css -->

<!-- ============================================================== -->













<!-- ============================================================== -->

<!-- Main wrapper - style you can find in pages.scss -->

<!-- ============================================================== -->

<div id="main-wrapper">

    <!-- ============================================================== -->

    <!-- Top bar header - style you can find in pages.scss -->

    <!-- ============================================================== -->

<?php echo $__env->make('admin.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- ============================================================== -->

    <!-- End Topbar header -->

    <!-- ============================================================== -->

    <!-- ============================================================== -->

    <!-- Left Sidebar - style you can find in sidebar.scss  -->

    <!-- ============================================================== -->

<?php echo $__env->make('admin.partials.nav', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- ============================================================== -->

    <!-- End Left Sidebar - style you can find in sidebar.scss  -->

    <!-- ============================================================== -->

    <!-- ============================================================== -->

    <!-- Page wrapper  -->

    <!-- ============================================================== -->

    <div class="page-wrapper">

        <!-- ============================================================== -->

        <!-- Container fluid  -->

        <!-- ============================================================== -->

    <?php echo $__env->yieldContent('content'); ?>

    <!-- ============================================================== -->

        <!-- End Container fluid  -->

        <!-- ============================================================== -->

        <!-- ============================================================== -->

        <!-- footer -->

        <!-- ============================================================== -->

        <footer class="footer">

            © 2017 Monster Admin by wrappixel.com

        </footer>

        <!-- ============================================================== -->

        <!-- End footer -->

        <!-- ============================================================== -->

    </div>

    <!-- ============================================================== -->

    <!-- End Page wrapper  -->

    <!-- ============================================================== -->

</div>

<!-- ============================================================== -->

<!-- End Wrapper -->

<!-- ============================================================== -->

<!-- ============================================================== -->

<!-- All Jquery -->

<!-- ============================================================== -->



<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap tether Core ui/JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap/js/popper.min.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/jquery.slimscroll.js"></script>

<!--Wave Effects -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/waves.js"></script>

<!--Menu sidebar -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/sidebarmenu.js"></script>

<!--stickey kit -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>

<!--Custom JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/custom.min.js"></script>

<!-- ============================================================== -->

<!-- This page plugins -->

<!-- ============================================================== -->

<!-- chartist chart -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/chartist-js/dist/chartist.min.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>

<!-- Chart JS -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/echarts/echarts-all.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/toast-master/js/jquery.toast.js"></script>

<!-- Chart JS -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/dashboard1.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/toastr.js"></script>

<!-- This is data table -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/datatables/jquery.dataTables.min.js"></script>



<!-- jQuery file upload -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/js/dropify.min.js"></script>



<script>

    $(document).ready(function() {

        // Basic
        $('.dropify').dropify();
        // Translated

        $('.dropify-fr').dropify({

            messages: {

                default: 'Glissez-déposez un fichier ici ou cliquez',

                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',

                remove: 'Supprimer',

                error: 'Désolé, le fichier trop volumineux'
            }
        });



        // Used events

        var drEvent = $('#input-file-events').dropify();



        drEvent.on('dropify.beforeClear', function(event, element) {

            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");

        });



        drEvent.on('dropify.afterClear', function(event, element) {

            alert('File deleted');

        });



        drEvent.on('dropify.errors', function(event, element) {

            console.log('Has Errors');

        });



        var drDestroy = $('#input-file-to-destroy').dropify();

        drDestroy = drDestroy.data('dropify')

        $('#toggleDropify').on('click', function(e) {

            e.preventDefault();

            if (drDestroy.isDropified()) {

                drDestroy.destroy();

            } else {

                drDestroy.init();

            }

        })

    });

</script>







<script>

    // $.toast({

    //     heading: 'Welcome to Monster admin',

    //     text: 'Use the predefined ones, or specify a custom position object.',

    //     position: 'top-right',

    //     loaderBg: '#ff6849',

    //     icon: 'info',

    //     hideAfter: 3000,

    //     stack: 6

    // });

</script>



<script>
    jQuery(
        function($) {
            $('#message').fadeIn (5050);
            $('#message').fadeOut (5050);
            /*  $('#message').fadeOut (550);*/
            /* $('#message').fadeIn (1050);
             $('#message').fadeOut (550);*/
        }
    )
</script>





// DataTable Script start//



<script>

    $(document).ready(function() {

        $('#myTable').DataTable();

        $(document).ready(function() {

            var table = $('#example').DataTable({

                "columnDefs": [{

                    "visible": false,

                    "targets": 2

                }],

                "order": [

                    [2, 'asc']

                ],

                "displayLength": 25,

                "drawCallback": function(settings) {

                    var api = this.api();

                    var rows = api.rows({

                        page: 'current'

                    }).nodes();

                    var last = null;

                    api.column(2, {

                        page: 'current'

                    }).data().each(function(group, i) {

                        if (last !== group) {

                            $(rows).eq(i).before('<tr class="group"><td colspan="5">'+ group +  '</td></tr>');

                            last = group;

                        }

                    });

                }

            });

            // Order by the grouping

            $('#example tbody').on('click', 'tr.group', function() {

                var currentOrder = table.order()[0];

                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {

                    table.order([2, 'desc']).draw();

                } else {

                    table.order([2, 'asc']).draw();

                }

            });

        });

    });

    $('#example23').DataTable({

        dom: 'Bfrtip',

        buttons: [

            'copy', 'csv', 'excel', 'pdf', 'print'

        ]

    });

</script>


<script>
    'use strict';
    // Grab elements, create settings, etc.
    var video = document.getElementById('video');
    var   photo = document.getElementById('photo');
    var    canvas = document.getElementById('canvas');
    var   context = canvas.getContext('2d');
    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            //video.src = window.URL.createObjectURL(stream);
            alert(stream);
            video.srcObject = stream;
            video.play();
        });
    }


    // var video = document.getElementById('video');

    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function() {

        context.drawImage(video, 0, 0, 400, 300);
        photo.setAttribute('src', canvas.toDataURL('image/png'));
    });
</script>



<!-- ============================================================== -->

<!-- Style switcher -->

<!-- ============================================================== -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>--}}

<script src="<?php echo e(asset('ajax/location.js')); ?>"></script>

</body>



</html>





















