<?php $__env->startSection('content'); ?>





    <div class="card">

        <?php if(session('message')): ?>
            <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong><?php echo e(session('message')); ?></strong>
            </div>
        <?php endif; ?>

        <div class="card-body">

            <div class="col-12 row">

                <div class="col-11">

                    <h2><i class="fa fa-list"></i> Division</h2>

                </div>

                <h3 class="box-title m-b-0 col-1"><a href="<?php echo e(url('division/create')); ?>" class="btn btn-dropbox"><i class="mdi mdi-plus-circle"></i> Add New</a></h3>

            </div>





            <div class="table-responsive m-t-40">

                <table id="myTable" class="table table-bordered table-striped">

                    <thead>

                    <tr>

                        <th>Division Name</th>

                        <th>Division Geo Code</th>

                        

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>

                            <td><?php echo e($division->title); ?></td>

                            <td><?php echo e($division->geo_code); ?></td>



                            <td>

                                <div class="dropdown row">



                                    <a href="<?php echo e(url('/division/'.$division->id)); ?>" title="Click to More details"

                                       class="btn btn-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                        <i class="mdi mdi-eye"></i>

                                    </a>





                                    <div class="btn-group ml-1">

                                        <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <i class="icon mdi mdi-settings"></i></button>

                                        <div class="dropdown-menu">

                                            <a class="dropdown-item" href="<?php echo e(url('/division/'.$division->id.'/edit' )); ?>"><i

                                                        class="mdi mdi-table-edit"></i> Edit</a>

                                            <a class="dropdown-item"   href="<?php echo e(url('/division-delete/'.$division->id)); ?>" ><i

                                                        class="mdi mdi-delete"></i> Delete</a>

                                        </div>

                                    </div>



                                </div>

                            </td>





                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

    <script>

        function confirm_delete()

        {

            return confirm("Are You Wanted Delete");

        }

    </script>





<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>