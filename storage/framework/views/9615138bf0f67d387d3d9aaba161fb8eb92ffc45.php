<?php $__env->startSection('content'); ?>
  <div class="card">
      <?php if(session('message')): ?>
          <div class="alert alert-success alert-dismissible" role="alert"  id="message">
              <div class="icon"><span class="mdi mdi-check"></span></div>
              <div class="message text-center "><?php echo e(session('message')); ?></div>
          </div>
      <?php endif; ?>
      <div class="card-body">
          <div class="col-12 row">

              <div class="col-11">
                  <h2><i class="fa fa-list"></i> Sub-sector List</h2>
              </div>

              <h3 class="box-title m-b-0 col-1"><a href="<?php echo e(url('sub-sector/create')); ?>" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>

          </div>
          <div class="table-responsive m-t-40">
              <table id="myTable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>Sub Sector Name</th>
                      <th>Sub Sector Code</th>
                      
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $subSectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subSector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                          <td><?php echo e($subSector->sub_sector); ?></td>
                          <td><?php echo e($subSector->code); ?></td>

                          <td>
                              <?php if($subSector->status==1): ?>
                                  <?php echo e('Publish'); ?>

                              <?php elseif($subSector->status==0): ?>
                                  <?php echo e('Unpublish'); ?>

                              <?php endif; ?>
                          </td>

                          <td>
                              <div class="dropdown row">
                                  <?php if($subSector->status == 1): ?>
                                      <a href="<?php echo e(url('sub-sector/status/'.$subSector->id)); ?>" title="Click to Unpublished"
                                         class="btn btn-success waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fa fa-check"></i>
                                      </a>
                                  <?php else: ?>
                                      <a href="<?php echo e(url('/sub-sector/status/'.$subSector->id)); ?>" title="Click to Published"
                                         class="btn btn-warning waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                          <i class="fa fa-warning"></i>
                                      </a>
                                  <?php endif; ?>
                                  <div class="btn-group ml-1">
                                      <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                          <i class="icon mdi mdi-settings"></i></button>
                                      <div class="dropdown-menu">
                                          <a class="dropdown-item" href="<?php echo e(url('sub-sector/'.$subSector->id.'/edit' )); ?>"><i
                                                      class="mdi mdi-table-edit"></i> Edit</a>
                                          <a class="dropdown-item" href="<?php echo e(url('/sub-sector-delete/'.$subSector->id)); ?>"><i
                                                      class="mdi mdi-delete"></i> Delete</a>
                                      </div>
                                  </div>
                              </div>
                          </td>


                      </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
              </table>
          </div>
      </div>
  </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>