<?php $__env->startSection('title', 'Register'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Authorized Representative</h3></div>
                    <div class="card-body">
                        <form class="form-horizontal" action="<?php echo e(url('/authorized-representative/store')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>


                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label">Authorized Representative of :</label>
                                <div class="col-sm-8">
                                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?php echo e(\Illuminate\Support\Facades\Auth::user()->profile->enterprise_name); ?>">
                                </div>
                            </div><hr>

                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-4 col-form-label">Position/Status in Company :</label>
                                <div class="col-sm-8">
                                    <?php if(Auth::user()->profile->organization == 'Public Limited' || Auth::user()->profile->organization == 'Private Limited'): ?>
                                        <div class="form-check mt-1" >
                                            <input class="form-check-input" onclick="getPositionInfo()" type="radio" name="position" value="Chairman" id="position1">
                                            <label class="form-check-label" for="position1">
                                                Chairman
                                            </label>
                                        </div>
                                        <div class="form-check mt-1">
                                            <input class="form-check-input" onclick="getManagingDirector()" type="radio" name="position" value="Managing Director" id="position2">
                                            <label class="form-check-label" for="position2">
                                                Managing Director
                                            </label>
                                        </div>
                                        <div class="form-check mt-1">
                                            <input class="form-check-input" onclick="getShareHolder()" type="radio" name="position" value="Shareholder Director" id="position3">
                                            <label class="form-check-label" for="position3">
                                                Share Holder Director
                                            </label>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-check mt-1">
                                            <input class="form-check-input" onclick="getManagingDirector()" type="radio" name="position" value="Managing Partner" id="position2">
                                            <label class="form-check-label" for="position">
                                                Managing Partner
                                            </label>
                                        </div>
                                        <div class="form-check mt-1">
                                            <input class="form-check-input" onclick="getShareHolder()" type="radio" name="position" value="Partner" id="position3">
                                            <label class="form-check-label" for="position2">
                                                Partner
                                            </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><hr>

                            <div id="shareholder" class="form-group row hidden">
                                <label for='staticEmail' class='col-sm-5 col-form-label'>Please Select one Share Holder Name :</label>
                                <div class="col-sm-7" id="share">

                                </div>
                            </div>

                            <div id="conduct_info" class="hidden">
                                <div class="form-row">
                                    <div class="form-group col-md-3" >
                                        <label for="owner_title">Title (Mrs./Mr./Ms.)</label>
                                        <select id="owner_title" class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" name="title">
                                            <option disabled selected>Select Your Title</option>
                                            <option value="Mrs." <?php echo e(old('title') == "Mrs." ? 'selected' : ''); ?>>Mrs.</option>
                                            <option value="Mr." <?php echo e(old('title') == "Mr." ? 'selected' : ''); ?>>Mr.</option>
                                            <option value="Ms" <?php echo e(old('title') == "Ms" ? 'selected' : ''); ?>>Ms</option>
                                        </select>
                                        <?php if($errors->has('title')): ?>
                                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('title')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-3" id="owner_first_name">
                                        <label for="owner_first_name">First Name</label>
                                        <input  type="text"
                                                class="form-control<?php echo e($errors->has('first_name') ? ' is-invalid' : ''); ?>"
                                                name="first_name" value="" placeholder="Enter your first name">
                                        <?php if($errors->has('first_name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('first_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-3" id="owner_middle_name">
                                        <label for="middle_name">Middle Name</label>
                                        <input type="text" class="form-control<?php echo e($errors->has('middle_name') ? ' is-invalid' : ''); ?>"
                                               name="middle_name" value="<?php echo e(old('middle_name')); ?>" placeholder="Enter your middle name">
                                        <?php if($errors->has('middle_name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('middle_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-3" id="owner_last_name">
                                        <label for="last_name">Last Name</label>
                                        <input  type="text" class="form-control<?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>"
                                                name="last_name" value="<?php echo e(old('last_name')); ?>" placeholder="Enter your last name">
                                        <?php if($errors->has('last_name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4" >
                                        <label for="organization">Position in the Organizatrion</label>
                                        <select id="organization" class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?>"
                                                name="position">
                                            <option disabled selected>Choose a option</option>

                                        </select>
                                        <?php if($errors->has('position')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('position')); ?></strong>
                                                    </span>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group col-md-4" id="shareholder_persent">
                                        <label for="shareholder">Shareholder (%)</label>
                                        <input type="number"
                                               class="form-control<?php echo e($errors->has('shareholder') ? ' is-invalid' : ''); ?>"
                                               name="shareholder" value="<?php echo e(old('shareholder')); ?>">
                                        <?php if($errors->has('shareholder')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('shareholder')); ?></strong>
                                                    </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group col-md-4" id="date_of_birth">
                                        <label for="date">Date Of Birth</label>
                                        <input id="date" type="datetime-local"
                                               class="form-control<?php echo e($errors->has('date_of_birth') ? ' is-invalid' : ''); ?>"
                                               name="date_of_birth" value="<?php echo e(old('date_of_birth')); ?>">
                                        <?php if($errors->has('date_of_birth')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('date_of_birth')); ?></strong>
                                                    </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">Present Address</div>
                                    <div id="ownerapp"  class="card-body bg-white">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="division1" >Division</label>
                                                <select  id="present_division" name="present_division" class="form-control<?php echo e($errors->has('present_division') ? ' is-invalid' : ''); ?> division1" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>

                                                    <option value=""></option>
                                                </select>
                                                <?php if($errors->has('present_division')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('present_division')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label for="district1">Distirct</label>
                                                <select id="present_district" name="present_district"  class="form-control<?php echo e($errors->has('present_district') ? ' is-invalid' : ''); ?>  district1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('present_district')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('present_district')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="upazila1">Upazila</label>
                                                <select id="present_upazila" name="present_upazila" class="form-control<?php echo e($errors->has('present_upazila') ? ' is-invalid' : ''); ?>  upazila1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('present_upazila')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('present_upazila')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="post_office1">Post Office</label>
                                                <select id="present_postoffice"  name="present_postoffice"  class="form-control<?php echo e($errors->has('present_postoffice') ? ' is-invalid' : ''); ?> suboffice1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('present_postoffice')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('present_postoffice')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3" id="present_postcode">
                                                <label for="post_code1">Post Code</label>
                                                <input id="post_code1" required="required" type="text" name="present_postcode" class="form-control<?php echo e($errors->has('present_postcode') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php if($errors->has('present_postcode')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('present_postcode')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="present_address">
                                                <textarea class="form-control" cols="" rows=""></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div class="card">
                                    <div class="card-header">Permanent Address</div>
                                    <div id="ownerapp"  class="card-body bg-white">
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="division1" >Division</label>
                                                <select  id="permanent_division" name="permanent_division" class="form-control<?php echo e($errors->has('permanent_division') ? ' is-invalid' : ''); ?> division1" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    <option value=""></option>
                                                </select>
                                                <?php if($errors->has('permanent_division')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('permanent_division')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="district1">Distirct</label>
                                                <select id="permanent_district" name="permanent_district"  class="form-control<?php echo e($errors->has('permanent_district') ? ' is-invalid' : ''); ?>  district1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('permanent_district')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('permanent_district')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="upazila1">Upazila</label>
                                                <select id="permanent_upazila" name="permanent_upazila" class="form-control<?php echo e($errors->has('permanent_upazila') ? ' is-invalid' : ''); ?>  upazila1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('permanent_upazila')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('permanent_upazila')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="post_office1">Post Office</label>
                                                <select id="permanent_postoffice"  name="permanent_postoffice"  class="form-control<?php echo e($errors->has('permanent_postoffice') ? ' is-invalid' : ''); ?> suboffice1" >
                                                    <option value="0" disabled="true" selected="true"></option>
                                                </select>
                                                <?php if($errors->has('permanent_postoffice')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('permanent_postoffice')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3" id="permanent_postcode">
                                                <label for="post_code1">Post Code</label>
                                                <input id="post_code1" required="required" type="text" name="permanent_postcode" class="form-control<?php echo e($errors->has('permanent_postcode') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php if($errors->has('permanent_postcode')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('permanent_postcode')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="permanent_address">
                                                <textarea class="form-control" cols="" rows=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function getPositionInfo(){
            var chairman=document.getElementById("position1").value;
            getPosition(chairman);


        }
        function getManagingDirector() {

            var managingDirector=document.getElementById("position2").value;
            getPosition(managingDirector);
        }
        function getShareHolder() {
            var shareHolder=document.getElementById("position3").value;

            getShareInfo(shareHolder);
        }


        function getPosition(compnayposition) {

            $.get('/position-info?position=' + compnayposition, function (data) {


                $('#conduct_info').removeClass('hidden');
                $('#shareholder').addClass('hidden');
                $('#owner_title').html('<option value="'+ data.owner_title +'">'+ data.owner_title +'</option>');
                $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ data.owner_first_name +'" readonly>');
                $('#owner_middle_name').html('<label>Middle Name</label> <input type="text" id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ data.owner_middle_name +'" readonly>');
                $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ data.owner_last_name +'" readonly>');
                $("#organization").html('<option value="option6">'+data.position+'</option>');
                $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ data.shareholder +'" readonly>');
                $("#date_of_birth").html('<label>Date Of Birth</label> <input type="date" id="" name="date_of_birth"  class="form-control" value="'+ data.date_of_birth +'" readonly>');
                $("#present_division").html('<option value="'+data.permanent_division+'">'+data.permanent_division+'</option>');
                $("#present_district").html('<option value="'+data.present_district+'">'+data.present_district+'</option>');
                $("#present_upazila").html('<option value="'+data.present_upazila+'">'+data.present_upazila+'</option>');
                $("#present_postoffice").html('<option value="'+data.presentpost_office+'">'+data.presentpost_office+'</option>');
                $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ data.post_code2 +'" readonly>');
                $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+data.present_address+'</textarea>');
                $("#permanent_division").html('<option value="'+data.permanent_division+'">'+data.permanent_division+'</option>');
                $("#permanent_district").html('<option value="'+data.permanent_district+'">'+data.permanent_district+'</option>');
                $("#permanent_upazila").html('<option value="'+data.permanent_upazila+'">'+data.permanent_upazila+'</option>');
                $("#permanent_postoffice").html('<option value="'+data.permanentpost_office+'">'+data.permanentpost_office+'</option>');
                $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ data.post_code3 +'" readonly>');
                $("#permanent_address").html('<label>Address</label><textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+data.permanent_address+'</textarea>');
            });

        }

        function getShareHolderId(id) {
            getSingleShareInfo(id);

        }
        function getSingleShareInfo(id) {
            $.get('/single-share-info?id=' + id, function (singleData) {
                $('#conduct_info').removeClass('hidden');
                /*$('#shareholder').addClass('hidden');*/
                $('#owner_title').html('<option value="'+ singleData.owner_title +'">'+ singleData.owner_title +'</option>');
                $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ singleData.owner_first_name +'" readonly>');
                $('#owner_middle_name').html('<label>Middle Name</label> <input type="text"  id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ singleData.owner_middle_name +'" readonly>');
                $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ singleData.owner_last_name +'" readonly>');
                $("#organization").html('<option value="option6">'+singleData.position+'</option>');
                $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ singleData.shareholder +'" readonly>');
                $("#date_of_birth").html('<label>Date Of Birth</label> <input type="date" id="" name="date_of_birth"  class="form-control" value="'+ singleData.date_of_birth +'" readonly>');
                $("#present_division").html('<option value="'+singleData.permanent_division+'">'+singleData.permanent_division+'</option>');
                $("#present_district").html('<option value="'+singleData.present_district+'">'+singleData.present_district+'</option>');
                $("#present_upazila").html('<option value="'+singleData.present_upazila+'">'+singleData.present_upazila+'</option>');
                $("#present_postoffice").html('<option value="'+singleData.presentpost_office+'">'+singleData.presentpost_office+'</option>');
                $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ singleData.post_code2 +'" readonly>');
                $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+singleData.present_address+'</textarea>');
                $("#permanent_division").html('<option value="'+singleData.permanent_division+'">'+singleData.permanent_division+'</option>');
                $("#permanent_district").html('<option value="'+singleData.permanent_district+'">'+singleData.permanent_district+'</option>');
                $("#permanent_upazila").html('<option value="'+singleData.permanent_upazila+'">'+singleData.permanent_upazila+'</option>');
                $("#permanent_postoffice").html('<option value="'+singleData.permanentpost_office+'">'+singleData.permanentpost_office+'</option>');
                $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ singleData.post_code3 +'" readonly>');
                $("#permanent_address").html('<label>Address</label><textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+singleData.permanent_address+'</textarea>');
            });
        }
        function getShareInfo(shareHolder) {
            $.get('/shareHolder?chairman=' + shareHolder, function (shareHolders) {
                if(shareHolders.length>1)
                {
                    $('#shareholder').removeClass('hidden');
                    $('#conduct_info').addClass('hidden');
                    $('#share').empty();
                    $.each(shareHolders, function (shareHolders, shareHolderObj) {

                        $('#share').append("<div class='col-sm-7'><div class='form-check mt-1'>\n" +
                            "                    <input class='form-check-input' onclick=\"getShareHolderId('"+shareHolderObj.id+"')\"  type='radio' name='share_holder' value='"+shareHolderObj.owner_last_name+"' id='"+shareHolderObj.owner_last_name+"'>\n" +
                            "                    <label class='form-check-label' for='share_holder1'>"+shareHolderObj.owner_first_name +' '+shareHolderObj.owner_middle_name +' '+ shareHolderObj.owner_last_name+"</label>\n" +
                            "                    </div></div>");
                    })
                }
                else
                {
                    $('#conduct_info').removeClass('hidden');
                    $('#shareholder').addClass('hidden');
                    $('#owner_title').html('<option value="'+ shareHolders.owner_title +'">'+ shareHolders.owner_title +'</option>');
                    $('#owner_first_name').html('<label>First Name</label> <input type="text" id="post_code\'+step+\'" name="first_name"  class="form-control" value="'+ shareHolders.owner_first_name +'" readonly>');
                    $('#owner_middle_name').html('<label>Middle Name</label> <input type="text"  id="post_code\'+step+\'" name="middle_name"  class="form-control" value="'+ shareHolders.owner_middle_name +'" readonly>');
                    $('#owner_last_name').html('<label>Last Name</label> <input type="text" id="post_code\'+step+\'" name="last_name"  class="form-control" value="'+ shareHolders.owner_last_name +'" readonly>');
                    $("#organization").html('<option value="option6">'+shareHolders.position+'</option>');
                    $("#shareholder_persent").html('<label>ShareHolder (%)</label> <input type="text" id="post_code\'+step+\'" name="shareholder"  class="form-control" value="'+ shareHolders.shareholder +'" readonly>');
                    $("#date_of_birth").html('<label>Date Of Birth</label> <input type="date" id="" name="date_of_birth"  class="form-control" value="'+ shareHolders.date_of_birth +'" readonly>')
                    $("#present_division").html('<option value="'+shareHolders.permanent_division+'">'+shareHolders.permanent_division+'</option>');
                    $("#present_district").html('<option value="'+shareHolders.present_upazila+'">'+shareHolders.present_district+'</option>');
                    $("#present_upazila").html('<option value="'+shareHolders.present_upazila+'">'+shareHolders.present_upazila+'</option>');
                    $("#present_postoffice").html('<option value="'+shareHolders.presentpost_office+'">'+shareHolders.presentpost_office+'</option>');
                    $("#present_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="present_postcode"  class="form-control" value="'+ shareHolders.post_code2 +'" readonly>')
                    $("#present_address").html('<label>Adress</label><textarea name="present_address" class=\"form-control\" cols=\"\" rows=\"\">'+shareHolders.present_address+'</textarea>');
                    $("#permanent_division").html('<option value="'+shareHolders.permanent_division+'">'+shareHolders.permanent_division+'</option>');
                    $("#permanent_district").html('<option value="'+shareHolders.permanent_district+'">'+shareHolders.permanent_district+'</option>');
                    $("#permanent_upazila").html('<option value="'+shareHolders.permanent_upazila+'">'+shareHolders.permanent_upazila+'</option>');
                    $("#permanent_postoffice").html('<option value="'+shareHolders.permanentpost_office+'">'+shareHolders.permanentpost_office+'</option>');
                    $("#permanent_postcode").html('<label>Post Code</label> <input type="text" id="post_code\'+step+\'" name="permanent_postcode"  class="form-control" value="'+ shareHolders.post_code3 +'" readonly>');
                    $("#permanent_address").html('<label>Address</label> <textarea name="permanent_address" class=\"form-control\" cols=\"\" rows=\"\">'+shareHolders.permanent_address+'</textarea>');
                }
            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>