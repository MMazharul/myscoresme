<?php $__env->startSection('title', 'Register'); ?><?php $__env->startSection('content'); ?>
    <style>
        .capture_button {
            color: gainsboro;
            bottom: 10px;
            border-radius: 50px;
            border: 2px solid grey;
            background: transparent;
            font-weight: bold;
            margin: 10px;
            outline: none;
            transition: .5s background, border ease;
            cursor: pointer;
        }

        .capture_button:hover {
            background: lightgrey;
            color: black;
            border: 2px solid lightgrey;
        }
        .button_grp {
            width: 100%;
            position: absolute;
            bottom: 10px;
            background: transparent;
        }

        .flex {
            display: flex;
            justify-content: center;
        }
    </style>

    <?php if(session('massage')): ?>
        <div class="col-md-10 offset-1 alert alert-success mt-4 mb-0" id="message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h3 class="text-success"><i class="fa fa-success"></i> Well Done !</h3><strong><?php echo e(session('massage')); ?></strong>
        </div>
    <?php endif; ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Director/Partners Details</h3></div>
                    <div class="card-body">
                        <form id="partnerEditValidation" class="form-horizontal" action="<?php echo e(url('/partner-director-info/update')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>

                            <?php if(Auth::user()->businessInfo->organization == 'Partnership' || Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited'): ?>
                                <?php for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ ): ?>
                                    <div class="card">
                                        <?php if(Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited'): ?>
                                            <div class="card-header"><h4 class="mb-0">Director - <?php echo e($i); ?></h4></div>
                                        <?php else: ?>
                                            <div class="card-header"><h4 class="mb-0">Partner - <?php echo e($i); ?></h4></div>
                                        <?php endif; ?>
                                        <div class="card-body bg-white">
                                            <div id="present">
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_title">Title (Mrs./Mr./Ms.)</label>
                                                        <select id="owner_title" class="form-control<?php echo e($errors->has('owner_title'.$i) ? ' is-invalid' : ''); ?>" name="owner_title<?php echo e($i); ?>">
                                                            <option disabled selected>Select Your Title</option>
                                                            <option value="Mrs."<?php if(old('owner_title'.$i)=='Mrs.'): ?> <?php echo e('selected'); ?> <?php elseif(isset($directorsParners['owner_title'.$i])): ?><?php echo e($directorsParners['owner_title'.$i] == "Mrs." ? 'selected' : ''); ?> <?php endif; ?>>Mrs.</option>
                                                            <option value="Mr." <?php if(old('owner_title'.$i)=='Mr.'): ?> <?php echo e('selected'); ?>  <?php elseif(isset($directorsParners['owner_title'.$i])): ?> <?php echo e($directorsParners['owner_title'.$i] == "Mr." ? 'selected' : ''); ?> <?php endif; ?>>Mr.</option>
                                                            <option value="Ms."  <?php if(old('owner_title'.$i)=='Ms'): ?> <?php echo e('selected'); ?>   <?php elseif(isset($directorsParners['owner_title'.$i])): ?> <?php echo e($directorsParners['owner_title'.$i] == "Ms." ? 'selected' : ''); ?> <?php endif; ?>>Ms.</option>
                                                        </select>
                                                        <?php if($errors->has('owner_title'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_title'.$i)); ?></strong>
                                                             </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_first_name">First Name</label>
                                                        <input id="owner_first_name" type="text"
                                                               class="form-control<?php echo e($errors->has('owner_first_name'.$i) ? ' is-invalid' : ''); ?>"
                                                               name="owner_first_name<?php echo e($i); ?>" value=" <?php echo e($directorsParners['owner_first_name'.$i]); ?> " placeholder="Enter your first name">
                                                        <?php if($errors->has('owner_first_name'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_first_name'.$i)); ?></strong>
                                                            </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_middle_name">Middle Name</label>
                                                        <input id="owner_middle_name" type="text"
                                                               class="form-control<?php echo e($errors->has('owner_middle_name'.$i) ? ' is-invalid' : ''); ?>"
                                                               name="owner_middle_name<?php echo e($i); ?>" value="<?php echo e($directorsParners['owner_middle_name'.$i]); ?>" placeholder="Enter your middle name">
                                                        <?php if($errors->has('owner_middle_name'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_middle_name'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="owner_last_name">Last Name</label>
                                                        <input id="owner_last_name" type="text" class="form-control<?php echo e($errors->has('owner_last_name'.$i) ? ' is-invalid' : ''); ?>"
                                                               name="owner_last_name<?php echo e($i); ?>" value="<?php echo e($directorsParners['owner_last_name'.$i]); ?>" placeholder="Enter your last name">
                                                        <?php if($errors->has('owner_last_name'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_last_name'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="owner_nid">NID Number</label>
                                                        <input id="owner_nid" type="text"
                                                               class="form-control<?php echo e($errors->has('owner_nid'.$i) ? ' is-invalid' : ''); ?>"
                                                               name="owner_nid<?php echo e($i); ?>" value="<?php echo e($directorsParners['owner_nid'.$i]); ?>" placeholder="Enter Your nid number">
                                                        <?php if($errors->has('owner_nid'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_nid'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_front_pic" >Upload NID Front Side</label>

                                                        <?php if(isset($directorsParners['nid_front_pic'.$i])): ?>
                                                            <input type="file" id="input-file-now" name="nid_front_pic<?php echo e($i); ?>" data-default-file="<?php echo e(asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['nid_front_pic'.$i])); ?>" class="dropify" data-height="150"/>
                                                        <?php else: ?>
                                                            <input type="file" id="input-file-now" name="nid_front_pic<?php echo e($i); ?>" data-default-file="" class="dropify"  data-height="150"/>
                                                        <?php endif; ?>


                                                        <?php if($errors->has('nid_front_pic'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('nid_front_pic'.$i)); ?></strong>
                                                            </span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="nid_back_pic">Upload NID Back Side</label>

                                                        <?php if(isset($directorsParners['nid_back_pic'.$i])): ?>
                                                            <input type="file" id="input-file-now" name="nid_back_pic<?php echo e($i); ?>" data-default-file="<?php echo e(asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['nid_back_pic'.$i])); ?>" class="dropify" data-height="150"/>
                                                        <?php else: ?>
                                                            <input type="file" id="input-file-now" name="nid_back_pic<?php echo e($i); ?>" data-default-file="" class="form-control <?php echo e($errors->has('nid_back_pic'.$i) ? ' is-invalid' : ''); ?> dropify"  data-height="150"/>
                                                        <?php endif; ?>

                                                        <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('nid_back_pic'.$i) ? ' is-invalid' : ''); ?>"></span>

                                                        <?php if($errors->has('nid_back_pic'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong><?php echo e($errors->first('nid_back_pic'.$i)); ?></strong>
                                                            </span>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="organization">Position in the Organizatrion</label>
                                                        <select id="position_<?php echo e($i); ?>" class="form-control<?php echo e($errors->has('position'.$i) ? ' is-invalid' : ''); ?> position"
                                                                name="position<?php echo e($i); ?>" onclick="positionChange()">
                                                            <option disabled selected>Choose Option</option>

                                                            <?php if( Auth::user()->businessInfo->organization == 'Public Limited' || Auth::user()->businessInfo->organization == 'Private Limited'): ?>

                                                                <option value="Chairman" <?php if(old('position'.$i)=='Chairman'): ?> <?php echo e('selected'); ?>  <?php elseif(isset($directorsParners['position'.$i])=='Chairman'): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                                                                    Chairman
                                                                </option>
                                                                <option value="Managing Director" <?php if(old('position'.$i)=='Managing Director'): ?> <?php echo e('selected'); ?> <?php elseif(isset($directorsParners['position'.$i])=='Managing Director'): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                                                                    Managing Director
                                                                </option>
                                                                <option value="Shareholder Director" <?php if(old('position'.$i)=='Shareholder Director'): ?> <?php echo e('selected'); ?> <?php elseif(isset($directorsParners['position'.$i])=='Shareholder Director'): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                                                                    Shareholder Director
                                                                </option>
                                                            <?php else: ?>
                                                                <option value="Managing Partner"  <?php if($directorsParners['position'.$i] =='Managing Partner'): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                                                                    Managing Partner
                                                                </option>
                                                                <option value="Partner"  <?php if($directorsParners['position'.$i]=='Partner'): ?> <?php echo e('selected'); ?> <?php endif; ?>>
                                                                    Partner
                                                                </option>
                                                            <?php endif; ?>

                                                        </select>
                                                        <?php if($errors->has('position'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('position'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="shareholder">Shareholder (%)</label>
                                                        <input id="shareholder_<?php echo e($i); ?>" type="text"
                                                               class="txt form-control<?php echo e($errors->has('shareholder'.$i) ? ' is-invalid' : ''); ?>"
                                                               name="shareholder<?php echo e($i); ?>" value="<?php if(old('shareholder'.$i)): ?> <?php echo e(old('shareholder'.$i)); ?> <?php elseif(isset($directorsParners['shareholder'.$i])): ?><?php echo e($directorsParners['shareholder'.$i]); ?><?php endif; ?>"
                                                               placeholder="Enter your ShareHolder %">
                                                        <?php if($errors->has('shareholder'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('shareholder'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="date_of_birth">Date Of Birth</label>


                                                        <label for="datepicker<?php echo e($i); ?>">Date Of Birth</label>
                                                        <input id="datepicker<?php echo e($i); ?>" type="text"
                                                               class="form-control<?php echo e($errors->has('date_of_birth'.$i) ? ' is-invalid' : ''); ?> dob<?php echo e($i); ?>"
                                                               name="date_of_birth<?php echo e($i); ?>" value="<?php echo e($directorsParners['date_of_birth'.$i]); ?>" placeholder="dd-mm-yyyy" readonly>

                                                        <?php if($errors->has('date_of_birth'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                                    <strong><?php echo e($errors->first('date_of_birth'.$i)); ?></strong>
                                                             </span>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>

                                                <div class="form-row">

                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Capture Your Photo</label>
                                                        <a class="form-control<?php echo e($errors->has('capture_pic'.$i) ? ' is-invalid' : ''); ?> btn btn-block btn-outline mb-1"
                                                           id="cap_btn<?php echo e($i); ?>" onclick="capture<?php echo e($i); ?>()" data-toggle="modal" data-target="#exampleModal<?php echo e($i); ?>">
                                                            <i class=" fa fa-camera"></i> Camera</a>
                                                        <input type="hidden" name="capture_pic<?php echo e($i); ?>" class="image-tag<?php echo e($i); ?>">
                                                        <?php if(isset($directorsParners['capture_pic'.$i])): ?>
                                                            <img style="width: 150px; width: 200px;" src="<?php echo e(asset('/ui/back-end/uploads/sme-photo/'.$directorsParners['capture_pic'.$i])); ?>">
                                                        <?php endif; ?>


                                                        <?php if($errors->has('capture_pic'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong><?php echo e($errors->first('capture_pic'.$i)); ?></strong>
                                                            </span>
                                                        <?php endif; ?>
                                                    </div>


                                                    <div class="form-group col-md-6">
                                                        <label for="picture">Upload Your Photo</label>


                                                        <?php if(isset($directorsParners['owner_pic'.$i])): ?>
                                                            <input type="file" id="input-file-now" name="owner_pic<?php echo e($i); ?>" data-default-file="<?php echo e(asset('/ui/back-end/uploads/director-Partner-image/'.$directorsParners['owner_pic'.$i])); ?>" class="dropify" data-height="150"/>
                                                        <?php else: ?>
                                                            <input type="file" id="input-file-now" name="owner_pic<?php echo e($i); ?>" data-default-file="" class="dropify"  data-height="150"/>
                                                        <?php endif; ?>


                                                        <?php if($errors->has('owner_pic'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                          <strong><?php echo e($errors->first('owner_pic'.$i)); ?></strong>
                                                         </span>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>


                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="division">Present Address</label>
                                                        <hr class="bg-success mt-0 mb-0">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="row">
                                                            <label class="col-sm-6 mr-0" for="present_address1">Permanent Address</label>
                                                        </div>
                                                        <hr class="bg-dark-blue mt-0 mb-0">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_division">Division</label>
                                                        <select onchange="" id="present_division<?php echo e($i); ?>" name="present_division<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('present_division'.$i) ? ' is-invalid' : ''); ?>">
                                                            <option value="0" disabled="true" selected="true">-Select division-</option>
                                                            <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                <option value="<?php echo e($division->id); ?>"
                                                                <?php if($directorsParners['present_division'.$i] == $division->title): ?>
                                                                    <?php echo e('selected'); ?>

                                                                    <?php endif; ?>>
                                                                    <?php echo e($division->title); ?>

                                                                </option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                        <?php if($errors->has('present_division'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_division'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <div class="row col-sm-12">
                                                            <label for="division3" class="col-sm-3 pl-0">Division</label>
                                                            <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0">
                                                                <i>*Same as Permanent Address</i></label>
                                                            <input id="check_<?php echo e($i); ?>" onclick="permanentFunction_<?php echo e($i); ?>()" type="checkbox" value="" class="col-sm-1 mt-1">
                                                        </div>
                                                        <select id="permanent_division<?php echo e($i); ?>" name="permanent_division<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('permanent_division'.$i) ? ' is-invalid' : ''); ?> ">
                                                            <option value="0" disabled="true" selected="true">-Select division-</option>

                                                            <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option value="<?php echo e($division->id); ?> "
                                                                <?php if($directorsParners['permanent_division'.$i] == $division->title): ?>
                                                                    <?php echo e('selected'); ?>

                                                                    <?php endif; ?>
                                                                >
                                                                    <?php echo e($division->title); ?>

                                                                </option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                        <?php if($errors->has('permanent_division'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_division'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_district">Distirct</label>
                                                        <select id="present_district<?php echo e($i); ?>" name="present_district<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('present_district'.$i) ? ' is-invalid' : ''); ?> ">

                                                            <?php if(isset($directorsParners['present_district'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['present_district'.$i]); ?>"><?php echo e($directorsParners['present_district'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value=""></option>
                                                            <?php endif; ?>
                                                        </select>
                                                        <?php if($errors->has('present_district'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_district'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="permanent_district">Distirct</label>
                                                        <select name="permanent_district<?php echo e($i); ?>" id="permanent_district<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('permanent_district'.$i) ? ' is-invalid' : ''); ?>">

                                                            <?php if(isset($directorsParners['permanent_district'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['permanent_district'.$i]); ?>"><?php echo e($directorsParners['permanent_district'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value=""></option>
                                                            <?php endif; ?>
                                                        </select>
                                                        <?php if($errors->has('permanent_district'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_district'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_upazila">Upazila</label>
                                                        <select id="present_upazila<?php echo e($i); ?>" name="present_upazila<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('present_upazila'.$i) ? ' is-invalid' : ''); ?> ">

                                                            <?php if(isset( $directorsParners['present_upazila'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['present_upazila'.$i]); ?>"><?php echo e($directorsParners['present_upazila'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value="" ></option>
                                                            <?php endif; ?>
                                                        </select>
                                                        <?php if($errors->has('present_upazila'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_upazila'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="permanent_upazila">Upazila</label>
                                                        <select id="permanent_upazila<?php echo e($i); ?>" name="permanent_upazila<?php echo e($i); ?>" class="form-control<?php echo e($errors->has('permanent_upazila'.$i) ? ' is-invalid' : ''); ?>">


                                                            <?php if(isset( $directorsParners['permanent_upazila'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['permanent_upazila'.$i]); ?>"><?php echo e($directorsParners['permanent_upazila'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value=""></option>

                                                            <?php endif; ?>
                                                        </select>
                                                        <?php if($errors->has('permanent_upazila'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_upazila'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="presentpost_office">Post Office</label>
                                                        <select id="present_suboffice<?php echo e($i); ?>" name="presentpost_office<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('presentpost_office'.$i) ? ' is-invalid' : ''); ?> ">

                                                            <?php if(isset($directorsParners['presentpost_office'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['presentpost_office'.$i]); ?>"><?php echo e($directorsParners['presentpost_office'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value=""></option>
                                                            <?php endif; ?>
                                                        </select>
                                                        <?php if($errors->has('presentpost_office'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('presentpost_office'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="permanentpost_office">Post Office</label>

                                                        <select id="permanent_suboffice<?php echo e($i); ?>" name="permanentpost_office<?php echo e($i); ?>" class="form-control<?php echo e($errors->has('permanentpost_office'.$i) ? ' is-invalid' : ''); ?> ">

                                                            <?php if(isset($directorsParners['permanentpost_office'.$i])): ?>
                                                                <option value="<?php echo e($directorsParners['permanentpost_office'.$i]); ?>"><?php echo e($directorsParners['permanentpost_office'.$i]); ?></option>
                                                            <?php else: ?>
                                                                <option value="" disabled="true" selected="true"></option>
                                                            <?php endif; ?>
                                                        </select>

                                                        <?php if($errors->has('permanentpost_office'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanentpost_office'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6" id="present_postcode<?php echo e($i); ?>">
                                                        <label for="post_code2">Post Code</label>
                                                        
                                                        


                                                        <input  type="text"  id="present_post_code<?php echo e($i); ?>"  value="<?php echo e($directorsParners['present_post_code'.$i]); ?>"   name="present_post_code<?php echo e($i); ?>"
                                                                class="form-control<?php echo e($errors->has('present_post_code'.$i) ? ' is-invalid' : ''); ?> " readonly>

                                                        <?php if($errors->has('present_post_code'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_post_code'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group col-md-6 " id="permanent_postcode<?php echo e($i); ?>">
                                                        <label for="permanentpost_code">Post Code</label>

                                                        <input  required="required" type="text" id="permanent_post_code<?php echo e($i); ?>"   name="post_code<?php echo e($i); ?>" value="<?php echo e($directorsParners['post_code'.$i]); ?>"
                                                                class="form-control<?php echo e($errors->has('post_code'.$i) ? ' is-invalid' : ''); ?> " readonly>

                                                        <?php if($errors->has('post_code'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('post_code'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="present_address">Address line-1</label>
                                                        <textarea id="present_address_<?php echo e($i); ?>" rows="4" type="text"
                                                                  class="form-control<?php echo e($errors->has('present_address'.$i) ? ' is-invalid' : ''); ?>"
                                                                  name="present_address<?php echo e($i); ?>"
                                                                  placeholder="Enter Your present address"><?php if(isset($directorsParners['present_address'.$i])): ?><?php echo e($directorsParners['present_address'.$i]); ?><?php endif; ?></textarea>
                                                        <?php if($errors->has('present_address'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_address'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="form-group col-md-6" id="permanent_<?php echo e($i); ?>">
                                                        <label for="permanent_address">Address line-2</label>
                                                        <textarea id="permanent_address_<?php echo e($i); ?>" rows="4" type="text"
                                                                  class="form-control<?php echo e($errors->has('permanent_address'.$i) ? ' is-invalid' : ''); ?>"
                                                                  name="permanent_address<?php echo e($i); ?>" placeholder="Enter Your permanent address"><?php if(isset($directorsParners['permanent_address'.$i])): ?><?php echo e($directorsParners['permanent_address'.$i]); ?><?php endif; ?></textarea>
                                                        <?php if($errors->has('permanent_address'.$i)): ?>
                                                            <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_address'.$i)); ?></strong>
                                                    </span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                <?php endfor; ?>

                            <?php endif; ?>

                            <div class="form-group m-b-0">
                                <a href="<?php echo e(url('/register/edit')); ?>" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb-3 pull-left">Back</a>
                                <button type="submit" name="action" value="next" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>
                                <button type="submit" name="action" value="save" class="btn btn-success waves-effect waves-light m-t-10 pt-3 pb-3 pull-right mr-3">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <?php for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ ): ?>
        <div class="modal fade" id="exampleModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div style="box-shadow: 0px 7px 21px 1px rgba(0,0,0,0.47);width:720px;padding: 0px;
                 border-radius: 20px;border:none; overflow: hidden;" class="modal-content">
                    <div style="padding:0" class="modal-body">
                        <button style="margin-top:5px;position: absolute; right: 10px;z-index: 5;color: white;width: 25px;height: 25px;"
                                type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="display: block;line-height: 0.3">&times;</span>
                        </button>

                        <div class="row">
                            <div class="col-12">
                                <div id="my_camera<?php echo e($i); ?>"></div>
                                <div class="button_grp">
                                    <div id="btns" class="flex">
                                        <button id="capture<?php echo e($i); ?>" style="display: none" onclick="capture<?php echo e($i); ?>()" class="capture_button">New</button>
                                        <button onclick="take_snapshot<?php echo e($i); ?>()" class="capture_button"> <i class="fa fa-camera" style="font-size: 30px"></i></button>
                                        <button id="save<?php echo e($i); ?>" class="capture_button" style="display: none" onclick="cap_save<?php echo e($i); ?>()" data-dismiss="modal" aria-label="Close">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endfor; ?>

    <!-- Configure a few settings and attach camera -->

    <?php for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ ): ?>
        <script language="JavaScript">
            function capture<?php echo e($i); ?>() {
                Webcam.set({
                    width: 720,
                    height: 405,
                    image_format: 'jpeg',
                    jpeg_quality: 90
                });

                Webcam.attach('#my_camera<?php echo e($i); ?>');
            };


            function take_snapshot<?php echo e($i); ?>() {
                Webcam.snap(function (data_uri) {
                    $(".image-tag<?php echo e($i); ?>").val(data_uri);
                    document.getElementById('my_camera<?php echo e($i); ?>').innerHTML = '<img src="' + data_uri + '"/>';
                    $('#capture<?php echo e($i); ?>').css('display', "block");
                    $('#save<?php echo e($i); ?>').css('display', "block");
                });
            }

            function cap_save<?php echo e($i); ?>() {
                take_snapshot<?php echo e($i); ?>();
                $("#cap_btn<?php echo e($i); ?>").append(" ✔");
            }
        </script>
    <?php endfor; ?>

    <script type="text/javascript">

        $(document).ready(function() {
            //this calculates values automatically

            calculateSum();

            $(".txt").on("keydown keyup", function() {
                calculateSum();
            });
        });

        function calculateSum() {

            var sum = 0;
            //iterate through each textboxes and add the values
            $(".txt").each(function() {
                //add only if the value is number
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value);
                    $(this).css("background-color", "#FEFFB0");
                }
                else if (this.value.length != 0){
                    $(this).css("background-color", "red");
                }
            });
            if(sum>100)
            {
                alert("Total Share holder Should 100%");
            }

            $("input#sum1").val(sum.toFixed(2));
        }
        function positionChange()
        {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>

            $("#position_<?php echo e($i); ?>").change(function () {


                    var value=$("#position_<?php echo e($i); ?>  :selected").val();

                    var id = $(this).children(":selected").closest("select").attr("id");
                    var check_<?php echo e($i); ?>=$("#position_<?php echo e($i); ?> :selected").val();


                    if(value=='Chairman')
                    {
                        <?php for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++): ?>

                        $("#position_<?php echo e($x); ?> option[value='Chairman']").remove();
                        $("#"+id).html('<option value="Chairman" selected>Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');

                        <?php endfor; ?>

                    }
                    else if(value=="Managing Director")
                    {
                        <?php for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++): ?>

                        $("#position_<?php echo e($x); ?> option[value='Managing Director']").remove();

                        if(check_<?php echo e($i); ?> == 'Managing Director')
                        {
                            $("#"+id).html('<option value="Managing Director" selected>Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');

                        }
                        else if(check_<?php echo e($i); ?> != 'Managing Director') {

                            var abc1 = $("#"+id).html('<option value="Managing Director" >Managing Director</option><option value="Shareholder Director">Shareholder Director<option>');

                        }
                        <?php endfor; ?>
                    }
                    else if(value=='Shareholder Director')
                    {

                        if(check_<?php echo e($i); ?>=='Managing Director' && check_<?php echo e($i); ?>=='Chairman')
                        {
                            console.log(true);
                            $("#position_<?php echo e($i); ?>").html('<option value="Shareholder Director" selected>Shareholder Director<option>');
                        }
                        $("#position_<?php echo e($i); ?>").html('<option value="Chairman">Chairman</option><option value="Managing Director">Managing Director</option><option value="Shareholder Director" selected>Shareholder Director<option>');
                    }
                    else if(value=='Managing Partner')
                    {

                        <?php for($x=1;$x<=Auth::User()->businessInfo->total_director_partner;$x++): ?>
                        $("#position_<?php echo e($x); ?> option[value='Managing Partner']").remove();
                        $("#"+id).html('<option value="Managing Partner" selected>Managing Partner</option><option value="Partner">Partner</option>');
                        <?php endfor; ?>
                    }
                    else if(value=='Partner')
                    {
                        $("#position_<?php echo e($i); ?>").html('<option value="Managing Partner">Managing Partner</option><option value="Partner" selected>Partner</option>');

                    }


                }

            );
            <?php endfor; ?>
        }

        <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>
        function permanentFunction_<?php echo e($i); ?>() {


            if ($("#check_<?php echo e($i); ?>").is(':checked')) {
                var division2 = $('#present_division<?php echo e($i); ?> :selected').text();
                var divisionValue = $('#present_division<?php echo e($i); ?> :selected').val();
                $('#permanent_division<?php echo e($i); ?>').html("<option value="+divisionValue+">" + division2 + "<option>");
                var district2 = $('#present_district<?php echo e($i); ?> :selected').text();
                var districtValue = $('#present_district<?php echo e($i); ?> :selected').val();
                $('#permanent_district<?php echo e($i); ?>').html("<option value="+districtValue+">" + district2 + "<option>");
                var upazila3 = $('#present_upazila<?php echo e($i); ?> :selected').text();
                var upazilaValue = $('#present_upazila<?php echo e($i); ?> :selected').val();
                $('#permanent_upazila<?php echo e($i); ?>').html("<option value="+upazilaValue+">" + upazila3 + "<option>");
                var suboffice3 = $('#present_suboffice<?php echo e($i); ?> :selected').text();
                var subofficeVal = $('#present_suboffice<?php echo e($i); ?> :selected').val();
                $('#permanent_suboffice<?php echo e($i); ?>').html("<option value="+subofficeVal+">" + suboffice3 + "<option>");
                var postcode3 = $("input:text#present_post_code<?php echo e($i); ?>").val();
                $('#permanent_postcode<?php echo e($i); ?>').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="' + postcode3 + '" readonly>');
                var present_address=$("textarea#present_address_<?php echo e($i); ?>").val();
                $('#permanent_<?php echo e($i); ?>').html("<label for=\"permanent_address\">Address line-2</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control<?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>\"\n" +
                    "                                                          name=\"permanent_address<?php echo e($i); ?>\" placeholder=\"Enter Your permanent address\" >"+present_address+"</textarea>");
            } else {
                $('#permanent_division<?php echo e($i); ?>').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n" + "  <option value=\"<?php echo e($division->id); ?>\"><?php echo e($division->title); ?></option>\n" + " <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>");
                $('#permanent_district<?php echo e($i); ?>').html("<option><option>");
                $('#permanent_upazila<?php echo e($i); ?>').html("<option><option>");
                $('#permanent_suboffice<?php echo e($i); ?>').html("<option><option>");
                $('#permanent_postcode<?php echo e($i); ?>').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="" readonly>');
                $('#permanent_<?php echo e($i); ?>').html("<label for=\"permanent_address\">Address line-2</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control<?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>\"\n" +
                    "                                                          name=\"permanent_address<?php echo e($i); ?>\" placeholder=\"Enter Your permanent address\" ></textarea>");
            }
        }
        <?php endfor; ?>
    </script>

    <?php for($i = 1; $i<= Auth::user()->businessInfo->total_director_partner; $i++ ): ?>
        <script>
            $( function() {

                $( "#datepicker<?php echo e($i); ?>" ).datepicker( "option", "showAnim", $( this ).val() );
                $( "#datepicker<?php echo e($i); ?>" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd-mm-yy",
                    yearRange: "-120:+0",

                    onSelect: function(date) {
                        var dateSplit = date.split("-");
                        var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                        var today = new Date();
                        if (dob.getFullYear() + 18 > today.getFullYear()) {

                            alert('You must be atleast 18 years old to submit register');
                            // $.alert({
                            //     title: 'Alert!',
                            //     content: 'You must be atleast 18 years old to submit register',
                            //     type: 'red',
                            //     typeAnimated: true,
                            // });
                            $(".dob<?php echo e($i); ?>").val("");
                        }
                    }

                });
            });

        </script>
    <?php endfor; ?>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>