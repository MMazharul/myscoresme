
<?php $__env->startSection('content'); ?>


   <div class="col-md-8 offset-2 pt-5">
       <div class="card card-body">
           <h3 class="box-title m-b-0">Question Edit</h3>
           <hr class="mb-5">
           <form class="form-horizontal" method="post" action="<?php echo e(url('/question/'.$questionEdit->id)); ?>">

               <?php echo csrf_field(); ?>
               <?php echo e(method_field('PUT')); ?>
               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Label English*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->label_en); ?>" name="label_en" class="form-control" id="inputEmail3" placeholder="label english">
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Label Bangla*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->label_bn); ?>" name="label_bn" class="form-control" id="inputEmail3" placeholder="label bangla">
                   </div>
               </div>

               <div class="form-group row">
                   <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select Type</label>
                   <div class="col-sm-8">
                       <select class="custom-select col-sm-12" name="field_type" id="inlineFormCustomSelect">
                           <option selected="">Choose Type...</option>
                           <?php $__currentLoopData = $titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <option value="<?php echo e($title); ?>" <?php if($title==$questionEdit->field_type): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($title); ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       </select>
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Range Value*</label>
                   <div class="col-sm-8 col-12">

                       <input type="number" value="<?php echo e($questionEdit->min); ?>" name="min" class="col-md-4 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Minimum Value">
                       <input type="number" value="<?php echo e($questionEdit->max); ?>" name="max" class="col-md-4 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Maximum Value">
                       <input type="number" value="<?php echo e($questionEdit->step); ?>" name="step" class="col-md-3 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Step">
                       <?php if($errors->has('help_en')): ?>
                           <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('help_en')); ?></strong>
                     </span>
                       <?php endif; ?>
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Help English*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->help_en); ?>" name="help_en" class="form-control" id="inputEmail3" placeholder="help english">
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Help Bangla*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->help_bn); ?>" name="help_bn" class="form-control" id="inputEmail3" placeholder="help bangla">
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Example English*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->example_en); ?>" name="example_en" class="form-control" id="inputEmail3" placeholder="example english">
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Example Bangla*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->example_bn); ?>" name="example_bn" class="form-control" id="inputEmail3" placeholder="example bangla">
                   </div>
               </div>




               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Placeholder*</label>
                   <div class="col-sm-8">
                       <input type="text" value="<?php echo e($questionEdit->placeholder); ?>" name="placeholder" class="form-control" id="inputEmail3" placeholder="column name">
                   </div>
               </div>


               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                   <div class="radio radio-success mt-2">
                       <input type="radio" name="status" id="radio14" <?php if($questionEdit->status==1): ?> <?php echo e('checked'); ?> <?php endif; ?>  value="1" checked>
                       <label for="radio14"> Publish </label>
                   </div>
                   <div class="radio radio-warning mt-2">
                       <input type="radio" name="status" id="radio17" <?php if($questionEdit->status==0): ?> <?php echo e('checked'); ?> <?php endif; ?>  value="0">
                       <label for="radio17"> Unpublish </label>
                   </div>
               </div>


               <div class="form-group m-b-0">
                   <div class="offset-sm-3 col-sm-8">
                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Question Update</button>
                   </div>
               </div>

           </form>
       </div>
   </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>