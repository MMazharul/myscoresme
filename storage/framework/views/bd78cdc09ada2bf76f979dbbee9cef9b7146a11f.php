<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="card-header bg-white"><h3 class="mt-3 mb-3">Company/Organization Documents Details</h3></div>
                    <div class="card-body">
                        <form id="attachmentValidation" class="form-horizontal" action="<?php echo e(url('/sme-attachment-store')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                            <div class="card">
                                <div class="card-header control-label">Company Information Details</div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-6"><label for="trade_license" class=" text-right control-label col-form-label">Trade License NO</label>
                                            <input id="trade_license"
                                                   type="text" class="form-control<?php echo e($errors->has('trade_license') ? ' is-invalid' : ''); ?>"
                                                   name="trade_license" value="<?php echo e(old('trade_license')); ?>"
                                                   placeholder="Enter your business trade license no">
                                            <?php if($errors->has('trade_license')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('trade_license')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="datepicker-autoclose" class=" text-right control-label col-form-label">Expire Date: </label>
                                            <input  type="text" class="form-control<?php echo e($errors->has('expire_date') ? ' is-invalid' : ''); ?>"
                                                    value="<?php echo e(old('expire_date')); ?>"
                                                    name="expire_date" id="datepicker" placeholder="dd/mm/yyyy">
                                            <?php if($errors->has('expire_date')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('expire_date')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-6 mb-4"><label>Add Attach file</label>
                                            <input type="file" id="input-file-now" name="trade_license_pic" class="dropify" data-height="150"/>

                                            <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('trade_license_pic') ? ' is-invalid' : ''); ?>"></span>

                                            <?php if($errors->has('trade_license_pic')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('trade_license_pic')); ?></strong>
                                                </span>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <?php if(Auth::User()->businessInfo->organization == "Sole Proprietorship"): ?>
                                            <div id="e-tin" class="form-group col-md-6"><label for="e_tin" class=" text-right control-label col-form-label">E-TIN NO</label>
                                                <input id="e_tin" type="number" class="form-control<?php echo e($errors->has('e_tin') ? ' is-invalid' : ''); ?>"
                                                       name="e_tin" value="<?php echo e(old('e_tin')); ?>"
                                                       placeholder="Enter your business e-tin no">

                                                <?php if($errors->has('e_tin')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('e_tin')); ?></strong>
                                                </span>
                                                <?php endif; ?>


                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="e_tin_pic" class="dropify"
                                                       data-height="150"/>
                                                <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('e_tin_pic') ? ' is-invalid' : ''); ?>"></span>

                                                <?php if($errors->has('e_tin_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('e_tin_pic')); ?></strong>
                                                </span>
                                                <?php endif; ?>

                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="bin" class=" text-right control-label col-form-label">BIN NO</label>
                                                <input id="bin" type="text" class="form-control<?php echo e($errors->has('bin') ? ' is-invalid' : ''); ?>" name="bin" value="<?php echo e(old('bin')); ?>"
                                                       placeholder="Enter Your Bin No">
                                                <?php if($errors->has('bin')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('bin')); ?></strong>

                                                        </span>
                                                <?php endif; ?>
                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="bin_pic" class="dropify" data-height="150"/>
                                                <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('bin_pic') ? ' is-invalid' : ''); ?>"></span>
                                                <?php if($errors->has('bin_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('bin_pic')); ?></strong>
                                            </span>
                                                <?php endif; ?>
                                            </div>
                                        <?php else: ?>
                                            <div id="e-tin" class="form-group col-md-3"><label for="e_tin" class=" text-right control-label col-form-label">E-TIN NO</label>
                                                <input id="e_tin" type="number" class="form-control<?php echo e($errors->has('e_tin') ? ' is-invalid' : ''); ?>"
                                                       name="e_tin" value="<?php echo e(old('e_tin')); ?>"
                                                       placeholder="Enter your business e-tin no">
                                                <?php if($errors->has('e_tin')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('e_tin')); ?></strong>
                                            </span>
                                                <?php endif; ?>
                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="e_tin_pic" class="dropify"
                                                       data-height="150"/>
                                                <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('e_tin_pic') ? ' is-invalid' : ''); ?>"></span>
                                                <?php if($errors->has('e_tin_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('e_tin_pic')); ?></strong>
                                                     </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="bin" class=" text-right control-label col-form-label">BIN NO</label>
                                                <input id="bin" type="number" class="form-control<?php echo e($errors->has('bin') ? ' is-invalid' : ''); ?>" name="bin" value="<?php echo e(old('bin')); ?>"
                                                       placeholder="Enter Your business bin no">

                                                <?php if($errors->has('bin')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('bin')); ?></strong>
                                                 </span>
                                                <?php endif; ?>

                                                <label>Add Attach file</label>
                                                <input type="file" id="input-file-now" name="bin_pic" class="dropify" data-height="150"/>

                                                <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('bin_pic') ? ' is-invalid' : ''); ?>"></span>
                                                <?php if($errors->has('bin_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('bin_pic')); ?></strong>
                                                 </span>
                                                <?php endif; ?>
                                            </div>
                                            <?php if(Auth::User()->businessInfo->organization == "Partnership"): ?>
                                                <div class="form-group col-md-6">
                                                    <label for="partnership_agreement" class=" text-right control-label col-form-label">Partnership Agreement Reg. No.</label>
                                                    <input id="partnership_agreement" type="number"
                                                           class="form-control<?php echo e($errors->has('partnership_agreement') ? ' is-invalid' : ''); ?>"
                                                           name="partnership_agreement"
                                                           value="" placeholder="Enter your partnership agreement reg. no.">
                                                    <?php if($errors->has('partnership_agreement')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('partnership_agreement')); ?></strong>
                                                        </span>
                                                    <?php endif; ?>



                                                    <label>Add Attach file</label>
                                                    <input type="file" id="input-file-now" name="partnership_agreement_pic" class="dropify"
                                                           data-height="150"/>

                                                    <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('certificate_incorporation_pic') ? ' is-invalid' : ''); ?>"></span>

                                                    <?php if($errors->has('partnership_agreement_pic')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('partnership_agreement_pic')); ?></strong>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            <?php else: ?>
                                                <div class="form-group col-md-6">
                                                    <label for="certificate_incorporation"
                                                           class=" text-right control-label col-form-label">Certificate of Incorporation No:</label>
                                                    <input id="certificate_incorporation" type="text"
                                                           class="form-control<?php echo e($errors->has('certificate_incorporation') ? ' is-invalid' : ''); ?>"
                                                           name="certificate_incorporation" value="<?php echo e(old('certificate_incorporation')); ?>" placeholder="Enter Your certificate of incorporation no:">


                                                    <?php if($errors->has('certificate_incorporation')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('certificate_incorporation')); ?></strong>
                                                  </span>
                                                    <?php endif; ?>

                                                    <label>Add Attach file</label>
                                                    <input type="file" id="input-file-now" name="certificate_incorporation_pic" class="dropify" data-height="150"/>

                                                    <span style="border:0px;margin: -15px;"   class="form-control <?php echo e($errors->has('certificate_incorporation_pic') ? ' is-invalid' : ''); ?>"></span>
                                                    <?php if($errors->has('certificate_incorporation_pic')): ?>
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('certificate_incorporation_pic')); ?></strong>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>

                                    <label class="text-right control-label col-form-label">Bank Details</label>
                                    <hr class="bg-dark-blue mt-1">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Account Title*</label>
                                        <div class="col-sm-9">
                                            <input type="text" value="<?php echo e(old('company_account_title')); ?>" class="form-control <?php echo e($errors->has('company_account_title') ? ' is-invalid' : ''); ?>" name="company_account_title" id="inputEmail3" placeholder="Enter your account title">

                                            <?php if($errors->has('company_account_title')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('company_account_title')); ?></strong>
                                                        </span>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Account No*</label>
                                        <div class="col-sm-9">
                                            <input type="number" value="<?php echo e(old('company_account_no')); ?>" class="form-control <?php echo e($errors->has('company_account_no') ? ' is-invalid' : ''); ?>" name="company_account_no" id="company_account_no" placeholder="Enter your account no">

                                            <?php if($errors->has('company_account_no')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('company_account_no')); ?></strong>
                                                     </span>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank*</label>
                                        <div class="col-sm-9">
                                            <select id="company_bank" class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?> bank"
                                                    name="company_bank">
                                                <option disabled selected>Choose Bank</option>
                                                <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($bank->id); ?>"><?php echo e($bank->bank_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-3 text-right control-label col-form-label">Division*</label>
                                        <div class="col-sm-9">
                                            <select id="bank_division" class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?> bank"
                                                    name="bank_division">
                                                <option disabled  selected id="division_option">Choose Division</option>
                                                <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option  value="<?php echo e($division->id); ?>"><?php echo e($division->title); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-3 text-right control-label col-form-label">District*</label>
                                        <div class="col-sm-9">
                                            <select id="bank_district" class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?> bank"
                                                    name="bank_district">
                                                <option disabled selected>Choose District</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-3 text-right control-label col-form-label">Branch*</label>
                                        <div class="col-sm-9">
                                            <select id="company_branch" class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?> bank"
                                                    name="company_branch">
                                                <option disabled selected>Choose Branch</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card" id="example1">
                                <div class="card-header control-label">Add Attachment</div>
                                <div class="card-body bg-white">

                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-5 nopadding">
                                            <label class="ml-5 control-label">Title</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control ml-5" id="Schoolname" name="title[]" value="" placeholder="Enter picture title">
                                            </div>
                                        </div>

                                        <div class="col-sm-5 ">
                                            <label class="ml-5 control-label">Picture</label>
                                            <div class="form-group">
                                                <input type="file" multiple="multiple" class="form-control ml-5" id="Major"  name="picture[]" value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-1 mt-1 ml-4">
                                            <label></label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success" type="button" onclick="education_fields();"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var room = 1;
        function education_fields() {
            room++;
            var objTo = document.getElementById('education_fields');
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML = '<div class="row"><div class="col-sm-5 nopadding"> <label class="ml-5">Title</label><div class="form-group"> <input type="text" class="form-control  ml-5" id="Major" name="title[]" value="" placeholder="Enter picture title "></div></div><div class="col-sm-5 "><label class="ml-5">Picture</label><div class="form-group"> <input type="file" class="form-control  ml-5" id="Degree" name="picture[]" value="" placeholder="Degree"></div></div><div class="col-sm-1 mt-1 ml-4"> <label></label><div class="form-group"><div class="input-group"><div class="input-group-append"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <i class="fa fa-minus"></i> </button></div></div></div></div><div class="clear"></div></row>';
            objTo.appendChild(divtest)
        }
        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>


    <script>
        $( function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
        } );
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>