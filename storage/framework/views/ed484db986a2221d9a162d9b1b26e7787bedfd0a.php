<?php $__env->startSection('content'); ?>

    <div id="main-wrapper">

        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <?php if(session('message')): ?>
                                <div class="alert alert-success alert-dismissible" role="alert"  id="message">
                                    <div class="icon"><span class="mdi mdi-check"></span></div>
                                    <div class="message text-center "><?php echo e(session('message')); ?></div>
                                </div>
                            <?php endif; ?>
                            <h4 class="card-title">SME Question Category</h4>
                            <h6 class="card-subtitle"></h6>

                            <div id="accordion2" class="minimal-faq">

                                <?php $__currentLoopData = $questionCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $questionCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="card m-b-0">
                                        <div class="card-header row col-lg-12" role="tab" id="headingOne11">
                                            <h5 class="mb-0 col-lg-11">
                                                <a href="" data-toggle="collapse" data-parent="<?php echo e($questionCategory->id); ?>" aria-expanded="true" aria-controls="collapseOne11">
                                                    <?php echo e($questionCategory->question_category); ?>
                                                </a>
                                            </h5>
                                            <div class="col-lg-1">
                                                <a href="<?php echo e(url('/question-category/'.$questionCategory->id).'/edit'); ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-edit text-blue m-r-10"></i> </a>
                                                <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="mdi mdi-delete text-danger"></i> </a>
                                            </div>
                                        </div>

                                        <div id="1" class="collapse" role="tabpanel" aria-labelledby="headingOne11">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="accordion1" role="tablist" aria-multiselectable="true">
                                                            <div class="card-header" role="tab" id="headingOne1" style="margin-top: -10px">
                                                                <h5 class="m-b-0">
                                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#<?php echo e($questionCategory->question_category); ?>" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                                                        Active Question
                                                                    </a>
                                                                </h5>
                                                            </div>

                                                            <div id="<?php echo e($questionCategory->id); ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne1" style=" margin-top: -15px;">
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <th>Category Id</th>
                                                                                <th>Label En</th>
                                                                                <th>Label En</th>
                                                                                <th>Status</th>

                                                                                <th>Edit</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                                                <tr>
                                                                                    <td><?php echo e($question->id); ?></td>
                                                                                    <td><?php echo e($question->question_category_id); ?></td>
                                                                                    <td><?php echo e($question->label_en); ?></td>
                                                                                    <td><?php echo e($question->label_bn); ?></td>
                                                                                    <td>
                                                                                        <div class="dropdown row">
                                                                                            <?php if($question->status == 1): ?>
                                                                                                <a href="<?php echo e(url('/question/status/'.$question->id)); ?>" title="Click to Unpublished"
                                                                                                   class="btn btn-success waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                                                                                    <i class="fa fa-check"></i>
                                                                                                </a>
                                                                                            <?php else: ?>
                                                                                                <a href="<?php echo e(url('/question/status/'.$question->id)); ?>" title="Click to Published"
                                                                                                   class="btn btn-warning waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                                                                                    <i class="fa fa-warning"></i>
                                                                                                </a>
                                                                                            <?php endif; ?>
                                                                                        </div>
                                                                                    </td>

                                                                                    <td class="text-nowrap">
                                                                                        <a href="<?php echo e(url('/question-category/'.$question->id.'/edit')); ?>" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                                                    </td>
                                                                                </tr>

                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>