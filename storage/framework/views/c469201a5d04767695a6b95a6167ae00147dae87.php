<?php $__env->startSection('content'); ?>
    <?php if(session('resend')): ?>
        <div class="container">
            <div class="col-md-8 offset-2 alert alert-success mt-4">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <?php echo e(session('resend')); ?>
            </div>
        </div>
     <?php endif; ?>

<div class="container">
    <div class="row justify-content-center mt-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-white"><?php echo e(__('Verify Your Email Address')); ?></div>
                <div class="card-body bg-white">
                    <?php if(session('resent')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(__('A fresh verification link has been sent to your email address.')); ?>
                        </div>
                    <?php endif; ?>
                    <?php echo e(__('Before proceeding, please check your email for a verification link.')); ?>
                    <?php echo e(__('If you did not receive the email')); ?>, <a href="<?php echo e(url('/email/resend')); ?>"><?php echo e(__('click here to request resend e-mail')); ?></a>.
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>