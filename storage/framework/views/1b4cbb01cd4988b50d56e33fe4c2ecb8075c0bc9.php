<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Edit User <?php echo e($user->id); ?></div>
        <div class="panel-body">

            <?php if($errors->any()): ?>
                <ul class="alert alert-danger">
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>
            <?php echo Form::model($user, [
                'method' => 'PATCH',
                'url' => ['/authorize/users', $user->id],
                'class' => 'form-horizontal',
                'files' => true
            ]); ?>

            <div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
                <?php echo Form::label('name', 'Name', ['class' => 'col-md-4 control-label']); ?>
                <div class="col-md-6">
                    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>
                    <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>
                </div>
            </div>
            <div class="form-group <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                <?php echo Form::label('email', 'Email', ['class' => 'col-md-4 control-label']); ?>
                <div class="col-md-6">
                    <?php echo Form::text('email', null, ['class' => 'form-control']); ?>
                    <?php echo $errors->first('email', '<p class="help-block">:message</p>'); ?>
                </div>
            </div>
            <div class="form-group <?php echo e($errors->has('role_id') ? 'has-error' : ''); ?>">
                <?php echo Form::label('role_id', 'Role', ['class' => 'col-md-4 control-label']); ?>
                <div class="col-md-6">
                    <?php echo Form::select('role_id', $roles, null, ['placeholder' => 'please select ...', 'class' => 'form-control']); ?>
                    <?php echo $errors->first('role_id', '<p class="help-block">:message</p>'); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>
                </div>
            </div>

            <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.authorize.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>