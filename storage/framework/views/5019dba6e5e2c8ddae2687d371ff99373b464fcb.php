<?php $__env->startSection('title', 'Register'); ?><?php $__env->startSection('content'); ?>
    <style>
        .capture_button {
            color: gainsboro;
            bottom: 10px;
            border-radius: 50px;
            border: 2px solid grey;
            background: transparent;
            font-weight: bold;
            margin: 10px;
            outline: none;
            transition: .5s background, border ease;
            cursor: pointer;
        }

        .capture_button:hover {
            background: lightgrey;
            color: black;
            border: 2px solid lightgrey;
        }

        .button_grp {
            width: 100%;
            position: absolute;
            bottom: 10px;
            background: transparent;
        }

        .flex {
            display: flex;
            justify-content: center;
        }
    </style>



    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-5">
                <div class="card bg-white">
                    <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="mb10 mt20  section-title text-center  ">
                            <!-- section title start-->
                            <h2>SME Register</h2>
                            <span class="text-success"><strong> Easy to apply for a loan with us,Once you have complete this form.</strong></span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <form id="signformEdit" method="post" action="<?php echo e(url('register/update/')); ?>" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>


                            <div class="card">
                                <div class="card-header"><strong class="control-label" style="text-transform: none">Enterprise/Company/Organization  Information</strong> </div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="enterprise_name">Enterprise Name (As Per Trade License)</label>
                                            <input id="enterprise_name" type="text"
                                                   class="form-control<?php echo e($errors->has('enterprise_name') ? ' is-invalid' : ''); ?>"
                                                   name="enterprise_name" value="<?php if(old('enterprise_name')): ?> <?php echo e(old('enterprise_name')); ?> <?php else: ?><?php echo e(Auth::user()->businessInfo->enterprise_name); ?><?php endif; ?>" placeholder="Enter Your Enterprise Name" >
                                            <?php if($errors->has('enterprise_name')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('enterprise_name')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-6" >
                                            <label for="organization">Organizatrion Structure</label>
                                            <select  id="organization"  class="form-control<?php echo e($errors->has('organization') ? ' is-invalid' : ''); ?>" name="organization" onchange="showHide()" disabled>
                                                <option disabled selected>Choose Your Organizatrion Structure</option>
                                                <option value="Sole Proprietorship" <?php echo e(Auth::user()->businessInfo->organization == "Sole Proprietorship" ? 'selected' : ''); ?>>Sole Proprietorship</option >
                                                <option value="Partnership" <?php echo e(Auth::user()->businessInfo->organization== "Partnership" ? 'selected' : ''); ?>>Partnership</option>
                                                <option value="Public Limited" <?php echo e(Auth::user()->businessInfo->organization == "Public Limited" ? 'selected' : ''); ?>>Public Limited</option>
                                                <option value="Private Limited" <?php echo e(Auth::user()->businessInfo->organization == "Private Limited" ? 'selected' : ''); ?>>Private Limited</option>
                                                <option value="Co-operative" <?php echo e(Auth::user()->businessInfo->organization == "Co-operative" ? 'selected' : ''); ?>>Co-operative</option>
                                                <option value="Other" <?php echo e(Auth::user()->businessInfo->organization == "Other" ? 'selected' : ''); ?>>Other</option>
                                            </select>
                                            <?php if($errors->has('organization')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('organization')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="industry">Industry</label>
                                            <select class="form-control <?php echo e($errors->has('industry') ? ' is-invalid' : ''); ?> " id="industry"
                                                    name="industry">
                                                <option value="0" disable="true" selected="true">---Select sector---</option>
                                                <?php $__currentLoopData = $industries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$industry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($id); ?>" <?php if(old('industry')==$id): ?> <?php echo e('selected'); ?> <?php elseif(Auth::user()->businessInfo->industry == $industry): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($industry); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                            <?php if($errors->has('industry')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('industry')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <?php if(isset(Auth::user()->businessInfo->sector)): ?>
                                            <div id="sector1" class="form-group col-md-12">
                                                <label for="sector">Sector</label>
                                                <select class="form-control <?php echo e($errors->has('sector') ? ' is-invalid' : ''); ?>" id="sector"
                                                        name="sector">
                                                    <option value="0" disable="true" selected="true">---Select sector---</option>
                                                    <option value="<?php echo e(Auth::user()->businessInfo->sector); ?>" disable="true" selected="true" ><?php echo e(Auth::user()->businessInfo->sector); ?></option>
                                                </select>
                                                <?php if($errors->has('sector')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('sector')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        <?php else: ?>
                                            <div id="sector1" class="form-group col-md-12 hidden">
                                                <label for="sector">Sector</label>
                                                <select class="form-control <?php echo e($errors->has('sector') ? ' is-invalid' : ''); ?>" id="sector"
                                                        name="sector">
                                                    <option value="0" disable="true" selected="true">---Select sector---</option>
                                                </select>
                                                <?php if($errors->has('sector')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('sector')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if(isset(Auth::user()->businessInfo->sub_sector)): ?>
                                        <div class="form-row">
                                            <div id="sub_sector1" class="form-group col-md-12">
                                                <label for="sub_sector">Sub Sector</label>
                                                <select class="form-control <?php echo e($errors->has('sector') ? ' is-invalid' : ''); ?>" id="sub_sector"
                                                        name="sub_sector">
                                                    <option value="<?php echo e(Auth::user()->businessInfo->sub_sector); ?>"><?php echo e(Auth::user()->businessInfo->sub_sector); ?></option>

                                                </select>
                                                <?php if($errors->has('sector')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('sector')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <div class="form-row">
                                            <div id="sub_sector1" class="form-group col-md-12 hidden">
                                                <label for="sub_sector">Sub Sector</label>
                                                <select class="form-control <?php echo e($errors->has('sector') ? ' is-invalid' : ''); ?>" id="sub_sector"
                                                        name="sub_sector">
                                                    <option value="0" disable="true" selected="true"></option>

                                                </select>
                                                <?php if($errors->has('sector')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('sector')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header"><strong class="control-label" style="text-transform: none"> Business Address</strong></div>
                                <div class="card-body bg-white">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="division1" >Division</label>
                                            <select  id="division1" name="org_division" class="form-control<?php echo e($errors->has('org_division') ? ' is-invalid' : ''); ?> division1" >
                                                <option value="0"  disabled="true" selected="true">-Select division-</option>
                                                <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($division->id); ?>" <?php if(old('org_division')): ?> <?php if($division->id==old('org_division')): ?> <?php echo e('selected'); ?> <?php endif; ?>  <?php elseif(Auth::user()->businessInfo->org_division == $division->title): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($division->title); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                            <?php if($errors->has('org_division')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('org_division')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="district1">Distirct</label>
                                            <select id="district1" name="org_district"  class="form-control<?php echo e($errors->has('org_district') ? ' is-invalid' : ''); ?>  district1" >


                                                <?php if(isset(Auth::user()->businessInfo->org_district)): ?>
                                                    <option value="<?php echo e(Auth::user()->businessInfo->org_district); ?>" selected><?php echo e(Auth::user()->businessInfo->org_district); ?></option>
                                                <?php else: ?>
                                                    <option value=""></option>
                                                <?php endif; ?>

                                            </select>
                                            <?php if($errors->has('org_district')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('org_district')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="upazila1">Upazila</label>
                                            <select id="upazila1" name="org_upazila" class="form-control<?php echo e($errors->has('org_upazila') ? ' is-invalid' : ''); ?>  upazila1" >

                                                <?php if(isset(Auth::user()->businessInfo->org_upazila)): ?>
                                                    <option value="<?php echo e(Auth::user()->businessInfo->org_upazila); ?>" selected><?php echo e(Auth::user()->businessInfo->org_upazila); ?></option>
                                                <?php else: ?>
                                                    <option value=""></option>
                                                <?php endif; ?>
                                            </select>
                                            <?php if($errors->has('org_upazila')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('org_upazila')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="post_office1">Post Office</label>
                                            <select id="post_office1"  name="orgpost_office"  class="form-control<?php echo e($errors->has('orgpost_office') ? ' is-invalid' : ''); ?> suboffice1" >

                                                <?php if(isset( Auth::user()->businessInfo->orgpost_office)): ?>
                                                    <option value="<?php echo e(Auth::user()->businessInfo->orgpost_office); ?>" selected><?php echo e(Auth::user()->businessInfo->orgpost_office); ?></option>
                                                <?php else: ?>
                                                    <option value=""></option>
                                                <?php endif; ?>
                                            </select>
                                            <?php if($errors->has('orgpost_office')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('orgpost_office')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="form-group col-md-2 postcode1">
                                            <label for="post_code1">Post Code</label>
                                            <?php if(old('post_code1')): ?>
                                                <input id="post_code1"  value="<?php echo e(old('post_code1')); ?>" type="text" name="post_code1" class="form-control<?php echo e($errors->has('post_code1') ? ' is-invalid' : ''); ?>" readonly>
                                            <?php else: ?>
                                                <input id="post_code1"  value="<?php echo e(Auth::user()->businessInfo->post_code1); ?>" type="text" name="post_code1" class="form-control<?php echo e($errors->has('post_code1') ? ' is-invalid' : ''); ?>" readonly>
                                            <?php endif; ?>
                                            <?php if($errors->has('post_code1')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('post_code1')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="address_1">Address Line 1</label>
                                            <textarea cols="" rows="3"  id="org_address" placeholder="Example : House No #20/a, Road #2 "
                                                      class="form-control<?php echo e($errors->has('org_address') ? ' is-invalid' : ''); ?>" name="org_address" ><?php if(old('org_address')): ?> <?php echo e(old('org_address')); ?> <?php else: ?><?php echo e(Auth::user()->businessInfo->org_address); ?> <?php endif; ?></textarea>
                                            <?php if($errors->has('org_address')): ?>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong><?php echo e($errors->first('org_address')); ?></strong>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <?php if( Auth::user()->businessInfo->organization == "Partnership" ||
                                 Auth::user()->businessInfo->organization == "Public Limited" || Auth::user()->businessInfo->organization == "Private Limited" ): ?>
                                <div class="card" id="partnership">
                                    <div class="card-header" >
                                        <strong class="control-label" style="text-transform: none" id="headlabel"><?php echo e(Auth::user()->businessInfo->organization); ?> Information</strong>
                                    </div>
                                    <div id="ownerapp"  class="card-body bg-white ">

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <?php if(Auth::user()->businessInfo->organization == "Partnership"): ?>
                                                    <label id="label" for="total_director_partner">Number of Partners </label>
                                                <?php else: ?>
                                                    <label id="label" for="total_director_partner">Number of Directors  </label>
                                                <?php endif; ?>
                                                <input id="total_director_partner" type="number"
                                                       class="form-control<?php echo e($errors->has('total_director_partner') ? ' is-invalid' : ''); ?>"
                                                       name="total_director_partner" value="<?php echo e(Auth::user()->businessInfo->total_director_partner); ?>" placeholder="" readonly>
                                                <?php if($errors->has('total_director_partner')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('total_director_partner')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="card" id="proprietorship">
                                    <div class="card-header"><strong class="control-label" style="text-transform: none">Sole Proprietorship Information </strong> </div>
                                    <div id="ownerapp"  class="card-body bg-white ">

                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <label for="title">Title (Mrs./Mr./Ms.)</label>
                                                <select id="title" class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>" name="title" >
                                                    <option disabled selected>Select Your Title</option>
                                                    <option value="Mrs." <?php if(old('title')=='Mrs.'): ?> <?php echo e('selected'); ?> <?php elseif(Auth::user()->title == "Mrs."): ?> <?php echo e('selected'); ?> <?php endif; ?>>Mrs.</option>
                                                    <option value="Mr."  <?php if(old('title')=='Mr.'): ?> <?php echo e('selected'); ?> <?php elseif(Auth::user()->title == "Mr."): ?> <?php echo e('selected'); ?> <?php endif; ?>>Mr.</option>
                                                    <option value="Ms." <?php if(old('title')=='Ms.'): ?> <?php echo e('selected'); ?> <?php elseif(Auth::user()->title == "Ms"): ?> <?php echo e('selected'); ?> <?php endif; ?>>Ms.</option>
                                                </select>
                                                <?php if($errors->has('title')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('title')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="owner_first">First Name</label>
                                                <input id="owner_first" type="text"
                                                       class="form-control<?php echo e($errors->has('first_name') ? ' is-invalid' : ''); ?>"
                                                       name="first_name" value="<?php if(old('first_name')): ?> <?php echo e(old('first_name')); ?> <?php else: ?><?php echo e(Auth::user()->first_name); ?><?php endif; ?>" placeholder="Enter your first name">
                                                <?php if($errors->has('first_name')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="owner_middle">Middle Name</label>
                                                <input id="owner_middle" type="text"
                                                       class="form-control<?php echo e($errors->has('middle_name') ? ' is-invalid' : ''); ?>"
                                                       name="middle_name" value="<?php if(old('middle_name')): ?> <?php echo e(old('middle_name')); ?> <?php else: ?><?php echo e(Auth::user()->middle_name); ?><?php endif; ?>" placeholder="Enter your middle name">
                                                <?php if($errors->has('middle_name')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('middle_name')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="owner_last">Last Name</label>
                                                <input id="owner_last" type="text" class="form-control<?php echo e($errors->has('last_name') ? ' is-invalid' : ''); ?>"
                                                       name="last_name" value="<?php if(old('last_name')): ?> <?php echo e(old('last_name')); ?> <?php else: ?><?php echo e(Auth::user()->last_name); ?><?php endif; ?>" placeholder="Enter your last name">
                                                <?php if($errors->has('last_name')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-md-4">
                                                <label for="owner_nid">NID Number</label>
                                                <input id="owner_nid" type="text"
                                                       class="form-control<?php echo e($errors->has('owner_nid') ? ' is-invalid' : ''); ?>"
                                                       name="owner_nid" value="<?php echo e(Auth::user()->profile->owner_nid); ?>" placeholder="Enter Your nid number" >
                                                <?php if($errors->has('owner_nid')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_nid')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="form-group col-md-4" >
                                                <label for="owner_contact">Upload NID Front Side</label>

                                                <input type="file" accept="image/*"  data-max-file-size="512K" id="input-file-now" name="nid_front_pic" data-default-file="<?php echo e(asset('/ui/back-end/uploads/sme-photo/'.Auth::user()->profile->nid_front_pic)); ?>" class="dropify" data-height="150"/>

                                                <span style="border:0px;"  class="form-control">Maximam file size is 512KB picture </span>


                                                <?php if($errors->has('owner_contact')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_contact')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="form-group col-md-4" >
                                                <label for="nid_back_pic">Upload NID Back Side</label>
                                                <input type="file" accept="image/*"  data-max-file-size="512K" id="input-file-now" name="nid_back_pic" data-default-file="<?php echo e(asset('/ui/back-end/uploads/sme-photo/'.Auth::user()->profile->nid_back_pic)); ?>" class="dropify" data-height="150"/>

                                                <span style="border:0px;"  class="form-control">Maximam file size is 512KB picture </span>

                                                <?php if($errors->has('nid_back_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('nid_back_pic')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="date_of_birth">Date Of Birth</label>



                                                <input id="date_of_birth" type="text"
                                                       class="form-control<?php echo e($errors->has('date_of_birth') ? ' is-invalid' : ''); ?> datepicker dob"
                                                       name="date_of_birth" value="<?php echo e(Auth::user()->profile->date_of_birth); ?>" placeholder="dd-mm-yyyy" readonly>

                                                <?php if($errors->has('date_of_birth')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('date_of_birth')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                        </div>

                                        <div class="form-row">


                                            <div class="form-group col-md-6">
                                                <label for="picture">Capture Your Photo</label>
                                                <a class="btn btn-block btn-outline mb-1"
                                                   id="cap_btn" onclick="capture()" data-toggle="modal" data-target="#exampleModal">
                                                    <i class="fa fa-camera"></i> Camera</a>
                                                <input type="hidden" name="capture_pic" class="image-tag">
                                                
                                                <?php if($errors->has('capture_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_pic')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="picture">Upload Your Photo</label>
                                                <input type="file" accept="image/*"  data-max-file-size="512K" id="input-file-now" name="nid_front_pic" data-default-file="<?php echo e(asset('/ui/back-end/uploads/sme-photo/'.Auth::user()->profile->owner_pic)); ?>" class="dropify" data-height="150"/>

                                                <span style="border:0px;"  class="form-control">Maximam file size is 512KB picture </span>

                                                <?php if($errors->has('owner_pic')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('owner_pic')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="division" >Present Address</label>
                                                <hr class="bg-success mt-0 mb-0">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="row">
                                                    <label class="col-sm-6 mr-0" for="present_address1">Permanent Address</label>
                                                </div>
                                                <hr class="bg-dark-blue mt-0 mb-0">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="present_division" >Division</label>
                                                <select  id="present_division" name="present_division" class="form-control<?php echo e($errors->has('present_division') ? ' is-invalid' : ''); ?> division2" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($division->id); ?>" <?php if(old('present_division')): ?> <?php if($division->id==old('present_division')): ?> <?php echo e('selected'); ?> <?php endif; ?>  <?php elseif(Auth::user()->profile->present_division == $division->title): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($division->title); ?></option>

                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <?php if($errors->has('present_division')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_division')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="division">
                                                <div class="row col-sm-12">
                                                    <label for="division3" class="col-sm-3 pl-0">Division</label>
                                                    <label for="present_address1" class="col-sm-7 offset-sm-1 pr-0">
                                                        <i>*Same as Permanent Address</i></label>
                                                    <input  id="check" onclick="permanentFunction()" type="checkbox" value="" class="col-sm-1 mt-1">
                                                </div>
                                                <select  id="permanent_division" name="permanent_division" class="form-control<?php echo e($errors->has('permanent_division') ? ' is-invalid' : ''); ?> division3" >
                                                    <option value="0" disabled="true" selected="true">-Select division-</option>
                                                    <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($division->id); ?>" <?php if(old('permanent_division')): ?> <?php if($division->id==old('permanent_division')): ?> <?php echo e('selected'); ?> <?php endif; ?>  <?php elseif(Auth::user()->profile->permanent_division == $division->title): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($division->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <?php if($errors->has('permanent_division')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_division')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="present_district">Distirct</label>
                                                <select id="present_district" name="present_district"  class="form-control<?php echo e($errors->has('present_district') ? ' is-invalid' : ''); ?>  district2" >

                                                    <?php if(Auth::user()->profile->present_district ): ?>
                                                        <option value="<?php echo e(Auth::user()->profile->present_district); ?>" selected><?php echo e(Auth::user()->profile->present_district); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>

                                                </select>
                                                <?php if($errors->has('present_district')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_district')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="district">
                                                <label for="permanent_district">Distirct</label>
                                                <select id="permanent_district" name="permanent_district"  class="form-control<?php echo e($errors->has('permanent_district') ? ' is-invalid' : ''); ?>  district3" >

                                                    <?php if(Auth::user()->profile->permanent_district): ?>
                                                        <option value="<?php echo e(Auth::user()->profile->permanent_district); ?>" selected><?php echo e(Auth::user()->profile->permanent_district); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>


                                                </select>
                                                <?php if($errors->has('permanent_district')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_district')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="present_upazila">Upazila</label>
                                                <select id="present_upazila" name="present_upazila" class="form-control<?php echo e($errors->has('present_upazila') ? ' is-invalid' : ''); ?>  upazila2" >

                                                    <?php if(Auth::user()->profile->present_upazila): ?>)
                                                    <option value="<?php echo e(Auth::user()->profile->present_upazila); ?>" ><?php echo e(Auth::user()->profile->present_upazila); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>

                                                </select>
                                                <?php if($errors->has('present_upazila')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_upazila')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="upazila" >
                                                <label for="permanent_upazila">Upazila</label>
                                                <select id="permanent_upazila" name="permanent_upazila" class="form-control<?php echo e($errors->has('permanent_upazila') ? ' is-invalid' : ''); ?>  upazila3" >

                                                    <?php if(Auth::user()->profile->permanent_upazila ): ?>
                                                        <option value="<?php echo e(Auth::user()->profile->permanent_upazila); ?>" selected><?php echo e(Auth::user()->profile->permanent_upazila); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>

                                                </select>
                                                <?php if($errors->has('permanent_upazila')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_upazila')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6" >
                                                <label for="presentpost_office">Post Office</label>
                                                <select id="presentpost_office"  name="presentpost_office"  class="form-control<?php echo e($errors->has('presentpost_office') ? ' is-invalid' : ''); ?> suboffice2" >

                                                    <?php if(Auth::user()->profile->presentpost_office): ?>
                                                        <option value="<?php echo e(Auth::user()->profile->presentpost_office); ?>" selected><?php echo e(Auth::user()->profile->presentpost_office); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>

                                                </select>
                                                <?php if($errors->has('presentpost_office')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('presentpost_office')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6" id="postoffice">
                                                <label for="permanentpost_office">Post Office</label>
                                                <select id="permanentpost_office"  name="permanentpost_office"  class="form-control<?php echo e($errors->has('permanentpost_office') ? ' is-invalid' : ''); ?> suboffice3" >

                                                    <?php if(Auth::user()->profile->permanentpost_office): ?>
                                                        <option value="<?php echo e(Auth::user()->profile->permanentpost_office); ?>" selected><?php echo e(Auth::user()->profile->permanentpost_office); ?></option>
                                                    <?php else: ?>
                                                        <option value=""></option>
                                                    <?php endif; ?>
                                                </select>
                                                <?php if($errors->has('permanentpost_office')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanentpost_office')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6 postcode2">
                                                <label for="post_code2">Post Code</label>
                                                <?php if(old('post_code2')): ?>
                                                    <input id="post_code1" required="required" value="<?php echo e(old('post_code2')); ?>" type="text" name="post_code2" class="form-control<?php echo e($errors->has('post_code2') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php else: ?>
                                                    <input id="post_code1" required="required" value="<?php echo e(Auth::user()->profile->post_code2); ?>" type="text" name="post_code2" class="form-control<?php echo e($errors->has('post_code2') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php endif; ?>
                                                <?php if($errors->has('post_code2')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('post_code2')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6 postcode3">
                                                <label for="permanentpost_code">Post Code</label>
                                                <?php if(old('post_code3')): ?>
                                                    <input id="post_code1" required="required" value="<?php echo e(old('post_code3')); ?>" type="text" name="post_code3" class="form-control<?php echo e($errors->has('post_code3') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php else: ?>
                                                    <input id="post_code1" required="required" value="<?php echo e(Auth::user()->profile->post_code3); ?>" type="text" name="post_code3" class="form-control<?php echo e($errors->has('post_code3') ? ' is-invalid' : ''); ?>" readonly>
                                                <?php endif; ?>
                                                <?php if($errors->has('post_code3')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('post_code3')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="present_address">Address line</label>
                                                <textarea id="present_address"  rows="4" type="text"
                                                          class="form-control<?php echo e($errors->has('present_address') ? ' is-invalid' : ''); ?> present_address"
                                                          name="present_address" placeholder="Enter Your present address" ><?php if(old('present_address')): ?> <?php echo e(old('present_address')); ?> <?php else: ?><?php echo e(Auth::user()->profile->present_address); ?> <?php endif; ?></textarea>
                                                <?php if($errors->has('present_address')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('present_address')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                            <div class="form-group col-md-6 permanent_address" id="permanent" >
                                                <label for="permanent_address">Address line</label>
                                                <textarea id="permanent_address" rows="4" type="text"
                                                          class="form-control<?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?> "
                                                          name="permanent_address" placeholder="Enter Your permanent address" ><?php if(old('permanent_address')): ?> <?php echo e(old('permanent_address')); ?> <?php else: ?><?php echo e(Auth::user()->profile->permanent_address); ?> <?php endif; ?></textarea>
                                                <?php if($errors->has('permanent_address')): ?>
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('permanent_address')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php endif; ?>

                            <button type="submit" class="btn btn-success pt-3 pb-3 pull-right">Next</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div style="width:auto" class="modal  fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div style="box-shadow: 0px 7px 21px 1px rgba(0,0,0,0.47);width:720px;padding: 0px;
                 border-radius: 20px;border:none; overflow: hidden;" class="modal-content">
                <div style="padding:0" class="modal-body">
                    <button style="margin-top:5px;position: absolute; right: 10px;z-index: 5;color: white;width: 25px;height: 25px;"
                            type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="display: block;line-height: 0.3">&times;</span>
                    </button>

                    <div class="row">
                        <div class="col-12">
                            <div id="my_camera"></div>
                            <div class="button_grp">
                                <div id="btns" class="flex">
                                    <button id="capture" style="display: none" onclick="capture()" class="capture_button">New</button>
                                    <button onclick="take_snapshot()" class="capture_button"> <i class="fa fa-camera" style="font-size: 30px"></i></button>
                                    <button id="save" class="capture_button" style="display: none" onclick="cap_save()" data-dismiss="modal" aria-label="Close">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Configure a few settings and attach camera -->
    <script language="JavaScript">
        function capture() {
            Webcam.set({
                width: 720,
                height: 405,
                image_format: 'jpeg',
                jpeg_quality: 90
            });

            Webcam.attach('#my_camera');
        };


        function take_snapshot() {
            Webcam.snap(function (data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('my_camera').innerHTML = '<img src="' + data_uri + '"/>';
                $('#capture').css('display', "block");
                $('#save').css('display', "block");
            });
        }

        function cap_save() {
            take_snapshot();
            $("#cap_btn").append(" ✔");
        }
    </script>

    <script type="text/javascript">
        function showHide() {
            var hidden = $('#organization').val();
            var   label_1 = $('#lavel');
            var   label_2 = $('#headlabel');
            if (hidden == 'Sole Proprietorship'){
                $('#proprietorship').show();
                $('#enterprise_name_label').html("Number of Partners");
            }

            else {
                $('#proprietorship').hide();
            }
            if (hidden == 'Partnership'){
                $('#partnership').show();
                $('#label').html("Number of Partners");
                $('#headlabel').html("Partnership Information");
                $('#total_director_partner').attr("placeholder", "Enter number of partners");

            }
            else if (hidden == 'Public Limited')
            {
                $('#partnership').show();
                $('#label').html("Number of Directors");
                $('#headlabel').html("Public Limited Information");
                $('#total_director_partner').attr("placeholder", "Enter number of directors");
            }
            else if (hidden == 'Private Limited')
            {
                $('#partnership').show();
                $('#label').html("Number of Directors");
                $('#headlabel').html("Private Limited Information");
                $('#total_director_partner').attr("placeholder", "Enter number of directors");
            }
            else {
                $('#partnership').hide();
            }
        }
    </script>

    <script>
        function permanentFunction() {
            if ($("#check").is(':checked')) {
                var division2 =$('.division2 :selected').text();
                var divisionValue =$('.division2 :selected').val();
                $('.division3').html("<option value="+ divisionValue +">" + division2 + "<option>");
                var district2 = $('.district2 :selected').text();
                var districtValue = $('.district2 :selected').val();
                $('.district3').html("<option value="+districtValue+">" + district2 + "<option>");
                var upazila3 = $('.upazila2 :selected').text();
                var upazilaValue = $('.upazila2 :selected').val();
                $('.upazila3').html("<option value="+upazilaValue+">" + upazila3 + "<option>");
                var suboffice3 = $('.suboffice2 :selected').text();
                var subofficeVallue = $('.suboffice2 :selected').val();
                $('.suboffice3').html("<option  value="+subofficeVallue+">" + suboffice3 + "<option>");
                var postcode3 = $("input:text#post_code1").val();

                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="' + postcode3 + '" readonly>');
                var present_address=$("textarea.present_address").val();
                $('.permanent_address').html("<label for=\"permanent_address\">Address line-2</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control<?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>\"\n" +
                    "                                                          name=\"permanent_address\" placeholder=\"Enter Your permanent address\" >"+present_address+"</textarea>");

                var divisionSpanHidden=$('#division').find("span");
                divisionSpanHidden.addClass("hidden");
                var districtSpanHidden=$('#district').find("span");
                districtSpanHidden.addClass("hidden");
                var upazilaSpanHiddden=$('#upazila').find("span");
                upazilaSpanHiddden.addClass("hidden");
                var postSpanHiddden=$('#postoffice').find("span");
                postSpanHiddden.addClass("hidden");
            } else {
                $('.division3').html("  <option value=\"0\"  disabled=\"true\" selected=\"true\">-Select division-</option>\n" + " <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n" + "  <option value=\"<?php echo e($division->id); ?>\"><?php echo e($division->title); ?></option>\n" + " <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>");
                $('.district3').html("<option><option>");
                $('.upazila3').html("<option><option>");
                $('.suboffice3').html("<option><option>");
                $('.postcode3').html('<label>Post Code</label><input type="text"  name="post_code3"  class="form-control" value="" readonly>');
                $('.permanent_address').html("<label for=\"permanent_address\">Address line-2</label> <textarea id=\"permanent_address\" rows=\"4\" type=\"\"\n" +
                    "                                                          class=\"form-control<?php echo e($errors->has('permanent_address') ? ' is-invalid' : ''); ?>\"\n" +
                    "                                                          name=\"permanent_address\" placeholder=\"Enter Your permanent address\" ></textarea>");
            }
        }
    </script>

    <script>
        $( function() {

            $( ".datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                yearRange: "-120:+0",

                onSelect: function(date) {
                    var dateSplit = date.split("-");
                    var dob = new Date(dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[2]);
                    var today = new Date();
                    if (dob.getFullYear() + 18 > today.getFullYear()) {

                        alert('You must be atleast 18 years old to submit register');
                        // $.alert({
                        //     title: 'Alert!',
                        //     content: 'You must be atleast 18 years old to submit register',
                        //     type: 'red',
                        //     typeAnimated: true,
                        // });
                        $(".dob").val("");
                    }
                }

            });
        });

    </script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>