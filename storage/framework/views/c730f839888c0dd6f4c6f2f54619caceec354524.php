<?php $__env->startSection('content'); ?>

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Post Office Edit</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/post-office/'.$id)); ?>">

                <?php echo csrf_field(); ?>
                <?php echo e(method_field('PUT')); ?>

                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12" name="upazila_id" id="inlineFormCustomSelect">
                            <option selected="">Choose Upazila...</option>
                            <?php $__currentLoopData = $upazilas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upazila): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($upazila->id); ?>" <?php if($upazila->id == $postOfficeEdit->upazila_id): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($upazila->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">PostOffice Name*</label>
                    <div class="col-sm-8">
                        <input type="text" value="<?php echo e($postOfficeEdit->title); ?>" name="title" class="form-control" id="inputEmail3" placeholder="Sub-Office Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="post_code" value="<?php echo e($postOfficeEdit->postcode->post_code); ?>" class="<?php echo e($errors->has('post_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="PostOffice Geo Code">

                        <?php if($errors->has('post_code')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('post_code')); ?></strong>
                     </span>
                        <?php endif; ?>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">PostOffice Update</button>
                    </div>
                </div>




            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>