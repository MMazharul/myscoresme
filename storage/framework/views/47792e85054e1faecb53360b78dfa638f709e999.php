<?php $__env->startSection('content'); ?>
    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Answer Create</h3>
                <div class="col-1">
                    <a href="<?php echo e(url('/option')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/option')); ?>">
                <?php echo csrf_field(); ?>
                <div class="form-group row">
                    <label class="col-12 col-sm-3 col-form-label text-sm-right">Question</label>
                    <div class="col-12 col-sm-8 col-lg-6 ">
                        <select class="select2 form-control" name="question_id">
                            <option>-- Choose a question --</option>
                            <optgroup>
                                <?php $__currentLoopData = $labels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($id); ?>"><?php echo e($label); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </optgroup>
                        </select>
                    </div>
                </div>
                <div class="optionRow">
                    <div class="form-group row" v-for="(option, index) in options">
                        <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Answer {{index+1}}</label>
                        <div class="col-sm-6">
                            <input type="text" name="title[]" v-model="option.name" class="form-control" id="inputEmail3" placeholder="Enter Sector Name">
                        </div>
                        <button href="#" class="col-md-off btn btn-success" type="button" @click="addNewOption"><i class="fa fa-plus"></i></button><span class="icon mdi mdi-close m-l-10 m-t-10" @click="deleteOption"></span>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Option Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        var app = new Vue({
            el: '.optionRow',
            data: {
                options:[
                    {
                        name: ''
                    }
                ]
            },
            methods: {
                addNewOption(){
                    this.options.push({
                        name: ''
                    })
                },
                deleteOption(index){
                    this.options.splice(index, 1)
                }
            }
        })
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>