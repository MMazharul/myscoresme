
<?php $__env->startSection('content'); ?>
    <div class="col-md-10 offset-md-1 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Branch Create</h3>
                <div class="col-1">
                    <a href="<?php echo e(url('/branch')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-4">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/branch')); ?>">
                <?php echo e(csrf_field()); ?>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank Name*</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="bank_id">
                            <option disabled="true" selected="true">-- Choose a Branch --</option>
                            <?php $__currentLoopData = $banks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($id); ?>"><?php echo e($bank); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="Mobile" class="col-sm-3 text-right control-label col-form-label">Branch Name</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="Mobile" type="text" name="branch_name"
                               placeholder="Enter Your Mobile Number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14" value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="branch_type" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Branch Add</button>
                    </div>
                </div>


            </form>
        </div>
    </div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>