
<?php $__env->startSection('content'); ?>
    <?php if(session('message')): ?>
        <div class="alert alert-success alert-dismissible" role="alert"  id="message">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message text-center "><?php echo e(session('message')); ?></div>
        </div>
    <?php endif; ?>

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Bank Create</h3>
                <div class="col-1">
                    <a href="<?php echo e(url('/bank')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('bank')); ?>">
                <?php echo csrf_field(); ?>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="bank_name" class="form-control" id="inputEmail3" placeholder="Enter Sector Name">
                    </div>
                </div>

                <div class="form-group row disabled">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish</label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Bank Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>