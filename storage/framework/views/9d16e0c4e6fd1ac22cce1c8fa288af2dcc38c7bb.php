<?php $__env->startSection('content'); ?>





    <div class="card">

        <?php if(session('message')): ?>
            <div class="col-md-10 offset-1 alert alert-success mt-2 mb-0"  id="message">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <strong><?php echo e(session('message')); ?></strong>
            </div>
        <?php endif; ?>

        <div class="card-body">

            <div class="col-12 row">



                <div class="col-11 m-l-0">

                    <h2 class="ml-l"><i class="fa fa-list"></i> Answer Option List</h2>

                </div>



                <h3 class="box-title m-b-0 col-1"><a href="<?php echo e(url('option/create')); ?>" class="btn btn-dropbox"><i class="fa fa-plus"></i> Add New</a></h3>



            </div>

            <div class=" m-t-30">

                <table id="myTable" class="table table-bordered table-striped">

                    <thead>

                    <tr>

                        <th>ID</th>

                        

                        <th>Question Lebel</th>

                        <th>title</th>

                        <th>Status</th>

                        <th>Action</th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>

                            <td><?php echo e($option->id); ?></td>

                            <td><?php echo e($option->question->label_en); ?></td>

                            

                            <td> <?php echo e($option->title); ?></td>

                            <td>

                                <?php if($option->status==1): ?>

                                    <?php echo e('Publish'); ?>


                                <?php elseif($option->status==0): ?>

                                    <?php echo e('Unpublish'); ?>


                                <?php endif; ?>

                            </td>



                            <td>

                                <div class="dropdown row">

                                    <?php if($option->status ==1): ?>

                                        <a href="<?php echo e(url('option/status/'.$option->id)); ?>" title="Click to Unpublished"

                                           class="btn btn-success waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                            <i class="fa fa-check"></i>

                                        </a>

                                    <?php else: ?>

                                        <a href="<?php echo e(url('/option/status/'.$option->id)); ?>" title="Click to Published"

                                           class="btn btn-warning waves-effect waves-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">

                                            <i class="fa fa-warning"></i>

                                        </a>

                                    <?php endif; ?>

                                    <div class="btn-group ml-1">

                                        <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <i class="icon mdi mdi-settings"></i></button>

                                        <div class="dropdown-menu">

                                            <a class="dropdown-item" href="<?php echo e(url('/option/'.$option->id.'/edit' )); ?>"><i

                                                        class="mdi mdi-table-edit"></i> Edit</a>

                                            <a class="dropdown-item" href="<?php echo e(url('/option-delete/'.$option->id)); ?>"><i

                                                        class="mdi mdi-delete"></i> Delete</a>

                                        </div>

                                    </div>



                                </div>

                            </td>





                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>