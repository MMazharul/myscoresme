
<?php $__env->startSection('content'); ?>

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Question Category Edit</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/question-category/'.$questionCategoryEdit->id)); ?>">
                <?php echo csrf_field(); ?>
                <?php echo e(method_field('PUT')); ?>
                <div class="form-group row">
                    <label for="question_category" class="col-sm-4 text-right control-label col-form-label">Question Category Name*</label>
                    <div class="col-sm-7">
                        <input type="text" value="<?php echo e($questionCategoryEdit->question_category); ?>" name="question_category"
                               class="form-control<?php echo e($errors->has('question_category') ? ' is-invalid' : ''); ?>"
                               id="question_category" placeholder="Enter question category">
                    </div>
                    <?php if($errors->has('question_category')): ?>
                        <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($errors->first('question_category')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14" <?php if($questionCategoryEdit->status == 1): ?> <?php echo e('checked'); ?><?php endif; ?> value="1"  checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" <?php if($questionCategoryEdit->status == 0): ?> <?php echo e('checked'); ?><?php endif; ?> id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10"><i class="fa fa-update"></i> Update</button>
                    </div>
                </div>

            </form>
        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>