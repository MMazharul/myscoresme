<?php $__env->startSection('title', 'Register'); ?>

<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <div class="card bg-white">
                    <div class="card-body">
                        <form class="form-horizontal" action="<?php echo e(url('/email-phone/store')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>



                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="email">Email Address</label>

                                    <input id="email" type="text"
                                           class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?> mb-0"
                                           name="email" value="<?php echo e(old('email')); ?>" placeholder="Enter your email address" >
                                    <small id="passwordHelpBlock" class="form-text text-muted ml-1">
                                         This email will be your user ID
                                    </small>
                                    <?php if($errors->has('email')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                                    </span>
                                    <?php endif; ?>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="owner_first">Mobile Number</label>

                                    <input id="owner_first" type="text" class="form-control<?php echo e($errors->has('mobile_number') ? ' is-invalid' : ''); ?> mb-0"
                                           name="mobile_number" value="<?php echo e(old('mobile_number')); ?>" placeholder="Enter your mobile number">

                                    <small id="passwordHelpBlock" class="form-text text-muted ml-1">
                                       Will be used for verification
                                    </small>
                                    <?php if($errors->has('mobile_number')): ?>
                                        <span class="invalid-feedback" role="alert">
                                                            <strong><?php echo e($errors->first('mobile_number')); ?></strong>
                                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group m-b-0">

                                <?php if( Auth::user()->businessInfo->organization == "Sole Proprietorship"): ?>
                                    <a href="<?php echo e(url('register/edit')); ?>" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb- pull-left">Back</a>
                                <?php else: ?>
                                    <a href="<?php echo e(url('authorized-representative/edit')); ?>" class="btn btn-outline waves-effect waves-light m-t-10 pt-3 pb- pull-left">Back</a>
                                <?php endif; ?>
                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3 pull-right">Next</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>