
<?php $__env->startSection('content'); ?>



    <div class="col-md-10 offset-1 pt-5">
      <div class="card card-body">
          <div class="col-12 row">
              
                  
                  
              
              <h3 class="box-title m-b-0 col-11">Question Create</h3>
              <div class="col-1">
                  <a href="<?php echo e(url('/question')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
              </div>
          </div>
          <hr class="mb-5">
          <form class="form-horizontal" method="post" action="<?php echo e(url('/question')); ?>">
              <?php echo csrf_field(); ?>
              <div class="form-group row">
                  <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Question Category</label>
                  <div class="col-sm-8">
                      <select class="custom-select col-sm-12 <?php echo e($errors->has('question_category_id') ? 'form-control is-invalid' : 'form-control'); ?>" name="question_category_id" id="inlineFormCustomSelect">
                          <option selected=""> --Choose a question category--</option>
                          <?php $__currentLoopData = $QuestionCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$questionCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($key); ?>"><?php echo e($questionCategory); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                      <?php if($errors->has('question_category_id')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('question_category_id')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>


              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Label English*</label>
                  <div class="col-sm-8">
                      <input type="text" name="label_en" class="<?php echo e($errors->has('label_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="label english">
                      <?php if($errors->has('label_en')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('label_en')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Label Bangla*</label>
                  <div class="col-sm-8">
                      <input type="text" name="label_bn" class="<?php echo e($errors->has('label_bn') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="label bangla">
                      <?php if($errors->has('label_bn')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('label_bn')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select Type</label>
                  <div class="col-sm-8">
                      <select class="custom-select col-sm-12 <?php echo e($errors->has('field_type') ? 'form-control is-invalid' : 'form-control'); ?>" name="field_type" id="inlineFormCustomSelect">
                          <option selected="">Choose Type...</option>
                          <?php $__currentLoopData = $titles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($title); ?>"><?php echo e($title); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>

                      <?php if($errors->has('field_type')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('field_type')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Range Value*</label>
                  <div class="col-sm-8 col-12">

                      <input type="number" name="min" class="col-md-3 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Minimum Value">
                      <input type="number" name="max" class="col-md-3 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Maximum Value">
                      <input type="number" name="step" class="col-md-3 <?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Step">

                      <?php if($errors->has('help_en')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('help_en')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Help English*</label>
                  <div class="col-sm-8">
                      <input type="text" name="help_en" class="<?php echo e($errors->has('help_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="help english">

                      <?php if($errors->has('help_en')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('help_en')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Help Bangla*</label>
                  <div class="col-sm-8">
                      <input type="text" name="help_bn" class="<?php echo e($errors->has('help_bn') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="help bangla">

                      <?php if($errors->has('help_bn')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('help_bn')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Example English*</label>
                  <div class="col-sm-8">
                      <input type="text" name="example_en" class="<?php echo e($errors->has('example_en') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="example english">

                      <?php if($errors->has('example_en')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('example_en')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>

              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Example Bangla*</label>
                  <div class="col-sm-8">
                      <input type="text" name="example_bn" class="<?php echo e($errors->has('example_bn') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="example bangla">

                      <?php if($errors->has('example_bn')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('example_bn')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>


              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Placeholder*</label>
                  <div class="col-sm-8">
                      <input type="text"  name="placeholder" class="<?php echo e($errors->has('placeholder') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="placeholder">

                      <?php if($errors->has('placeholder')): ?>
                          <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('placeholder')); ?></strong>
                     </span>
                      <?php endif; ?>
                  </div>
              </div>


              <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                  <div class="radio radio-success mt-2">
                      <input type="radio" name="status" id="radio14"   value="1" checked>
                      <label for="radio14"> Publish </label>
                  </div>
                  <div class="radio radio-warning mt-2">
                      <input type="radio" name="status" id="radio17" value="0">
                      <label for="radio17"> Unpublish </label>
                  </div>
              </div>


              <div class="form-group m-b-0">
              <div class="offset-sm-3 col-sm-8">
              <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Question Add</button>
              </div>
              </div>

          </form>
      </div>
  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>