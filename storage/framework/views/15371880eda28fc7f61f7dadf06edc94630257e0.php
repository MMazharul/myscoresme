<?php $__env->startSection('main-content'); ?>
    <div class="container-fluid">
        <div>
            <div class="col-12">
                <div class="card wizard-content">
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="col-md-6 mb-4"><h4 class="card-title mb-4">SME Information</h4><h6
                                    class="card-subtitle text-danger"><strong>Required *</strong></h6></div>
                        </div>
                        <form id="submit-form" name="form_name" action="<?php echo e(url('/question-store')); ?>"
                              method="post">
                            <?php echo e(csrf_field()); ?>

                            
                            <div class="wizards">
                                <div class="progressbar">
                                    <div class="progress-line" data-now-value="6.45" data-number-of-steps="10"
                                         style="width: 6.45%;"></div> <!-- 19.66% --></div>
                                <div class="col-md-12 row">
                                    <div class="form-wizard active col-md-1 ml-5 mr-3">
                                        <div class="wizard-icon"><i class="">1</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">2</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">3</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-3">
                                        <div class="wizard-icon"><i class="">4</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-3">
                                        <div class="wizard-icon"><i class="">5</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">6</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">7</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">8</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">9</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">10</i></div>
                                    </div>
                                </div>
                            </div> <?php $__currentLoopData = $questionCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $questionCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <fieldset><h5 class="mt-5 ml-4"><?php echo e($questionCategory->question_category); ?></h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">
                                                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($question->question_category_id==$questionCategory->id): ?>
                                                            <?php if($question->field_type=='text'): ?>
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4">
                                                                        <?php echo e($question->label_en); ?>

                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <input id="<?php echo e($question->column_name); ?>" type="text" name="<?php echo e($question->column_name); ?>"
                                                                               placeholder="<?php echo e($question->placeholder); ?>"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='number'): ?>
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8">
                                                                        <input type="number" id="number"
                                                                               name="<?php echo e($question->column_name); ?>"
                                                                               value=""
                                                                               placeholder="<?php echo e($question->placeholder); ?>"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><select
                                                                            name="<?php echo e($question->column_name); ?>"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                <option value="<?php echo e($option->title); ?>"><?php echo e($option->title); ?></option>                                                                                <?php endif; ?>                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='calender'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4 "><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control datepicker"
                                                                               name="<?php echo e($question->column_name); ?>" 
                                                                               placeholder="mm/dd/yyyy">
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='radio'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8 row ml-0">
                                                                        <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($question->id == $option->question_id): ?>
                                                                                <?php if($option->question->field_type =='radio'): ?>
                                                                                    <div class="m-b-10 ml-1 row"><label
                                                                                            class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name); ?>"
                                                                                                   value="<?php echo e($option->title); ?>"
                                                                                                   type="radio"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"> <?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='checkbox'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <?php if($option->question->field_type =='checkbox'): ?>
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name.'[]'); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"><?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='textarea'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><textarea rows="3"
                                                                                                    name="<?php echo e($question->column_name); ?>"
                                                                                                    placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='multi-Select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <option value="<?php echo e($question->column_name.'[]'); ?>"><?php echo e($option->title); ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='range'): ?>

                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><input type="text"
                                                                                                 name="<?php echo e($question->column_name); ?>"
                                                                                                 value=""
                                                                                                 placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                 class="amount form-control"></div>
                                                                </div>

                                                                <hr>
                                                            <?php endif; ?>                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <button class="btn btn-next">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $catagoryLastStep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catagorylast): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <fieldset><h5 class="mt-5 ml-4"><?php echo e($catagorylast->question_category); ?></h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">
                                                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($question->question_category_id==$catagorylast->id): ?>
                                                            <?php if($question->field_type=='text'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><input type="text"
                                                                                                 name="<?php echo e($question->column_name); ?>"
                                                                                                 value=""
                                                                                                 placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='number'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><input type="number"
                                                                                                 name="<?php echo e($question->column_name); ?>"
                                                                                                 value=""
                                                                                                 placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><select
                                                                            name="<?php echo e($question->column_name); ?>"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                <option value="<?php echo e($option->title); ?>"><?php echo e($option->title); ?></option>                                                                                <?php endif; ?>                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='calender'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" name="<?php echo e($question->column_name); ?>"
                                                                               value="" class="form-control singledate" placeholder="dd/mm/yyyy"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='radio'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9 row ml-0">
                                                                        <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($question->id == $option->question_id): ?>
                                                                                <?php if($option->question->field_type =='radio'): ?>
                                                                                    <div class="m-b-10 ml-1 row"><label
                                                                                            class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="radio"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"> <?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='checkbox'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <?php if($option->question->field_type =='checkbox'): ?>
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name.'[]'); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"><?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='textarea'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><textarea rows="3"
                                                                                                    name="<?php echo e($question->column_name); ?>"
                                                                                                    placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='multi-Select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <option value="<?php echo e($question->column_name.'[]'); ?>"><?php echo e($option->title); ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='range'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3 mt-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9 ml-0">
                                                                        <div class="range-slider range-slider_<?php echo e($question->id); ?>">
                                                                            <div class=""><span
                                                                                    style="font-size: 14px"><?php echo e($question->min); ?></span>
                                                                                <span style="font-size: 14px"
                                                                                      class="float-right"><?php echo e($question->max); ?></span>
                                                                            </div>
                                                                            <input class="range-slider__range range-slider__range_<?php echo e($question->id); ?>"
                                                                                   type="range" step="<?php echo e($question->step); ?>"
                                                                                   value="1" min="<?php echo e($question->min); ?>"
                                                                                   max="<?php echo e($question->max); ?>"> <span
                                                                                class="range-slider__value range-slider__value_<?php echo e($question->id); ?>">0</span>
                                                                            <input type="hidden"
                                                                                   name="<?php echo e($question->column_name); ?>"
                                                                                   id="<?php echo e($question->id); ?>"></div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <input onclick="submit_by_name()" type="submit"
                                                           class="btn btn-success" value="Complete"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('input.amount').keyup(function(event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function(index, value) {
                return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
            });
        });
    </script>

    <script>

        $(document).ready(function () {
            $("#submit-form").submit(function (e) {
                e.preventDefault();
                var that = $(this);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: that.attr('action'),
                    data: that.serialize(),
                    cache: false,
                    success: function (data) {
                        console.log(data)
                    }/*,                     error: function (xhr, status, error) {                        alert("An AJAX error occured: " + status + "\nError: " + error);                     }*/
                });
            });
        });
    </script>
    <script>
        function submit_by_name() {
            var x = document.getElementsByName('form_name');
            x[0].submit();       }
    </script>


    <script>
        $( function() {
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
        } );
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.sme.layout.master-dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>