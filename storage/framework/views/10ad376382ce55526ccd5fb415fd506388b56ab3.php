<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
    <?php echo Form::label('name', 'Name', ['class' => 'col-md-4 control-label']); ?>
    <div class="col-md-6">
        <?php echo Form::text('name', null, ['class' => 'form-control']); ?>
        <?php echo $errors->first('name', '<p class="help-block">:message</p>'); ?>
    </div>
</div>
<div class="form-group <?php echo e($errors->has('alias') ? 'has-error' : ''); ?>">
    <?php echo Form::label('alias', 'Alias', ['class' => 'col-md-4 control-label']); ?>
    <div class="col-md-6">
        <?php echo Form::text('alias', null, ['class' => 'form-control']); ?>
        <?php echo $errors->first('alias', '<p class="help-block">:message</p>'); ?>
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <?php echo Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']); ?>
    </div>
</div>
<?php $__env->startPush('scripts'); ?>
<script>
    var convertName2Alias = function () {
        var name = $(this).val().trim().toLowerCase().replace(/\s+/g, '_');
        var alias = $('#alias').val();
        if (alias == '') {
            $('#alias').val(name);
        }
    };
    $(function () {
        $('#name').on('change', convertName2Alias);
    });
</script>
<?php $__env->stopPush(); ?>