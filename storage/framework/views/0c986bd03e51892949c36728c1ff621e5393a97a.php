<?php $__env->startSection('content'); ?>
    <div class="col-md-10 offset-md-1 pt-5">
        <div class="card card-body">
            <?php if(session('message')): ?>
                <div class="alert alert-success alert-dismissible" role="alert" id="message">
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message text-center "><?php echo e(session('message')); ?></div>
                </div>
            <?php endif; ?>

            <div class="col-12 row"><h3 class="box-title m-b-0 col-11">Bank Signup</h3>
                <div class="col-1"><a href="<?php echo e(url('/bank')); ?>" class="btn btn-primary"><i class="fa fa-list"></i>
                        List</a></div>
            </div>
            <hr class="mb-4">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/bank/store')); ?>"
                  enctype="multipart/form-data">                <?php echo e(csrf_field()); ?>
                <div class="form-group row"><label for="category"
                                                   class="col-sm-3 text-right control-label col-form-label">Category:</label>
                    <div class="col-sm-7"><select class="form-control" name="category">
                            <option value="0" disabled="true" selected="true">-Select Category-</option>
                            <option value="Bank">Bank</option>
                            <option value="NBI">NBFI</option>
                            <option value="BI">MFI</option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="inputEmail3"
                                                   class="col-sm-3 text-right control-label col-form-label">Firm/Organization
                        Name:</label>
                    <div class="col-sm-7"><input type="text" name="organization_name" class="form-control"
                                                 id="inputEmail3" placeholder="Firm/Organization Name"></div>
                </div>
                <div class="form-group row"><label for="description"
                                                   class="col-sm-3 text-right control-label col-form-label">Overall
                        Narrative Description:</label>
                    <div class="col-sm-7"><textarea class="form-control" rows="3" id="description" name="description"
                                                    placeholder="Overall Narrative Description"></textarea></div>
                </div>
                <div class="form-group row"><label for="fax" class="col-sm-3 text-right control-label col-form-label">Country
                        of Incorporation:</label>
                    <div class="col-sm-7"><select class="form-control" name="country_incorporation">
                            <option value="0" disabled="true" selected="true">-Select Country-</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="England">England</option>
                            <option value="SouthAfrica">SouthAfrica</option>
                            <option value="Australia">Australia</option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="fax" class="col-sm-3 text-right control-label col-form-label">Year
                        of Incorporation:</label>
                    <div class="col-sm-7"><select class="form-control" name="year_incorporation">
                            <option value="0" disabled="true" selected="true">-Select Year-</option>
                            <option value="2011">2011</option>
                            <option value="2012">2012</option>
                            <option value="2013">2013</option>
                            <option value="2014">2014</option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="Mobile"
                                                   class="col-sm-3 text-right control-label col-form-label">Incorporation
                        Document or Registration Number:</label>
                    <div class="col-sm-7"><input type="text" class="form-control" name="registration_number"
                                                 placeholder="Incorporation Document or Registration Number"></div>
                </div>
                <div class="form-group row"><label for="Mobile"
                                                   class="col-sm-3 text-right control-label col-form-label">Type of
                        Organization:</label>
                    <div class="col-sm-7"><select class="form-control" name="type_organization">
                            <option value="0" disabled="true" selected="true">-Select Organization Type-</option>
                            <option value="xyz">Micro</option>
                            <option value="xyz">Small</option>
                            <option value="xyz">Medium</option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="division1"
                                                   class="col-sm-3 text-right control-label col-form-label">Division</label>
                    <div class="col-sm-7"><select id="division1" name="division"
                                                  class="form-control<?php echo e($errors->has('division') ? ' is-invalid' : ''); ?> division4">
                            <option value="0" disabled="true" selected="true">-Select division-
                            </option> <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($division->id); ?>"><?php echo e($division->title); ?></option>                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select></div>
                </div>
                <div class="form-group row"><label for="district1"
                                                   class="col-sm-3 text-right control-label col-form-label">Distirct</label>
                    <div class="col-sm-7"><select id="district1" name="district"
                                                  class="form-control<?php echo e($errors->has('district') ? ' is-invalid' : ''); ?>  district4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="upazila1"
                                                   class="col-sm-3 text-right control-label col-form-label">Upazila</label>
                    <div class="col-sm-7"><select id="upazila1" name="upazila"
                                                  class="form-control<?php echo e($errors->has('upazila') ? ' is-invalid' : ''); ?>  upazila4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="post_office1"
                                                   class="col-sm-3 text-right control-label col-form-label">Post
                        Office</label>
                    <div class="col-sm-7"><select id="post_office1" name="post_office"
                                                  class="form-control<?php echo e($errors->has('post_office') ? ' is-invalid' : ''); ?> suboffice4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select></div>
                </div>
                <div class="form-group row"><label for="post_code1"
                                                   class="col-sm-3 text-right control-label col-form-label">Post
                        Code:</label>
                    <div class="col-sm-7 postcode4"><input id="post_code1" required="required" type="text"
                                                           name="post_code4"
                                                           class=" form-control<?php echo e($errors->has('post_code') ? ' is-invalid' : ''); ?>"
                                                           readonly></div>
                </div>
                <div class="form-group row"><label for="telephone"
                                                   class="col-sm-3 text-right control-label col-form-label">Telephone:</label>
                    <div class="col-sm-7"><input type="number" name="telephone" class="form-control" id="telephone"
                                                 placeholder="telephone"></div>
                </div>
                <div class="form-group row"><label for="mobile_number"
                                                   class="col-sm-3 text-right control-label col-form-label">Mobile
                        Number:</label>
                    <div class="col-sm-7"><input type="text" name="owner_contact" class="form-control "
                                                 id="mobile_number" placeholder="mobile number"></div>
                </div>
                <div class="form-group row"><label for="email" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                    <div class="col-sm-7"><input type="email" name="email"
                                                 class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>"
                                                 id="email" placeholder="Email"> <?php if($errors->has('email')): ?><span
                                class="invalid-feedback"
                                role="alert">                                <strong><?php echo e($errors->first('email')); ?></strong>                            </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="retype_email"
                                                   class="col-sm-3 text-right control-label col-form-label">Re Type
                        Email:</label>
                    <div class="col-sm-7"><input type="email" name="retype_email" class="form-control" id="retype_email"
                                                 placeholder="Retype Email"></div>
                </div>
                <div class="form-group row"><label for="alternate_email"
                                                   class="col-sm-3 text-right control-label col-form-label"> Alternate
                        Email:</label>
                    <div class="col-sm-7"><input type="email" name="alternate_email" class="form-control"
                                                 id="alternate_email" placeholder="Alternate Email"></div>
                </div>
                <div class="form-group row"><label for="professional_staff"
                                                   class="col-sm-3 text-right control-label col-form-label">Number of
                        full time professional Staff: </label>
                    <div class="col-sm-7"><input type="number" name="professional_staff" class="form-control"
                                                 id="professional_staff"
                                                 placeholder="Number of full time professional Staff"></div>
                </div>
                <div class="form-group row"><label for="branch_number"
                                                   class="col-sm-3 text-right control-label col-form-label">Number of
                        Branches:</label>
                    <div class="col-sm-7"><input type="number" name="branch_number" class="form-control"
                                                 id="branch_number" placeholder="Number of Branches"></div>
                </div>
                <div class="form-group row"><label for="professional_staff"
                                                   class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7"><label>Name of CEO:</label></div>
                </div>
                <div class="form-group row"><label for="title" class="col-sm-3 text-right control-label col-form-label">Title</label>
                    <div class="col-sm-7"><select id="title"
                                                  class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>"
                                                  name="title">
                            <option disabled selected>-Select Title-</option>
                            <option value="Mrs." <?php echo e(old('title') == "Mrs." ? 'selected' : ''); ?>>Mrs.</option>
                            <option value="Mr." <?php echo e(old('title') == "Mr." ? 'selected' : ''); ?>>Mr.</option>
                            <option value="Ms" <?php echo e(old('title') == "Ms" ? 'selected' : ''); ?>>Ms</option>
                        </select> <?php if($errors->has('title')): ?>                            <span class="invalid-feedback"
                                                                                               role="alert">                                <strong><?php echo e($errors->first('title')); ?></strong>                            </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="first_name"
                                                   class="col-sm-3 text-right control-label col-form-label">First
                        Name</label>
                    <div class="col-sm-7"><input id="first_name" type="text"
                                                 class="form-control<?php echo e($errors->has('contact_first_name') ? ' is-invalid' : ''); ?>"
                                                 name="first_name" value="<?php echo e(old('contact_first_name')); ?>"
                                                 placeholder="Enter your first name"> <?php if($errors->has('contact_first_name')): ?>
                            <span class="invalid-feedback"
                                  role="alert">                                <strong><?php echo e($errors->first('contact_first_name')); ?></strong>                             </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="middle_name"
                                                   class="col-sm-3 text-right control-label col-form-label">Middle
                        Name</label>
                    <div class="col-sm-7"><input id="middle_name" type="text"
                                                 class="form-control<?php echo e($errors->has('contact_middle_name') ? ' is-invalid' : ''); ?>"
                                                 name="middle_name" value="<?php echo e(old('contact_middle_name')); ?>"
                                                 placeholder="Enter your middle name"> <?php if($errors->has('contact_middle_name')): ?>
                            <span class="invalid-feedback"
                                  role="alert">                                <strong><?php echo e($errors->first('contact_middle_name')); ?></strong>                            </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="last_name"
                                                   class="col-sm-3 text-right control-label col-form-label">Last
                        Name</label>
                    <div class="col-sm-7"><input id="last_name" type="text"
                                                 class="form-control<?php echo e($errors->has('contact_last_name') ? ' is-invalid' : ''); ?>"
                                                 name="last_name" value="<?php echo e(old('last_name')); ?>"
                                                 placeholder="Enter your last name"> <?php if($errors->has('contact_last_name')): ?>
                            <span class="invalid-feedback"
                                  role="alert">                                            <strong><?php echo e($errors->first('contact_last_name')); ?></strong>                                        </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="logo" class="col-sm-3 text-right control-label col-form-label">Upload
                        Bank logo</label>
                    <div class="col-sm-7"><input type="file" class="form-control" name="bank_logo" id="logo"></div>
                </div>
                <div class="form-group row"><label for="attachment"
                                                   class="col-sm-3 text-right control-label col-form-label">Upload Bank
                        Attachment</label>
                    <div class="col-sm-7"><input type="file" class="form-control" id="attachment"
                                                 name="bank_attachment"></div>
                </div>
                <div class="form-group row"><label for="password"
                                                   class="col-sm-3 text-right control-label col-form-label">Password </label>
                    <div class="col-sm-7"><input type="password"
                                                 class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                                                 id="password" name="password"
                                                 placeholder="Enter your password"> <?php if($errors->has('password')): ?><span
                                class="invalid-feedback"
                                role="alert">                                            <strong><?php echo e($errors->first('password')); ?></strong>                                        </span>                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group row"><label for="password"
                                                   class="col-sm-3 text-right control-label col-form-label">Confirm
                        Password </label>
                    <div class="col-sm-7"><input id="password-confirm" type="password" class="form-control"
                                                 name="password_confirmation" placeholder="Enter your confirm password">
                    </div>
                </div>
                <div class="form-group row"><label for="branch_type"
                                                   class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </div><?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>