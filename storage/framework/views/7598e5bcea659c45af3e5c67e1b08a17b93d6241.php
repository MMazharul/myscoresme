

<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Roles</div>
        <div class="panel-body">

            <a href="<?php echo e(url('/' . Config("authorization.route-prefix") . '/roles/create')); ?>" class="btn btn-primary btn-xs" title="Add New Role"><span
                        class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th> Name</th>
                        <th> Alias</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($item->id); ?></td>
                            <td><?php echo e($item->name); ?></td>
                            <td><?php echo e($item->alias); ?></td>
                            <td>
                                <a href="<?php echo e(url('/' . Config("authorization.route-prefix") . '/roles/' . $item->id)); ?>" class="btn btn-success btn-xs"
                                   title="View Role"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                <a href="<?php echo e(url('/' . Config("authorization.route-prefix") . '/roles/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs"
                                   title="Edit Role"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                                <?php if($item->id > 2): ?>
                                <?php echo Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/' . Config("authorization.route-prefix") . '/roles', $item->id],
                                    'style' => 'display:inline'
                                ]); ?>

                                <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Role" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Role',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )); ?>

                                <?php echo Form::close(); ?>

                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <div class="pagination-wrapper"> <?php echo $roles->render(); ?> </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.authorize.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>