<?php $__env->startSection('content'); ?>

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0">Upazila Edit</h3>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/upazila/'.$upazilaEdit->id)); ?>">

                <?php echo csrf_field(); ?>
                <?php echo e(method_field('PUT')); ?>


                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12" name="district_id" id="inlineFormCustomSelect">
                            <option selected="">Choose District...</option>
                            <?php $__currentLoopData = $districts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($key); ?>" <?php if($key == $upazilaEdit->district_id): ?> <?php echo e('selected'); ?> <?php endif; ?> ><?php echo e($district); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="title" value="<?php echo e($upazilaEdit->title); ?>" class="form-control" id="inputEmail3" placeholder="Thana Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Upazila Geo code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="geo_code" value="<?php echo e($upazilaEdit->geo_code); ?>" class="<?php echo e($errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Upazila Geo Code">

                        <?php if($errors->has('geo_code')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('geo_code')); ?></strong>
                     </span>
                        <?php endif; ?>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" <?php if($upazilaEdit->status==1): ?> <?php echo e('checked'); ?> <?php endif; ?> name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" <?php if($upazilaEdit->status==0): ?> <?php echo e('checked'); ?> <?php endif; ?> name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Upazila Update</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>