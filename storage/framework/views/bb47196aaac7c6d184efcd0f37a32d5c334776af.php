<?php $__env->startSection('content'); ?>
    <?php if(session('message')): ?>
        <div class="alert alert-success alert-dismissible" role="alert"  id="message">
            <div class="icon"><span class="mdi mdi-check"></span></div>
            <div class="message text-center "><?php echo e(session('message')); ?></div>
        </div>
    <?php endif; ?>

    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Sub-sector Create</h3>
                <div class="col-1">
                    <a href="<?php echo e(url('/sub-sector')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>
            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/sub-sector')); ?>">
                <?php echo csrf_field(); ?>

                <div class="form-group row">
                    <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Sector Type</label>
                    <div class="col-sm-8">
                        <select class="custom-select col-sm-12 <?php echo e($errors->has('	sector_id') ? 'form-control is-invalid' : 'form-control'); ?>" name="sector_id" id="inlineFormCustomSelect">
                            <option selected=""> --Choose a Sector Type--</option>
                              <?php $__currentLoopData = $sectors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$sector): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($id); ?>"><?php echo e($sector); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('	sector_id')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('	sector_id')); ?></strong>
                     </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="subsector" class="col-sm-3 text-right control-label col-form-label">Sub sector Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="sub_sector" class="form-control" id="subsector" placeholder="Enter Sector Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sector" class="col-sm-3 text-right control-label col-form-label">Sub sector Code*</label>
                    <div class="col-sm-8">
                        <input type="text" name="code" class="form-control" id="sector" placeholder="Enter Industry Sector Code">
                    </div>
                </div>

                <div class="form-group row disabled">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" name="status" id="radio14"   value="1" checked>
                        <label for="radio14"> Publish</label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Subsector create</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>