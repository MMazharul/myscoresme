<?php $__env->startSection('content'); ?>


    <div class="card">
        <?php if(session('message')): ?>
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center "><?php echo e(session('message')); ?></div>
            </div>
        <?php endif; ?>
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> Bank Information</button>
                </div>



            </div>
            <div class="m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Bank Logo</th>
                        <th>Catagory Name</th>
                        <th>Organization Name</th>
                        <th>Registration Number</th>
                        <th>Type Organization</th>
                        <th>Email</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $bankUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><img width="100px" height="50px" src="<?php echo e(asset('ui/back-end/bank/bank-logo/'. $bank->bank_logo )); ?>"></td>
                            <td><?php echo e($bank->category); ?></td>
                            <td><?php echo e($bank->organization_name); ?></td>
                            <td><?php echo e($bank->registration_number); ?></td>
                            <td><?php echo e($bank->type_organization); ?></td>
                            <td><?php echo e($bank->retype_email); ?></td>

                            <td class="pt-lg-5">
                                <div class="dropdown row">

                                    <a href="" title="Click to More details"
                                       class="btn btn-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-eye"></i>
                                    </a>
                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </td>


                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>