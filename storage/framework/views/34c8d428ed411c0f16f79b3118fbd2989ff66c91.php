<?php $__env->startSection('content'); ?>
    <div class="col-md-10 offset-md-1 pt-5">
        <div class="card card-body">
            <h3 class="box-title m-b-0"> Slider Add Form</h3>
            <hr class="mb-4">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/slider/'.$sliderEdit->id)); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>
                <?php echo e(method_field('PUT')); ?>
                <div class="form-group row">
                    <label for="title" class="col-sm-3 text-right control-label col-form-label"> Title*</label>
                    <div class="col-sm-7">
                        <input type="text" value="<?php echo e($sliderEdit->title); ?>" name="title" class="form-control" id="title" placeholder="title">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 text-right control-label col-form-label">Slider Image</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="picture" type="file" name="picture" placeholder="Enter Your Mobile Number">
                    </div>

                </div>

                <div class="form-group row">
                    <label for="picture" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                    <img src="<?php echo e(asset('ui/back-end/slider-image/'.$sliderEdit->picture)); ?>" width="200px" height="150px"/>
                </div>
                </div>


                <div class="form-group row">
                    <label for="ShortDescription" class="col-sm-3 text-right control-label col-form-label">Short Description*</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" id="ShortDescription" cols="10" rows="4" name="short_description" placeholder="Enter Your Fax Number"><?php echo e($sliderEdit->short_description); ?></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="buttonTitle" class="col-sm-3 text-right control-label col-form-label">Button Title*</label>
                    <div class="col-sm-7">
                        <input class="form-control" value="<?php echo e($sliderEdit->button_title); ?>" id="buttonTitle" type="text" name="button_title" placeholder="Enter Your Email">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Status*</label>
                    <div class="radio radio-success mt-2">
                        <input type="radio" <?php if($sliderEdit->status==1): ?> <?php echo e('checked'); ?> <?php endif; ?> name="status" id="radio14" value="1" checked>
                        <label for="radio14"> Publish </label>
                    </div>
                    <div class="radio radio-warning mt-2">
                        <input type="radio" <?php if($sliderEdit->status==0): ?> <?php echo e('checked'); ?> <?php endif; ?> name="status" id="radio17" value="0">
                        <label for="radio17"> Unpublish </label>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="branch_type" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Slider Update</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>