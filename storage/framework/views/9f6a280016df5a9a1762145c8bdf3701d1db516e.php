<?php $__env->startSection('content'); ?>


    <div class="card">
        <?php if(session('message')): ?>
            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center "><?php echo e(session('message')); ?></div>
            </div>
        <?php endif; ?>
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> Branch Information</button>
                </div>



            </div>
            <div class="m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Title</th>
                        <th>Trade License Number</th>
                        <th>Commision rate</th>
                        <th>Email</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $affiliateUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $affiliate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($affiliate->category); ?></td>
                            <td><?php echo e($affiliate->affilate_name); ?></td>
                            <td><?php echo e($affiliate->trade_license); ?></td>
                            <td><?php echo e($affiliate->commision_rate); ?> % </td>
                            <td><?php echo e($affiliate->person_email); ?></td>
                            <td class="pt-lg-5">
                                <div class="dropdown row">

                                    <a href="" title="Click to More details"
                                       class="btn btn-light ml-1" type="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-eye"></i>
                                    </a>
                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href=""><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </td>


                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>