<?php $__env->startSection('title', 'Register'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">

            <?php if(session('massage')): ?>

                <div class="col-md-8 alert alert-success mt-4">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                    <h3 class="text-success"><i class="fa fa-check-circle"></i> Success</h3> <?php echo e(session('massage')); ?>

                </div>

                
            <?php endif; ?>

            <div class="col-md-8 mt-4">
                <div class="card bg-white">
                    <div class="card-body">
                        <form class="form-horizontal" action="<?php echo e(url('/password/store')); ?>" method="POST" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="text-right control-label col-form-label" style="text-transform: none" for="password">Create Password</label>
                                    <input id="password" type="password"
                                           class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                                           name="password" value="" placeholder="Create your password">
                                    <?php if($errors->has('password')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" style="text-transform: none" for="owner_first">Re-type Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Retype Your Password" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" value="1" name="policy_agree" type="checkbox"
                                           id="gridCheck" required>
                                    <label class="form-check-label" for="gridCheck">
                                        Declaration will be provided by MyScoreSME authority. After Getting the
                                        statement we will incorporate shortly.
                                    </label>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-10 pt-3 pb-3">Sign up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('front-end.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>