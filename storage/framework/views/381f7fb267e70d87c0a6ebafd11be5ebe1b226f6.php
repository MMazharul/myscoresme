<?php $__env->startSection('content'); ?>





   <div class="col-md-8 offset-2 pt-5">

       <div class="card card-body">

           <h3 class="box-title m-b-0">Division Edit</h3>

           <hr class="mb-5">

           <form class="form-horizontal" method="post" action="<?php echo e(url('/division/'.$divisionEdit->id)); ?>">



               <?php echo csrf_field(); ?>

               <?php echo e(method_field('PUT')); ?>


               <div class="form-group row">

                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Division Name*</label>

                   <div class="col-sm-8">

                       <input type="text" name="title" value="<?php echo e($divisionEdit->title); ?>" class="form-control" id="inputEmail3" placeholder="Division Name">

                   </div>

               </div>



               <div class="form-group row">

                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Division Geo code *</label>

                   <div class="col-sm-8">

                       <input type="text" name="geo_code" value="<?php echo e($divisionEdit->geo_code); ?>" class="<?php echo e($errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Division Geo Code">



                       <?php if($errors->has('geo_code')): ?>

                           <span class="invalid-feedback" role="alert">

                        <strong><?php echo e($errors->first('geo_code')); ?></strong>

                     </span>

                       <?php endif; ?>

                   </div>

               </div>

               <div class="form-group row">

                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>

                   <div class="col-sm-8">

                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Divistion Update</button>

                   </div>

               </div>







           </form>

       </div>

   </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>