
<?php $__env->startSection('content'); ?>
    <div class="be-content">
    <div class="main-content container-fluid">
        <?php if(session('message')): ?>
            <div class="alert alert-success alert-dismissible" role="alert"  id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center "><?php echo e(session('message')); ?></div>
            </div>
        <?php endif; ?>
    <div class="row">
        <div class="col-md-12 ">
            <div class="card card-table">
                <div class="card-header">Field Type List
                </div>
                <div class="card-body">
                    <h4 class="text-center text-danger"><?php echo e(session('delete')); ?></h4>

                    <table class="table table-striped table-hover table-fw-widget" id="table3">
                        <thead>
                        <tr>
                            <th>Field Type Name</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <td> <?php echo e($type->title); ?></td></tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>