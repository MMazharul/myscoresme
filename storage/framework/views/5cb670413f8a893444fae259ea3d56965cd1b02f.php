<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="" alt="user" /> </div>
            <!-- User profile text-->
            <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo e(Auth::user()->affiliate['affilate_name']); ?> <span class="caret"></span></a>
                <div class="dropdown-menu animated flipInY">
                    <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div> <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <div class="dropdown-divider"></div> <a href="login.html" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                </div>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap"></li>
                <li>
                    <a  href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Affiliate Dashboard </span></a>
                </li>

                <li>
                    <a  class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-sliders"></i><span class="hide-menu">Affiliate Resource</span></a>
                    <ul aria-expanded="false" class="collapse">


                        <li>
                            <a  class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-sliders"></i><span class="hide-menu"> Loan Aplicant</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo e(url('/slider')); ?>"><i class="mdi mdi-settings"></i>  Loan Aplicant Manage</a></li>
                            </ul>
                        </li>


                    </ul>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->


    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                      <?php echo csrf_field(); ?>
    </form><i class="mdi mdi-power"></i>

    <div class="sidebar-footer">
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item-->
        <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <!-- item-->
       

        <a href="<?php echo e(route('logout')); ?>" class="link" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>

    </div>
    <!-- End Bottom points-->
</aside>