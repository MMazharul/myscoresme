

<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Update Permission</div>
        <div class="panel-body">

            <?php if($errors->any()): ?>
                <ul class="alert alert-danger">
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            <?php endif; ?>

            <?php echo Form::open(['url' => '/' . Config("authorization.route-prefix") . '/permissions', 'class' => 'form-horizontal']); ?>

            <div class="form-group <?php echo e($errors->has('role_id') ? 'has-error' : ''); ?>">
                <?php echo Form::label('role_id', 'Role', ['class' => 'col-md-4 control-label']); ?>

                <div class="col-md-6">
                    <?php echo Form::select('role_id', $roles, null, ['placeholder' => 'Please select ...', 'class' => 'form-control', 'required' => 'required']); ?>

                    <?php echo $errors->first('role_id', '<p class="help-block">:message</p>'); ?>

                </div>
            </div>
            <div class="form-group <?php echo e($errors->has('controller') ? 'has-error' : ''); ?>">
                <?php echo Form::label('controller', 'Actions', ['class' => 'col-md-4 control-label']); ?>

                <div class="col-md-6">
                    <ul id="tree">
                        <?php $__currentLoopData = $actions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $namespace => $controllers): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($namespace); ?>

                            <ul>
                                <?php $__currentLoopData = $controllers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $controller => $methods): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><?php echo e($controller); ?>

                                    <ul>
                                        <?php $__currentLoopData = $methods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $method => $actions): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($method); ?>

                                            <ul>
                                                <?php $__currentLoopData = $actions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $action): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li>
                                                        <?php echo e(Form::checkbox('actions[]', $namespace . '-' . $controller . '-' . $method . '-' . $action, null, ['class' => 'field'])); ?>

                                                        <?php echo e($action); ?>

                                                    </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                </div>
            </div>

            <?php echo Form::close(); ?>


        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('styles'); ?>
<link href="/vendor/authorize/css/tree.css" rel="stylesheet">
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
<script src="/vendor/authorize/js/tree.js"></script>
<script>
    $(function () {
        $('#role_id').on('change', function () {
            var role_id = $(this).val();
            $('#tree').find('input[type=checkbox]:checked').prop('checked', '');
            if (role_id > 0) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': Laravel.csrfToken
                    },
                    type: "POST",
                    url: '<?php echo e(url('/' . Config("authorization.route-prefix") . '/permissions/getSelectedRoutes')); ?>',
                    data: {role_id: role_id},
                    cache: false,
                    success: function (res) {
                        $.each(res.selectedActions, function (key, val) {
                            var value = val.replace(/\\/g, '\\\\');
                            $('input[type="checkbox"][value="' + value + '"]').prop('checked', 'checked');
                        });
                    },
                    error: function (xhr, status, error) {
                        alert("An AJAX error occured: " + status + "\nError: " + error);
                    }
                });
            }
        });
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('vendor.authorize.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>