<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">Users</div>
        <div class="panel-body">
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($item->id); ?></td>
                            <td><?php echo e($item->name); ?></td>
                            <td><?php echo e($item->email); ?></td>
                            <td><?php echo e($item->role['name']); ?></td>
                            <td>
                                <a href="<?php echo e(url('/' . Config("authorization.route-prefix") . '/users/' . $item->id . '/edit')); ?>" class="btn btn-primary btn-xs"
                                   title="Edit User"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>

                                <?php if($item->id != Auth::user()->id): ?>
                                <?php echo Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/' . Config("authorization.route-prefix") . '/users', $item->id],
                                    'style' => 'display:inline'
                                ]); ?>

                                <?php echo Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete User',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )); ?>

                                <?php echo Form::close(); ?>

                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <div class="pagination-wrapper"> <?php echo $users->render(); ?> </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('vendor.authorize.layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>