﻿<!doctype html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="Borrow - is the loan company, Business Website Template.">

    <meta name="keywords" content="Financial Website Template, Bootstrap Template, Loan Product, Personal Loan">



    <title><?php echo $__env->yieldContent('title'); ?></title>

    <script src="<?php echo e(asset('ajax/jquery.min.js')); ?>"></script> 

<!-- multi-select -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap -->

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/style.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/fontello.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/css/dropify.min.css">

    <!-- Login design -->
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->

<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/')); ?>login-ui/css/main.css">
    <!--===============================================================================================-->

    <!-- Datepicker css -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <!-- Camera Webcam -->
    <script src="<?php echo e(asset('/camera-ui/js/webcam.min.js')); ?>"></script>


    <!-- Daterange picker plugins css -->

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">



    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CMerriweather:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- owl Carousel Css -->

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/owl.carousel.css" rel="stylesheet">

    <link href="<?php echo e(asset('/')); ?>ui/front-end/css/owl.theme.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <script src="<?php echo e(asset('/')); ?>ui/back-end/vue/vue.js"></script>

    <style>
        .hidden{
            display:none;
        }
    </style>

</head>

<body>

<?php echo $__env->make('front-end.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>



<?php echo $__env->yieldContent('content'); ?>



<a href="#0" class="cd-top" title="Go to top">Top</a>



<?php echo $__env->make('front-end.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.js"></script>
<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/jquery/jquery.min.js"></script>



<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="<?php echo e(asset('/')); ?>ui/front-end/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/menumaker.js"></script>



<!-- multi select -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/multiselect/js/jquery.multi-select.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>



<!-- sticky header -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/jquery.sticky.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/sticky-header.js"></script>

<!-- slider script -->

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/slider-carousel.js"></script>

<script type="text/javascript" src="<?php echo e(asset('/')); ?>ui/front-end/js/service-carousel.js"></script>

<!-- Back to top script -->

<script src="<?php echo e(asset('/')); ?>ui/front-end/js/back-to-top.js" type="text/javascript"></script>

<script src="<?php echo e(asset('ajax/branch.js')); ?>"></script>

<script src="<?php echo e(asset('ajax/location.js')); ?>"></script>
<script src="<?php echo e(asset('ajax/sector.js')); ?>"></script>



<!-- Date Picker Plugin JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- Date range Plugin JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/daterangepicker/daterangepicker.js"></script>

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/moment/moment.js"></script>




<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>












<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo e(asset('jquery-validation/validation.js')); ?>"></script>

<script>

    <?php if(isset(Auth::User()->businessInfo->total_director_partner)): ?>
    $.validator.addMethod('filesize', function (value, element, arg) {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>

        var file_size = $('#'+element.id)[0].files[0].size;

        if(file_size<2097152)
        {
            return true;

        }

        <?php endfor; ?>

    });

    $("#partner_validation" ).validate({

        rules: {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>
            owner_title<?php echo e($i); ?>: "required",
            owner_first_name<?php echo e($i); ?>: "required",
            owner_last_name<?php echo e($i); ?>: "required",
            position<?php echo e($i); ?>: "required",
            shareholder<?php echo e($i); ?>: "required",
            date_of_birth<?php echo e($i); ?>: "required",
            present_division<?php echo e($i); ?>: "required",
            present_district<?php echo e($i); ?>: "required",
            present_upazila<?php echo e($i); ?>: "required",
            presentpost_office<?php echo e($i); ?>: "required",
            present_post_code<?php echo e($i); ?>: "required",
            present_address<?php echo e($i); ?>: "required",
            permanent_division<?php echo e($i); ?>: "required",
            permanent_district<?php echo e($i); ?>: "required",
            permanent_upazila<?php echo e($i); ?>: "required",
            permanentpost_office<?php echo e($i); ?>: "required",
            post_code<?php echo e($i); ?>: "required",
            permanent_address<?php echo e($i); ?>: "required",
            owner_nid<?php echo e($i); ?>:{
                required:true,
                number: true,
                maxlength: 17

            },
            owner_pic<?php echo e($i); ?>:{
                required:true,
                accept:"application/pdf,image/jpeg,image/png",
                filesize:2097152   //max size 2MB
            },
            nid_front_pic<?php echo e($i); ?>:{
                required:true,
                accept:"application/pdf,image/jpeg,image/png",
                filesize:  2097152     //max size 2MB
            },
            nid_back_pic<?php echo e($i); ?>:{
                required:true,
                accept:"application/pdf,image/jpeg,image/png",
                filesize:  2097152     //max size 2MB
            },
            <?php endfor; ?>

        },
        messages: {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>

            owner_nid<?php echo e($i); ?>:{
                number:"Please Enter a Valid Nid"

            },
            owner_pic<?php echo e($i); ?>:{
                filesize:" file size must be less than 2MB.",
                accept:"Please upload .jpg or .png "

            },
            nid_front_pic<?php echo e($i); ?>:{
                filesize:"file size must be less than 2MB.",
                accept:"Please upload .jpg or .png ",
                required:"Please upload file."
            },
            nid_back_pic<?php echo e($i); ?>:{
                filesize:"file size must be less than 2MB.",
                accept:"Please upload .jpg or .png ",
                required:"Please upload file."
            },
            <?php endfor; ?>
        },


        errorElement: "span",

        errorPlacement: function ( error, element ) {


            // Add the `help-block` class to the error element
            error.addClass( "help-block").css('color','#D93025');

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
//        highlight: function ( element, errorClass, validClass ) {
//            $( element ).parents( ".form-group" ).css("css","#A9446C").removeClass( "has-success" );
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
//        },

        submitHandler: function(form) {
            form.submit();
        }
    });
    <?php endif; ?>
</script>

<script>

    var id =$('select[name="organization"]').val();
    if(id=="Partnership" || id=="Public Limited" || id=='Private Limited')
    {
        $('#partnership').show();
        $('#label').html("Number of Partners");
        $('#headlabel').html("Partnership Information");
        $('#total_director_partner').attr("placeholder", "Enter number of partners");
    }
    if(id=="Sole Proprietorship")
    {
        $('#proprietorship').show();
        $('#enterprise_name_label').html("Number of Partners");
        $('#total_director_partner').attr('min', '2');
    }


</script>


<script>
    <?php if(isset(Auth::User()->businessInfo->total_director_partner)): ?>
     $("#partnerEditValidation" ).validate({
        rules: {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>
            owner_title<?php echo e($i); ?>: "required",
            owner_first_name<?php echo e($i); ?>: "required",
            owner_last_name<?php echo e($i); ?>: "required",
            position<?php echo e($i); ?>: "required",
            shareholder<?php echo e($i); ?>: "required",
            date_of_birth<?php echo e($i); ?>: "required",
            present_division<?php echo e($i); ?>: "required",
            present_district<?php echo e($i); ?>: "required",
            present_upazila<?php echo e($i); ?>: "required",
            presentpost_office<?php echo e($i); ?>: "required",
            present_post_code<?php echo e($i); ?>: "required",
            present_address<?php echo e($i); ?>: "required",
            permanent_division<?php echo e($i); ?>: "required",
            permanent_district<?php echo e($i); ?>: "required",
            permanent_upazila<?php echo e($i); ?>: "required",
            permanentpost_office<?php echo e($i); ?>: "required",
            post_code<?php echo e($i); ?>: "required",
            permanent_address<?php echo e($i); ?>: "required",
            owner_nid<?php echo e($i); ?>:{
                required:true,
                number: true,
                maxlength: 17

            },
            <?php endfor; ?>


        },
        messages: {
            <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>

            owner_nid<?php echo e($i); ?>:{
                number:"Please Enter a Valid Nid"

            },
            <?php endfor; ?>
        },


        errorElement: "span",

        errorPlacement: function ( error, element ) {


            // Add the `help-block` class to the error element
            error.addClass( "help-block").css('color','#D93025');

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
//        highlight: function ( element, errorClass, validClass ) {
//            $( element ).parents( ".form-group" ).css("css","#A9446C").removeClass( "has-success" );
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
//        },

        submitHandler: function(form) {
            form.submit();
        }
    });
    <?php endif; ?>
</script>



<!--Custom JavaScript -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/js/custom.min.js"></script>



<!-- jQuery file upload -->

<script src="<?php echo e(asset('/')); ?>ui/back-end/assets/plugins/dropify/dist/js/dropify.min.js"></script>

<?php if(isset(Auth::User()->businessInfo->total_director_partner)): ?>
    <?php for($i=1; $i<=Auth::User()->businessInfo->total_director_partner; $i++): ?>
        <script type="text/javascript">

            $('#present_division<?php echo e($i); ?>').on('change', function(e){
                console.log(e);

                var division_id = e.target.value;

                getPresentDistrict(division_id, <?php echo e($i); ?>);
            });

            $('#permanent_division<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var division_id = e.target.value;

                getPermanentDistrict(division_id, <?php echo e($i); ?>)
            });

            function getPresentDistrict(divisionId, step){
                $.get('/finddistrict?division_id=' + divisionId,function(data) {


                    $("textarea#present_address_"+step).val('');


                    $('#present_post_code'+step).removeAttr('value');
//                    var check=document.getElementById('#present_post_code'+step);
//                    console.log(check);

                    var postoffice = $('#present_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');

                    var upazila = $('#present_upazila'+step);
                    upazila.empty();
                    upazila.append('<option value="0" disabled="true" selected="true">--Select Upazila--</option>');

                    var district = $('#present_district'+step);

                    district.empty();
                    district.append('<option value="0" disabled="true" selected="true">--Select District--</option>');
                    $.each(data, function(index, districtObj){
                        district.append('<option value="'+ districtObj.id +'">'+ districtObj.title +'</option>');
                    })
                });
            }

            function getPermanentDistrict(divisionId, step){
                $.get('/finddistrict?division_id=' + divisionId,function(data) {

                    $("textarea#permanent_address_"+step).val('');

                    $('#permanent_post_code'+step).removeAttr('value');


                    var postoffice = $('#permanent_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');

                    var upazila = $('#permanent_upazila'+step);
                    upazila.empty();
                    upazila.append('<option value="0" disabled="true" selected="true">--Select Upazila--</option>');

                    var district = $('#permanent_district'+step);
                    district.empty();
                    district.append('<option value="0" disabled="true" selected="true">--Select District--</option>');
                    $.each(data, function(index, districtObj){
                        district.append('<option value="'+ districtObj.id +'">'+ districtObj.title +'</option>');
                    })
                });
            }

            $('#present_district<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var district_id = e.target.value;
                console.log(district_id);
                getPresentUpazila(district_id, <?php echo e($i); ?>)
            });

            $('#permanent_district<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var district_id = e.target.value;
                console.log(district_id);
                getPermanentUpazila(district_id,<?php echo e($i); ?>)
            });


            function  getPresentUpazila(districtId, step) {
                $.get('/findthana?district_id=' + districtId,function(data) {

                    $("textarea#present_address_"+step).val('');

                    var postoffice = $('#present_suboffice'+step);
                    postoffice.empty();

                    $('#present_post_code'+step).removeAttr('value');

                    var updazila = $('#present_upazila'+step);
                    updazila.empty();
                    updazila.append('<option value="0" disabled="true" selected="true">-Select Upazila-</option>');
                    $.each(data, function(index, thanaObj){
                        updazila.append('<option value="'+ thanaObj.id +'">'+ thanaObj.title +'</option>');
                    })
                });
            }

            function  getPermanentUpazila(districtId, step) {
                $.get('/findthana?district_id=' + districtId,function(data) {

                    $("textarea#permanent_address_"+step).val('');

                    var postoffice = $('#permanent_suboffice'+step);
                    postoffice.empty();

                    $('#permanent_post_code'+step).removeAttr('value');

                    var updazila = $('#permanent_upazila'+step);
                    updazila.empty();
                    updazila.append('<option value="0" disabled="true" selected="true">-Select Upazila-</option>');
                    $.each(data, function(index, thanaObj){
                        updazila.append('<option value="'+ thanaObj.id +'">'+ thanaObj.title +'</option>');
                    })
                });
            }

            $('#present_upazila<?php echo e($i); ?>').on('change', function(e){
                console.log(e);


                var upazilaId = e.target.value;
                getPresentPostOffice(upazilaId, <?php echo e($i); ?>)
            });

            $('#permanent_upazila<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var upazilaId = e.target.value;
                getPermanentPostOffice(upazilaId,<?php echo e($i); ?>)
            });


            function getPresentPostOffice(upazilaId,step) {
                $.get('/find-suboffice?thana_id=' + upazilaId,function(data) {
                    $("textarea#present_address_"+step).val('');

                    $('#present_post_code'+step).removeAttr('value');

                    var postoffice=$('#present_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');
                    $.each(data, function(index, subofficeObj){
                        postoffice.append('<option value="'+ subofficeObj.id +'">'+ subofficeObj.title +'</option>');
                    })
                });
            }


            function getPermanentPostOffice(upazilaId,step) {
                $.get('/find-suboffice?thana_id=' + upazilaId,function(data) {

                    $("textarea#permanent_address_"+step).val('');

                    $('#permanent_post_code'+step).removeAttr('value');

                    var postoffice=$('#permanent_suboffice'+step);
                    postoffice.empty();
                    postoffice.append('<option value="0" disabled="true" selected="true">--Select PostOffice--</option>');
                    $.each(data, function(index, subofficeObj){
                        postoffice.append('<option value="'+ subofficeObj.id +'">'+ subofficeObj.title +'</option>');
                    })
                });
            }

            $('#present_suboffice<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var postCodeId = e.target.value;
                getPresentPostCode(postCodeId, <?php echo e($i); ?>)
            });

            $('#permanent_suboffice<?php echo e($i); ?>').on('change', function(e){
                console.log(e);
                var postCodeId = e.target.value;
                getPermanentPostCode(postCodeId, <?php echo e($i); ?>)
            });


            function getPresentPostCode(postCodeId,step) {
                $.get('/find-post-code?postoffice_id=' + postCodeId,function(data) {
                    $("textarea#present_address_"+step).val('');
                    var postCode=$('#present_postcode'+step);
                    console.log(data);
                    postCode.empty();
                    postCode.append('<label>Post Code</label>');

                    /* $.each(data, function(index, subofficeObj){*/
                    postCode.append('<input type="text" id="present_post_code'+step+'" name="present_post_code'+step+'"  class="form-control" value="'+ data.post_code +'" readonly>');
                    /* })*/
                });

            }

            function getPermanentPostCode(postCodeId,step) {
                $.get('/find-post-code?postoffice_id=' + postCodeId,function(data) {

                    $("textarea#permanent_address_"+step).val('');

                    var postCode=$('#permanent_postcode'+step);
                    postCode.empty();
                    postCode.append('<label>Post Code</label>');

                    /*$.each(data, function(index, subofficeObj){*/
                    postCode.append('<input type="text" id="permanent_post_code'+step+'" name="post_code'+step+'"  class="form-control" value="'+ data.post_code +'" readonly>');
                    /*       })*/
                });

            }

        </script>
    <?php endfor; ?>
<?php endif; ?>





<script>

    /*******************************************/

    // Basic Date Range Picker

    /*******************************************/

    $('.daterange').daterangepicker();



    /*******************************************/

    // Date & Time

    /*******************************************/

    $('.datetime').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        locale: {

            format: 'MM/DD/YYYY h:mm A'

        }

    });



    /*******************************************/

    //Calendars are not linked

    /*******************************************/

    $('.timeseconds').daterangepicker({

        timePicker: true,

        timePickerIncrement: 30,

        timePicker24Hour: true,

        timePickerSeconds: true,

        locale: {

            format: 'MM-DD-YYYY h:mm:ss'

        }

    });



    /*******************************************/

    // Single Date Range Picker

    /*******************************************/
    //
    //    jQuery('.mydatepicker, #datepicker').datepicker();
    //    jQuery('#datepicker-autoclose').datepicker({
    //        autoclose: true,
    //        todayHighlight: true
    //    });

    $('.singledate').daterangepicker({

        singleDatePicker: true,
        showDropdowns: true,
        autoclose: false,
        todayHighlight: false

    });



    /*******************************************/

    // Auto Apply Date Range

    /*******************************************/

    $('.autoapply').daterangepicker({

        autoApply: true,

    });



    /*******************************************/

    // Calendars are not linked

    /*******************************************/

    $('.linkedCalendars').daterangepicker({

        linkedCalendars: false,

    });



    /*******************************************/

    // Date Limit

    /*******************************************/

    $('.dateLimit').daterangepicker({

        dateLimit: {

            days: 7

        },

    });



    /*******************************************/

    // Show Dropdowns

    /*******************************************/

    $('.showdropdowns').daterangepicker({

        showDropdowns: true,

    });



    /*******************************************/

    // Show Week Numbers

    /*******************************************/

    $('.showweeknumbers').daterangepicker({

        showWeekNumbers: true,

    });



    /*******************************************/

    // Date Ranges

    /*******************************************/

    $('.dateranges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        }

    });



    /*******************************************/

    // Always Show Calendar on Ranges

    /*******************************************/

    $('.shawCalRanges').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        alwaysShowCalendars: true,

    });



    /*******************************************/

    // Top of the form-control open alignment

    /*******************************************/

    $('.drops').daterangepicker({

        drops: "up" // up/down

    });



    /*******************************************/

    // Custom button options

    /*******************************************/

    $('.buttonClass').daterangepicker({

        drops: "up",

        buttonClasses: "btn",

        applyClass: "btn-info",

        cancelClass: "btn-danger"

    });



    /*******************************************/

    // Language

    /*******************************************/

    $('.localeRange').daterangepicker({

        ranges: {

            "Aujourd'hui": [moment(), moment()],

            'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],

            'Les 7 derniers jours': [moment().subtract('days', 6), moment()],

            'Les 30 derniers jours': [moment().subtract('days', 29), moment()],

            'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],

            'le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]

        },

        locale: {

            applyLabel: "Vers l'avant",

            cancelLabel: 'Annulation',

            startLabel: 'Date initiale',

            endLabel: 'Date limite',

            customRangeLabel: 'SÃ©lectionner une date',

            // daysOfWeek: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi','Samedi'],

            daysOfWeek: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],

            monthNames: ['Janvier', 'fÃ©vrier', 'Mars', 'Avril', 'ÐœÐ°i', 'Juin', 'Juillet', 'AoÃ»t', 'Septembre', 'Octobre', 'Novembre', 'Decembre'],

            firstDay: 1

        }

    });

</script>


<script>
    jQuery(
        function($) {
            $('#message').fadeIn (5550);
            $('#message').fadeOut (5550);
        }
    )
</script>


<script>

    $(document).ready(function() {

        // Basic

        $('.dropify').dropify();



        // Translated

        $('.dropify-fr').dropify({

            messages: {

                default: 'Glissez-déposez un fichier ici ou cliquez',

                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',

                remove: 'Supprimer',

                error: 'Désolé, le fichier trop volumineux'

            }

        });



        // Used events

        var drEvent = $('#input-file-events').dropify();



        drEvent.on('dropify.beforeClear', function(event, element) {

            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");

        });



        drEvent.on('dropify.afterClear', function(event, element) {

            alert('File deleted');

        });



        drEvent.on('dropify.errors', function(event, element) {

            console.log('Has Errors');

        });



        var drDestroy = $('#input-file-to-destroy').dropify();

        drDestroy = drDestroy.data('dropify')

        $('#toggleDropify').on('click', function(e) {

            e.preventDefault();

            if (drDestroy.isDropified()) {

                drDestroy.destroy();

            } else {

                drDestroy.init();

            }

        })

    });

</script>


<script>

    jQuery(document).ready(function() {

        // Switchery

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        $('.js-switch').each(function() {

            new Switchery($(this)[0], $(this).data());

        });

        // For select 2

        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin

        $(".vertical-spin").TouchSpin({

            verticalbuttons: true,

            verticalupclass: 'ti-plus',

            verticaldownclass: 'ti-minus'

        });

        var vspinTrue = $(".vertical-spin").TouchSpin({

            verticalbuttons: true

        });

        if (vspinTrue) {

            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();

        }

        $("input[name='tch1']").TouchSpin({

            min: 0,

            max: 100,

            step: 0.1,

            decimals: 2,

            boostat: 5,

            maxboostedstep: 10,

            postfix: '%'

        });

        $("input[name='tch2']").TouchSpin({

            min: -1000000000,

            max: 1000000000,

            stepinterval: 50,

            maxboostedstep: 10000000,

            prefix: '$'

        });

        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({

            initval: 40

        });

        $("input[name='tch5']").TouchSpin({

            prefix: "pre",

            postfix: "post"

        });

        // For multiselect

        $('#pre-selected-options').multiSelect();

        $('#optgroup').multiSelect({

            selectableOptgroup: true

        });

        $('#public-methods').multiSelect();

        $('#select-all').click(function() {

            $('#public-methods').multiSelect('select_all');

            return false;

        });

        $('#deselect-all').click(function() {

            $('#public-methods').multiSelect('deselect_all');

            return false;

        });

        $('#refresh').on('click', function() {

            $('#public-methods').multiSelect('refresh');

            return false;

        });

        $('#add-option').on('click', function() {

            $('#public-methods').multiSelect('addOption', {

                value: 42,

                text: 'test 42',

                index: 0

            });

            return false;

        });

        $(".ajax").select2({

            ajax: {

                url: "https://api.github.com/search/repositories",

                dataType: 'json',

                delay: 250,

                data: function(params) {

                    return {

                        q: params.term, // search term

                        page: params.page

                    };

                },

                processResults: function(data, params) {

                    // parse the results into the format expected by Select2

                    // since we are using custom formatting functions we do not need to

                    // alter the remote JSON data, except to indicate that infinite

                    // scrolling can be used

                    params.page = params.page || 1;

                    return {

                        results: data.items,

                        pagination: {

                            more: (params.page * 30) < data.total_count

                        }

                    };

                },

                cache: true

            },

            escapeMarkup: function(markup) {

                return markup;

            }, // let our custom formatter work

            minimumInputLength: 1,

            templateResult: formatRepo, // omitted for brevity, see the source of this page

            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page

        });

    });

</script>



</body>

</html>
