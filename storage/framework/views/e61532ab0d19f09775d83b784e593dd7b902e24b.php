<?php $__env->startSection('content'); ?>


    <div class="col-md-8 offset-2 pt-5">
        <div class="card card-body">
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">District Create</h3>
                <div class="col-1">
                    <a href="<?php echo e(url('/district')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
                </div>
            </div>

            <hr class="mb-5">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/district')); ?>">

                <?php echo csrf_field(); ?>

                <div class="form-group row">
                    <label for="division_id" class="col-sm-3 text-right control-label col-form-label">Select Division</label>
                    <div class="col-sm-8">
                        <select class="<?php echo e($errors->has('division_id') ? 'form-control is-invalid' : 'form-control'); ?>" name="division_id" id="division_id">
                            <option>Choose district...</option>
                            <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($key); ?>" <?php if($key==old('division_id')): ?> <?php echo e('selected'); ?> <?php endif; ?> ><?php echo e($division); ?></option>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('division_id')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('division_id')); ?></strong>
                     </span>
                        <?php endif; ?>

                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Name*</label>
                    <div class="col-sm-8">
                        <input type="text" name="title" value="<?php echo e(old('title')); ?>" class="<?php echo e($errors->has('title') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="District Name">
                        <?php if($errors->has('title')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('title')); ?></strong>
                     </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Geo code *</label>
                    <div class="col-sm-8">
                        <input type="text" name="geo_code" class="<?php echo e($errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="District Geo Code">

                        <?php if($errors->has('geo_code')): ?>
                            <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('geo_code')); ?></strong>
                     </span>
                        <?php endif; ?>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">District Add</button>
                    </div>
                </div>


            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>