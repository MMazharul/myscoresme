<?php $__env->startSection('content'); ?>

    <div class="card">
        <?php if(session('message')): ?>
            <div class="alert alert-success alert-dismissible" role="alert"  id="message">
                <div class="icon"><span class="mdi mdi-check"></span></div>
                <div class="message text-center "><?php echo e(session('message')); ?></div>
            </div>
        <?php endif; ?>
        <div class="card-body">
            <div class="col-12 row">

                <div class="col-11">
                    <button class="btn btn-primary"><i class="fa fa-list"></i> List</button>
                </div>

                <h3 class="box-title m-b-0 col-1"><a href="<?php echo e(url('branch/create')); ?>" class="btn btn-dropbox"><i class="mdi mdi-plus-circle"></i> Add New</a></h3>

            </div>
            <div class="m-t-40">
                <table  class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        
                        <th>Bank Name</th>

                        <th>Branch Name</th>

                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <td> <?php echo $branch->bank['bank_name']; ?></td>
                            <td> <?php echo $branch->branch_name; ?></td>
                           

                            <td>
                                <div class="dropdown row">
                                  

                                    <div class="btn-group ml-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon mdi mdi-settings"></i></button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="<?php echo e(url('/branch/'.$branch->id.'/edit' )); ?>"><i
                                                        class="mdi mdi-table-edit"></i> Edit</a>
                                            <a class="dropdown-item" href="<?php echo e(url('/branch-delete/'.$branch->id)); ?>"><i
                                                        class="mdi mdi-delete"></i> Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </td>


                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>

                </table>

                <div class=" tt-page__pagination">

                    <?php if($branches->hasPages()): ?>
                    <ul class="pagination">
                        
                        <?php if($branches->onFirstPage()): ?>
                            <li class="page-item disabled"><span class="page-link"><?php echo app('translator')->getFromJson('pagination.previous'); ?></span></li>
                        <?php else: ?>
                            <li class="page-item"><a class="page-link" href="<?php echo e($branches->previousPageUrl()); ?>" rel="prev"><?php echo app('translator')->getFromJson('pagination.previous'); ?></a></li>
                        <?php endif; ?>

                        <?php echo e($branches->links()); ?>

                        <?php if($branches->hasMorePages()): ?>
                            <li class="page-item"><a class="page-link" href="<?php echo e($branches->nextPageUrl()); ?>" rel="next"><?php echo app('translator')->getFromJson('pagination.next'); ?></a></li>
                        <?php else: ?>
                            <li class="page-item disabled"><span class="page-link"><?php echo app('translator')->getFromJson('pagination.next'); ?></span></li>
                        <?php endif; ?>
                    </ul>
                    <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>