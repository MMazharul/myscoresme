<?php $__env->startSection('main-content'); ?>



    <div class="container-fluid">
        <div>
            <div class="col-12">
                <div class="card wizard-content">
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="col-md-6 mb-4"><h4 class="card-title mb-4">SME Information</h4><h6
                                    class="card-subtitle text-danger"><strong>Required *</strong></h6></div>
                        </div>
                        <form id="submit-form" name="form_name" action="<?php echo e(url('/question-store')); ?>"
                              method="post">
                            <?php echo e(csrf_field()); ?>

                            
                            <div class="wizards">
                                <div class="progressbar">
                                    <div class="progress-line" data-now-value="6.45" data-number-of-steps="10"
                                         style="width: 6.45%;"></div> <!-- 19.66% --></div>
                                <div class="col-md-12 row">
                                    <div class="form-wizard active col-md-1 ml-5 mr-3">
                                        <div class="wizard-icon"><i class="">1</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">2</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-2">
                                        <div class="wizard-icon"><i class="">3</i></div>
                                    </div>
                                    <div class="form-wizard col-sm-1 mr-3">
                                        <div class="wizard-icon"><i class="">4</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-3">
                                        <div class="wizard-icon"><i class="">5</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">6</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">7</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">8</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">9</i></div>
                                    </div>
                                    <div class="form-wizard col-md-1 mr-2">
                                        <div class="wizard-icon"><i class="">10</i></div>
                                    </div>
                                </div>
                            </div> <?php $__currentLoopData = $questionCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$questionCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <fieldset><h5 class="mt-5 ml-4"><?php echo e($questionCategory->question_category); ?></h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">

                                                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($question->question_category_id==$questionCategory->id): ?>
                                                            <?php if($question->field_type=='text'): ?>
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4">
                                                                        <?php echo e($question->label_en); ?>

                                                                    </label>
                                                                    <div class="col-md-8">
                                                                        <input id="<?php echo e($question->column_name); ?>" type="text" name="<?php echo e($question->column_name); ?>"
                                                                               placeholder="<?php echo e($question->placeholder); ?>"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='number'): ?>
                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8">
                                                                        <input type="number" id="number"
                                                                               name="<?php echo e($question->column_name); ?>"
                                                                               value=""
                                                                               placeholder="<?php echo e($question->placeholder); ?>"
                                                                               class="form-control" >
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><select
                                                                            name="<?php echo e($question->column_name); ?>"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                <option value="<?php echo e($option->title); ?>"><?php echo e($option->title); ?></option>                                                                                <?php endif; ?>                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='calender'): ?>

                                                                
                                                                
                                                            
                                                                
                                                                        
                                                                    
                                                                        
                                                                               
                                                                               
                                                                    
                                                                


                                                                <div class="form-group row">
                                                                    <label class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8 row">
                                                                        <div class="col-md-4">
                                                                            <select id="dobmonth" name="<?php echo e($question->column_name); ?>" class="form-control">
                                                                                <option value="" disabled>Choose option</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <select id="dobyear" name="<?php echo e($question->column_name); ?>" class="form-control">
                                                                                <option value="" disabled>Choose option</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <hr>
                                                            <?php elseif($question->field_type=='radio'): ?>
                                                                <div class="form-group row prentid">
                                                                    <label class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8 row ml-0">
                                                                        <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($question->id == $option->question_id): ?>
                                                                                <?php if($option->question->field_type =='radio'): ?>
                                                                                    <div class="m-b-10 ml-1 row">
                                                                                        <label class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name); ?>"
                                                                                                   
                                                                                                   type="radio"
                                                                                                   class="custom-control-input">
                                                                                            <span class="custom-control-label"> <?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </div>

                                                                    <div class="offset-4">
                                                                        <span class="radioError text-danger ml-3 mt-0"></span>
                                                                    </div>


                                                                    <?php $__currentLoopData = $dependents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dependent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php if($question->id == $dependent->question_id): ?>
                                                                            <hr>
                                                                            <div id="open<?php echo e($dependent->question_id); ?>" class="<?php echo e($dependent->question_id); ?> form-group row" style="display: none">
                                                                                <label for="<?php echo e($dependent->placeholder); ?>" class="control-label text-right col-sm-4">
                                                                                    <?php echo e($dependent->label_en); ?>

                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <input  id="<?php echo e($dependent->question_id); ?>"  type="hidden" name="<?php echo e($dependent->column_name); ?>"
                                                                                            placeholder="<?php echo e($dependent->placeholder); ?>" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                                </div>

                                                                <hr>
                                                            <?php elseif($question->field_type=='checkbox'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <?php if($option->question->field_type =='checkbox'): ?>
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name.'[]'); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"><?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='textarea'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><textarea rows="3"
                                                                                                    name="<?php echo e($question->column_name); ?>"
                                                                                                    placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='multi-Select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-8"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <option value="<?php echo e($question->column_name.'[]'); ?>"><?php echo e($option->title); ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                                <hr>
                                                            <?php elseif($question->field_type=='amount'): ?>

                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label><div class="col-md-8">
                                                                        <input type="text" name="<?php echo e($question->column_name); ?>" value=""
                                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace();"
                                                                           placeholder="<?php echo e($question->placeholder); ?>" class="amount form-control"></div>
                                                                </div>
                                                                <hr>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <button type="button" class="btn btn-next">Next</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $catagoryLastStep; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $catagorylast): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <fieldset><h5 class="mt-5 ml-4"><?php echo e($catagorylast->question_category); ?></h5>
                                    <div class="col-lg-12">
                                        <div class="card card-outline-info">
                                            <div class="card-body">
                                                <div class="form-body">
                                                    <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if($question->question_category_id==$catagorylast->id): ?>
                                                            <?php if($question->field_type=='text'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><input type="text"
                                                                                                 name="<?php echo e($question->column_name); ?>"
                                                                                                 value=""
                                                                                                 placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='number'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><input type="number"
                                                                                                 name="<?php echo e($question->column_name); ?>"
                                                                                                 value=""
                                                                                                 placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                 class="form-control"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><select
                                                                            name="<?php echo e($question->column_name); ?>"
                                                                            class="form-control">
                                                                            <option value="">Choose option
                                                                            </option> <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                <option value="<?php echo e($option->title); ?>"><?php echo e($option->title); ?></option>                                                                                <?php endif; ?>                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='calender'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" name="<?php echo e($question->column_name); ?>"
                                                                               value="" class="form-control singledate" placeholder="dd/mm/yyyy"></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='radio'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9 row ml-0">
                                                                        <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php if($question->id == $option->question_id): ?>
                                                                                <?php if($option->question->field_type =='radio'): ?>
                                                                                    <div class="m-b-10 ml-1 row"><label
                                                                                            class="custom-control custom-radio mr-4">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="radio"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"> <?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='checkbox'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"> <?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9">
                                                                        <div class="m-b-10 ml-1 row">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <?php if($option->question->field_type =='checkbox'): ?>
                                                                                        <label class="custom-control custom-checkbox">
                                                                                            <input id="radio5"
                                                                                                   name="<?php echo e($question->column_name.'[]'); ?>"
                                                                                                   value="<?php echo e($option->title); ?>" type="checkbox"
                                                                                                   class="custom-control-input"> <span
                                                                                                class="custom-control-label"><?php echo e($option->title); ?></span>
                                                                                        </label>
                                                                                    <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='textarea'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><textarea rows="3"
                                                                                                    name="<?php echo e($question->column_name); ?>"
                                                                                                    placeholder="<?php echo e($question->placeholder); ?>"
                                                                                                    class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                            <?php elseif($question->field_type=='multi-Select'): ?>
                                                                <div class="form-group row"><label
                                                                        class="control-label text-right col-md-3"><?php echo e($question->label_en); ?></label>
                                                                    <div class="col-md-9"><select
                                                                            class="select2 m-b-10 select2-multiple"
                                                                            style="width: 100%" multiple="multiple"
                                                                            data-placeholder="Choose">
                                                                            <?php $__currentLoopData = $options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                <?php if($question->id == $option->question_id): ?>
                                                                                    <option value="<?php echo e($question->column_name.'[]'); ?>"><?php echo e($option->title); ?></option>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        </select></div>
                                                                </div>
                                                            <?php elseif($question->field_type=='amount'): ?>
                                                                <div class="form-group row"><label
                                                                            class="control-label text-right col-md-4"> <?php echo e($question->label_en); ?></label><div class="col-md-8">
                                                                        <input type="text" name="<?php echo e($question->column_name); ?>" value=""
                                                                               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1').replace();"
                                                                               placeholder="<?php echo e($question->placeholder); ?>" class="amount form-control"></div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                                <div class="wizard-buttons">
                                                    <button type="button" class="btn btn-previous">Previous</button>
                                                    <button class="btn btn-primary">Save</button>
                                                    <input onclick="submit_by_name()" type="submit"
                                                           class="btn btn-success" value="Complete"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('input.amount').keyup(function(event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function(index, value) {
                return value
                    // .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
            });
        });
    </script>

    <!-- Material unchecked -->

    <script>

        function optionQustionId(id){

            <?php $__currentLoopData = $dependents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dependent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            if(id[0].id == <?php echo e($dependent->question_id); ?>)
            {
                if(id[0].value == "No")
                {

                    
                    $('#open<?php echo e($dependent->question_id); ?> input[type=hidden]').attr('type','text');

                    $(".<?php echo e($dependent->question_id); ?>").show();

                }

            }
            if(id[0].id == <?php echo e($dependent->question_id); ?>)
            {

                if(id[0].value=="Yes")
                {
                    console.log(id[0]);
                    
                    $('#open<?php echo e($dependent->question_id); ?> input[type=text]').attr('type','hidden');
                    $(".<?php echo e($dependent->question_id); ?>").hide();
                }
            }

            if(id[0].id == <?php echo e($dependent->question_id); ?>)
            {

                if(id[0].value=="Maybe")
                {
                    
                    $('#open<?php echo e($dependent->question_id); ?> input[type=hidden]').attr('type','text');
                    $(".<?php echo e($dependent->question_id); ?>").show();
                }

            }

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        }
    </script>

    <script>
        $(document).ready(function () {
            $("#submit-form").submit(function (e) {
                e.preventDefault();
                var that = $(this);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                    url: that.attr('action'),
                    data: that.serialize(),
                    cache: false,
                    success: function (data) {
                        console.log(data)
                    }/*,                     error: function (xhr, status, error) {                        alert("An AJAX error occured: " + status + "\nError: " + error);                     }*/
                });
            });
        });
    </script>

    <script>
        function submit_by_name() {
            var x = document.getElementsByName('form_name');
            x[0].submit();       }
    </script>

    <script>
        $(document).ready(function() {
            $.dobPicker({
                // daySelector: '#dobday', /* Required */
                monthSelector: '#dobmonth', /* Required */
                yearSelector: '#dobyear', /* Required */
                // dayDefault: 'Day', /* Optional */
                monthDefault: 'Month', /* Optional */
                yearRange: "-120:+0",
                yearDefault: 'Year', /* Optional */
                // minimumAge: 12, /* Optional */
                // maximumAge: 80 /* Optional */
            });
        });
    </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.sme.layout.master-dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>