<?php $__env->startSection('content'); ?>

   <div class="col-md-8 offset-2 pt-5">
       <div class="card card-body">
           <div class="col-12 row">
               <h3 class="box-title m-b-0 col-11">Post Office Create</h3>
               <div class="col-1">
                   <a href="<?php echo e(url('/post-office')); ?>" class="btn btn-primary"><i class="fa fa-list"></i> List</a>
               </div>
           </div>
           <hr class="mb-5">
           <form class="form-horizontal" method="post" action="<?php echo e(url('/post-office')); ?>">

               <?php echo csrf_field(); ?>

               <div class="form-group row">
                   <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select Upazila</label>
                   <div class="col-sm-8">
                       <select class="<?php echo e($errors->has('upazila_id') ? 'form-control is-invalid' : 'form-control'); ?>" name="upazila_id" id="inlineFormCustomSelect" required autofocus>
                           <option selected="">Choose Upazila...</option>
                           <?php $__currentLoopData = $upazilas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$upazila): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                               <option value="<?php echo e($key); ?>" <?php if($key==old('upazila_id')): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($upazila); ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       </select>
                       <?php if($errors->has('upazila_id')): ?>
                           <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('upazila_id')); ?></strong>
                            </span>
                       <?php endif; ?>
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Office Name*</label>
                   <div class="col-sm-8">
                       <input type="text" name="title" class="<?php echo e($errors->has('title') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Post Office Name">
                       <?php if($errors->has('title')): ?>
                           <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('title')); ?></strong>
                     </span>
                       <?php endif; ?>
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Post Code Number*</label>
                   <div class="col-sm-8">
                       <input type="text"  name="post_code" value="<?php echo e(old('post_code')); ?>" class="<?php echo e($errors->has('post_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="Post Code Number">
                       <?php if($errors->has('post_code')): ?>
                           <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('post_code')); ?></strong>
                     </span>
                       <?php endif; ?>
                   </div>
               </div>

               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>
                   <div class="col-sm-8">
                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Post Office Add</button>
                   </div>
               </div>




           </form>
       </div>
   </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>