<?php $__env->startSection('content'); ?>





   <div class="col-md-8 offset-2 pt-5">

       <div class="card card-body">

           <h3 class="box-title m-b-0">District Edit</h3>

           <hr class="mb-5">

           <form class="form-horizontal" method="post" action="<?php echo e(url('/district/'.$districtEdit->id)); ?>">
               <?php echo csrf_field(); ?>
               <?php echo e(method_field('PUT')); ?>

               <div class="form-group row">

                   <label for="example-month-input" class="col-sm-3 text-right control-label col-form-label">Select</label>

                   <div class="col-sm-8">

                       <select class="custom-select col-sm-12" name="division_id" id="inlineFormCustomSelect">

                           <option selected="">Choose district...</option>

                           <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                               <option value="<?php echo e($key); ?>" <?php if($key==$districtEdit->division_id): ?> <?php echo e('selected'); ?> <?php endif; ?>><?php echo e($division); ?></option>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       </select>
                   </div>
               </div>
               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Name*</label>
                   <div class="col-sm-8">
                       <input type="text" name="title" value="<?php echo e($districtEdit->title); ?>" class="form-control" id="inputEmail3" placeholder="District Name">
                   </div>
               </div>


               <div class="form-group row">
                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">District Geo code *</label>
                   <div class="col-sm-8">
                       <input type="text" name="geo_code" value="<?php echo e($districtEdit->geo_code); ?>" class="<?php echo e($errors->has('geo_code') ? 'form-control is-invalid' : 'form-control'); ?>" id="inputEmail3" placeholder="District Geo Code">
                       <?php if($errors->has('geo_code')): ?>
                           <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('geo_code')); ?></strong>
                     </span>
                       <?php endif; ?>
                   </div>
               </div>

               <div class="form-group row">

                   <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label"></label>

                   <div class="col-sm-8">

                       <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">District Update</button>

                   </div>

               </div>





           </form>

       </div>

   </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>