
<?php $__env->startSection('content'); ?>
    <div class="col-md-10 offset-md-1 pt-5">
        <div class="card card-body">

		 <?php if(session('message')): ?>
                <div class="alert alert-success alert-dismissible" role="alert" id="message">
                    <div class="icon"><span class="mdi mdi-check"></span></div>
                    <div class="message text-center "><?php echo e(session('message')); ?></div>
                </div>
            	<?php endif; ?>
            <div class="col-12 row">
                <h3 class="box-title m-b-0 col-11">Branch Create</h3>
              
            </div>
            <hr class="mb-4">
            <form class="form-horizontal" method="post" action="<?php echo e(url('/branch-store')); ?>" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 text-right control-label col-form-label">Bank Name*</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="bank_id">
                            <option disabled="true" selected="true">-- Choose a Bank --</option>
                             <?php $__currentLoopData = $bankdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$bankdetail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($id); ?>"><?php echo e($bankdetail); ?></option>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="branch_name" class="col-sm-3 text-right control-label col-form-label">Branch
                        Name</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="branch_name" type="text" name="branch_name"
                               placeholder="Enter Your Branch Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date_estabilish" class="col-sm-3 text-right control-label col-form-label">Date
                        Establish</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="date_estabilish" type="date" name="date_estabilish"
                               placeholder="Enter Your Branch Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="division1" class="col-sm-3 text-right control-label col-form-label">Division</label>
                    <div class="col-sm-7">
                        <select id="division1" name="division"
                                class="form-control<?php echo e($errors->has('division') ? ' is-invalid' : ''); ?> division4">
                            <option value="0" disabled="true" selected="true">-Select division-</option>
                            <?php $__currentLoopData = $divisions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $division): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($division->title); ?>"><?php echo e($division->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="district1" class="col-sm-3 text-right control-label col-form-label">Distirct</label>
                    <div class="col-sm-7">
                        <select id="district1" name="district"
                                class="form-control<?php echo e($errors->has('district') ? ' is-invalid' : ''); ?>  district4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="upazila1" class="col-sm-3 text-right control-label col-form-label">Upazila</label>
                    <div class="col-sm-7">
                        <select id="upazila1" name="upazila"
                                class="form-control<?php echo e($errors->has('upazila') ? ' is-invalid' : ''); ?>  upazila4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="post_office1" class="col-sm-3 text-right control-label col-form-label">Post
                        Office</label>
                    <div class="col-sm-7">
                        <select id="post_office1" name="post_office"
                                class="form-control<?php echo e($errors->has('post_office') ? ' is-invalid' : ''); ?> suboffice4">
                            <option value="0" disabled="true" selected="true"></option>
                        </select>
                    </div>

                </div>

                <div class="form-group row">
                    <label for="post_code1" class="col-sm-3 text-right control-label col-form-label">Post Code:</label>
                    <div class="col-sm-7 postcode4">
                        <input id="post_code1" required="required" type="text" name="post_code4"
                               class=" form-control<?php echo e($errors->has('post_code4') ? ' is-invalid' : ''); ?>" readonly>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="telephone" class="col-sm-3 text-right control-label col-form-label">Telephone
                        Number*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="telephone" type="text" name="telephone"
                               placeholder="Enter Your telephone number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="mobile" class="col-sm-3 text-right control-label col-form-label">Mobile*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="mobile" type="text" name="contact_number"
                               placeholder="Enter Your Mobile Number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-sm-3 text-right control-label col-form-label">Email*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="email" type="email" name="email" placeholder="Enter Your Email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="retype_email" class="col-sm-3 text-right control-label col-form-label">Re Type
                        Email*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="retype_email" type="email" name="retype_email"
                               placeholder="Enter Your Re Type Email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="alternate_email" class="col-sm-3 text-right control-label col-form-label">Alternate
                        Email*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="alternate_email" type="email" name="alternate_email"
                               placeholder="Enter Your Re Alternate Email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="profesional_staff" class="col-sm-3 text-right control-label col-form-label">Number of
                        full time professional Staff*</label>
                    <div class="col-sm-7">
                        <input class="form-control" id="profesional_staff" type="number" name="profesional_staff"
                               placeholder="Enter Your Number of Professional Staff">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="professional_staff" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <label>Branch Manager Information:</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="title" class="col-sm-3 text-right control-label col-form-label">Title</label>
                    <div class="col-sm-7">
                        <select id="title" class="form-control<?php echo e($errors->has('title') ? ' is-invalid' : ''); ?>"
                                name="title">
                            <option disabled selected>-Select Title-</option>
                            <option value="Mrs." <?php echo e(old('title') == "Mrs." ? 'selected' : ''); ?>>Mrs.</option>
                            <option value="Mr." <?php echo e(old('title') == "Mr." ? 'selected' : ''); ?>>Mr.</option>
                            <option value="Ms" <?php echo e(old('title') == "Ms" ? 'selected' : ''); ?>>Ms</option>
                        </select>
                        <?php if($errors->has('title')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('title')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                    <div class="col-sm-7">
                        <input id="first_name" type="text"
                               class="form-control<?php echo e($errors->has('contact_first_name') ? ' is-invalid' : ''); ?>"
                               name="first_name" value="<?php echo e(old('contact_first_name')); ?>"
                               placeholder="Enter your first name">
                        <?php if($errors->has('contact_first_name')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('contact_first_name')); ?></strong>
                             </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="middle_name" class="col-sm-3 text-right control-label col-form-label">Middle
                        Name</label>
                    <div class="col-sm-7">
                        <input id="middle_name" type="text"
                               class="form-control<?php echo e($errors->has('contact_middle_name') ? ' is-invalid' : ''); ?>"
                               name="middle_name" value="<?php echo e(old('contact_middle_name')); ?>"
                               placeholder="Enter your middle name">
                        <?php if($errors->has('contact_middle_name')): ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($errors->first('contact_middle_name')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="last_name" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                    <div class="col-sm-7">
                        <input id="last_name" type="text"
                               class="form-control<?php echo e($errors->has('contact_last_name') ? ' is-invalid' : ''); ?>"
                               name="last_name" value="<?php echo e(old('last_name')); ?>" placeholder="Enter your last name">
                        <?php if($errors->has('contact_last_name')): ?>
                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('contact_last_name')); ?></strong>
                                        </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="designation"
                           class="col-sm-3 text-right control-label col-form-label">Designation</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="designation" id="designation">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="joining_date" class="col-sm-3 text-right control-label col-form-label">Joining
                        Date</label>
                    <div class="col-sm-7">
                        <input type="date" class="form-control" name="joining_date" id="joining_date">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="owner_contact" class="col-sm-3 text-right control-label col-form-label">Contact
                        Number</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="owner_number" id="contact_number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                    <div class="col-sm-7">
                        <input type="email" class="form-control" name="contact_email" id="contact_email">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contact_email" class="col-sm-3 text-right control-label col-form-label">Routing
                        Number</label>
                    <div class="col-sm-7">
                        <input type="number" class="form-control" name="routing_number" id="routing_number">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="attachment" class="col-sm-3 text-right control-label col-form-label">Upload Your
                        Attachment</label>
                    <div class="col-sm-7">
                        <input type="file" class="form-control" name="attachment" id="attachment">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-3 text-right control-label col-form-label">Password
                    </label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                               id="password" name="password" placeholder="Enter your password">

                        <?php if($errors->has('password')): ?>
                            <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                        <?php endif; ?>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-sm-3 text-right control-label col-form-label">Confirm Password
                    </label>
                    <div class="col-sm-7">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               placeholder="Enter your confirm password">
                    </div>
                </div>


                <div class="form-group row">
                    <label for="branch_type" class="col-sm-3 text-right control-label col-form-label"></label>
                    <div class="col-sm-7">
                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Branch Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.bank.partials.master-layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>