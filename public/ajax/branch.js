
$('#company_bank').on('change', function(e) {

    $("option:selected").prop("selected", false);

    $("#division_option").attr({"selected": "selected"});

    $('#company_branch').empty();
    $('#bank_district').empty();


});


$('#bank_division').on('change', function(e) {

    console.log(e);

    var division_id = e.target.value;

    getDistrictInfo(division_id);

});


$('#bank_district').on('change', function(e) {

    console.log(e);
    var district_id = e.target.value;

    getBranch(district_id);

});



function getBranch(district_id) {

    var bank_id = $("#company_bank").val();
    var division_id = $("#bank_division").val();

    var data = { company_bank: bank_id,bank_district:district_id,bank_division:division_id};



    console.log(data);

    $.ajax({
        url: "/branchInfo",
        type: "get",
        data: data,
        cache: false,
        success: function (branches)
        {
            console.log(branches);
            var branchTrack = $('#company_branch');
            branchTrack.empty();
            branchTrack.append('<option value="0" disabled="true" selected="true">--Select District--</option>');

            for(var branche of branches)
            {

                branchTrack.append('<option value="'+ branche.branch_name +'">'+branche.branch_name  +'</option>');

            }
        }
    });

}

function getDistrictInfo(division_id) {


    $.get('/finddistrict?division_id=' + division_id, function (districts) {


        $('#company_branch').empty();
        var district = $('#bank_district');
        district.empty();
        district.append('<option value="0" disabled="true" selected="true">--Select District--</option>');



        for(var districtList of districts)
        {

            district.append('<option value="'+ districtList.id +'">'+districtList.title +'</option>');

        }

    });

}

