$.validator.addMethod('filesize', function (value, element, arg) {
//        var minsize=2097152;
//        var file_size = $('#owner_pic')[0].files[0].size;

    var file_size = $('#'+element.id)[0].files[0].size;

    if(file_size<2097152)
    {
        return true;

    }



});

$("#signupForm" ).validate({
    rules: {
        enterprise_name: "required",
        organization: "required",
        org_division: "required",
        org_district: "required",
        org_upazila: "required",
        orgpost_office: "required",
        org_address: "required",
        title: "required",
        first_name: "required",
        last_name: "required",
        date_of_birth: "required",
        present_division: "required",
        present_district: "required",
        present_upazila: "required",
        presentpost_office: "required",
        post_code2: "required",
        present_address: "required",
        permanent_division: "required",
        permanent_district: "required",
        permanent_upazila: "required",
        permanentpost_office: "required",
        post_code3: "required",
        permanent_address: "required",
        total_director_partner: {
            required : true,
            number: true,
            min: 2,
            max: 100
        },
        owner_nid:{
            required:true,
            number: true,
        },
        owner_pic:{
            required:true,
            accept:"application/pdf,image/jpeg,image/png",
            filesize:2097152   //max size 2MB
        },
        nid_front_pic :{
            required:true,
            accept:"application/pdf,image/jpeg,image/png",
            filesize:  2097152     //max size 2MB
        },
        nid_back_pic :{
            required:true,
            accept:"application/pdf,image/jpeg,image/png",
            filesize:  2097152     //max size 2MB
        },
        industry: "required"


    },messages: {

        total_director_partner:{
            min:"Minimum 2 number required.",
            max:"Maximum number 100."

        },

        owner_nid:{
            number:"Please Enter a Valid Nid"

        },
        owner_pic:{
            filesize:" file size must be less than 2MB.",
            accept:"Please upload .jpg or .png "

        },
        nid_front_pic:{
            filesize:"file size must be less than 2MB.",
            accept:"Please upload .jpg or .png ",
            required:"Please upload file."
        },
        nid_back_pic:{
            filesize:"file size must be less than 2MB.",
            accept:"Please upload .jpg or .png ",
            required:"Please upload file."
        }

    },


    errorElement: "span",

    errorPlacement: function ( error, element ) {

        // Add the `help-block` class to the error element
        error.addClass( "help-block").css('color','#D93025');

        if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.parent( "label" ) );
        } else {
            error.insertAfter( element );
        }
    },
//        highlight: function ( element, errorClass, validClass ) {
//            $( element ).parents( ".form-group" ).css("css","#A9446C").removeClass( "has-success" );
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
//        },

    submitHandler: function(form) {

        form.submit();
    }
});

$.validator.addMethod('filesize', function (value, element, arg) {
//        var minsize=2097152;
//        var file_size = $('#owner_pic')[0].files[0].size;

    var file_size = $('#'+element.id)[0].files[0].size;

    if(file_size<2097152)
    {
        return true;

    }



});

$("#attachmentValidation" ).validate({
    rules: {
        trade_license: "required",
        expire_date: "required",
        e_tin: "required",
        bin: "required",
        partnership_agreement: "required",
        company_account_title: "required",
        company_account_no: "required",
        company_bank: "required",
        bank_division: "required",
        bank_district: "required"
    },

    errorElement: "span",

    errorPlacement: function ( error, element ) {

        // Add the `help-block` class to the error element
        error.addClass( "help-block").css('color','#D93025');

        if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.parent( "label" ) );
        } else {
            error.insertAfter( element );
        }
    },
//        highlight: function ( element, errorClass, validClass ) {
//            $( element ).parents( ".form-group" ).css("css","#A9446C").removeClass( "has-success" );
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
//        },

    submitHandler: function(form) {

        form.submit();
    }
});


$("#signformEdit" ).validate({
    rules: {
        enterprise_name: "required",
        organization: "required",
        org_division: "required",
        org_district: "required",
        org_upazila: "required",
        orgpost_office: "required",
        org_address: "required",
        title: "required",
        first_name: "required",
        last_name: "required",
        date_of_birth: "required",
        present_division: "required",
        present_district: "required",
        present_upazila: "required",
        presentpost_office: "required",
        post_code2: "required",
        present_address: "required",
        permanent_division: "required",
        permanent_district: "required",
        permanent_upazila: "required",
        permanentpost_office: "required",
        post_code3: "required",
        permanent_address: "required",
        total_director_partner: "required",
        owner_nid:{
            required:true,
            number: true,

        },
        industry: "required"


    },

    errorElement: "span",

    errorPlacement: function ( error, element ) {

        // Add the `help-block` class to the error element
        error.addClass( "help-block").css('color','#D93025');

        if ( element.prop( "type" ) === "checkbox" ) {
            error.insertAfter( element.parent( "label" ) );
        } else {
            error.insertAfter( element );
        }
    },
//        highlight: function ( element, errorClass, validClass ) {
//            $( element ).parents( ".form-group" ).css("css","#A9446C").removeClass( "has-success" );
//        },
//        unhighlight: function (element, errorClass, validClass) {
//            $( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
//        },

    submitHandler: function(form) {

        form.submit();
    }
});


