function scroll_to_class(element_class, removed_height) {
    var scroll_to = $(element_class).offset().top - removed_height;
    if($(window).scrollTop() != scroll_to) {
        $('html, body').stop().animate({scrollTop: scroll_to}, 0);
    }
}
function bar_progress(progress_line_object, direction) {
    var number_of_steps = progress_line_object.data('number-of-steps');
    var now_value = progress_line_object.data('now-value');
    var new_value = 0;
    if(direction == 'right') {
        new_value = now_value + ( 100 / number_of_steps );
    }
    else if(direction == 'left') {
        new_value = now_value - ( 100 / number_of_steps );
    }
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}
jQuery(document).ready(function() {

    $('form fieldset:first').fadeIn('slow');

    $('form input[type="text"], form input[type="password"], form textarea').on('focus', function() {
        $(this).removeClass('input-error');
    });

    $('form .btn-next').on('click', function() {
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;
        var current_active_step = $(this).parents('form').find('.form-wizard.active');
        var progress_line = $(this).parents('form').find('.progress-line');



        parent_fieldset.find('input[type="text"], input[type="password"], input[type="username"], input[type="email"], input[type="tel"], input[type="url"], textarea').each(function() {
            if( $(this).val() == "" ) {
                $(this).addClass('input-error');
                next_step = false;
            }
            else {
                $(this).removeClass('input-error');
            }
        });


        // $(document).on("click",".btn-next", function() {
        var names = [];

        parent_fieldset.find('input[type="radio"]').each(function() {
            // Creates an array with the names of all the different checkbox group.
            names[$(this).attr('name')] = true;
        });

        // Goes through all the names and make sure there's at least one checked.

        for (name  in names) {

            var radio_buttons = $("input[name='" + name + "']");
            // var perentId = $("#prentid"+id++);
            // var perentId = $("#prentid");

            if (radio_buttons.filter(':checked').length == 0) {
                $('.radioError').html("field is required");
                next_step = false;

                $(radio_buttons).click(function(){
                    if(radio_buttons.filter(':checked').length == !0){

                        alert(1);

                        // $(perentId+'.radioError').hide();
                        // console.log("#prentid"+id++);
                            $(".prentid").children('span').remove();
                    }
                })

            }
            else {
                // If you need to use the result you can do so without
                // another (costly) jQuery selector call:
                var val = radio_buttons.val();
            }
        }
        // });














        // parent_fieldset.find('input[type="checkbox"]').each(function() {
        // 	if( $(this).prop("checked") == false ) {
        // 		$('.form-check-label').css("color","red");
        // 		next_step = false;
        // 	}
        // 	else {
        // 		$('.form-check-label').css("color","black");
        // 	}
        // });


        //  var radio = parent_fieldset.find('.radio');
        //  for (let index = 1; index <= radio.length; index++) {
        // 	console.log(index);
        // 	parent_fieldset.find('.radio'+index+' input[type="radio"]').each(function() {
        // 		var result = $('.radio'+index+' input[type="radio"]:checked');
        // 		if(result.val()){
        // 		}else{
        // 			$('.radioError').html("field is requerd")
        // 			next_step = false;
        // 			// $('.radioError').html("field is requerd")
        // 			// next_step = false;
        // 		}
        // 	});
        //  }






        if( next_step ) {
            parent_fieldset.fadeOut(400, function() {
                current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                bar_progress(progress_line, 'right');
                $(this).next().fadeIn();
                scroll_to_class( $('form'), 20 );
            });
        }

    });

    // previous step
    $('form .btn-previous').on('click', function() {
        var current_active_step = $(this).parents('form').find('.form-wizard.active');
        var progress_line = $(this).parents('form').find('.progress-line');

        $(this).parents('fieldset').fadeOut(400, function() {
            current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
            bar_progress(progress_line, 'left');
            $(this).prev().fadeIn();
            scroll_to_class( $('form'), 20 );
        });
    });

    $('form').on('submit', function(e) {
        $(this).find('input[type="text"], input[type="password"], input[type="username"], input[type="email"], input[type="tel"], input[type="url"], textarea').each(function() {
            if( $(this).val() == "" ) {
                e.preventDefault();
                $(this).addClass('input-error');
            }
            else {
                $(this).removeClass('input-error');
            }
        });
    });

});
